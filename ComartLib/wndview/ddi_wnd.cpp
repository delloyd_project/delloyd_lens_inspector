﻿
/* 	filename: 	DDI WND
 * **************************************************************************************
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2	[2004/08/23] : CODE APPEND..
 *  ..
 *  ..
 *  version  :  10	[2013/01/31] : 960H support (code alignment)
 *
 * **************************************************************************************
 */

#include "stdafx.h"
#include "ddi_wnd.h"

#include "..\\boxlib\\cspace_lite.h"
#include "..\\boxlib\\ddif.h"

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#include <vfw.h>
#include <strsafe.h>

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * DEFINE		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef	THIS_FILE
static	char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * 
// * MESSAGE MAP DEFINE	 
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BEGIN_MESSAGE_MAP(DDIWnd,		CWnd)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP	 ()
	ON_WM_CLOSE		 ()
	ON_WM_PAINT		 ()	
END_MESSAGE_MAP()

/* 
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
* CLASS		: DDI wnd
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
*/

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		Construction / Destruction / Initialize
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DDIWnd::DDIWnd()  
{ 
	m_pddif		= NULL;
	
	ZeroMemory(m_lpszWndName, sizeof(m_lpszWndName));
	m_nPaintEn = 0;
}

DDIWnd::~DDIWnd() 
{
	ddi_close(m_dwCH);
	
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		MESSAGE MAP CODE
//!	@
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

void DDIWnd::OnClose() {	}		//{ CWnd::OnClose();	}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void DDIWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonDown(nFlags, point);
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void DDIWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonUp(nFlags, point);
    PostMessage(WM_NCLBUTTONUP, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL DDIWnd::DestroyWindow() 
{	
	ddi_close(m_dwCH);

	return CWnd::DestroyWindow();
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void DDIWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting	
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * DDI 
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	DDIWnd::ddi_open(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw411)
{
	if (GetSafeHwnd() == NULL) return;

	if (m_pddif)
	{	ddi_close(m_dwCH);
	}
	m_pddif = new ddif;
	if (m_pddif == NULL) return;

	if (m_pddif->ddi_open(this) < 0)
	{	ddi_close(IN_dwCH);
		return;
	}
	
	m_dwCH		= IN_dwCH;
	m_dwXZ		= IN_dwXZ;
	m_dwYZ		= IN_dwYZ;	
	m_dw411		= IN_dw411 & 3;

	DWORD	nycinx  = 0;
	DWORD	dwIMGSZ	= (IN_dwXZ * IN_dwYZ * 2);

	switch (IN_dw411)
	{	case 0 : nycinx = DDIF_UYVY;		break;
		case 2 : nycinx = DDIF_YUY2;		break;
		case 1 : 
		default: nycinx = DDIF_YUY2;
				 m_bpYUY2 = (LPBYTE)m_amem.t_new(dwIMGSZ + 4096, 256);
				 break;
	}

	m_pddif->chraw_open(IN_dwCH, IN_dwXZ, IN_dwYZ, nycinx);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	DDIWnd::ddi_close(DWORD IN_dwCH)
{	
	if (m_pddif)
	{	m_pddif->chraw_close((int)IN_dwCH);
		
		delete m_pddif;
		m_pddif = NULL;
	}

	if (m_bpYUY2)
	{	m_amem.t_del();
		m_bpYUY2 = NULL;
	}
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: WINDOWS CREATE 
// 
// * Arguments	: IN_dwClientXZ : WINDOW CLIENT WIDTH
//                IN_dwClientYZ : WINDOW CLIENT HEIGHT
// * Return		: none
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	DDIWnd::CreateNShow
(DWORD IN_dwCH, DWORD IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP)
{	
	RECT			rect;
	int				nxz, nyz;
	
	rect.left	= 100;
	rect.top	= 100;
	rect.right	= rect.left + IN_dwClientXZ;
	rect.bottom	= rect.top  + IN_dwClientYZ;
	::AdjustWindowRectEx(&rect, 
		WS_CAPTION | WS_CLIPSIBLINGS | WS_THICKFRAME | WS_VISIBLE | WS_OVERLAPPED | WS_POPUP, 
		//NO FRAME WINDOW: WS_VISIBLE | WS_POPUP, 
		0, 0);

	nxz	= rect.right  - rect.left + 1;
	nyz	= rect.bottom - rect.top  + 1;

	if ((::GetSystemMetrics(SM_CXSCREEN) <= nxz) || (::GetSystemMetrics(SM_CYSCREEN) <= nyz) ) 
	{	m_rectOrSz.left		= 0;
		m_rectOrSz.top		= 0;
		m_rectOrSz.right	= nxz;
		m_rectOrSz.bottom	= nyz;
	} else 
	{	m_rectOrSz.left		= ((::GetSystemMetrics(SM_CXSCREEN) - nxz) >> 1);
		m_rectOrSz.top		= ((::GetSystemMetrics(SM_CYSCREEN) - nyz) >> 1);
		m_rectOrSz.right	= nxz + m_rectOrSz.left;
		m_rectOrSz.bottom	= nyz + m_rectOrSz.top;
	}

	StringCbPrintf(m_lpszWndName, sizeof(m_lpszWndName), _T("%s"), IN_lptcszWndName);
	CreateEx(0, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, ::LoadCursor(NULL, IDC_ARROW), 
									::CreateSolidBrush(RGB(0xFF, 0xFF, 0xFF)), 0), m_lpszWndName, 
									WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_POPUP | WS_OVERLAPPEDWINDOW, 
									//NO FRAME WINDOW: 	WS_VISIBLE | WS_POPUP,
									 m_rectOrSz, NULL, 0, NULL);
	ShowWindow(SW_SHOW);


	if (IN_dwTOP) 
	{	SetWindowPos(&CWnd::wndTop, m_rectOrSz.left, m_rectOrSz.top, nxz, nyz, SWP_SHOWWINDOW);
		UpdateWindow();	
	}
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * render
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	DDIWnd::render(DWORD IN_dwCH, LPBYTE IN_bpYC, RECT *in_psrc_rect)
{
	if (m_pddif == NULL) return;

	int		nrv;
	LPBYTE	bpYC = IN_bpYC;

	if (m_dw411 == 1)
	{	if (m_bpYUY2)
		{	nrv  = CSPACE_Y41P_TO_YUY2(IN_bpYC, m_dwXZ, m_dwYZ, m_bpYUY2, 0);
			bpYC = m_bpYUY2;		
		}
	} 

	if (bpYC)
	{	m_pddif->ch_render(IN_dwCH, bpYC, in_psrc_rect);
	}
}

