﻿/* 	filename: 	DDI WND
 * **************************************************************************************
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2	[2004/08/23] : CODE APPEND..
 *  ..
 *  ..
 *  version  :  10	[2013/01/31] : 960H support (code alignment)
 *
 * **************************************************************************************
 */

#include "stdafx.h"
#include "rgb_wnd.h"

#include "..\\boxlib\\cspace_lite.h"
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#include <vfw.h>
#include <strsafe.h>

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * DEFINE		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef	THIS_FILE
static	char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * 
// * MESSAGE MAP DEFINE	 
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BEGIN_MESSAGE_MAP(RGBWnd,		CWnd)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP	 ()
	ON_WM_CLOSE		 ()
	ON_WM_PAINT		 ()	
END_MESSAGE_MAP()

/* 
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
* CLASS		: RGB wnd
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
*/

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		Construction / Destruction / Initialize
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
RGBWnd::RGBWnd()  
{ 
	m_lpbRGB	= NULL;
	m_dwRGBSZ   = 0;

	ZeroMemory(m_lpszWndName, sizeof(m_lpszWndName));
	m_nPaintEn = 0;

	m_bUseOverlay = TRUE;
}

RGBWnd::~RGBWnd() 
{
	if (m_lpbRGB) 
	{	
		m_lpbRGB = NULL;
		m_amem.t_del();		
	}
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		MESSAGE MAP CODE
//!	@
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

void RGBWnd::OnClose() {	}		//{ CWnd::OnClose();	}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void RGBWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonDown(nFlags, point);
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void RGBWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonUp(nFlags, point);
    PostMessage(WM_NCLBUTTONUP, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL RGBWnd::DestroyWindow() 
{	
	return CWnd::DestroyWindow();
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void RGBWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	if (m_nPaintEn != 0) 
		DIBBLT(&dc, m_nPaintEn);
	else
		DrawEmpty(&dc);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: WINDOWS CREATE 
// 
// * Arguments	: IN_dwClientXZ : WINDOW CLIENT WIDTH
//                IN_dwClientYZ : WINDOW CLIENT HEIGHT
// * Return		: none
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	RGBWnd::CreateNShow
(DWORD	IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP)
{	
	RECT			rect;
	int				iWndWidth, iWndHeight;
	
	rect.left	= 100;
	rect.top	= 100;
	rect.right	= rect.left + IN_dwClientXZ;
	rect.bottom	= rect.top  + IN_dwClientYZ;
	::AdjustWindowRectEx(&rect, WS_CAPTION | WS_CLIPSIBLINGS | WS_THICKFRAME | WS_VISIBLE | WS_OVERLAPPED | WS_POPUP, 0, 0);

	iWndWidth	= rect.right  - rect.left;
	iWndHeight	= rect.bottom - rect.top;

	if ((::GetSystemMetrics(SM_CXSCREEN) <= iWndWidth) || 
		(::GetSystemMetrics(SM_CYSCREEN) <= iWndHeight) ) 
	{	m_rectOrSz.left		= 0;
		m_rectOrSz.top		= 0;
		m_rectOrSz.right	= iWndWidth;
		m_rectOrSz.bottom	= iWndHeight;

	} else 
	{
		m_rectOrSz.left		= ((::GetSystemMetrics(SM_CXSCREEN) - iWndWidth) >> 1);
		m_rectOrSz.top		= ((::GetSystemMetrics(SM_CYSCREEN) - iWndHeight) >> 1);
		m_rectOrSz.right	= iWndWidth  + m_rectOrSz.left;
		m_rectOrSz.bottom	= iWndHeight + m_rectOrSz.top;
	}

	StringCbPrintf(m_lpszWndName, sizeof(m_lpszWndName), _T("%s"), IN_lptcszWndName);
	CreateEx(0, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 
									::LoadCursor(NULL, IDC_ARROW), 
									::CreateSolidBrush(RGB(0xFF, 0xFF, 0xFF)), 0),
			 m_lpszWndName, 
			 WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_POPUP | WS_OVERLAPPEDWINDOW, 
			 m_rectOrSz, NULL, 0, NULL);

	ShowWindow(SW_SHOW);

	if (!IN_dwTOP) return;
	SetWindowPos(&CWnd::wndTop, m_rectOrSz.left, m_rectOrSz.top,
		         IN_dwClientXZ, IN_dwClientYZ, SWP_SHOWWINDOW);
	UpdateWindow();	

}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	RGBWnd::RGBCONV
(DWORD	IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	ZeroMemory(&m_biInf, sizeof(m_biInf));
	
	m_biInf.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInf.bmiHeader.biWidth		= (ULONG)IN_dwXZ;
	m_biInf.bmiHeader.biHeight		= (ULONG)IN_dwYZ;
	m_biInf.bmiHeader.biPlanes		= 1;
	m_biInf.bmiHeader.biBitCount	= 32;
	m_biInf.bmiHeader.biCompression	= BI_RGB;
	m_biInf.bmiHeader.biSizeImage	= (ULONG)IN_dwXZ * (ULONG)IN_dwYZ * 4;
	
	if (m_dwRGBSZ != m_biInf.bmiHeader.biSizeImage) 
	{	
		if (m_lpbRGB) 
		{	m_lpbRGB = NULL;
			m_amem.t_del();		    
		}
	}

	if (m_lpbRGB == NULL) 
	{
		m_lpbRGB = (LPBYTE)m_amem.t_new(m_biInf.bmiHeader.biSizeImage + 128, 256);		
		m_dwRGBSZ= m_biInf.bmiHeader.biSizeImage;
	}

	int		nrv = -1;
	switch(IN_dw4CC)
	{	case MKFOURCC('U','Y','V','Y'):	
				nrv = CSPACE_UYVY_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0); 
				break;

		case MKFOURCC('Y','4','1','P'):					
				nrv = CSPACE_Y41P_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0); 				
				break;

		case MKFOURCC('Y','U','Y','2'):	
				nrv = CSPACE_YUY2_TO_RGB32(IN_lpbYC422, IN_dwXZ, IN_dwYZ, m_lpbRGB, 0); 
				break;

		default:break;
			
		//dek> SAVEDIB ( IN_dwWidth, IN_dwHeight, MKFOURCC('U','Y','V','Y'), IN_lpbYC422 );

	}
}

void RGBWnd::RGB_BYPASS(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	ZeroMemory(&m_biInf, sizeof(m_biInf));

	m_biInf.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInf.bmiHeader.biWidth		= (ULONG)IN_dwXZ;
	m_biInf.bmiHeader.biHeight		= (ULONG)IN_dwYZ;
	m_biInf.bmiHeader.biPlanes		= 1;
	m_biInf.bmiHeader.biBitCount	= 32;
	m_biInf.bmiHeader.biCompression = BI_RGB;
	m_biInf.bmiHeader.biSizeImage	= (ULONG)IN_dwXZ * (ULONG)IN_dwYZ * 4;

	if (m_dwRGBSZ != m_biInf.bmiHeader.biSizeImage)
	{
		if (m_lpbRGB)
		{
			m_lpbRGB = NULL;
			m_amem.t_del();
		}
	}

	if (m_lpbRGB == NULL)
	{
		m_lpbRGB = (LPBYTE)m_amem.t_new(m_biInf.bmiHeader.biSizeImage + 128, 256);
		m_dwRGBSZ = m_biInf.bmiHeader.biSizeImage;
	}	

	memcpy (m_lpbRGB, IN_lpbYC422, m_dwRGBSZ);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	RGBWnd::DIBBLT(CDC *IN_pDC, ULONG IN_nLineInc)
{	
	CDC	*pDC;

	if (IN_pDC == NULL)
		pDC = this->GetDC();
	else 
		pDC = IN_pDC;

	// 메모리 DC 선언
	CDC memDC;
	CBitmap *pOldBitmap, bitmap;
	CRect rect;
	GetClientRect(&rect);

	// 화면 DC와 호환되는 메모리 DC 객체를 생성
	memDC.CreateCompatibleDC(pDC);

	// 마찬가지로 화면 DC와 호환되는 Bitmap 생성
	bitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());

	pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.PatBlt(0, 0, rect.Width(), rect.Height(), WHITENESS); // 흰색으로 

	::SetStretchBltMode(memDC, COLORONCOLOR);

	RECT	rc;
	GetClientRect(&rc);
	rc.right -= rc.left;
	rc.bottom -= rc.top;
	if (IN_nLineInc <= -1)
		rc.top = rc.bottom - 1;

	::StretchDIBits(memDC, rc.left, rc.top, rc.right, rc.bottom * IN_nLineInc,
		0, 0, m_biInf.bmiHeader.biWidth, m_biInf.bmiHeader.biHeight,
		m_lpbRGB, &m_biInf, DIB_RGB_COLORS, SRCCOPY);

	if (m_bUseOverlay)
	{
		//DrawRect(&memDC);
		for (UINT nIdx = 0; nIdx < 9; nIdx++)
		{
			if (rectNoise[nIdx].bSelect)
			{
				DrawRect(&memDC, nIdx);

				DrawNoiseText(&memDC, nIdx);
			}
		}
	}

	// 메모리 DC를 화면 DC에 고속 복사
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	bitmap.DeleteObject();

	if (IN_pDC == NULL)
	{
		this->ReleaseDC(pDC);
	}
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	RGBWnd::SAVEDIB
(DWORD	IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{		
	TCHAR		szFN[MAX_PATH];
	time_t		timer;
	struct		tm	t;
	
	BITMAPFILEHEADER	bfh;
	
	RGBCONV(IN_dwXZ, IN_dwYZ, IN_dw4CC, IN_lpbYC422);

	bfh.bfType		= 0x4D42;
	bfh.bfSize		= m_biInf.bmiHeader.biSizeImage + m_biInf.bmiHeader.biSize + sizeof(BITMAPFILEHEADER);
	bfh.bfReserved1 = 0;
	bfh.bfReserved2 = 0;
	bfh.bfOffBits	= m_biInf.bmiHeader.biSize + sizeof(BITMAPFILEHEADER);
	
	m_biInf.bmiHeader.biHeight	= (int)IN_dwYZ * -1;

	time (&timer);
	localtime_s(&t, &timer);

	StringCbPrintf(szFN, sizeof(szFN), _T("%04d_%02d%02d_%02d%02d_%02d_%04d.bmp"), 
			t.tm_year+1900, t.tm_mon+1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, GetTickCount()%1000);
	
	FILE	*fp;
	errno_t	err;
	err = _tfopen_s(&fp, szFN, _T("wb"));
	if (err) 
		return;

	fwrite((LPBYTE)&bfh,	sizeof(BITMAPFILEHEADER),	  1, fp);
	fwrite((LPBYTE)&m_biInf,sizeof(BITMAPINFOHEADER),	  1, fp);
	fwrite((LPBYTE)m_lpbRGB,m_biInf.bmiHeader.biSizeImage,1, fp);
	fclose(fp);
}
	
void	RGBWnd::render
(DWORD	IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	RGBCONV(IN_dwXZ, IN_dwYZ, IN_dw4CC, IN_lpbYC422);
	
	// 테스트용
	//RGB_BYPASS(IN_dwXZ, IN_dwYZ, IN_dw4CC, IN_lpbYC422);
	
 	DWORD dwLineSize = IN_dwXZ * 4;
 	PBYTE pOffset = m_lpbRGB;
 	PBYTE pTarget = NULL;
 	for (DWORD iCnt = 0; iCnt < IN_dwYZ; iCnt += 2)
 	{
 		pTarget = pOffset + dwLineSize;
 		memcpy(pTarget, pOffset, dwLineSize);
		//memcpy(pOffset, pTarget, dwLineSize);
 
 		pOffset += dwLineSize;
 		pOffset += dwLineSize;
 	}

	DIBBLT(NULL, -1);
	
	m_nPaintEn = -1;
}

void RGBWnd::render_bypass(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422)
{
	RGB_BYPASS(IN_dwXZ, IN_dwYZ, IN_dw4CC, IN_lpbYC422);

	DIBBLT(NULL, -1);

	m_nPaintEn = -1;
}

void RGBWnd::AddOverlayRect(RECT rect)
{
	if (m_arrRect.GetCount() < 9)
	{
		m_arrRect.Add(rect);
	}
}

void RGBWnd::DrawEmpty(CDC* pDC)
{
	CBrush NewBrush;
	NewBrush.CreateStockObject(WHITE_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(0, 0, 0);
	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);


	CRect rectClient;
	GetClientRect(rectClient);
	pDC->Rectangle(rectClient);


	CFont font;
	font.CreatePointFont(100, _T("Arial"));
	pDC->SelectObject(&font);
	pDC->SetTextAlign(TA_LEFT | TA_BASELINE);
	CString strText = _T("Stand By ...");
	pDC->TextOut(10, 20, strText, strText.GetLength());
	strText = _T("카메라 OFF");
	pDC->TextOut(rectClient.CenterPoint().x, rectClient.Height() - 20, strText, strText.GetLength());
	font.DeleteObject();

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

void RGBWnd::DrawRect(CDC* pDC)
{
	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(0, 0, 255);

	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);
	CBrush NewBrush;
	NewBrush.CreateStockObject(NULL_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	CRect rectOv;
	CRect rectClient;
	GetClientRect(rectClient);

	for (int iCnt = 0; iCnt < m_arrRect.GetCount(); iCnt++)
	{
		rectOv = m_arrRect.GetAt(iCnt);

		rectOv.left = (rectOv.left * rectClient.Width()) / m_biInf.bmiHeader.biWidth;
		rectOv.top = (rectOv.top * rectClient.Height()) / m_biInf.bmiHeader.biHeight;
		rectOv.right = (rectOv.right * rectClient.Width()) / m_biInf.bmiHeader.biWidth;
		rectOv.bottom = (rectOv.bottom * rectClient.Height()) / m_biInf.bmiHeader.biHeight;

		pDC->Rectangle(rectOv);
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

void RGBWnd::DrawRect(CDC* pDC, UINT nIndex)
{
	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	if (rectNoise[nIndex].bFail)
		lb.lbColor = RGB(255, 0, 0);
	else
		lb.lbColor = RGB(0, 0, 255);

	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);
	CBrush NewBrush;
	NewBrush.CreateStockObject(NULL_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	CRect rectOv;
	CRect rectClient;
	GetClientRect(rectClient);

	rectOv = rectNoise[nIndex].Region;

	rectOv.left = (rectOv.left * rectClient.Width()) / m_biInf.bmiHeader.biWidth;
	rectOv.top = (rectOv.top * rectClient.Height()) / m_biInf.bmiHeader.biHeight;
	rectOv.right = (rectOv.right * rectClient.Width()) / m_biInf.bmiHeader.biWidth;
	rectOv.bottom = (rectOv.bottom * rectClient.Height()) / m_biInf.bmiHeader.biHeight;

	pDC->Rectangle(rectOv);

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

void RGBWnd::DrawNoiseText(CDC* pDC, UINT nIndex)
{
	CBrush NewBrush;
	NewBrush.CreateStockObject(WHITE_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	if (rectNoise[nIndex].bFail)
		lb.lbColor = RGB(255, 0, 0);
	else
		lb.lbColor = RGB(0, 0, 255);
	
	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);

	CRect rectClient;
	GetClientRect(rectClient);
	//rectClient.Height() / 3;
	if (rectNoise[nIndex].bFail)
		pDC->SetTextColor(RGB(255,0,0));
	else
		pDC->SetTextColor(RGB(0,0,255));

	CFont font;
	font.CreatePointFont(rectClient.Height() / 3, _T("Arial"));
	pDC->SelectObject(&font);
	pDC->SetTextAlign(TA_LEFT | TA_BASELINE);
	CString strText;
	strText.Format(_T("[%d] %.2f dB"), nIndex + 1, rectNoise[nIndex].dDbThreshold);
	pDC->TextOut(rectNoise[nIndex].Region.left, rectNoise[nIndex].Region.top - 5, strText, strText.GetLength());
	font.DeleteObject();

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

void RGBWnd::UpdateEmpty()
{
	m_nPaintEn = 0;
	Invalidate();
}

void RGBWnd::ResetRect_Noise()
{
	for (UINT nIdx = 0; nIdx < 9; nIdx++)
	{
		rectNoise[nIdx].Reset();
	}
}

void RGBWnd::ResetRect_NoiseJudge()
{
	for(UINT nIdx = 0; nIdx < 9; nIdx++)
	{
		rectNoise[nIdx].bFail = FALSE;
	}
}

void RGBWnd::SetNoiseFail(UINT nIndex, BOOL bFail)
{
	rectNoise[nIndex].bFail = bFail;
}

void RGBWnd::SetNoiseResult(UINT nIndex, DOUBLE dDbNoise)
{
	rectNoise[nIndex].dDbThreshold = dDbNoise;
}
