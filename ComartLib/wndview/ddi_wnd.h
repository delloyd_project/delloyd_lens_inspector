﻿
/* 	filename: 	DDI WND
 * **************************************************************************************
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2	[2004/08/23] : CODE APPEND..
 *  ..
 *  ..
 *  version  :  10	[2013/01/31] : 960H support (code alignment)
 *
 * **************************************************************************************
 */

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_DDIWND_dI_)
#define _DDIWND_dI_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//#include "..\\resource.h"
#include "..\\boxlib\\stools.h"
#include "..\\boxlib\\ddif.h"

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
class	 ddif;

class	 DDIWnd		: public CWnd
{
public:
		 DDIWnd();
virtual ~DDIWnd();

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	void	CreateNShow	(DWORD IN_dwCH, DWORD IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP);
	void	render		(DWORD IN_dwCH, LPBYTE IN_bpYC, RECT *in_psrc_rect = NULL);
	
	void	ddi_open	(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC);
	void	ddi_close	(DWORD IN_dwCH);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	ddif	   *m_pddif;	
	DWORD		m_dwCH, m_dwXZ,  m_dwYZ,  m_dwIMGSZ, m_dw411;

	LPBYTE		m_bpYUY2;	
	aligmem		m_amem;

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	int			m_nPaintEn;

	TCHAR		m_lpszWndName[MAX_PATH];	
    RECT		m_rectOrSz;	

	BITMAPINFO	m_biInf;
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * MFC MESSAGE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	virtual BOOL	DestroyWindow();
	
public:
	DECLARE_MESSAGE_MAP()	
	afx_msg void	OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp(  UINT nFlags, CPoint point);
	afx_msg void	OnClose();
	afx_msg void	OnPaint();
	
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_DDIWND_dI_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
