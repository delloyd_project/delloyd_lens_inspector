﻿
/* 	filename : 	mem osd
 * **************************************************************************************
 * 	created  :	2014/05/30
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 * 
 *  version  :  1   [2014/05/30] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "mem_osd.h"


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

#define	COLOR_KEY()							RGB(1,0,1)

/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * CLASS		: 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

mem_osd::mem_osd(void)
{	
	m_bposd		= NULL;
	m_nready	= 0;
}

mem_osd::~mem_osd(void)
{
	if (m_bposd)
	{	delete [] m_bposd;
		m_bposd	= NULL;		
	}

	m_nready	= 0;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL	mem_osd::make_str
(TCHAR *in_szstr, 
 TCHAR *in_szfontname, int in_nfontyz, COLORREF in_nfontcolor,
 int	in_nxz, int in_nyz)
{
	boxreturn((in_szstr		 == NULL), FALSE, DBGTR, _T("in ptr is null"));
	boxreturn((in_szfontname == NULL), FALSE, DBGTR, _T("in ptr is null"));

	boxreturn((in_nxz == 0), FALSE, DBGTR, _T("in xz is null"));
	boxreturn((in_nyz == 0), FALSE, DBGTR, _T("in yz is null"));

	HDC			hDC;
	HBRUSH		hBrush;
	HBITMAP		hBitmap;
	HFONT		hFont;
	
	COLORREF	rgbkey = COLOR_KEY();
	RECT		rect;

	SetRect(&rect, 0, 0, in_nxz, in_nyz);
	
	hDC		= CreateCompatibleDC(NULL);
	hBrush	= CreateSolidBrush	(rgbkey);		// key color : back ground
	hBitmap = CreateBitmap		(in_nxz, in_nyz, 1, 32, NULL);
	
	hFont = CreateFont(	in_nfontyz,		0,0,0,		
						FW_BOLD,
						FALSE,	FALSE,	FALSE,
						DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,	CLIP_DEFAULT_PRECIS,
						DEFAULT_QUALITY,DEFAULT_PITCH,
						in_szfontname);

	SelectObject(hDC, hBitmap);
	SelectObject(hDC, hBrush);
	SelectObject(hDC, hFont);

	if (m_bposd)
	{	delete [] m_bposd;
		m_bposd = NULL;
	}
	m_bposd		= new BYTE [in_nxz * in_nyz * 4];
	m_nosdxz	= in_nxz;
	m_nosdyz	= in_nyz;

	//BOXDBG>
	//BOXDBG>boxlog(1, DBGTR, ("new=> %X:%X"), m_bposd, in_nxz * in_nyz * 4);

	//i: fill key color draw
	FillRect(hDC, &rect, hBrush);

	COLORREF textColor	 = in_nfontcolor;
	COLORREF shadowColor = RGB(0, 0, 0);

	SetBkMode	(hDC, TRANSPARENT);
	
	/*	EDGE
	SetTextColor(hDC, shadowColor);
	DrawText	(hDC, in_szstr, lstrlen(in_szstr), &rect, DT_LEFT|DT_SINGLELINE);

	OffsetRect	(&rect, 2, 0);
	DrawText	(hDC, in_szstr, lstrlen(in_szstr), &rect, DT_LEFT|DT_SINGLELINE);

	OffsetRect	(&rect, 2, 2);
	DrawText	(hDC, in_szstr, lstrlen(in_szstr), &rect, DT_LEFT|DT_SINGLELINE);
	*/

	SetTextColor(hDC, textColor);
	DrawText	(hDC, in_szstr, lstrlen(in_szstr), &rect, DT_LEFT|DT_SINGLELINE);

	GetBitmapBits(hBitmap, in_nxz * in_nyz * 4, m_bposd);

	DeleteObject(hBitmap);
	DeleteObject(hBrush);
	DeleteObject(hFont);
	DeleteDC	(hDC);

	m_nready	= 1;

	return TRUE;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL	mem_osd::merge
(LPBYTE in_bpimg, 
 int	in_nstax,  int in_nstay,
 int	in_nimgxz, int in_nimgyz)
{
	boxreturn((in_bpimg		== NULL),	FALSE, DBGTR, _T("in ptr is null, skip"));
	boxreturn((in_nimgxz	== 0),		FALSE, DBGTR, _T("in xz is null, skip"));
	boxreturn((in_nimgyz	== 0),		FALSE, DBGTR, _T("in yz is null, skip"));	
	boxreturn((m_bposd		== NULL),	FALSE, DBGTR, _T("internal ptr is null, skip"));
	
	BYTE  * pimg, * posd;
	WORD  * pIMG;
	DWORD * pOSD;
	DWORD 	dwPXL;

	for (int n = 0; n < m_nosdyz; n++)
	{
		pimg = (BYTE *)in_bpimg + ((n + in_nstay) * in_nimgxz * 2) + (in_nstax * 2);
		posd = (BYTE *)m_bposd  + (n * m_nosdxz  * 4);
		pIMG = (WORD *)pimg;
		pOSD = (DWORD*)posd;

		for(int  m = 0; m < m_nosdxz; m++)
		{
			if (*pOSD != COLOR_KEY())
			{	dwPXL = *pOSD;
				dwPXL &= 0x0000FF00;	//i: GRAY  PXL
				dwPXL |= 0x00000080;	//i: COLOR PXL
				*pIMG = (WORD)dwPXL;
			}
			pIMG++;
			pOSD++;
		}
	}

	return TRUE;
}

