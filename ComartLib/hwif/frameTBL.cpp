

/* 	filename: 	sliceTBL
 * **************************************************************************************
 *
 * 	created  :	2012/10/26
 *	author   :	boxucom@gmail.com
 *  file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2   [2012/10/22] : move folder (code alignment)
 *
 * **************************************************************************************
 */

#include "stdafx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"


#include "hwif.h"

#include <windows.h>
#include <stdio.H>
#include <strsafe.h>

#include <math.h>
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)











// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *
// *	
// *
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef	THIS_FILE
static	char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define FRAME_CODE_SLEEP_CODE					99
#define FRAME_CODE_MAX_FRAME					30
#define FRAME_CODE_MAX_CHNUM					80









// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *
// *	
// *
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
static	stuint	g_VCAPT_sliceFPSTBL[2][9][30] = 
{
	{		
		{30, 0,   0,  0,  0, 	 0,  0,  0,  0,  0, 	   },	// 0	CH USE
		{30, 0,   0,  0,  0, 	 0,  0,  0,  0,  0, 	   },	// 1	CH USE
		{15, 15,  0,  0,  0, 	 0,  0,  0,  0,  0, 	   },	// 2	CH USE
		{10, 10, 10,  0,  0, 	 0,  0,  0,  0,  0, 	   },	// 3	CH USE	
		{ 8,  8,  7,  7,  0, 	 0,  0,  0,  0,  0, 	   },	// 4	CH USE
		{ 6,  6,  6,  6,  6, 	 0,  0,  0,  0,  0, 	   },	// 5	CH USE
		{ 5,  5,  5,  5,  5, 	 5,  0,  0,  0,  0, 	   },	// 6	CH USE
		{ 5,  5,  4,  4,  4, 	 4,  4,  0,  0,  0, 	   },	// 7	CH USE
		{ 4,  4,  4,  4,  4, 	 4,  3,  3,  0,  0, 	   },	// 8	CH USE
	
	}, 
	{
		{25,  0,  0,  0,  0, 	 0,  0,  0,  0,  0, 	  },	// 0	CH USE
		{25,  0,  0,  0,  0, 	 0,  0,  0,  0,  0, 	  },	// 1	CH USE
		{13, 12,  0,  0,  0, 	 0,  0,  0,  0,  0, 	  },	// 2	CH USE
		{ 9,  8,  8,  0,  0, 	 0,  0,  0,  0,  0, 	  },	// 3	CH USE	
		{ 7,  6,  6,  6,  0, 	 0,  0,  0,  0,  0, 	  },	// 4	CH USE
		{ 5,  5,  5,  5,  5, 	 0,  0,  0,  0,  0, 	  },	// 5	CH USE
		{ 5,  4,  4,  4,  4, 	 4,  0,  0,  0,  0, 	  },	// 6	CH USE
		{ 4,  4,  4,  4,  3, 	 3,  3,  0,  0,  0, 	  },	// 7	CH USE
		{ 4,  3,  3,  3,  3, 	 3,  3,  3,  0,  0, 	  },	// 8	CH USE
	},
};	







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	GetG_VCAPT_SliceFPSTBL(stuint IN_stNTSC, stuint IN_stUseCH, stuint *IO_stCamFPSTBL)
{	
	boxassert((IN_stUseCH    >    16), BOXEXIT, _T("input (IN_ARGS) is range over \n"));
	boxassert((IO_stCamFPSTBL== NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));

	
	DWORD	dwInx = (IN_stNTSC)? 0 : 1;
	CopyMemory(IO_stCamFPSTBL, &(g_VCAPT_sliceFPSTBL[dwInx][IN_stUseCH][0]), 30 * sizeof(stuint));
}






















//  --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  
static	int	_sort_tbl(stuint IO_stTBL[], stuint IO_stInxTBL[], stuint IN_stTblCnt)
{
	stuint	stD;
	stuint	sti, stk, stmax;
	
	for (sti = 0; sti < IN_stTblCnt; sti++)
	{	IO_stInxTBL[sti] = sti;
	}

	for (sti = 0; sti < IN_stTblCnt-1; sti++)
	{	
		stmax = sti;
		for (stk = sti+1; stk < IN_stTblCnt; stk++)
		{	if (IO_stTBL[stmax] < IO_stTBL[stk] ) 
				stmax = stk;				
		}

		stD					= IO_stTBL[stmax];
		IO_stTBL[stmax]		= IO_stTBL[sti];
		IO_stTBL[sti  ]		= stD;
		
		stD					= IO_stInxTBL[stmax];
		IO_stInxTBL[stmax]	= IO_stInxTBL[sti];
		IO_stInxTBL[sti  ]	= stD;
		
	}

	return 0;
}

//  --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  
static	int	_get_count(stuint IN_stTBL[], stuint IN_stTblCnt, stuint IN_stOne)
{	
	stuint		ni, ncnt;

	ncnt = 0;
	for (ni = 0; ni < (int)IN_stTblCnt; ni++)
	{
		if (IN_stOne)
		{	if (IN_stTBL[ni]) ncnt++;
		} else 
		{	if (IN_stTBL[ni]==0) ncnt++;
		}		
	}

	return (int)ncnt;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * IN_dtCamFPSInTBL	: table[] = { 0, 10, 0,10, 0,10, ..};
//						: 0, 2, 4, CH is sleep 
//						: 1, 3, 5, CH is 10 frame	
// * IN_dwTblCnt		: count of (IN_dtCamFPSInTBL, IO_dtCamInxTBL) : maybe 30 or 25
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	Get_frameTBL
(stuint *IN_stCamFPSInTBL, stuint IN_stInTblCnt,
 stuint *IO_stCamInxTBL,   stuint IN_stIoTblCnt)

{	
	boxassert((IN_stCamFPSInTBL == NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxassert((IO_stCamInxTBL   == NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxassert((IN_stInTblCnt    >  FRAME_CODE_MAX_CHNUM), BOXEXIT, _T("input range over \n"));
	boxassert((IN_stIoTblCnt    >  FRAME_CODE_MAX_FRAME), BOXEXIT, _T("input range over \n"));



	DWORD		dwcnt,			dwk;
	DWORD		dwinx,			dwinx2;
	DWORD		dwCHInx,		dwCHpFPS;	
	DWORD		dwFreeInxCnt,	dwUsedCamCnt;
	DOUBLE		dbInterval,		dbInx;

	stuint		stSortCamFPSTBL	[FRAME_CODE_MAX_CHNUM];
	stuint		stChNmCamFPSTBL	[FRAME_CODE_MAX_CHNUM];
	stuint		stFreeCIndexTBL [FRAME_CODE_MAX_CHNUM];
	stuint		stRsltCamFPSTBL	[FRAME_CODE_MAX_CHNUM];
		
	
	
	dwUsedCamCnt	 = (DWORD) _get_count(IN_stCamFPSInTBL, IN_stInTblCnt, 1);
	if (dwUsedCamCnt == 0)
	{	for (dwk = 0; dwk < (DWORD)IN_stIoTblCnt; dwk++)
		{	IO_stCamInxTBL[dwk] = FRAME_CODE_SLEEP_CODE;
		}
		return;
	}

	ZeroMemory( &(stChNmCamFPSTBL[0]), sizeof(stChNmCamFPSTBL));
	ZeroMemory( &(stRsltCamFPSTBL[0]), sizeof(stRsltCamFPSTBL));
	CopyMemory( &(stSortCamFPSTBL[0]), IN_stCamFPSInTBL, IN_stInTblCnt * sizeof(stuint));
	_sort_tbl (   stSortCamFPSTBL,     stChNmCamFPSTBL,  IN_stInTblCnt);

	// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- 
	// * loop  : used camera count
	// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- 
	for (dwcnt = 0; dwcnt < dwUsedCamCnt; dwcnt++) 
	{
		ZeroMemory(&(stFreeCIndexTBL[0]), sizeof(stFreeCIndexTBL));
		for (dwinx = 0, dwk = 0; dwk < (DWORD)IN_stIoTblCnt; dwk++) 
		{	
			if  (stRsltCamFPSTBL[dwk    ] == 0) 
			{	 stFreeCIndexTBL[dwinx++] = (stuint)dwk;	//i: index number				 
			}
		}
		dwFreeInxCnt = dwinx;

		// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- 
		// * interval count = (total free table number) / (channel per frame)
		// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- 
		dwCHpFPS	= (DWORD)stSortCamFPSTBL[dwcnt];
		dwCHInx		= (DWORD)stChNmCamFPSTBL[dwcnt];
		dbInterval	= (double)dwFreeInxCnt / (double)dwCHpFPS;		

		for (dwk = 0; dwk < dwCHpFPS; dwk++) 
		{	
			dbInx		= dbInterval * (dwk+1);						//i: dwk   start 0, 
			dbInx	   += 0.5;										//i: round up
			dwinx		= (DWORD)floor(dbInx) -1;					//i: index start 0
			dwinx2		= (DWORD)stFreeCIndexTBL[dwinx] % IN_stIoTblCnt;			

			stRsltCamFPSTBL[dwinx2] = (stuint)dwCHInx + 1;			//i: b.of SLEEP CODE
		}
	}	
	

	for (dwk = 0; dwk < (DWORD)IN_stIoTblCnt; dwk++) 
	{
		if (stRsltCamFPSTBL[dwk] == 0) 
			stRsltCamFPSTBL[dwk] = FRAME_CODE_SLEEP_CODE;
		else 
			stRsltCamFPSTBL[dwk]--;
	}

	CopyMemory(IO_stCamInxTBL, stRsltCamFPSTBL, IN_stIoTblCnt * sizeof(stuint));
}














