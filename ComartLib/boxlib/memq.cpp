﻿
/* 	filename : 	mem q
 * **************************************************************************************
 *
 * 	created  :	2005/11/30
 *	author   :	boxucom@gmail.com
 *
 *  version  :  0	[2005/11/30] : START CODE -> CLASS MADE / CODE WRITING..
 *					original code : nios II (GCC no MMU)
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "stools.h"
#include "memq.h"





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif







/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: 
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */


box_memq::~box_memq()
{
	m_bpool_ptr	=	NULL;
	m_npool_sz	=	0;

	m_pofst_tbl	=   NULL;
	m_psize_tbl	=   NULL;

	m_bpop_buf	=	NULL;
	m_npop_buf_sz=  0;

	m_hmtx		=	NULL;
}

box_memq:: box_memq() 
{	
	t_del();
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		box_memq::t_new(int in_npool_sz, int in_npool_cnt, int in_npop_bufsz)
{	
	boxreturn((in_npool_sz  == 0), -1, DBGTR, _T("too small : %d"), in_npool_sz);
	boxreturn((in_npool_cnt == 0), -1, DBGTR, _T("too small : %d"), in_npool_cnt);
	
	m_hmtx	= CreateMutex(NULL, FALSE, NULL);


	m_bpool_ptr	= (LPBYTE)_aligned_malloc(in_npool_sz + 1024, 32);
	m_npool_sz	= in_npool_sz;		
	m_npool_cnt = in_npool_cnt;

	m_pofst_tbl = new int [in_npool_cnt];
	m_psize_tbl = new int [in_npool_cnt];

	//BOXDBG>
	//BOXDBG>boxlog(1, DBGTR, ("new=> %X:%X"), m_pofst_tbl, in_npool_cnt);
	//BOXDBG>
	//BOXDBG>boxlog(1, DBGTR, ("new=> %X:%X"), m_psize_tbl, in_npool_cnt);

	m_bpop_buf		= (LPBYTE)_aligned_malloc(in_npop_bufsz + 1024, 32);
	m_npop_buf_sz	= in_npop_bufsz;
		
	//dbg>boxlog(1, DBGTR, _T("box_memq : new(%d)"), in_npool_sz);

	flush();	

	return 1;
}

void	box_memq::t_del()
{
	if (m_pofst_tbl)
	{	delete [] m_pofst_tbl;
		m_pofst_tbl = NULL;
	}

	if (m_psize_tbl)
	{	delete [] m_psize_tbl;
		m_psize_tbl = NULL;
	}

	if (m_bpool_ptr)
	{	_aligned_free(m_bpool_ptr);

		m_bpool_ptr	=	NULL;
		m_npool_sz	=	0;

		//dbg>boxlog(1, DBGTR, _T("box_memq : del()"));
	}	

	if (m_bpop_buf)
	{	_aligned_free(m_bpop_buf);

		m_bpop_buf		= NULL;
		m_npop_buf_sz	= 0;
	}

	if (m_hmtx)
	{	CloseHandle(m_hmtx);
		m_hmtx = NULL;
	}

	
}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_memq::flush()
{
	auto_mtx	mtx(m_hmtx);

	m_nq_head	= 0;
	m_nq_tail	= 0;

	m_nq_cnt	= 0;
	m_nq_size	= 0;
	m_nq_ofst	= 0;

	ZeroMemory(m_bpool_ptr, m_npool_sz);

	ZeroMemory(m_pofst_tbl, m_npool_cnt * sizeof(int));
	ZeroMemory(m_psize_tbl, m_npool_cnt * sizeof(int));

	ZeroMemory(m_bpop_buf, m_npop_buf_sz);

}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_memq::head_inc(int in_nsize)
{
	m_nq_cnt++;

	m_nq_size += in_nsize;

	m_nq_ofst += in_nsize;		
	m_nq_ofst %= m_npool_sz;

	m_nq_head++;	
	m_nq_head %= m_npool_cnt;
}


void	box_memq::tail_inc()
{
	if (m_nq_cnt == 0) return;

	int		nitmsz = m_psize_tbl[m_nq_tail];

	m_nq_tail++;
	m_nq_tail %= m_npool_cnt;

	m_nq_cnt--;
	m_nq_size -= nitmsz;

	boxassert((m_nq_size < 0), BOXEXIT, _T("bug mem q tail_inc"));
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_memq::push(BYTE *in_bpmem, int in_nbufsz)
{
	auto_mtx	mtx(m_hmtx);
	

	boxreturn0((in_bpmem            == NULL), DBGTR, _T("box_memq::push() input ptr is null"));
	boxreturn0(((in_nbufsz*2) >= m_npool_sz), DBGTR, _T("box_memq::push() too big input"));
	boxreturn0((in_nbufsz  >= m_npop_buf_sz), DBGTR, _T("box_memq::push() buf >=  popbuf"));
	


	int		nrem, nrem_sz;

	while(1)
	{	nrem	= m_npool_sz - m_nq_size;		
		if ((nrem <= in_nbufsz)  || (m_nq_cnt >= m_npool_cnt))
		{	tail_inc();
			continue;
		}			
		break;
	}



	if ((m_nq_ofst + in_nbufsz) >= m_npool_sz)
	{	
		nrem	= m_npool_sz - m_nq_ofst;
		CopyMemory(&(m_bpool_ptr[m_nq_ofst]), in_bpmem, nrem);

		in_bpmem   += nrem;		
		nrem_sz		= in_nbufsz - nrem;
		CopyMemory(&(m_bpool_ptr[0]), in_bpmem, nrem_sz);		

	} else
	{	
		CopyMemory(&(m_bpool_ptr[m_nq_ofst]), in_bpmem, in_nbufsz);
	}

	m_pofst_tbl[m_nq_head]	= m_nq_ofst;
	m_psize_tbl[m_nq_head]	= in_nbufsz;

	head_inc(in_nbufsz);

}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	box_memq::pop()
{
	auto_mtx	mtx(m_hmtx);

	if (m_nq_cnt == 0) return 0;		

	


	int		nrem,  nrem_sz;
	int		nofst, nsize;


	LPBYTE	io_bpmem = m_bpop_buf;

	nofst	= m_pofst_tbl[m_nq_tail];
	nsize	= m_psize_tbl[m_nq_tail];


	if ((nofst + nsize) >= m_npool_sz)
	{
		nrem	= m_npool_sz - nofst;
		CopyMemory(io_bpmem, &(m_bpool_ptr[nofst]), nrem);

		io_bpmem   += nrem;
		nrem_sz	    = nsize - nrem;
		CopyMemory(io_bpmem, &(m_bpool_ptr[0]), nrem_sz);		
		
	} else
	{	
		CopyMemory(io_bpmem, &(m_bpool_ptr[nofst]), nsize);		
	}

	
	tail_inc();


	return nsize;
}



























