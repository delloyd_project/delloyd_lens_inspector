


/* 	filename : 	exsdi
 * **************************************************************************************
 *
 * 	created  :	2016/06/10
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	reference code [ ]
 *  version  :  1   START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_EXSDI_LITE_DEI_)
#define _EXSDI_LITE_DEI_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef __cplusplus
extern "C" {
#endif









	



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef	EXSDI_LITE_EXPORTS
	#define EXSDI_LITE_API extern "C" __declspec(dllexport)

#else //EXSDI_LITE_EXPORTS
	#define EXSDI_LITE_API			  __declspec(dllimport)

#endif//EXSDI_LITE_EXPORTS



//---------------------------------------------------------------------------------------
// FUNCTION OPERATION VALUE							//INF: DWORD IN_dwCPUCMD
//---------------------------------------------------------------------------------------
#define CSPACE_CMD_AUTO								0
#define CSPACE_CMD_NORMAL							10
#define CSPACE_CMD_EXT								20







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	X86 cpu flags
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

#ifndef X86CPU_CMD
#define X86CPU_CMD

#define X86CPU_CMOV						0x0000001
#define X86CPU_MMX						0x0000002
#define X86CPU_MMX2						0x0000004	// MMX2 aka MMXEXT aka ISSE 
#define X86CPU_MMXEXT					X264_CPU_MMX2
#define X86CPU_SSE						0x0000008
#define X86CPU_SSE2						0x0000010
#define X86CPU_SSE3						0x0000020
#define X86CPU_SSSE3					0x0000040
#define X86CPU_SSE4						0x0000080	// SSE4.1 
#define X86CPU_SSE42					0x0000100	// SSE4.2 
#define X86CPU_LZCNT					0x0000200	// Phenom support for "leading zero count" instruction. 
#define X86CPU_AVX						0x0000400	// AVX support: requires OS support even if YMM registers aren't used. 
#define X86CPU_XOP						0x0000800	// AMD XOP 
#define X86CPU_FMA4						0x0001000	// AMD FMA4 
#define X86CPU_FMA3						0x0002000	// FMA3 
#define X86CPU_AVX2						0x0004000	// AVX2 
#define X86CPU_BMI1						0x0008000	// BMI1 
#define X86CPU_BMI2						0x0010000	// BMI2 



#define X86CPU_CACHELINE_32				0x0020000	// avoid memory loads that span the border between two cachelines 
#define X86CPU_CACHELINE_64				0x0040000	// 32/64 is the size of a cacheline in bytes 
#define X86CPU_SSE2_IS_SLOW				0x0080000	// avoid most SSE2 functions on Athlon64 
#define X86CPU_SSE2_IS_FAST				0x0100000	// a few functions are only faster on Core2 and Phenom 
#define X86CPU_SLOW_SHUFFLE				0x0200000	// The Conroe has a slow shuffle unit (relative to overall SSE performance) 
#define X86CPU_STACK_MOD4				0x0400000	// if stack is only mod4 and not mod16 
#define X86CPU_SLOW_CTZ					0x0800000	// BSR/BSF x86 instructions are really slow on some CPUs 

#define X86CPU_SLOW_ATOM				0x1000000	// The Atom is terrible: slow SSE unaligned loads, slow


//* SIMD multiplies, slow SIMD variable shifts, slow pshufb,
//* cacheline split penalties -- gather everything here that
//* isn't shared by other CPUs to avoid making half a dozen
//* new SLOW flags. 

#define X86CPU_SLOW_PSHUFB				0x2000000	// such as on the Intel Atom 
#define X86CPU_SLOW_PALIGNR				0x4000000	// such as on the AMD Bobcat 

#endif	//X86CPU_CMD




			






// *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-
// *  
// * CLASS		: cspace lite
// *
// * 
// *
// *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief			cpu cmd
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
EXSDI_LITE_API	DWORD	exsdi_get_cputype(void);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
EXSDI_LITE_API	void	exsdi_open (DWORD in_nch);
EXSDI_LITE_API	void	exsdi_close(DWORD in_nch);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
EXSDI_LITE_API	int		exsdi_dec_strm
(DWORD   in_nch,				//i: channel number
 BYTE  **io_yuv,				//o: output yuv ptr
 BYTE  **io_bpexsdistrm,		//o: output exsdi stream ptr

 BYTE  **io_bpjstrm1,			//o: output jstream ptr
 DWORD  *io_npjstrm1_sz,		//o: output jstream size
 BYTE  **io_bpjstrm2,			//o: output jstream ptr
 DWORD  *io_npjstrm2_sz,		//o: output jstream size

 BYTE	*in_bpexsdistrm,		//i: input exsdi stream ptr
 DWORD	 in_nexsdistrm_sz,		//i: input exsdi stream size
 DWORD	 in_dwcpucmd			//i: operation method : auto.. manual..
);





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief			cpu cmd
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	exsdif_get_cputype(void);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	exsdif_open (DWORD in_nch);
void	exsdif_close(DWORD in_nch);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		exsdif_dec_strm
(DWORD   in_nch,				//i: channel number
 BYTE  **io_yuv,				//o: output yuv ptr
 BYTE  **io_bpexsdistrm,		//o: output exsdi stream ptr

 BYTE  **io_bpjstrm1,			//o: output jstream ptr
 DWORD  *io_npjstrm1_sz,		//o: output jstream size
 BYTE  **io_bpjstrm2,			//o: output jstream ptr
 DWORD  *io_npjstrm2_sz,		//o: output jstream size

 BYTE	*in_bpexsdistrm,		//i: input exsdi stream ptr
 DWORD	 in_nexsdistrm_sz,		//i: input exsdi stream size
 DWORD	 in_dwcpucmd			//i: operation method : auto.. manual..
 );














// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef  __cplusplus
}
#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_EXSDI_LITE_DEI_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


