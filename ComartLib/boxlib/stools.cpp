
/* 	filename : 	small tools
 * **************************************************************************************
 *
 * 	created  :	2011/11/13
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	[08/07/2003] : reference code [ CAT3D SDK  ]
 *  version  :  1   [2011/11/13] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "stools.h"

#include <windows.h>
#include <stdio.H>
#include <strsafe.h>

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)
















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif











/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: tickchecker
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

tickchecker::~tickchecker()	
{
}

tickchecker:: tickchecker()
{	
	m_dwBaseTick = GetTickCount();
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

void	tickchecker:: rstbasetick()
{	
	m_dwBaseTick = GetTickCount();
}



BOOL	tickchecker::CompareTick(DWORD IN_dwTick)
{			
	DWORD	dwCurTick = GetTickCount();

	
	//i: tick overrun
	if (dwCurTick < m_dwBaseTick) 
	{	m_dwBaseTick = dwCurTick;
		return FALSE;	
	}
		
	if (dwCurTick < (m_dwBaseTick + IN_dwTick)) return FALSE;

	m_dwBaseTick = dwCurTick;
	return TRUE;
}








/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: FPSchecker
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

FPSchecker::~FPSchecker() 
{
}

FPSchecker:: FPSchecker() 
{	init();
}

void	FPSchecker:: init() 
{
	m_nImgCnt	= 0;
	m_nEnFPS	= 0;

	m_blHaveCnt = QueryPerformanceFrequency (&m_lnBaseFreq);
	BOOL blrv   = QueryPerformanceCounter	(&m_lbBaseCnt);
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	FPSchecker::Calcurate()
{
	LARGE_INTEGER		lnCurCnt;
	LONGLONG			llResult;
	LONGLONG			llImgCnt;


	if (m_lnBaseFreq.QuadPart == 0) 
	{	init();
		return;
	}


	m_nImgCnt++;
	QueryPerformanceCounter(&lnCurCnt);
	
	//i: count overrun
	if (lnCurCnt.QuadPart < m_lbBaseCnt.QuadPart)
	{	
		m_lbBaseCnt = lnCurCnt;
		m_nImgCnt	= 0;	
		m_nEnFPS	= 0;
		return;
	}

	//i: check 1 second 
	llResult  = lnCurCnt.QuadPart - m_lbBaseCnt.QuadPart;
	if (llResult < m_lnBaseFreq.QuadPart) return;



	//i: get 100u second unit
	llResult  /= (m_lnBaseFreq.QuadPart / 10000);	
	llImgCnt   = (m_nImgCnt * 100000) / llResult;
	
	m_nFPS     = (int) llImgCnt;

	m_lbBaseCnt= lnCurCnt;
	m_nImgCnt  = 0;	
	m_nEnFPS   = 1;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	FPSchecker::GetFPS()
{
	if (m_nEnFPS) return m_nFPS;
	return 0;

}

















/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: aligmem
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

#define POINTER_SIZE			sizeof(void *)

aligmem::~aligmem() 
{
	t_del();
}

aligmem:: aligmem() 
{	
	m_new_ptr   = NULL;
	m_align_ptr = NULL;
}

void	*aligmem:: t_new(int IN_nmem_size, int IN_nalign)
{
	boxreturn( (IN_nalign < 0), NULL, DBGTR, _T("miss align size"));

	if (m_new_ptr) t_del();

	IN_nalign = ( (IN_nalign > POINTER_SIZE)? IN_nalign : POINTER_SIZE) -1;

	LPBYTE		reptr;
	INT_PTR		ninx;	

	m_new_ptr	= (LPBYTE)_aligned_malloc(IN_nmem_size + (1024 + IN_nalign), 32);
	ninx		= (INT_PTR)m_new_ptr;
	ninx	   += IN_nalign;
	ninx	   &=(~IN_nalign);
	reptr		= (BYTE *)ninx;

	m_align_ptr = reptr;
	m_nsize		= IN_nmem_size;

	return (void *)reptr;
}


void	aligmem:: t_del()
{
	if (m_new_ptr)
	{	//del>boxlog(1, DBGTR, _T("del align mem : %p"), m_new_ptr); 
		
		_aligned_free(m_new_ptr);
		
		m_new_ptr   = NULL;
		m_align_ptr = NULL;
	}
}


















/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: auto mutex
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

basic_mtx::basic_mtx()
{	
	m_hmtx = NULL;
	m_hmtx = CreateMutex(NULL, FALSE, NULL);
}

basic_mtx::~basic_mtx()
{
	if (m_hmtx)
	{	CloseHandle(m_hmtx);
		m_hmtx = NULL;
	}
}





auto_mtx::auto_mtx()
{	m_hmtx = NULL;
}

auto_mtx::auto_mtx(HANDLE in_hmtx)
{
	boxassert( (in_hmtx == NULL), BOXEXIT, _T("miss input in_hmtx"));
	m_hmtx = in_hmtx;

	WaitForSingleObject(in_hmtx, INFINITE);
}

auto_mtx::~auto_mtx()
{
	if (m_hmtx)
	{	ReleaseMutex(m_hmtx);
		m_hmtx = NULL;
	}
}



















/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: basic thread (WIN32)
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */


basic_thrd::basic_thrd()	
{	m_pid		= NULL;
	m_nid		= 0;

	m_nact		= 1;
	m_pcmdevnt	= NULL;
	m_pext_func	= NULL;
	m_vparg		= NULL;
}

basic_thrd::~basic_thrd()	{	end();		}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		basic_thrd::sta(void * in_vpfunc, void *in_vparg, TCHAR *in_szname)
{
	DWORD	dwID = 0;


	m_pext_func	= (bt_ext_func) in_vpfunc;
	m_vparg		= in_vparg;
	m_pid		= 0;
	m_nact		= 1;

	StringCbPrintf(m_szname, sizeof(m_szname), _T("%s"), in_szname);
	m_pcmdevnt	= ::CreateEvent(NULL, FALSE, FALSE, NULL);
	boxreturn((m_pcmdevnt == NULL), -1, DBGTR, _T("create event fail : err:%d"), GetLastError());


	m_pid	= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)thrd_func, this, 0, (LPDWORD)&dwID);
	if (m_pid == NULL)
	{	
		m_pext_func	= NULL;
		m_vparg		= NULL;		
		boxreturn(1, -1, DBGTR, _T("make thread fail : err:%d"), GetLastError());		
	}

	m_nid	= dwID;
	return 1;
}

void	basic_thrd::end()
{
	if (m_pid	== NULL) return;

	m_nact	= 0;
	SetEvent(m_pcmdevnt);
	WaitForSingleObject(m_pid, INFINITE);

	m_pid		= NULL;
	m_nid		= 0;

	CloseHandle(m_pcmdevnt);
	m_pcmdevnt	= NULL;

}

void	basic_thrd::sync()
{
	SetEvent(m_pcmdevnt);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	WINAPI	basic_thrd::thrd_func(LPVOID in_lpvparam)
{
	basic_thrd	*pthis = static_cast <basic_thrd *>(in_lpvparam);

	
	boxlog(1, DBGTR, _T("%s thread sta"), pthis->m_szname);
	
	while (pthis->m_nact)
	{
		WaitForSingleObject(pthis->m_pcmdevnt, INFINITE);
		if (pthis->m_nact==0) break;

		if (pthis->m_pext_func)
		{	pthis->m_pext_func(pthis->m_vparg);
		} else 
		{	boxassert(1, 3, _T("invliad thread sub function may be code err or runtime bug"));			
			break;
		}
	}
	
	boxlog(1, DBGTR, _T("%s thread end"), pthis->m_szname);
	

	ExitThread(1);
	return 1;
}





























/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: check sum 
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

DWORD		checksum(BYTE *in_bpmem, int in_ncnt)
{
	int		ni;
	DWORD	ncsum = 0;

	boxreturn( (in_bpmem == NULL) , 0, DBGTR, _T("miss in ptr"));
	boxreturn( (in_ncnt  ==    0) , 0, DBGTR, _T("miss in cnt"));

	

	for (ni = 0; ni < in_ncnt; ni++)
	{	
		ncsum += in_bpmem[ni];		
	}

	return ncsum;

}























