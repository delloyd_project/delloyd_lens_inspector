


/* 	filename : 	cspace lite
 * **************************************************************************************
 *
 * 	created  :	2011/11/13
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	reference code [08/07/2003	- CAT3D SDK  ]
 *  version  :  1   START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_CSPACE_LITE_VXI_)
#define _CSPACE_LITE_VXI_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef __cplusplus
extern "C" {
#endif









	



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef	CSPACE_LITE_EXPORTS
	#define CSPACE_LITE_API extern "C" __declspec(dllexport)

#else //CPSACE_LITE_EXPORTS
	#define CSPACE_LITE_API			   __declspec(dllimport)
#endif//CPSACE_LITE_EXPORTS



//---------------------------------------------------------------------------------------
// FUNCTION OPERATION VALUE							//INF: DWORD IN_dwCPUCMD
//---------------------------------------------------------------------------------------
#define CSPACE_CMD_AUTO								0
#define CSPACE_CMD_NORMAL							10
#define CSPACE_CMD_EXT								20








			






// *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-
// *  
// * CLASS		: cspace lite
// *
// * 
// *
// *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief			cpu cmd
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int	CSPACE_SWAP_MEMORY
(BYTE  *IO_bpDst,		//INF: DEST   BUFFER
 BYTE  *IN_bpSrc,		//INF: SOURCE BUFFER
 DWORD  IN_dwSIZE,		//INF: SIZE 
 DWORD  IN_dwCPUCMD);	//INF: OPERATION METHOD

CSPACE_LITE_API	int	CSPACE_COPY_MEMORY
(BYTE  *IO_bpDst,		//INF: DEST   BUFFER
 BYTE  *IN_bpSrc,		//INF: SOURCE BUFFER
 DWORD  IN_dwSIZE,		//INF: SIZE 
 DWORD  IN_dwCPUCMD);	//INF: OPERATION METHOD








// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief				UYVY
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_UYVY_TO_RGB32
(BYTE  *IN_bpUYVYADDR,	//INF:	SOURCE	UYVY LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpRGBADDR,	//INF:	DEST	RGB32	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_UYVY_TO_YUY2
(BYTE  *IN_bpUYVYADDR,	//INF:	SOURCE	UYVY LAW IMAGE BUFFER ADDRESS
DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
DWORD  IN_dwHEIGHT,		//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL	
BYTE  *IO_bpYUY2ADDR,	//INF:	DEST	YUY2 LAW IMAGE BUFFER ADDRESS 
DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_UYVY_TO_Y420
(BYTE  *IN_bpUYVYADDR,	//INF:	SOURCE	UYVY LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpYADDR,		//INF:	DEST	Y	 LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpUADDR,		//INF:	DEST	U	 LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpVADDR,		//INF:	DEST	V	 LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_UYVY_TO_NV12
(BYTE  *IN_bpUYVYADDR,	//INF:	SOURCE	UYVY LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpYADDR,		//INF:	DEST	Y    LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpUVADDR,	//INF:	DEST	UV   LAW IMAGE BUFFER ADDRESS  
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief				Y41P
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_Y41P_TO_Y420
(BYTE  *IN_bpY41PADDR,	//INF:	SOURCE	Y41P LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpYADDR,		//INF:	SOURCE	Y    LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpUADDR,		//INF:	SOURCE	U    LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpVADDR,		//INF:	SOURCE	V    LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_Y41P_TO_NV12
(BYTE  *IN_bpY41PADDR,	//INF:	SOURCE	Y41P RAW IMAGE BUFFER ADDRESS
DWORD  IN_dwWIDTH,		//INF:	SOURCE		 RAW IMAGE WIDTH  PIXEL
DWORD  IN_dwHEIGHT,		//INF:	SOURCE		 RAW IMAGE HEIGHT PIXEL
BYTE  *IO_bpYADDR,		//INF:	DEST	Y    RAW IMAGE BUFFER ADDRESS
BYTE  *IO_bpUVADDR,		//INF:	DEST	UV   RAW IMAGE BUFFER ADDRESS 
DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_Y41P_TO_UYVY
(BYTE  *IN_bpY41PADDR,	//INF:	SOURCE	Y41P LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL 
 BYTE  *IO_bpUYVYADDR,	//INF:	DEST	UYVY LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_Y41P_TO_YUY2
(BYTE  *IN_bpY41PADDR,	//INF:	SOURCE	Y41P LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL 
 BYTE  *IO_bpYUY2ADDR,	//INF:	DEST	YUY2 LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_Y41P_TO_RGB32
(BYTE  *IN_bpY41PADDR,	//INF:	SOURCE	Y41P LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpRGBADDR,	//INF:	DEST	RGB32	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD







 // *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 //!	@brief				YUY2
 //!	@		
 // *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_YUY2_TO_UYVY
(BYTE  *IN_bpYUY2ADDR,	//INF:	SOURCE	YUY2 LAW IMAGE BUFFER ADDRESS
DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
DWORD  IN_dwHEIGHT,		//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL	
BYTE  *IO_bpUYVYADDR,	//INF:	DEST	UYVY LAW IMAGE BUFFER ADDRESS 
DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_YUY2_TO_RGB32
(BYTE  *IN_bpYUY2ADDR,	//INF:	SOURCE	YUY2 LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
 BYTE  *IO_bpRGBADDR,	//INF:	DEST	RGB32	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_YUY2_TO_Y420
(BYTE  *IN_bpYUY2ADDR,	//INF:	SOURCE	YUY2 LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
 DWORD  IN_dwHEIGHT,	//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL	
 BYTE  *IO_bpYADDR,		//INF:	DEST	Y	 LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpUADDR,		//INF:	DEST	U	 LAW IMAGE BUFFER ADDRESS
 BYTE  *IO_bpVADDR,		//INF:	DEST	V	 LAW IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_YUY2_TO_NV12
(BYTE  *IN_bpYUY2ADDR,	//INF:	SOURCE	YUY2 LAW IMAGE BUFFER ADDRESS
DWORD  IN_dwWIDTH,		//INF:	SOURCE		 LAW IMAGE WIDTH  PIXEL
DWORD  IN_dwHEIGHT,		//INF:	SOURCE		 LAW IMAGE HEIGHT PIXEL
BYTE  *IO_bpYADDR,		//INF:	DEST	Y    LAW IMAGE BUFFER ADDRESS
BYTE  *IO_bpUVADDR,		//INF:	DEST	UV   LAW IMAGE BUFFER ADDRESS  
DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD










// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief				ARGB
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_ARGB_TO_UYVY
(BYTE  *IN_bpRGB4ADDR,		//INF:	SOURCE	RGB4 RAW IMAGE BUFFER ADDRESS
DWORD  IN_dwWIDTH,			//INF:	SOURCE		 RAW IMAGE WIDTH  PIXEL
DWORD  IN_dwHEIGHT,			//INF:	SOURCE		 RAW IMAGE HEIGHT PIXEL
BYTE  *IO_bpUYVYDDR,		//INF:	DEST	UYVY	 IMAGE BUFFER ADDRESS
DWORD  IN_dwCPUCMD);		//INF:	OPERATION METHOD



CSPACE_LITE_API	int		CSPACE_ABRGB_PX_GAIN
(
BYTE  *io_bpargbaddr,			//INF:	source dest  argb	 IMAGE BUFFER ADDRESS
int    in_again,				//INF:  alpha gain value
int    in_rgain,				//INF:  red   gain value
int    in_ggain,				//INF:  green gain value
int    in_bgain,				//INF:  blue  gain value
DWORD  IN_dwPITCH,				//INF:  dest  width  pitch byte
DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
DWORD  IN_dwHEIGHT,				//INF:	source		     IMAGE HEIGHT PIXEL
DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD



















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief				BRGB
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_BRGBPACK_TO_BRGB420
(BYTE  *IN_bpBRGBADDR,	//INF:	source bayer rgb IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,	//INF:	source		     IMAGE HEIGHT PIXEL
 BYTE  *IO_bpGADDR,		//INF:	dest   bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBADDR,		//INF : dest   bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpRADDR,		//INF : desta  bayer r	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_BRGBPACK_TO_BRGB12
(BYTE  *IN_bpBRGBADDR,	//INF:	source bayer rgb IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,	//INF:	source		     IMAGE HEIGHT PIXEL
 BYTE  *IO_bpGADDR,		//INF:	dest   bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBRADDR,	//INF : dest   bayer br	 IMAGE BUFFER ADDRESS 
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_BRGB420_TO_BRGBPACK
(BYTE  *IN_bpGADDR,		//INF:	source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBADDR,		//INF : source bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpRADDR,		//INF : source bayer r	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBRGBADDR,	//INF:	dest   bayer rgb IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,	//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_BRGB12_TO_BRGBPACK
(BYTE  *IN_bpGADDR,		//INF:	source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBRADDR,	//INF : source bayer br	 IMAGE BUFFER ADDRESS 
 BYTE  *IO_bpBRGBADDR,	//INF:	dest   bayer rgb IMAGE BUFFER ADDRESS
 DWORD  IN_dwWIDTH,		//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,	//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);	//INF:	OPERATION METHOD





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * if  1936 x 1100 then
// *
// * brgb8plan : g : 968(1936/2)x1100, b :968(1936/2)x550(1100/2), r :968(1936/2)x550(1100/2)
// * brgb2pack : 3872(968*4)       x 550(1100/2)
// * brgb10pack: 3872(968*2*2byte) x 1100
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * brgbplan 8bit format  (brgb8plan)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * G0 G1 G2 G3 G4 G5 G6 G7	G8 G9 Ga Gb Gc Gd Ge Gf		: G addr (odd line : Xz/2 * Ysz/2)
// * K0 K1 K2 K3 K4 K5 K6 K7	K8 K9 Ka Kb Kc Kd Ke Kf		: G addr (even line: Xz/2 * Ysz/2)
// *
// * B0 B1 B2 B3 B4 B5 B6 B7	B8 B9 Ba Bb Bc Bd Be Bf		: B addr (Xsz/2 *  Ysz/2)
// * 
// * R0 R1 R2 R3 R4 R5 R6 R7    R8 R9 Ra Rb Rc Rd Re Rf		: R addr (Xsz/2 *  Ysz/2)
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * brgb 8bit pack format (brgb2pack)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * G0 B0 K0 R0 G1 B1 K1 R1    G2 B2 K2 R2 G3 B3 K3 R3 ..	: 1st line (Xz * 2 color)
// * G0 B0 K0 R0 G1 B1 K1 R1    G2 B2 K2 R2 G3 B3 K3 R3 ..	: 2nd line (Xz * 2 color)
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * brgb 16bit pack format  (brgb16bpack)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * 00 B0 00 G0 00 B1 00 G1    00 B2 00 G2 00 B3 00 G3 ..  : 1st line (Xz)
// * 00 K0 00 R0 00 K1 00 R1    00 K2 00 R2 00 K3 00 R3 ..  : 2nd line (Xz)
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_BRGB2PACK_TO_BRGB16PACK
(BYTE *IN_bpBRBG2PACKADDR,		//INF: source bayer bggr2pack 8bit image buffer address
 BYTE *IO_bpBRGB16PACKADDR,		//INF: dest   bayer bggr     16bit image buffer address
 DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_BRGB8PLAN_TO_BRGB16PACK
(BYTE  *IN_bpGADDR,				//INF:	source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBADDR,				//INF : source bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpRADDR,				//INF : source bayer r	 IMAGE BUFFER ADDRESS
 BYTE *IO_bpBRGB16PACKADDR,		//INF: dest   bayer bggr     16bit image buffer address
 DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_BRGB82PLAN_TO_BRGB16PACK
(BYTE  *IN_bpGADDR,				//INF:	source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBADDR,				//INF : source bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpRADDR,				//INF : source bayer r	 IMAGE BUFFER ADDRESS
 BYTE *IO_bpBRGB16PACKADDR,		//INF: dest   bayer bggr     16bit image buffer address
 DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD




CSPACE_LITE_API	int		CSPACE_BRGB8PLAN_TO_BRGB2PACK
(BYTE  *IN_bpGADDR,				//INF: source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBADDR,				//INF: source bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpRADDR,				//INF: source bayer r	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBRGB2PACKADDR,		//INF: dest   bayer bggr     16bit image buffer address
 DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF: source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF: source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF: sOPERATION METHOD


CSPACE_LITE_API	int		CSPACE_BRGB2PACK_TO_BRGB8PLAN
(BYTE  *IN_bpBRGB2PACKADDR,		//INF: source bayer bggr2pack 8bit image buffer address
 BYTE  *IO_bpGADDR,				//INF: dest   bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBADDR,				//INF: dest   bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpRADDR,				//INF: dest   bayer r	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwPITCH,				//INF: dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF: source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_BRGBPACK_TO_BRGB8PLAN
(BYTE  *IN_bpBRGBPACKADDR,		//INF: source bayer bggrpack 8bit image buffer address
 BYTE  *IO_bpGADDR,				//INF: dest   bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBADDR,				//INF: dest   bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpRADDR,				//INF: dest   bayer r	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwPITCH,				//INF: dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF: source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD

CSPACE_LITE_API	int		CSPACE_BRGB8PLAN_TO_BRGBPACK
(BYTE  *IN_bpGADDR,				//INF: src   bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpBADDR,				//INF: src   bayer b	 IMAGE BUFFER ADDRESS
 BYTE  *IN_bpRADDR,				//INF: src   bayer r	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpBRGBPACKADDR,		//INF: dest  bayer bggrpack 8bit image buffer address
 DWORD  IN_dwPITCH,				//INF: dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF: source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF: source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF: OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_BRGB82PLAN_TO_BRGB8PLAN
(
 BYTE  *IN_bpGADDR,				//INF:	source bayer g	 IMAGE BUFFER ADDRESS
 BYTE  *IO_bpGADDR,				//INF:	dest   bayer g	 IMAGE BUFFER ADDRESS
 DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
 DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
 DWORD  IN_dwHEIGHT,			//INF:	source		     IMAGE HEIGHT PIXEL
 DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-



CSPACE_LITE_API	int		CSPACE_BRGB8PLAN_PX_OBGAIN
(
BYTE  *IO_bpGADDR,				//INF:	source dest  g	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpBADDR,				//INF:  source dest  b	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpRADDR,				//INF:  source dest  r	 IMAGE BUFFER ADDRESS
BYTE   in_bob,					//INF:  optical black
double in_dggain,				//INF:  g gain value
double in_dbgain,				//INF:  b gain value
double in_drgain,				//INF:  r gain value
DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
DWORD  IN_dwHEIGHT,				//INF:	source		     IMAGE HEIGHT PIXEL
DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
CSPACE_LITE_API	int		CSPACE_BRGB8PLAN_PX_RGB8PLAN
(
BYTE  *IN_bpGADDR,				//INF:	source bayer g	 IMAGE BUFFER ADDRESS
BYTE  *IN_bpBADDR,				//INF:  source bayer b	 IMAGE BUFFER ADDRESS
BYTE  *IN_bpRADDR,				//INF:  source bayer r	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpGADDR,				//INF:	dest scaled  g	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpBADDR,				//INF:  dest scaled  b	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpRADDR,				//INF:  dest scaled  r	 IMAGE BUFFER ADDRESS
DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
DWORD  IN_dwHEIGHT,				//INF:	source		     IMAGE HEIGHT PIXEL
DWORD  IN_dwCPUCMD);				//INF:	OPERATION METHOD


CSPACE_LITE_API	int		CSPACE_RGB8PLAN_TO_ARGB
(
BYTE  *IN_bpGADDR,				//INF:	source bayer g	 IMAGE BUFFER ADDRESS
BYTE  *IN_bpBADDR,				//INF:  source bayer b	 IMAGE BUFFER ADDRESS
BYTE  *IN_bpRADDR,				//INF:  source bayer r	 IMAGE BUFFER ADDRESS
BYTE  *IO_bpARGBADDR,			//INF:	dest   ARGB		 IMAGE BUFFER ADDRESS
DWORD  IN_dwPITCH,				//INF:  dest   width  pitch byte
DWORD  IN_dwWIDTH,				//INF:	source		     total image width pixel
DWORD  IN_dwHEIGHT,				//INF:	source		     IMAGE HEIGHT PIXEL
DWORD  IN_dwCPUCMD);			//INF:	OPERATION METHOD
















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef  __cplusplus
}
#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_CSPACE_LITE_VXI_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


