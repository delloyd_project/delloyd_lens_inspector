

/* 	filename : 	direct draw interface
 * **************************************************************************************
 *
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	[08/07/2003] : reference code [ CAT3D SDK  ]
 *  version  :  1   [2013/01/31] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_DDIF_IX_)
#define _DDIF_IX_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define	MAX_DDIF_CH						128

#define DDIF_UYVY						0
#define DDIF_NV12						1
#define DDIF_YUY2						2
#define DDIF_I420						3
#define DDIF_RGBA						4


#define DDIF_OWNSYNC					1
#define DDIF_EXTSYNC					0



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	@class		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
struct			ddrawbox;




class			ddif
{
public:		   ~ddif();
				ddif();
	 
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:	
	int			ddi_open (CWnd *in_pcwnd, int IN_nownsync = 0);
	void		ddi_close();




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//			own sync generator
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:					

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:	
	void		own_sync_init();
	int			own_sync_sta();
	void		own_sync_end();

private:	
	HANDLE		m_pown_sync_hd;
	HANDLE		m_pown_sync_evnt;
	DWORD		m_nown_sync_id;

public:		
	int			m_nown_sync_act;

static	
	DWORD	WINAPI	own_sync_thrd(LPVOID in_lpvparam);
	void			own_sync_ext();

	


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//			channel 
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	int			squadscrn_open   (DWORD IN_nmaxch, DWORD in_nddiftype);
	void		squadscrn_close	 ();

	void		squadscrn_ch_rect(DWORD IN_nch, RECT *in_psrcrect, RECT *in_pdstrect);
	void		squadscrn_clear	 ();

private:
	basic_mtx	m_chamem_mtx;

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:			
	int			chamem_open		(DWORD IN_nch, DWORD IN_nxz, DWORD IN_nyz, DWORD IN_ntype);
	void		chamem_close	(DWORD IN_nch);


	int			chamem_isalive	(DWORD IN_nch);
	LPBYTE		chamem_get_buf	(DWORD IN_nch);
	void 		chamem_put_buf	(DWORD IN_nch);
	void		chamem_updt_img	(DWORD IN_nch);


// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:						
	int			chsraw_open		(DWORD IN_nch, DWORD IN_nxz, DWORD IN_nyz, DWORD IN_ntype=DDIF_UYVY);
	void		chsraw_close		(DWORD IN_nch);
	
	int			chsraw_isalive	(DWORD IN_nch);
	LPBYTE		chsraw_get_buf	(DWORD IN_nch);
	void		chsraw_put_buf	(DWORD IN_nch);

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:						
	void		copyto_yscale	(LPBYTE io_bpdst, DWORD in_dwdstyz, DWORD in_dwdstpitch, 
								 LPBYTE in_bpsrc, DWORD in_dwsrcyz, DWORD in_dwsrcpitch, DWORD in_dwdtype,
								 DWORD  in_dwdstrectyz, DWORD*io_cvrtyz);

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:							
	int			chsraw_squadscrn_render();

	int			ch_render(DWORD IN_nch, LPBYTE IN_bpYC422, RECT *in_psrc_rect = NULL);
	

	
 

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:	
	ddrawbox   *m_ddbox;
	CWnd	   *m_pwnd;
	


// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:	
	DWORD		m_dwBaseTick;

};












// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_DDIF_IX_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


