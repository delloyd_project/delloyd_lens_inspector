
/* 	filename : 	memory pingpong
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 *
 * 	created  :	2017/11/21 
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	reference code [   ]
 *  version  :  1   [2005/11/30] : START CODE -> CLASS MADE / CODE WRITING..
 *                         
 *
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 */



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef _BOX_MEM_PP_S_
#define _BOX_MEM_PP_S_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


#define BOX_MEMPP_CNT			16


#define SYNC_TRG_INX			1			// changed index
#define SYNC_TRG_SIZE			2			// size  fill
#define SYNC_TRG_CNT			4			// count fill
#define SYNC_TRG_TICK			8			// tick  fill
#define SYNC_TRG_USR			16			// usr set












// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	@class		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


class			box_mempp
{
public:			box_mempp(); 
			   ~box_mempp();
		   
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:	
	int		    t_new(int	in_nid, int in_npool_sz, int in_nppon_cnt,
					  int	in_nsync_use,		// sync trigger use
					  int	in_nsync_sz,		// sync trigger size
					  int	in_nsync_cnt,		// sync trigger cnt
					  int	in_nsync_tick);		// sync trigger tick
		
	void		t_del();	
	
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:		
	int			m_nid;						//i: identify, no-mean
	HANDLE		m_hmtx;						//i: re-entry, item protect 
	
	
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:
	LPBYTE		m_bpool_buf;				//i: memory  pool	
	int			m_npool_sz;					//i: memory  size
	
	LPBYTE		m_bppon_ptr[BOX_MEMPP_CNT];	//i: memory  ping pong ptr	
	int			m_nppon_sz;					//i: memory  ping pong size	
	int			m_nppon_cnt;				//i: ping pong count

	LPBYTE		m_bpopl_buf;

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:
	int			m_nsync_use;
	int			m_nsync_sz;
	int			m_nsync_cnt;
	int			m_nsync_tick;
	
	
	int			m_ndbgmsg;

// = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- =
public:	
	void		flush();

	void		push	(BYTE * in_bpmem, int in_nbufsz);	
	int 		popl	(BYTE **io_bpmemout);
	
	
	void		set_trg (DWORD in_utrg);
	DWORD		get_trg ();	
	DWORD		chk_trg (int in_nbufsz);
		

// = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- =
private:
	void		head_inc(DWORD in_utrg);
	void		tail_inc();
	
	int			m_nq_hdinx;	
	int			m_nq_tlinx;
	int			m_nq_cnt;

	DWORD		m_uq_trgtbl[BOX_MEMPP_CNT];
	int			m_nq_siztbl[BOX_MEMPP_CNT];
	int			m_nq_cnttbl[BOX_MEMPP_CNT];
	DWORD		m_uq_tcktbl[BOX_MEMPP_CNT];
	

};








// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_BOX_MEM_PP_S_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


















