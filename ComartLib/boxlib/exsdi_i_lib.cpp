
/* 	filename : 	exsdi lib link
 * **************************************************************************************
 *
 * 	created  :	2016/07/12
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 * 
 *  version  :  1	[2016/07/12] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
//#include "..\\env_set.h"
#include "..\\boxinc\\box_type_win.h"
#include "..\\boxinc\\box_log_win.h"


#include "exsdi_lite.h"



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)

#ifdef EXSDI_DEC_USE	

#if defined(_WIN64)
#pragma	comment(lib,"exsdi_lite_64b.lib")
#else
#pragma	comment(lib,"exsdi_lite_32b.lib")
#endif

#endif





/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: 
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

DWORD	exsdif_get_cputype(void)
{
	#ifdef EXSDI_DEC_USE	
	return exsdi_get_cputype();
	#endif
	
	return 0;
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	exsdif_open(DWORD in_nch)
{
	#ifdef EXSDI_DEC_USE
	exsdi_open(in_nch);
	#endif
}

void	exsdif_close(DWORD in_nch)
{
	#ifdef EXSDI_DEC_USE
	exsdi_close(in_nch);
	#endif
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	exsdif_dec_strm
(DWORD   in_nch,				//i: channel number
 BYTE  **io_yuv,				//o: output yuv ptr
 BYTE  **io_bpexsdistrm,		//o: output exsdi stream ptr

 BYTE  **io_bpjstrm1,			//o: output jstream ptr
 DWORD  *io_npjstrm1_sz,		//o: output jstream size
 BYTE  **io_bpjstrm2,			//o: output jstream ptr
 DWORD  *io_npjstrm2_sz,		//o: output jstream size

 BYTE	*in_bpexsdistrm,		//i: input exsdi stream ptr
 DWORD	 in_nexsdistrm_sz,		//i: input exsdi stream size
 DWORD	 in_dwcpucmd			//i: operation method : auto.. manual..
 )
{
	int		nrv  = 0;
	#ifdef EXSDI_DEC_USE
	nrv = exsdi_dec_strm(in_nch, io_yuv,	io_bpexsdistrm, 
							io_bpjstrm1,	io_npjstrm1_sz,		io_bpjstrm2, io_npjstrm2_sz, 
							in_bpexsdistrm, in_nexsdistrm_sz,	in_dwcpucmd);
	#endif

	return nrv;
}








