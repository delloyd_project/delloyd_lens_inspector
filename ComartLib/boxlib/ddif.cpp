
/* 	filename : 	direct draw interface
 * **************************************************************************************
 *
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	[08/07/2003] : reference code [ CAT3D SDK  ]
 *  version  :  1   [2013/01/31] : START CODE -> CLASS MADE / CODE WRITING..
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"
#include "..\boxlib\stools.h"

#include "ddif.h"
#include <ddraw.h>


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if defined(_WIN64)
	#pragma	comment(lib,"..\\CAT_SDK\\boxlib\\X64\\ddraw.lib")
	#pragma	comment(lib,"..\\CAT_SDK\\boxlib\\X64\\dxguid.lib")
#else
	#pragma	comment(lib,"..\\CAT_SDK\\boxlib\\X32\\ddraw.lib")
	#pragma	comment(lib,"..\\CAT_SDK\\boxlib\\X32\\dxguid.lib")
#endif




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * check 1: IDirectDrawSurface::Blt (Compact 2013) 
//			: https://msdn.microsoft.com/ko-kr/library/ee491237.aspx
//			: remakr => The RECT structures are defined so that the right and bottom 
//			: members are exclusive: right minus left equals the width of the rectangle, 
//			: not one less than the width.























// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
struct						ddrawbox
{
	LPDIRECTDRAW7			pDD;					// DDraw ��ü
	LPDIRECTDRAWSURFACE7	pDDSPrimary;			// Primary Surface
	LPDIRECTDRAWSURFACE7	pDDSOffScreen;			// off     surface (display mode)	
	
	
	LPDIRECTDRAWCLIPPER		pDDClipper;				// Clipper


	//-=- -=- -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=- -=- -=-
	struct					T_SQUAD_SURFACE_INFO
	{	LPDIRECTDRAWSURFACE7	psquad;				// squad    surface (1920 x 1080)	

		DWORD				nxz, nyz, nxbc, nsz;
		DWORD				ntype;
		DWORD				nmaxch;
	}	tddsquad;


	//-=- -=- -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=- -=- -=-
	struct					T_MEM_CH
	{	aligmem				tmem;					// Raw data memory	 (image size)		

		DWORD				nxz, nxbc, nyz, nsz;

		DWORD				ntype;
		DWORD				nupdt;

		RTSZ				rtsz;
		RECT				rect;
	}	amem				[MAX_DDIF_CH];


	//-=- -=- -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=- -=- -=- 
	struct					T_RAW_CH
	{	LPDIRECTDRAWSURFACE7	psraw;				// Raw Data Surface  (image size)
	
		DWORD				nxz, nxbc, nyz, nsz;
	
		DWORD				ntype;
		DWORD				nupdt;
	
		RTSZ				rtsz;
		RECT				rect;
	}	sraw				[MAX_DDIF_CH];	


	//-=- -=- -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=-  -=- -=- -=- 
	RTSZ					dtdvsn_rtsz	[MAX_DDIF_CH];	
	RECT					divisn_rect	[MAX_DDIF_CH];

};






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define SQUADSCRN_XZ					(1920)
#define SQUADSCRN_YZ					(1080)





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define UPT_SRC_RECT					1
#define UPT_DST_RECT					2
#define UPT_READY						3

#define UPT_AMEM_IMG					1





/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: 
 *
 *
 * 
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

typedef	struct		_DDSURFACE2_TAG
{
	DWORD			dwflags;			// pixel format flags
	DWORD			dwfourcc;			// (FOURCC code)
	DWORD			dwbitcnt;			// how many bits per pixel	
	DWORD			dwxz_mul;

}	t_dds2_ddifinfo;

// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
t_dds2_ddifinfo		g_ddif_info[] = 
{	
	{ DDPF_FOURCC|DDPF_YUV, MAKEFOURCC('U','Y','V','Y'), 16, 2,	},	//DDIF_UYVY
	{ DDPF_FOURCC|DDPF_YUV, MAKEFOURCC('N','V','1','2'), 12, 1,	},	//DDIF_NV12
	{ DDPF_FOURCC|DDPF_YUV, MAKEFOURCC('Y','U','Y','2'), 16, 2,	},	//DDIF_YUY2
	{ DDPF_FOURCC|DDPF_YUV, MAKEFOURCC('I','4','2','0'), 12, 1,	},	//DDIF_I420
	{ DDPF_RGB,             0,                           32, 4,	},	//DDIF_RGBA
};












// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		Construction / Destruction / Initialize
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
ddif::ddif()
{
	m_pwnd		= NULL;

	m_ddbox		= new ddrawbox;	
	ZeroMemory(m_ddbox, sizeof(ddrawbox));	

	own_sync_init();
}


ddif::~ddif()
{
	ddi_close();

	delete  m_ddbox;
	m_ddbox = NULL;
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::ddi_open(CWnd *in_pwnd, int IN_nownsync)
{

	HRESULT	   hr = S_OK;
	DDSURFACEDESC2	ddsd;


	// 1>init direct draw 7
	hr = DirectDrawCreateEx(NULL, (void**)&(m_ddbox->pDD), IID_IDirectDraw7, NULL);
	boxreturn (FAILED(hr), -1, DBGTR, _T("direct draw fail"));

	hr = m_ddbox->pDD->SetCooperativeLevel(in_pwnd->m_hWnd, DDSCL_NORMAL);
	if (FAILED(hr))
	{	ddi_close();
		boxreturn (1, -1, DBGTR, _T("direct draw fail"));
	}



	// 2>init Primary Surface
	ZeroMemory(&ddsd, sizeof(DDSURFACEDESC2));
	ddsd.dwSize			= sizeof(DDSURFACEDESC2);
	ddsd.dwFlags		= DDSD_CAPS;
	ddsd.ddsCaps.dwCaps	= DDSCAPS_PRIMARYSURFACE;
	hr = m_ddbox->pDD->CreateSurface(&ddsd, &m_ddbox->pDDSPrimary, NULL);
	if (FAILED(hr))
	{	ddi_close();
		boxreturn (1, -1, DBGTR, _T("primary surface - direct draw fail"));
	}


	// 3>Off Screen Surface ( Window Size )
	ZeroMemory(&ddsd, sizeof(DDSURFACEDESC2));
	ddsd.dwSize		= sizeof(DDSURFACEDESC2);
	hr = m_ddbox->pDD->GetDisplayMode(&ddsd);	

	ddsd.dwSize			= sizeof(ddsd);
	ddsd.dwFlags		= DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT;
	ddsd.ddsCaps.dwCaps	= DDSCAPS_OFFSCREENPLAIN;	
	hr = m_ddbox->pDD->CreateSurface(&ddsd, &m_ddbox->pDDSOffScreen, NULL);
	if (FAILED(hr))
	{	ddi_close();
		boxreturn (1, -1, DBGTR, _T("offscreen surface - direct draw fail"));
	}	

	

	// 4> clipper
	hr = m_ddbox->pDD->CreateClipper(0, &m_ddbox->pDDClipper, NULL);
	if(SUCCEEDED(hr))
	{	hr = m_ddbox->pDDClipper->SetHWnd(0, in_pwnd->m_hWnd);
		if(SUCCEEDED(hr))
		hr = m_ddbox->pDDSPrimary->SetClipper(m_ddbox->pDDClipper);
	}	


	if (IN_nownsync)
	{	own_sync_sta();
	}

	m_pwnd	= in_pwnd;	
	return 1;
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::ddi_close()
{

	int  ni;

	own_sync_end();


	for (ni = 0; ni < MAX_DDIF_CH; ni++)
	{	chsraw_close(ni);
		chamem_close(ni);
	}

	if (m_ddbox->pDDClipper)
	{	m_ddbox->pDDClipper->Release();
		m_ddbox->pDDClipper = NULL;
	}

	if (m_ddbox->tddsquad.psquad)
	{	m_ddbox->tddsquad.psquad->Release();
		m_ddbox->tddsquad.psquad = NULL;
	}
	
	if (m_ddbox->pDDSOffScreen)
	{	m_ddbox->pDDSOffScreen->Release();
		m_ddbox->pDDSOffScreen = NULL;
	}

	if (m_ddbox->pDDSPrimary)
	{	m_ddbox->pDDSPrimary->Release();
		m_ddbox->pDDSPrimary = NULL;
	}

	if (m_ddbox->pDD)
	{	m_ddbox->pDD->Release();
		m_ddbox->pDD = NULL;
	}

	m_pwnd = NULL;
}

































// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief 
//
//
//!	@		
//
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::own_sync_init(void)
{
	m_pown_sync_evnt	= NULL;
	m_pown_sync_hd		= NULL;
	m_nown_sync_id		= 0;
	m_nown_sync_act		= 0;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::own_sync_sta()
{
	DWORD	dwID	= 0;
	m_pown_sync_hd	= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)own_sync_thrd, this, 0, (LPDWORD)&dwID);
	if (m_pown_sync_hd  == NULL)
	{	boxlog    (1, DBGTR, _T("make own sync thread fail !!"));
		boxlog    (1, DBGTR, _T("make own sync thread fail !!"));
		return -1;
	}

	m_pown_sync_evnt	= ::CreateEvent(NULL, FALSE, FALSE, NULL);
	if (m_pown_sync_evnt == NULL)
	{	boxlog    (1, DBGTR, _T("make own sync event fail !!"));
		boxlog    (1, DBGTR, _T("make own sync event fail !!"));
		return -1;
	}
	m_nown_sync_id		= dwID;
	m_nown_sync_act		= 1;

	return 1;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void		ddif::own_sync_end()
{
	if (m_pown_sync_hd == NULL) return;

	m_nown_sync_act = 0;
	SetEvent(m_pown_sync_evnt);
	WaitForSingleObject(m_pown_sync_hd, INFINITE);

	CloseHandle(m_pown_sync_evnt);


	m_pown_sync_evnt	= NULL;	
	m_pown_sync_hd		= NULL;
	m_nown_sync_id		= 0;
}
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	WINAPI	ddif::own_sync_thrd(LPVOID in_lpvparam)
{
	ddif	*pcthis = static_cast <ddif *>(in_lpvparam);
	
	boxlog(1, DBGTR, _T("[Direct Draw][own sync thread start") );

	while(pcthis->m_nown_sync_act)
	{	
		//del:nomean> 1thread always run..
		//if (pcthis->m_ddbox->pDD->WaitForVerticalBlank(DDWAITVB_BLOCKBEGIN, 0) ==DD_OK)
		Sleep(27);
		//Sleep(32);
		{	if (pcthis->m_nown_sync_act == 0) break;
			pcthis->chsraw_squadscrn_render ();
		}
	}

	boxlog(1, DBGTR, _T("[Direct Draw][own sync thread end"));

	ExitThread(1);
	return 1;
}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::own_sync_ext()
{
	if (m_pown_sync_hd	== NULL) return;
	if (m_pown_sync_evnt== NULL) return;

	SetEvent(m_pown_sync_evnt);
	
}

























// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::chamem_open(DWORD IN_nch, DWORD IN_nxz, DWORD IN_nyz, DWORD IN_nddiftype)
{	
	auto_mtx	mtx(m_chamem_mtx.m_hmtx);

	boxreturn((IN_nch >MAX_DDIF_CH), -1, DBGTR, _T("max ch too big : %d"), IN_nch); 	
	
	DWORD	dwxbc   = (IN_nxz * g_ddif_info[IN_nddiftype].dwxz_mul);
	int 	nmemsz	= (IN_nxz * IN_nyz * g_ddif_info[IN_nddiftype].dwbitcnt) / 8;

	
	
	m_ddbox->amem[IN_nch].tmem.t_new(nmemsz + 4095, 256);	//b.of> 4095 : page.. ^^; no mean


	m_ddbox->amem[IN_nch].nxz	= IN_nxz;
	m_ddbox->amem[IN_nch].nxbc	= dwxbc;
	m_ddbox->amem[IN_nch].nyz	= IN_nyz;	
	m_ddbox->amem[IN_nch].nsz	= nmemsz;	
	m_ddbox->amem[IN_nch].ntype = IN_nddiftype;
	m_ddbox->amem[IN_nch].nupdt	= 0;

	SET_RTSZ(m_ddbox->amem[IN_nch].rtsz,	0, 0, 0, 0);
	SET_RECT(m_ddbox->amem[IN_nch].rect,	0, 0, 0, 0);
		
	boxreturn( (m_ddbox->amem[IN_nch].tmem.m_align_ptr == NULL), -1, DBGTR, _T("align mem alloc fail()"));
	return 1;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::chamem_close(DWORD IN_nch)
{
	auto_mtx	mtx(m_chamem_mtx.m_hmtx);


	if (m_ddbox->amem[IN_nch].tmem.m_align_ptr == NULL) return;

	m_ddbox->amem[IN_nch].nxz	= 0;
	m_ddbox->amem[IN_nch].nxbc	= 0;
	m_ddbox->amem[IN_nch].nyz	= 0;	
	m_ddbox->amem[IN_nch].nsz	= 0;	
	m_ddbox->amem[IN_nch].ntype = 0;
	m_ddbox->amem[IN_nch].nupdt	= 0;
	
	SET_RTSZ(m_ddbox->amem[IN_nch].rtsz,	0, 0, 0, 0);
	SET_RECT(m_ddbox->amem[IN_nch].rect,	0, 0, 0, 0);

	m_ddbox->amem[IN_nch].tmem.t_del();	
	

}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// no inline
int		ddif::chamem_isalive(DWORD IN_nch)
{
	return ( (m_ddbox->amem[IN_nch].tmem.m_align_ptr)? 1 : 0);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
LPBYTE	ddif::chamem_get_buf(DWORD IN_nch)
{
	boxreturn((IN_nch  > MAX_DDIF_CH),	 NULL, DBGTR, _T("max ch too big : %d"), IN_nch); 
	boxreturn((m_ddbox->amem[IN_nch].tmem.m_align_ptr == NULL), NULL, DBGTR, _T("off scrren null"));	
		
	//auto_mtx 
	if (m_chamem_mtx.m_hmtx)
		WaitForSingleObject(m_chamem_mtx.m_hmtx, INFINITE);

	return ( (LPBYTE)m_ddbox->amem[IN_nch].tmem.m_align_ptr);
}

void	ddif::chamem_put_buf(DWORD IN_nch)
{
	if (m_chamem_mtx.m_hmtx)	
		ReleaseMutex(m_chamem_mtx.m_hmtx);
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::chamem_updt_img(DWORD IN_nch)
{	
	m_ddbox->amem[IN_nch].nupdt |= UPT_AMEM_IMG;	
}

/*
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::chamem_copy_img(DWORD IN_nch, LPBYTE in_bpimg)
{
	auto_mtx	mtx(m_chamem_mtx.m_hmtx);

	boxreturn((IN_nch  > MAX_DDIF_CH),			 -1, DBGTR, _T("max ch too big : %d"), IN_nch); 
	boxreturn((m_ddbox->amem[IN_nch].tmem.m_align_ptr == NULL), -1, DBGTR, _T("ch amem  null"));	
	boxreturn((in_bpimg			                      == NULL), -1, DBGTR, _T("in_bpimg null"));	

	RECT			cvt_rect;
	DWORD			nch, cvt_yz,  nbltcnt;
	LPBYTE			bpsrc, bpdst;

	bpdst = (LPBYTE)(m_ddbox->amem[IN_nch].tmem.m_align_ptr);
	bpsrc = (LPBYTE)(in_bpimg);
	copyto_yscale(bpdst, m_ddbox->dtdvsn_rtsz[IN_nch].yz, m_ddbox->amem[IN_nch].nxbc, 			
				  bpsrc, m_ddbox->amem[IN_nch].nyz,		  m_ddbox->amem[IN_nch].nxbc,  &cvt_yz);
	

	//CopyMemory(m_ddbox->amem[IN_nch].tmem.m_align_ptr, in_bpimg, m_ddbox->amem[IN_nch].nsz/4);




	m_ddbox->amem[IN_nch].nupdt |= UPT_AMEM_IMG;
	return 1;
}
*/



























// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::chsraw_open(DWORD IN_nch, DWORD IN_nxz, DWORD IN_nyz, DWORD IN_nddiftype)
{
	boxreturn((IN_nch >MAX_DDIF_CH), -1, DBGTR, _T("max ch too big : %d"), IN_nch); 	


	DWORD		dwxbc	= 0;
	DWORD		dwsiz	= 0;
	HRESULT		hr		= S_OK;
	DDSURFACEDESC2		ddsd;

	// Raw Data Surface ( image Size )
	ZeroMemory(&ddsd, sizeof(DDSURFACEDESC2));
	ddsd.dwSize							= sizeof(ddsd);
	ddsd.dwFlags						= DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT|DDSD_PIXELFORMAT;
	ddsd.ddsCaps.dwCaps					= DDSCAPS_OFFSCREENPLAIN;
	ddsd.dwWidth						= (DWORD)IN_nxz;
	ddsd.dwHeight						= (DWORD)IN_nyz;	
	ddsd.ddpfPixelFormat.dwSize			= sizeof(DDPIXELFORMAT);	
	ddsd.ddpfPixelFormat.dwFlags		= g_ddif_info[IN_nddiftype].dwflags;
	
	dwxbc	= (IN_nxz * g_ddif_info[IN_nddiftype].dwxz_mul);
	dwsiz	= (IN_nxz * IN_nyz * g_ddif_info[IN_nddiftype].dwbitcnt) / 8;

	if (IN_nddiftype == DDIF_RGBA)
	{	ddsd.ddpfPixelFormat.dwRGBBitCount	= 32;			
		ddsd.ddpfPixelFormat.dwRBitMask		= 0x00ff0000;
		ddsd.ddpfPixelFormat.dwGBitMask		= 0x0000ff00;
		ddsd.ddpfPixelFormat.dwBBitMask		= 0x000000ff;			

	} else 
	{	ddsd.ddpfPixelFormat.dwYUVBitCount	= g_ddif_info[IN_nddiftype].dwbitcnt;
		ddsd.ddpfPixelFormat.dwFourCC		= g_ddif_info[IN_nddiftype].dwfourcc;		
	}


	hr = m_ddbox->pDD->CreateSurface(&ddsd, &(m_ddbox->sraw[IN_nch].psraw), NULL);
	if (FAILED(hr))
	{	boxreturn (1, -1, DBGTR, _T("%dch fourcc offscreen surface - fail"), IN_nch);
	}	

	m_ddbox->sraw[IN_nch].nxz	= IN_nxz;
	m_ddbox->sraw[IN_nch].nxbc	= dwxbc;
	m_ddbox->sraw[IN_nch].nyz	= IN_nyz;	
	m_ddbox->sraw[IN_nch].nsz	= dwsiz;
	m_ddbox->sraw[IN_nch].ntype	= IN_nddiftype;
	m_ddbox->sraw[IN_nch].nupdt = 0;

	SET_RTSZ(m_ddbox->sraw[IN_nch].rtsz,	0, 0, 0, 0);
	SET_RECT(m_ddbox->sraw[IN_nch].rect,	0, 0, 0, 0);

	return 1;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddif::chsraw_close(DWORD IN_nch)
{
	if (m_ddbox->sraw[IN_nch].psraw == NULL) return;
	
	m_ddbox->sraw[IN_nch].nxz	= 0;
	m_ddbox->sraw[IN_nch].nxbc	= 0;
	m_ddbox->sraw[IN_nch].nyz	= 0;	
	m_ddbox->sraw[IN_nch].nsz	= 0;	
	m_ddbox->sraw[IN_nch].ntype	= 0;
	m_ddbox->sraw[IN_nch].nupdt	= 0;

	SET_RTSZ(m_ddbox->sraw[IN_nch].rtsz,	0, 0, 0, 0);
	SET_RECT(m_ddbox->sraw[IN_nch].rect,	0, 0, 0, 0);

	m_ddbox->sraw[IN_nch].psraw->Release();
	m_ddbox->sraw[IN_nch].psraw = NULL;		
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// no inline 
int		ddif::chsraw_isalive(DWORD IN_nch)
{
	return ( (m_ddbox->sraw[IN_nch].psraw)? 1 : 0);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
LPBYTE	ddif::chsraw_get_buf(DWORD IN_nch)
{
	boxreturn ((m_ddbox->sraw[IN_nch].psraw== NULL), NULL, DBGTR, _T("off scrren null"));


	HRESULT			hr	= S_OK;
	DDSURFACEDESC2	ddsd;
	ddsd.dwSize		= sizeof(ddsd);	

	hr =  m_ddbox->sraw[IN_nch].psraw->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_WAIT, NULL);
	boxreturn ((FAILED(hr)), NULL, DBGTR, _T("off scrren lock fail"));
		
	return ( (LPBYTE)ddsd.lpSurface);
}

void	ddif::chsraw_put_buf(DWORD IN_nch)
{
	HRESULT			hr	= S_OK;
	if (m_ddbox->sraw[IN_nch].psraw)
	{	hr = m_ddbox->sraw[IN_nch].psraw->Unlock(NULL);
	}
}




















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//
//
//
//
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	ddif::squadscrn_open(DWORD IN_nmaxch, DWORD in_nddiftype)
{
	boxreturn ( (m_ddbox->pDDSPrimary == NULL), -1, DBGTR, _T("ddraw primary is null"));

	DDSURFACEDESC2	ddsd;
	HRESULT			hr;
	DWORD			dwxbc,  dwsiz;
	DWORD			nxz,    nyz;
	

	//4>Off Screen Surface for squad (1920x1080)	
	nxz		= SQUADSCRN_XZ;
	nyz		= SQUADSCRN_YZ;

	
	ZeroMemory(&ddsd, sizeof(DDSURFACEDESC2));
	ddsd.dwSize				= sizeof(ddsd);
	ddsd.dwFlags			= DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT|DDSD_PIXELFORMAT;
	ddsd.ddsCaps.dwCaps		= DDSCAPS_OFFSCREENPLAIN;
	ddsd.dwWidth			= nxz;
	ddsd.dwHeight			= nyz;	
	ddsd.ddpfPixelFormat.dwSize		= sizeof(DDPIXELFORMAT);
	ddsd.ddpfPixelFormat.dwFlags	= DDPF_FOURCC | DDPF_YUV;	

	dwxbc	= (nxz * g_ddif_info[in_nddiftype].dwxz_mul);
	dwsiz	= (nxz * nyz * g_ddif_info[in_nddiftype].dwbitcnt) / 8;

	ddsd.ddpfPixelFormat.dwYUVBitCount	= g_ddif_info[in_nddiftype].dwbitcnt;
	ddsd.ddpfPixelFormat.dwFourCC		= g_ddif_info[in_nddiftype].dwfourcc;		


	hr = m_ddbox->pDD->CreateSurface(&ddsd, &m_ddbox->tddsquad.psquad, NULL);
	if (FAILED(hr))
	{	boxreturn (1, -1, DBGTR, _T("fourcc offscreen surface - fail"));
	}


	m_ddbox->tddsquad.nxz	= nxz;
	m_ddbox->tddsquad.nyz	= nyz;
	m_ddbox->tddsquad.nxbc	= dwxbc;
	m_ddbox->tddsquad.nsz	= dwsiz;
	m_ddbox->tddsquad.ntype	= in_nddiftype;
	m_ddbox->tddsquad.nmaxch= IN_nmaxch;

	return 1;
}


// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
void	ddif::squadscrn_close()
{
	if (m_ddbox->tddsquad.psquad == NULL) return;

	m_ddbox->tddsquad.nxz	= 0;
	m_ddbox->tddsquad.nyz	= 0;
	m_ddbox->tddsquad.nyz	= 0;
	m_ddbox->tddsquad.nsz	= 0;
	m_ddbox->tddsquad.ntype	= 0;

	m_ddbox->tddsquad.psquad->Release();
	m_ddbox->tddsquad.psquad = NULL;

}



// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
void	ddif::squadscrn_ch_rect(DWORD IN_nch, RECT *in_psrcrect, RECT *in_pdstrect)
{
	if (in_psrcrect) 
	{	
		SET_RTSZ(m_ddbox->amem[IN_nch].rtsz, 0, 0, 
											 (in_psrcrect->right  - in_psrcrect->left + 1),
											 (in_psrcrect->bottom - in_psrcrect->top  + 1) );

		SET_RECT(m_ddbox->amem[IN_nch].rect, in_psrcrect->left,  in_psrcrect->top, 											
											 in_psrcrect->right, in_psrcrect->bottom);
											
	}		

	if (in_pdstrect)
	{	
		SET_RTSZ(m_ddbox->dtdvsn_rtsz[IN_nch], in_pdstrect->left,     in_pdstrect->top, 
											   (in_pdstrect->right  - in_pdstrect->left + 1),
											   (in_pdstrect->bottom - in_pdstrect->top  + 1) );
		
		SET_RECT(m_ddbox->divisn_rect[IN_nch], in_pdstrect->left,  in_pdstrect->top, 											
											   in_pdstrect->right, in_pdstrect->bottom);
	}
	
}


// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
void	ddif::squadscrn_clear()
{
	auto_mtx	mtx(m_chamem_mtx.m_hmtx);
	
	boxreturn0( ((m_ddbox->pDDSPrimary  == NULL) || 
				 (m_ddbox->tddsquad.psquad == NULL)), DBGTR, _T("clear() surface is null"));


	HRESULT				hr;
	DDSURFACEDESC2		ddsd;
	ddsd.dwSize	= sizeof(ddsd);


	hr =  m_ddbox->tddsquad.psquad->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_DONOTWAIT, NULL);
	boxreturn0( (FAILED(hr)), DBGTR, _T("squad screen lock fail"));
				
	FillMemory(ddsd.lpSurface, m_ddbox->tddsquad.nsz , 0x80);
	hr = m_ddbox->tddsquad.psquad->Unlock(NULL);
	
	
	for (int ni = 0; ni < MAX_DDIF_CH; ni++)
		m_ddbox->amem[ni].nupdt = 0;
	
}
































// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// copy to chsraw from chamem : b.of copy time : 
// 1. src size	: 960(720)x240(288),  960(720)x480(576),    1280x720,     1920x1080
//
// 2. dst size	: 1920x1080(#0),    960x540(#1),   1920x540(#2),  640x540(#4)
//				   640x360 (#5),    480x270(#6),   1440x810(#6), 

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void 	ddif::copyto_yscale
(LPBYTE io_bpdst, DWORD in_dwdstyz, DWORD in_dwdstpitch,  
 LPBYTE in_bpsrc, DWORD in_dwsrcyz, DWORD in_dwsrcpitch, DWORD  in_ndtype,
 DWORD  in_dwdstrectyz, DWORD*io_cvrtyz)
{
	DWORD	dwsrcinc, dwi, dwyed;

	LPBYTE	bpsrc	= in_bpsrc;
	LPBYTE	bpdst	= io_bpdst;
	
	//. example	
	//. 270 <= 216(1080/5)	480 <= 115(576/5)
	//. 270 <= 270(1080/4)	480 <= 144(576/4)
	//. 270 <= 360(1080/3)	480 <= 192(576/3)
    //. 270 <= 540(1080/2)	480 <= 288(576/2)

	if		(in_dwdstrectyz < (in_dwsrcyz/5) ) { dwsrcinc = (in_dwsrcpitch * 5);	dwyed = *io_cvrtyz = in_dwsrcyz / 5; } 
	else if (in_dwdstrectyz < (in_dwsrcyz/4) ) { dwsrcinc = (in_dwsrcpitch * 4);	dwyed = *io_cvrtyz = in_dwsrcyz / 4; } 
	else if (in_dwdstrectyz < (in_dwsrcyz/3) ) { dwsrcinc = (in_dwsrcpitch * 3);	dwyed = *io_cvrtyz = in_dwsrcyz / 3; } 
	else if (in_dwdstrectyz <=(in_dwsrcyz/2) ) { dwsrcinc = (in_dwsrcpitch * 2);	dwyed = *io_cvrtyz = in_dwsrcyz / 2; } 
	else								       { dwsrcinc = (in_dwsrcpitch    );	dwyed = *io_cvrtyz = in_dwsrcyz;	 }

	for (dwi = 0; dwi < dwyed; dwi++)
	{	CopyMemory(bpdst, bpsrc, in_dwsrcpitch);
		//dbg>memset(bpdst, 0x80, in_dwsrcpitch);
		bpdst += in_dwdstpitch;
		bpsrc += dwsrcinc;
	}


	if (in_ndtype == DDIF_NV12) 
	{
		LPBYTE	bpdstUV	= io_bpdst	+ (in_dwdstpitch * in_dwdstyz);
		LPBYTE	bpsrcUV	= in_bpsrc	+ (in_dwsrcpitch * in_dwsrcyz);		

		if		(in_dwdstrectyz < (in_dwsrcyz/5) ) { dwsrcinc = (in_dwsrcpitch * 5);	dwyed = in_dwsrcyz / 10; } 
		else if (in_dwdstrectyz < (in_dwsrcyz/4) ) { dwsrcinc = (in_dwsrcpitch * 4);	dwyed = in_dwsrcyz / 8;  } 
		else if (in_dwdstrectyz < (in_dwsrcyz/3) ) { dwsrcinc = (in_dwsrcpitch * 3);	dwyed = in_dwsrcyz / 6;  } 
		else if (in_dwdstrectyz <=(in_dwsrcyz/2) ) { dwsrcinc = (in_dwsrcpitch * 2);	dwyed = in_dwsrcyz / 4;  } 
		else								       { dwsrcinc = (in_dwsrcpitch    );	dwyed = in_dwsrcyz / 2;	 }

		for (dwi = 0; dwi < dwyed; dwi++)
		{	CopyMemory(bpdstUV, bpsrcUV, in_dwsrcpitch);	
			//dbg>	memset(bpdstUV, 0x80, in_dwsrcpitch);

			bpdstUV += in_dwdstpitch;			
			bpsrcUV += dwsrcinc;			
		}
	}
	else if (in_ndtype == DDIF_I420) 
	{		
		LPBYTE	bpdstU	= io_bpdst	+ (in_dwdstpitch * in_dwdstyz);
		LPBYTE	bpdstV	= bpdstU	+ (in_dwdstpitch * in_dwdstyz / 4);

		LPBYTE	bpsrcU	= in_bpsrc	+ (in_dwsrcpitch * in_dwsrcyz);
		LPBYTE	bpsrcV	= bpsrcU	+ (in_dwsrcpitch * in_dwsrcyz / 4);

		DWORD 	dwdstUVpitch = in_dwdstpitch / 2;
		DWORD	dwsrcUVpitch = in_dwsrcpitch / 2;

		if		(in_dwdstrectyz < (in_dwsrcyz/5) ) { dwsrcinc = (dwsrcUVpitch * 5);	dwyed = in_dwsrcyz / 10; } 
		else if (in_dwdstrectyz < (in_dwsrcyz/4) ) { dwsrcinc = (dwsrcUVpitch * 4);	dwyed = in_dwsrcyz / 8;  } 
		else if (in_dwdstrectyz < (in_dwsrcyz/3) ) { dwsrcinc = (dwsrcUVpitch * 3);	dwyed = in_dwsrcyz / 6;  } 
		else if (in_dwdstrectyz <=(in_dwsrcyz/2) ) { dwsrcinc = (dwsrcUVpitch * 2);	dwyed = in_dwsrcyz / 4;  } 
		else								       { dwsrcinc = (dwsrcUVpitch    );	dwyed = in_dwsrcyz / 2;	 }

		for (dwi = 0; dwi < dwyed; dwi++)
		{		
			CopyMemory(bpdstU, bpsrcU, dwsrcUVpitch);
			CopyMemory(bpdstV, bpsrcV, dwsrcUVpitch);		

			bpdstU += dwdstUVpitch;
			bpdstV += dwdstUVpitch;
			bpsrcU += dwsrcinc;
			bpsrcV += dwsrcinc;
		}
	}

}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::chsraw_squadscrn_render()
{	
	auto_mtx	mtx(m_chamem_mtx.m_hmtx);

	boxreturn ( (m_ddbox->tddsquad.psquad == NULL), -1, DBGTR, _T("squad screen is null"));

	
	
	HRESULT			hr;
	DDSURFACEDESC2	ddsd;	
	RECT			cvt_rect, dst_rect;
	DWORD			nch, cvt_yz,  nbltcnt;
	LPBYTE			bpsrcYC;
	LPBYTE			bpdstYC;
		
		
	nbltcnt  = 0;
	for (nch = 0; nch < m_ddbox->tddsquad.nmaxch; nch++)
	{	if (m_ddbox->amem[nch].nupdt    != UPT_AMEM_IMG) continue;		// image not ready
		m_ddbox->amem[nch].nupdt = 0;		

		if (m_ddbox->amem[nch].tmem.m_align_ptr	== NULL) continue;
		if (m_ddbox->sraw[nch].psraw			== NULL) continue;
		
				
		ddsd.dwSize		= sizeof(ddsd);
		hr =  m_ddbox->sraw[nch].psraw->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_DONOTWAIT, NULL);
		if (FAILED(hr)) continue;				

		bpdstYC = (LPBYTE)  (ddsd.lpSurface);
		bpsrcYC = (LPBYTE)  (m_ddbox->amem[nch].tmem.m_align_ptr);		

		copyto_yscale(bpdstYC, m_ddbox->sraw[nch].nyz,  ddsd.lPitch,    
					  bpsrcYC, m_ddbox->amem[nch].nyz,  m_ddbox->amem[nch].nxbc, 
					  m_ddbox->amem[nch].ntype, m_ddbox->dtdvsn_rtsz[nch].yz, &cvt_yz);

		m_ddbox->sraw[nch].psraw->Unlock(NULL);


		//b.of>rect is +1 blt option flag : view msdn : DirectDrawSurface::Blt (Compact 2013)
		SET_RECT( cvt_rect, 0, 0,  (DWORD)m_ddbox->sraw[nch].nxz, cvt_yz);					
		CPY_RECT(&dst_rect, &m_ddbox->divisn_rect[nch]);
		dst_rect.right  += 1;
		dst_rect.bottom += 1;
		hr = m_ddbox->tddsquad.psquad->Blt(&dst_rect, m_ddbox->sraw[nch].psraw, &cvt_rect, DDBLT_DONOTWAIT, NULL);

		nbltcnt++;
	}


	if (nbltcnt == 0) return 1;

	  
    DDBLTFX		ddbltfx;
	ddbltfx.dwSize	= sizeof(ddbltfx);
	ddbltfx.dwDDFX	= DDBLTFX_NOTEARING;

	RECT		rclient;
	RECT		squadcrn_rect = { 0, 0, SQUADSCRN_XZ, SQUADSCRN_YZ};

	m_pwnd->GetClientRect (&rclient);
	m_pwnd->ClientToScreen(&rclient);

	hr = m_ddbox->pDDSPrimary->Blt(&rclient, m_ddbox->tddsquad.psquad, &squadcrn_rect, DDBLT_DONOTWAIT, NULL);// DDBLT_DDFX, &ddbltfx);


	return 1;
}
















// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddif::ch_render(DWORD IN_nch, LPBYTE IN_bpsrcimg, RECT *in_psrc_rect)
{	
	boxreturn (((m_ddbox->pDDSOffScreen == NULL) || 
				(m_ddbox->sraw[IN_nch].psraw == NULL)), -1, DBGTR, _T("off screen is null"));
	boxreturn ( ((m_pwnd == NULL) || (m_pwnd->GetSafeHwnd() == NULL)), -1, DBGTR, _T("wnd is null"));
	

	DDSURFACEDESC2	ddsd;
	HRESULT			hr		= S_OK;

	DWORD			nXBC	= m_ddbox->sraw[IN_nch].nxbc;
	DWORD			nXZ		= m_ddbox->sraw[IN_nch].nxz ;
	DWORD			nYZ		= m_ddbox->sraw[IN_nch].nyz ;
	DWORD			nDtype	= m_ddbox->sraw[IN_nch].ntype;

	ddsd.dwSize	= sizeof(ddsd);
	
	//del>hr =  m_ddbox->pDDSRaw[IN_nch]->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_DONOTWAIT, NULL);
	//del>
	hr =  m_ddbox->sraw[IN_nch].psraw->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_WAIT, NULL);
	boxreturn ((FAILED(hr)), -1, DBGTR, _T("off scrren lock fail"));
		
	DWORD	ni;
	LPBYTE	pRawSurface = (LPBYTE)ddsd.lpSurface;
	LPBYTE	pSrcImg		= (LPBYTE)IN_bpsrcimg;
	
	for (ni = 0; ni < nYZ; ni++)
	{	//reverse>memcpy(pRawSurface, IN_bpYC422 + (dwYZ - n - 1) * dwXZ, dwXZ);
		memcpy(pRawSurface, pSrcImg, nXBC);			
		pSrcImg	   	+= nXBC;
		pRawSurface += ddsd.lPitch;
	}


	if (nDtype == DDIF_NV12)
	{
		LPBYTE	pRawUV	= (LPBYTE)ddsd.lpSurface + (ddsd.lPitch *nYZ);
		LPBYTE	pSrcUV	= (LPBYTE)IN_bpsrcimg    + (nXBC * nYZ);
		
		for (ni = 0; ni < nYZ/2; ni++)
		{				
			memcpy(pRawUV, pSrcUV, nXBC );	
						
			pSrcUV	   	+= nXBC;
			pRawUV		+= ddsd.lPitch;
		}
	}	
	else if (nDtype == DDIF_I420)
	{
		LPBYTE	pRawU	= (LPBYTE)ddsd.lpSurface + (ddsd.lPitch *nYZ);
		LPBYTE	pRawV	= pRawU					 + (ddsd.lPitch *nYZ / 4);

		LPBYTE	pSrcU	= (LPBYTE)IN_bpsrcimg    + (nXBC * nYZ);
		LPBYTE	pSrcV	= pSrcU				     + (nXBC * nYZ / 4);

		for (ni = 0; ni < nYZ/2; ni++)
		{				
			memcpy(pRawU, pSrcU, nXBC / 2);	
			memcpy(pRawV, pSrcV, nXBC / 2);			

			pSrcU	   	+= (nXBC/2);
			pSrcV	   	+= (nXBC/2);

			pRawU		+= (ddsd.lPitch / 2);
			pRawV		+= (ddsd.lPitch / 2);
		}
	}
	

	hr = m_ddbox->sraw[IN_nch].psraw->Unlock(NULL);

	

	CRect	rcclient;
	CRect	rcwindow;
	CRect	rcimage (0, 0, nXZ, nYZ);

	if (in_psrc_rect)
	{	rcimage = *in_psrc_rect;
	}


	m_pwnd->GetClientRect (&rcclient);
	m_pwnd->ClientToScreen(&rcclient);
	//del>m_pwnd->GetWindowRect(&rcwindow);
	//del>m_pwnd->ScreenToClient(&rcwindow);


	//del>hr = m_ddbox->pDDSOffScreen->Blt(&rcimage,  m_ddbox->pDDSRaw[IN_nch], &rcimage,  DDBLT_WAIT, NULL);
	//del>hr = m_ddbox->pDDSPrimary->Blt  (&rcwindow, m_ddbox->pDDSOffScreen,		  &rcclient, DDBLT_WAIT, NULL);

	
	hr = m_ddbox->pDDSPrimary->Blt  (&rcclient, m_ddbox->sraw[IN_nch].psraw, &rcimage, DDBLT_DONOTWAIT, NULL);
	//ori>hr = m_ddbox->pDDSPrimary->Blt  (&rcwindow, m_ddbox->pDDSRaw[IN_nch], &rcimage, DDBLT_WAIT, NULL);


	if (hr == DDERR_SURFACELOST)
	{
		m_ddbox->pDDSPrimary->Restore();
		m_ddbox->pDDSOffScreen->Restore();
		m_ddbox->sraw[IN_nch].psraw->Restore();
	}


	return 1;

}





















