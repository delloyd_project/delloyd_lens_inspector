
/* 	filename: 	winlog
 * **************************************************************************************
 *
 * 	created  :	2012/06/04
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *  version  :	0	[2003/01/07] : reference code [ CAT3D DRV :  ]
 *				1   [2012/06/04] : START CODE -> CLASS MADE / CODE WRITING.. 
 *				2	[2012/06/10] : 32bit -> 64bit porting	
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "box_type_win.h"
#include "box_log_win.h"

#include <windows.h>
#include <stdio.H>
#include <strsafe.h>







// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#define LOG_LEVEL					"LOG_LEVEL"
#define LOG_FILE_ENABLE				"LOG_FILE_ENABLE"
#define LOG_FILE_NAME				"LOG_FILE_NAME"






// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
struct			cwinlog
{
			   ~cwinlog();
				cwinlog();
	
	stuint		m_stDBGLv;

	BOOL		m_blwrfile;
	TCHAR		m_szfilename[MAX_PATH * 2];	

	HANDLE		m_hmtx;

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:	
	void		chk_env();
	void		chk_profile();


	
};







/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * 
 * CLASS		: 
 *
 *
 *
 *
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

static	cwinlog			g_log;
#define BOX_LOG_MTX		_T("box_log_mtx")




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: registry flag check
// * Description: 
//				  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
cwinlog::~cwinlog()
{  
	if (m_hmtx)
	{	CloseHandle(m_hmtx);
		m_hmtx = NULL;
	}
}

cwinlog:: cwinlog()
{
	m_stDBGLv		= 0;
	m_blwrfile		= 0;
	ZeroMemory(m_szfilename, sizeof(m_szfilename));

	m_hmtx = NULL;
	m_hmtx = CreateMutex(NULL, FALSE, BOX_LOG_MTX);
	if (m_hmtx == NULL)
	{	m_hmtx = OpenMutex(NULL, FALSE, BOX_LOG_MTX);
	}



	chk_env();
	chk_profile();
	
	windprintf(DBGTR, _T("cat3d sdk.exe start.. log file en:%d, log fn:%s"), m_blwrfile, m_szfilename);
}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: 
// * Description: 
//				  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void		cwinlog::chk_env()
{
	m_stDBGLv	= (DBGTR | EYECHK | NO0D0A	);
}


#define		PROFILE_NAME		(_T("sdk27.ini"))

#include <Shlwapi.h>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
void		cwinlog::chk_profile()
{						
	TCHAR	szdir[MAX_PATH * 2] = { 0, };
	TCHAR	szbuf[MAX_PATH * 2] = { 0, };
	
		
	GetModuleFileName (NULL, szdir, sizeof(szdir));
	PathRemoveFileSpec (szdir);	
	SetCurrentDirectory(szdir);	
	//dbg>GetCurrentDirectory(sizeof(szdir), szbuf);	
	StringCbPrintf  (szdir, sizeof(m_szfilename), _T("%s\\%s"), szdir, PROFILE_NAME);




	int		nrv = GetPrivateProfileInt(_T("GLOBAL"),_T("LOG_FILE_EN"), 0, szdir);
	if (nrv == 0) return;


	ZeroMemory(szbuf, sizeof(szbuf));
	DWORD	drv = GetPrivateProfileString(_T("GLOBAL"), _T("LOG_FILE_NAME"), _T(""), szbuf, 256, szdir);
	if (drv < 1) return;
		
	StringCbPrintf  (m_szfilename, sizeof(m_szfilename), _T("%s"), szbuf);
	m_blwrfile	= 1;	

}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: 
// * Description: 
//				  
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void		windprintf(stuint IN_stlev, TCHAR *IN_lpcszStr, ...)
{

	//1:dbg msg level check>
	if (!  ( (IN_stlev & DMSG_HWERR) || 
			 (IN_stlev & DMSG_SWERR) || 
			 (g_log.m_stDBGLv  & IN_stlev)  )  )  return;

	if (g_log.m_hmtx) WaitForSingleObject(g_log.m_hmtx, INFINITE);


	TCHAR			lpszBuf[1024];
	TCHAR			lpszMSG[1024];
	va_list			args;	

	ZeroMemory(lpszBuf, sizeof(lpszBuf));
	ZeroMemory(lpszMSG, sizeof(lpszMSG));
	va_start		 (args, IN_lpcszStr);	
	StringCbVPrintf  (lpszBuf, sizeof(lpszBuf), IN_lpcszStr, args)	;
	va_end			 (args);


	SYSTEMTIME		ct;
	//del>::GetSystemTime(&ct);
	::GetLocalTime(&ct);


	//2:msg converting>
	if (IN_stlev & DMSG_HWERR)
	{	StringCbPrintf  (lpszMSG, sizeof(lpszMSG), _T("\n[HW ERROR][%s]\r\n"), lpszBuf);

	} else if (IN_stlev & DMSG_SWERR)
	{	StringCbPrintf  (lpszMSG, sizeof(lpszMSG), _T("\n[SW ERROR][%s]\r\n"), lpszBuf);
	} else if (g_log.m_stDBGLv & IN_stlev) 
	{			
		if (IN_stlev & EYECHK)
				StringCbPrintf  (lpszMSG, sizeof(lpszMSG), _T("[EYE][%s]\r\n"), lpszBuf);
		else if (IN_stlev & DBGTR)
				StringCbPrintf  (lpszMSG, sizeof(lpszMSG), _T("[MSG][%s]\r\n"), lpszBuf);
		else if (IN_stlev & NO0D0A)
				 StringCbPrintf  (lpszMSG, sizeof(lpszMSG), _T("%s"), lpszBuf);	
	} 
	//OutputDebugString(lpszMSG);

	StringCbPrintf(	lpszBuf, sizeof(lpszBuf), _T("[EXE:%02d:%02d:%02d:%03d]%s"), 
					ct.wHour, ct.wMinute, ct.wSecond, ct.wMilliseconds, lpszMSG);
	OutputDebugString(lpszBuf);


	//3:msg file check>
	if (g_log.m_blwrfile == FALSE)
	{	if (g_log.m_hmtx) ReleaseMutex(g_log.m_hmtx);
		return;
	}

		
	
	
	FILE			*fp;
	int  			nStrSz;


	_tfopen_s( &fp, g_log.m_szfilename, _T("rb+") );
	if (fp == NULL)
	{	_tfopen_s( &fp, g_log.m_szfilename,_T("wb+"));
	}
	if (fp == NULL) return;

	fseek(fp, 0, SEEK_END);	

	::GetSystemTime(&ct);
	StringCbPrintf    (lpszMSG, sizeof(lpszMSG), _T("[%02d:%02d]%s"),
						ct.wMonth,  ct.wDay, lpszBuf);
	
	nStrSz = (int) _tcslen(lpszMSG);
	fwrite(lpszMSG, nStrSz, 1, fp);

	fclose(fp);

	if (g_log.m_hmtx) ReleaseMutex(g_log.m_hmtx);
}


















