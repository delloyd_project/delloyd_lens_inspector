﻿// *****************************************************************************
// Filename	: 	DigitalIOCtrl.h
// Created	:	2017/02/05 - 15:59
// Modified	:	2017/02/05 - 15:59
// 
// Author	:	KHO
// 	
// Purpose	:	
// *****************************************************************************
// #ifndef DigitalIOCtrl_h__
// #define DigitalIOCtrl_h__
// 
// #pragma once
// 
// #include "AXD.h"
// #include "AXL.h"
// #include "Def_DigitiIO.h"
// #include <TimeAPI.h>
// 
// class CDigitalIOCtrl
// {
// public:
// 	CDigitalIOCtrl();
// 	~CDigitalIOCtrl();
// 
// 	void DoEvents();
// 	void DoEvents(DWORD dwMiliSeconds);
// 
// 	int		ThreadFunction		();
// 	BOOL	DestroyThread		();
// 
// 	// Lib 초기화 및 IN & OUT SETTING
// 	BOOL	AXTInit				();
// 	BOOL	AXTState			();
// 	void	GetInputAssign		();
// 	void	GetOutputAssign		();
// 
// 	UINT	GetInputPortCnt		();
// 	UINT	GetOutputPortCnt	();
// 
// 	BOOL	StartIOMonitoring	();
// 
// 	BOOL	Get_DI_Status		(__in  long   lOffset);
// 
// 	BOOL	GetInStatus			(UINT nInPort);
// 	BOOL	GetOutStatus		(UINT nOutPort);
// 
// 	BOOL	SetOutPort			(__in long nOutPort,  __in enum_IO_SignalType SignalType);
// 	BOOL	SetOutPortStatus	(__in long lModuleNo, __in long lOffset, __in enum_IO_SignalType SignalType = IO_SignalT_SetOn);
// 
// private:
// 
// 	BOOL	m_bRun;
// 	HANDLE	m_hThreadIO;
// 	static UINT WINAPI Thread_Fun(LPVOID lParam);
// 
// 	// 베이스 보드 및 모듈 처리에 사용하는 구조체
// 	ST_AxdBoardInfo m_AxdBoardInfo;
// 
// 	// 1 -> 0 으로 bit 처리하는데 필요한 지연시간 (ms)
// 	long	m_lOutPulseDelay;
// 
// 	// 토글 횟수, -1의 경우 무한 토글
// 	long	m_lToggleCount;
// 
// 	// 토글 기능 On Time 시간 간격 (1 ~ 30000 ms)
// 	long	m_lToggleOnTimeDelay;
// 
// 	// 토글 기능 Off Time 시간 간격 (1 ~ 30000 ms)
// 	long	m_lToggleOffTimeDelay;
// 
// 	int	 *m_iInputModuleNo;
// 	BOOL **m_ModuleInPort;
// 	int	 m_InputModuleCnt;
// 	long m_TotalInputPortCnt;
// 	BOOL *m_InportStatus;
// 
// 	int	 *m_iOutputModuleNo;
// 	BOOL **m_ModuleOutPort;
// 	int	 m_OutputModuleCnt;
// 	long m_TotalOutputPortCnt;
// 	BOOL *m_OutportStatus;
// };
// 
// #endif // DigitalIOCtrl_h__
