﻿// *****************************************************************************
// Filename	: 	DigitalIOCtrl.cpp
// Created	:	2017/02/05 - 15:59
// Modified	:	2017/02/05 - 15:59
// 
// Author	:	KHO
// 	
// Purpose	:	
// *****************************************************************************
// #include "stdafx.h"
// #include "DigitalIOCtrl.h"
// 
// #if defined(_WIN64)
// #pragma	comment(lib,"..\\Lib\\64bit\\AXL_x64.lib")
// #else
// #pragma	comment(lib,"..\\Lib\\32bit\\AXL.lib")
// #endif
// 
// CDigitalIOCtrl::CDigitalIOCtrl()
// {
// 	m_hThreadIO = NULL;
// 	m_bRun = FALSE;
// 
// 	m_iInputModuleNo = NULL;
// 	m_ModuleInPort = NULL;
// 	m_InputModuleCnt = 0;
// 	m_TotalInputPortCnt = 0;
// 	m_InportStatus = NULL;
// 
// 	m_iOutputModuleNo = NULL;
// 	m_ModuleOutPort = NULL;
// 	m_OutputModuleCnt = 0;
// 	m_TotalOutputPortCnt = 0;
// 	m_OutportStatus = NULL;
// 
// 	m_lOutPulseDelay = 1500;
// 	m_lToggleOnTimeDelay = 1000;
// 	m_lToggleOffTimeDelay = 1000;
// 	m_lToggleCount = -1;
// }
// 
// 
// CDigitalIOCtrl::~CDigitalIOCtrl()
// {
// 	m_bRun = FALSE;
// 	DestroyThread();
// 
// 	delete[]m_iInputModuleNo;
// 	delete[]m_InportStatus;
// 
// 	for (int i = 0; i < m_InputModuleCnt; i++)
// 	{
// 		delete[]m_ModuleInPort[i];
// 	}
// 
// 	delete[]m_ModuleInPort;
// 
// 
// 	delete[]m_iOutputModuleNo;
// 	delete[]m_OutportStatus;
// 
// 	for (int i = 0; i < m_OutputModuleCnt; i++)
// 	{
// 		delete[]m_ModuleOutPort[i];
// 	}
// 
// 	delete[]m_ModuleOutPort;
// }
// 
// 
// =============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
// =============================================================================
// void CDigitalIOCtrl::DoEvents()
// {
// 	MSG msg;
// 
// 	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 	{
// 		if (!AfxGetApp()->PreTranslateMessage(&msg))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 		}
// 	}
// 
// 	::Sleep(0);
// }
// 
// =============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Parameter	: DWORD dwMiliSeconds
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
// =============================================================================
// void CDigitalIOCtrl::DoEvents(DWORD dwMiliSeconds)
// {
// 	timeBeginPeriod(1);
// 
// 	clock_t start_tm = clock();
// 	do
// 	{
// 		DoEvents();
// 		::Sleep(1);
// 	} while ((DWORD)(clock() - start_tm) < dwMiliSeconds);
// }
// 
// =============================================================================
// Method		: Thread_Fun
// Access		: private static  
// Returns		: UINT
// Parameter	: LPVOID p
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
// =============================================================================
// UINT CDigitalIOCtrl::Thread_Fun(LPVOID p)
// {
// 	CDigitalIOCtrl* pMain = (CDigitalIOCtrl*)p;
// 
// 	int iReturn = pMain->ThreadFunction();
// 
// 	TRACE(_T("- 쓰레드 종료 : CIoControl::Thread_Fun()\n"));
// 
// 	return 0;
// }
// 
// =============================================================================
// Method		: ThreadFunction
// Access		: public  
// Returns		: int
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
// =============================================================================
// int CDigitalIOCtrl::ThreadFunction()
// {
// 	DWORD dwInPortStatus, dwOutPortStatus;
// 	int iInOffset = 0, iOutOffset = 0;
// 
// 	while (m_bRun)
// 	{
// 		dwInPortStatus = 0, dwOutPortStatus = 0;
// 		iInOffset = 0, iOutOffset = 0;
// 
// 		// Input
// 		for (int i = 0; i < m_InputModuleCnt; i++)
// 		{
// 			if (!m_bRun)
// 				break;
// 
// 			int PortCnt = _msize(m_ModuleInPort[i]) / sizeof(*m_ModuleInPort[i]);
// 			
// 			for (int j = 0; j < PortCnt; j++)
// 			{
// 				if (!m_bRun)
// 					break;
// 
// 				AxdiReadInportBit(m_iInputModuleNo[i], j, &dwInPortStatus);
// 
// 				if (dwInPortStatus == 0x01)
// 				{
// 					m_ModuleInPort[i][j] = TRUE;
// 					m_InportStatus[(/*i **/ iInOffset) + j] = TRUE;
// 				}
// 				else
// 				{
// 					m_ModuleInPort[i][j] = FALSE;
// 					m_InportStatus[(/*i **/ iInOffset) + j] = FALSE;
// 				}
// 			}
// 
// 			iInOffset += PortCnt;
// 		}
// 
// 		// Output
// 		for (int i = 0; i < m_OutputModuleCnt; i++)
// 		{
// 			if (!m_bRun)
// 				break;
// 
// 			int PortCnt = _msize(m_ModuleOutPort[i]) / sizeof(*m_ModuleOutPort[i]);
// 
// 			for (int j = 0; j < PortCnt; j++)
// 			{
// 				if (!m_bRun)
// 					break;
// 
// 				AxdoReadOutportBit(m_iOutputModuleNo[i], j, &dwOutPortStatus);
// 
// 				if (dwOutPortStatus == 0x01)
// 				{
// 					m_ModuleOutPort[i][j] = TRUE;
// 					m_OutportStatus[(/*i * */iOutOffset) + j] = TRUE;
// 				}
// 				else
// 				{
// 					m_ModuleOutPort[i][j] = FALSE;
// 					m_OutportStatus[(/*i * */iOutOffset) + j] = FALSE;
// 				}
// 			}
// 
// 			iOutOffset += PortCnt;
// 		}
// 
// 		Sleep(10);
// 
// 	}
// 
// 	return 0;
// }
// 
// 
// =============================================================================
// Method		: DestroyThread
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::DestroyThread()
// {
// 	m_bRun = FALSE;
// 
// 	DWORD dwExitCode = NULL;
// 	GetExitCodeThread(m_hThreadIO, &dwExitCode);
// 
// 	if (STILL_ACTIVE == dwExitCode)
// 	{
// 		WaitForSingleObject(m_hThreadIO, 500);
// 		GetExitCodeThread(m_hThreadIO, &dwExitCode);
// 
// 		if (STILL_ACTIVE == dwExitCode)
// 		{
// 			TerminateThread(m_hThreadIO, dwExitCode);
// 			WaitForSingleObject(m_hThreadIO, WAIT_ABANDONED);
// 		}
// 	}
// 
// 	CloseHandle(m_hThreadIO);
// 	m_hThreadIO = NULL;
// 	return TRUE;
// }
// 
// =============================================================================
// Method		: StartIOMonitoring
// Access		: public  
// Returns		: BOOL
// Qualifier	: //Input Check Thread Start
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::StartIOMonitoring()				//Input Check Thread Start
// {
// 	m_bRun		= TRUE;
// 
// 	if (NULL != m_hThreadIO)
// 	{
// 		CloseHandle(m_hThreadIO);
// 		m_hThreadIO = NULL;
// 	}
// 
//  	m_hThreadIO = HANDLE(_beginthreadex(NULL, 0, Thread_Fun, this, 0, NULL));
// 	return TRUE;
// }
// 
// =============================================================================
// Method		: AXTInit
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/5 - 15:21
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::AXTInit()
// {
// 	//AxdInfoIsDIOModule	: AxdInfoIsDIOModule 함수는 실제 DIO 모듈이 존재하는가 여부를 확인하는 함수이다. DIO 관련 함수를 사용하기 전에 DIO 모듈이 있는지를 먼저 확인하기 위해 사용한다.
// 	//AxdInfoGetModuleCount : AxdInfoGetModuleCount 함수는 전체 시스템에 장착되어 있는 DIO 모듈의 개수를 확인하는 함수이다.
// 	//AxdInfoGetInputCount	: AxdInfoGetInputCount함수는 지정한 DIO 모듈에서 입력채널의 개수를 확인하는 함수 이다.
// 	//AxdInfoGetOutputCount : AxdInfoGetOutputCount 함수는 지정한 DIO 모듈에서 출력채널의 개수를 확인하는 함수이다.
// 	//AxdInfoGetModuleNo	: 초기화 되어 있는 전체 DIO 모듈 번호를 확인한다
// 
// 	// Initialize library 
// 	// 7은 IRQ를 뜻한다. PCI에서는 자동으로 IRQ가 설정된다.
// 	DWORD Code = AxlOpenNoReset(AXL_DEFAULT_IRQNO);
// 	if (Code == AXT_RT_SUCCESS)
// 	{
// 		TRACE(_T("AXL Library is initialized .\n"));
// 
// 		// Inspect if DIO module exsits
// 		Code = AxdInfoIsDIOModule(&m_AxdBoardInfo.dwStatus);
// 		if (m_AxdBoardInfo.dwStatus == STATUS_EXIST)
// 		{
// 			TRACE(_T("DIO module exists.\n"));
// 
// 			// DIO 모듈의 개수를 확인
// 			Code = AxdInfoGetModuleCount(&m_AxdBoardInfo.lModuleCounts);
// 
// 			if (Code == AXT_RT_SUCCESS)
// 			{
// 				TRACE(_T("Number of DIO module: %d\n"), m_AxdBoardInfo.lModuleCounts);
// 
// 				//INPUT IO Check
// 				GetInputAssign();
// 
// 				//OUTPUT IO Check
// 				GetOutputAssign();
// 			}
// 			else
// 			{
// 				TRACE(_T("AxdInfoGetModuleCount() : ERROR code Ox%x\n"), Code);
// 			}
// 		}
// 		else
// 		{
// 			TRACE(_T("Module not exist.\n"));
// 			return FALSE;
// 		}
// 	}
// 	else
// 	{
// 		TRACE(_T("Failed initialization.\n"));
// 		return FALSE;
// 	}
// 
// 	StartIOMonitoring();
// 	m_AxdBoardInfo.bAxlIsOpened = TRUE;
// 
// 	return TRUE;
// }
// 
// =============================================================================
// Method		: AXTState
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/5 - 15:55
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::AXTState()
// {
// 	return	m_AxdBoardInfo.bAxlIsOpened;
// }
// 
// =============================================================================
// Method		: ModuleInputAssign
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// void CDigitalIOCtrl::GetInputAssign()
// {
// 	long lTotalModuleCnt = 0;
// 	long lnputPortCnt = 0;
// 	m_TotalInputPortCnt = 0;
// 
// 	m_InputModuleCnt = 0;
// 	
// 	// 모듈의 개수
// 	AxdInfoGetModuleCount(&lTotalModuleCnt);
// 
// 	// 1개도 없으면 리턴
// 	if (lTotalModuleCnt < 1)
// 		return;
// 
// 	// 모듈의 개수 대로 배열 할당
// 	m_ModuleInPort = new BOOL*[lTotalModuleCnt];
// 
// 	m_iInputModuleNo = new int[lTotalModuleCnt];
// 	memset(m_iInputModuleNo, 0, sizeof(int)*lTotalModuleCnt);
// 
// 	for (int i = 0; i < lTotalModuleCnt; i++)
// 	{
// 		// 해당 모듈의 Input 포트 개수
// 		AxdInfoGetInputCount(i, &lnputPortCnt);
// 
// 		if (lnputPortCnt > 0)
// 		{
// 			// Input 포트의 유무대로 배열을 할당
// 			m_ModuleInPort[m_InputModuleCnt] = new BOOL[lnputPortCnt];
// 			memset(m_ModuleInPort[m_InputModuleCnt], 0, sizeof(BOOL)*lnputPortCnt);
// 
// 			// Input 접점이 포함된 모듈의 번호를 저장
// 			m_iInputModuleNo[m_InputModuleCnt] = i;
// 
// 			// 입력 점점의 총 개수
// 			m_TotalInputPortCnt += lnputPortCnt;
// 
// 			m_InputModuleCnt++;
// 		}
// 	}
// 
// 	if (m_InputModuleCnt < 1)
// 		return;
// 
// 	// inputport의 개수 만큼 inputsignal 상태 확인 배열 할당
// 	m_InportStatus = new BOOL[m_TotalInputPortCnt];
// 	memset(m_InportStatus, 0, sizeof(BOOL)*m_TotalInputPortCnt);
// }
// 
// =============================================================================
// Method		: ModuleOutputAssign
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// void CDigitalIOCtrl::GetOutputAssign()
// {
// 	long lTotalModuleCnt = 0;
// 	long OutputPortCnt = 0;
// 	m_TotalOutputPortCnt = 0;
// 
// 	m_OutputModuleCnt = 0;
// 
// 	// 모듈의 개수
// 	AxdInfoGetModuleCount(&lTotalModuleCnt);
// 
// 	// 1개도 없으면 리턴
// 	if (lTotalModuleCnt < 1)
// 		return;
// 
// 	// 모듈의 개수 대로 배열 할당
// 	m_ModuleOutPort = new BOOL*[lTotalModuleCnt];
// 
// 	m_iOutputModuleNo = new int[lTotalModuleCnt];
// 	memset(m_iOutputModuleNo, 0, sizeof(int)*lTotalModuleCnt);
// 
// 	for (int i = 0; i < lTotalModuleCnt; i++)
// 	{
// 		// 해당 모듈의 Input 포트 개수
// 		AxdInfoGetOutputCount(i, &OutputPortCnt);
// 
// 		if (OutputPortCnt > 0)
// 		{
// 			// Input 포트의 유무대로 배열을 할당
// 			m_ModuleOutPort[m_OutputModuleCnt] = new BOOL[OutputPortCnt];
// 			memset(m_ModuleOutPort[m_OutputModuleCnt], 0, sizeof(BOOL)*OutputPortCnt);
// 
// 			// Input 접점이 포함된 모듈의 번호를 저장
// 			m_iOutputModuleNo[m_OutputModuleCnt] = i;
// 
// 			// 입력 점점의 총 개수
// 			m_TotalOutputPortCnt += OutputPortCnt;
// 
// 			m_OutputModuleCnt++;
// 		}
// 	}
// 
// 	if (m_OutputModuleCnt < 1)
// 		return;
// 
// 	// inputport의 개수 만큼 inputsignal 상태 확인 배열 할당
// 	m_OutportStatus = new BOOL[m_TotalOutputPortCnt];
// 	memset(m_OutportStatus, 0, sizeof(BOOL)*m_TotalOutputPortCnt);
// }
// 
// =============================================================================
// Method		: GetInputPortCnt
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/6/28 - 11:40
// Desc.		:
// =============================================================================
// UINT CDigitalIOCtrl::GetInputPortCnt()
// {
// 	return m_TotalInputPortCnt;
// }
// 
// 
// 
// =============================================================================
// Method		: GetOutputPortCnt
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/6/28 - 11:40
// Desc.		:
// =============================================================================
// UINT CDigitalIOCtrl::GetOutputPortCnt()
// {
// 	return m_TotalOutputPortCnt;
// }
// 
// =============================================================================
// Method		: GetInStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nInPort
// Qualifier	:
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::GetInStatus(UINT nInPort)
// {
// 	return m_InportStatus[nInPort];
// }
// 
// =============================================================================
// Method		: GetOutStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nOutPort
// Qualifier	:
// Last Update	: 2017/2/3 - 17:34
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::GetOutStatus(UINT nOutPort)
// {
// 	return m_OutportStatus[nOutPort];
// }
// 
// =============================================================================
// Method		: SetOutPort
// Access		: public  
// Returns		: BOOL
// Parameter	: __in long lOutPort
// Parameter	: __in enum_IO_SignalType SignalType
// Qualifier	:
// Last Update	: 2017/2/3 - 17:47
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::SetOutPort(__in long lOutPort, __in enum_IO_SignalType SignalType)
// {
// 	if (m_OutputModuleCnt < 1)
// 		return FALSE;
// 
// 	if (m_TotalOutputPortCnt < 1)
// 		return FALSE;
// 
// 	if (lOutPort < 0 || lOutPort > m_TotalOutputPortCnt - 1)
// 		return FALSE;
// 
// 	long lOutPortCnt = 0;
// 
// 	int ModuleIndex = 0;
// 	int ModulePort = 0;
// 	int ModuleOffset = 0;
// 
// 	for (int i = 0; i < m_OutputModuleCnt; i++)
// 	{
// 		lOutPortCnt = _msize(m_ModuleOutPort[i]) / sizeof(*m_ModuleOutPort[i]);
// 
// 		ModulePort = lOutPort - ModuleOffset;
// 		if (lOutPortCnt > ModulePort)
// 		{
// 			ModuleIndex = i;
// 			break;
// 		}
// 
// 		ModuleOffset += lOutPortCnt;
// 	}
// 
// 	SetOutPortStatus(m_iOutputModuleNo[ModuleIndex], ModulePort, SignalType);
// 
// 	return TRUE;
// }
// 
// =============================================================================
// Method		: SetOutPortStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __in long lModuleNo
// Parameter	: __in long lOffset
// Parameter	: __in enum_IO_SignalType SignalType
// Qualifier	:
// Last Update	: 2017/2/3 - 17:40
// Desc.		:
// =============================================================================
// BOOL CDigitalIOCtrl::SetOutPortStatus(__in long lModuleNo, __in long lOffset, __in enum_IO_SignalType SignalType /*= IO_SignalT_Set*/)
// {
// 	DWORD dwResult;
// 
// 	switch (SignalType)
// 	{
// 		case IO_SignalT_SetOff: // 0으로 세팅
// 			dwResult = AxdoWriteOutportBit(lModuleNo, lOffset, 0x00);
// 			break;
// 
// 		case IO_SignalT_SetOn:	// 1로 세팅
// 			dwResult = AxdoWriteOutportBit(lModuleNo, lOffset, 0x01);
// 			break;
// 
// 		case IO_SignalT_PulseOff: // 0으로 세팅 후 1로 다시 세팅
// 			dwResult = AxdoOutPulseOff(lModuleNo, lOffset, m_lOutPulseDelay);
// 			break;
// 
// 		case IO_SignalT_PulseOn: // 1로 세팅 후 0으로 다시 세팅
// 			dwResult = AxdoOutPulseOn(lModuleNo, lOffset, m_lOutPulseDelay);
// 			break;
// 
// 		case IO_SignalT_ToggleStart: // 점멸 기능 : 1 -> 0 -> 1 -> 0 ....
// 			dwResult = AxdoToggleStart(lModuleNo, lOffset, 1, m_lToggleOnTimeDelay, m_lToggleOffTimeDelay, m_lToggleCount);
// 			break;
// 
// 		case IO_SignalT_ToggleStop:
// 			dwResult = AxdoToggleStop(lModuleNo, lOffset, 0x00);
// 			break;
// 
// 		default:
// 			break;
// 	}
// 
// 	if (dwResult == AXT_RT_SUCCESS)
// 		return FALSE;
// 
// 	return TRUE;
// }
