﻿// *****************************************************************************
// Filename	: 	File_Motor
// Created	:	2016/9/23 - 13:04
// Modified	:	2016/9/23 - 13:04
// 
// Author	:	KHO
// 	
// Purpose	:	
// *****************************************************************************
// #include "stdafx.h"
// #include "File_Motor.h"
// #include <tchar.h>
// 
// #define		MotorControlAppName		_T("MotorAxis")
// #define		MotorSequenceAppName	_T("MotorSequence")
// 
// CFile_Motor::CFile_Motor()
// {
// }
// 
// CFile_Motor::~CFile_Motor()
// {
// }
// 
// =============================================================================
// Method		: LoadMotionParameterFiles
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_AllMotorData & stMotionInfo
// Qualifier	:
// Last Update	: 2016/9/23 - 13:04
// Desc.		:
// =============================================================================
// BOOL CFile_Motor::LoadMotionParamFiles(__in LPCTSTR szPath, __out ST_AllMotorData& stMotionInfo)
// {
// 	if (szPath == NULL)
// 		return FALSE;
// 
// 	CString strAxis;
// 	CString strAppName;
// 	TCHAR inBuff[MAX_PATH] = { 0, };
// 
// 	for (UINT nAxis = 0; nAxis < (UINT)stMotionInfo.MotorInfo.lMaxAxisCnt; nAxis++)
// 	{
// 		strAppName.Format(_T("%s #%d"), MotorControlAppName, nAxis);
// 
// 		strAxis.Format(_T("AXIS 0%d"), nAxis);
// 		GetPrivateProfileString(strAppName, _T("AxisName"), strAxis, inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].szAxisName = inBuff;
// 
// 		GetPrivateProfileString(strAppName, _T("AxisUse"), _T("1"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisUse = _ttoi(inBuff);
// 
// 		strAxis.Format(_T("%d"), nAxis);
// 		GetPrivateProfileString(strAppName, _T("AxisNum"), strAxis, inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisNum = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MaxVel"), _T("10000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMaxVel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Vel"), _T("10.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbVel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Acc"), _T("25000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbAcc = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginAccFirst"), _T("5000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginAccFirst = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginAccSecond"), _T("3000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginAccSecond = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Dec"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbDec = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("CoarseVel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbCoarseVel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("FineVel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbFineVel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginVelFirst"), _T("1000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginVelFirst = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginVelSecond"), _T("1000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginVelSecond = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginVelThird"), _T("100.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginVelThird = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginVelLast"), _T("100.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginVelLast = _ttof(inBuff);
// 		
// 		GetPrivateProfileString(strAppName, _T("NegLimit"), _T("1000000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbNegLimit = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("PosLimit"), _T("1000000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbPosLimit = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("BufNegLimit"), _T("1000000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbBufNegLimit = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("BufPosLimit"), _T("1000000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbBufPosLimit = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Numerator"), _T("1.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbNumerator = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Denominator"), _T("1.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbDenominator = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Unit"), _T("360.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbnit = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("Pulse"), _T("800.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbPulse = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("GearRatio"), _T("1.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbGearRatio = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginOffset"), _T("1000.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOriginOffset = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("GantryOffset"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbGantryOffset = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OffsetRange"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbOffsetRange = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("EncoderMethod"), _T("3.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbEncoderMethod = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("CoordDirection"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbCoordDirection = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorType"), _T("1.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorType = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("AxisStatus"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbAxisStatus = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("EncoderType"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbEncoderType = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("StepMotorSmoothing"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbStepMotorSmoothing = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("VelPulse"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbVelPulse = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorInpositionLevel"), _T("1"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisInpositionLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorAmpEnableLevel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorAmpEnableLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorAmpResetLevel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorAmpResetLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorAmpFaultLevel"), _T("1.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorAmpFaultLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorNegaLimitLevel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorNegaLimitLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("MotorPosiLimitLevel"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbMotorPosiLimitLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("GantrySelect"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbGantrySelect = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("StartSpeed"), _T("0.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbStartSpeed = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("StopLevel"), _T("2.0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].dbStopLevel = _ttof(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("ServoAlarmLevel"), _T("1"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisAlarmLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("HomeLevel"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisHomeSenLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OriginMode"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisHomeSig = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("OutPutMethod"), _T("6"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisOutputMethod = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("LimitPlus"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisPosLimitSenLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("LimitMin"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisNegLimitSenLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("HomeDir"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisHomeDir = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("EncInputMethod"), _T("3"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisEncInputMethod = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("AxisServoLevel"), _T("1"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisServoLevel = _ttoi(inBuff);
// 
// 		GetPrivateProfileString(strAppName, _T("AxisEmergencyLevel"), _T("2"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.pMotionParam[nAxis].iAxisEmergencyLevel = _ttoi(inBuff);
// 	}
// 
// 	return TRUE;
// }
// 
// =============================================================================
// Method		: SaveMotionParameterFiles
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_AllMotorData * pstMotionInfo
// Qualifier	:
// Last Update	: 2016/9/23 - 13:04
// Desc.		:
// =============================================================================
// BOOL CFile_Motor::SaveMotionParamFiles(__in LPCTSTR szPath, __in const ST_AllMotorData* pstMotionInfo)
// {
// 	if ((szPath == NULL) && (pstMotionInfo == NULL))
// 		return FALSE;
// 
// 	CString strValue;
// 	CString strAppName;
// 
// 	for (UINT nAxis = 0; nAxis < (UINT)pstMotionInfo->MotorInfo.lMaxAxisCnt; nAxis++)
// 	{
// 		strAppName.Format(_T("%s #%d"), MotorControlAppName, nAxis);
// 
// 		strValue = pstMotionInfo->pMotionParam[nAxis].szAxisName;
// 		WritePrivateProfileString(strAppName, _T("AxisName"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisUse);
// 		WritePrivateProfileString(strAppName, _T("AxisUse"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisNum);
// 		WritePrivateProfileString(strAppName, _T("AxisNum"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMaxVel);
// 		WritePrivateProfileString(strAppName, _T("MaxVel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbVel);
// 		WritePrivateProfileString(strAppName, _T("Vel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbAcc);
// 		WritePrivateProfileString(strAppName, _T("Acc"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginAccFirst);
// 		WritePrivateProfileString(strAppName, _T("OriginAccFirst"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginAccSecond);
// 		WritePrivateProfileString(strAppName, _T("OriginAccSecond"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbDec);
// 		WritePrivateProfileString(strAppName, _T("Dec"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbCoarseVel);
// 		WritePrivateProfileString(strAppName, _T("CoarseVel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbFineVel);
// 		WritePrivateProfileString(strAppName, _T("FineVel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginVelFirst);
// 		WritePrivateProfileString(strAppName, _T("OriginVelFirst"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginVelSecond);
// 		WritePrivateProfileString(strAppName, _T("OriginVelSecond"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginVelThird);
// 		WritePrivateProfileString(strAppName, _T("OriginVelThird"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginVelLast);
// 		WritePrivateProfileString(strAppName, _T("OriginVelLast"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbNegLimit);
// 		WritePrivateProfileString(strAppName, _T("NegLimit"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbPosLimit);
// 		WritePrivateProfileString(strAppName, _T("PosLimit"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbBufNegLimit);
// 		WritePrivateProfileString(strAppName, _T("BufNegLimit"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbBufPosLimit);
// 		WritePrivateProfileString(strAppName, _T("BufPosLimit"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbNumerator);
// 		WritePrivateProfileString(strAppName, _T("Numerator"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbDenominator);
// 		WritePrivateProfileString(strAppName, _T("Denominator"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbnit);
// 		WritePrivateProfileString(strAppName, _T("Unit"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbPulse);
// 		WritePrivateProfileString(strAppName, _T("Pulse"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbGearRatio);
// 		WritePrivateProfileString(strAppName, _T("GearRatio"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOriginOffset);
// 		WritePrivateProfileString(strAppName, _T("OriginOffset"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbGantryOffset);
// 		WritePrivateProfileString(strAppName, _T("GantryOffset"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbOffsetRange);
// 		WritePrivateProfileString(strAppName, _T("OffsetRange"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbEncoderMethod);
// 		WritePrivateProfileString(strAppName, _T("EncoderMethod"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbCoordDirection);
// 		WritePrivateProfileString(strAppName, _T("CoordDirection"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorType);
// 		WritePrivateProfileString(strAppName, _T("MotorType"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbAxisStatus);
// 		WritePrivateProfileString(strAppName, _T("AxisStatus"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbEncoderType);
// 		WritePrivateProfileString(strAppName, _T("EncoderType"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbStepMotorSmoothing);
// 		WritePrivateProfileString(strAppName, _T("StepMotorSmoothing"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbVelPulse);
// 		WritePrivateProfileString(strAppName, _T("VelPulse"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisInpositionLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorInpositionLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorAmpEnableLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorAmpEnableLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorAmpResetLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorAmpResetLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorAmpFaultLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorAmpFaultLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorNegaLimitLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorNegaLimitLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbMotorPosiLimitLevel);
// 		WritePrivateProfileString(strAppName, _T("MotorPosiLimitLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbGantrySelect);
// 		WritePrivateProfileString(strAppName, _T("GantrySelect"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbStartSpeed);
// 		WritePrivateProfileString(strAppName, _T("StartSpeed"), strValue, szPath);
// 
// 		strValue.Format(_T("%6.1f"), pstMotionInfo->pMotionParam[nAxis].dbStopLevel);
// 		WritePrivateProfileString(strAppName, _T("StopLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisAlarmLevel);
// 		WritePrivateProfileString(strAppName, _T("ServoAlarmLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisHomeSenLevel);
// 		WritePrivateProfileString(strAppName, _T("HomeLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisHomeSig);
// 		WritePrivateProfileString(strAppName, _T("OriginMode"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisOutputMethod);
// 		WritePrivateProfileString(strAppName, _T("OutPutMethod"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisPosLimitSenLevel);
// 		WritePrivateProfileString(strAppName, _T("LimitPlus"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisNegLimitSenLevel);
// 		WritePrivateProfileString(strAppName, _T("LimitMin"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisHomeDir);
// 		WritePrivateProfileString(strAppName, _T("HomeDir"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisEncInputMethod);
// 		WritePrivateProfileString(strAppName, _T("EncInputMethod"), strValue, szPath);
// 		
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisServoLevel);
// 		WritePrivateProfileString(strAppName, _T("AxisServoLevel"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->pMotionParam[nAxis].iAxisEmergencyLevel);
// 		WritePrivateProfileString(strAppName, _T("AxisEmergencyLevel"), strValue, szPath);
// 	}
// 
// 	return TRUE;
// }
// 
// BOOL CFile_Motor::LoadMotionSequenceFiles(__in LPCTSTR szPath, __out ST_AllMotorData& stMotionInfo)
// {
// 	if (szPath == NULL)
// 		return FALSE;
// 
// 	CString strSeq;
// 	CString strStep;
// 	CString strAxis;
// 	CString strAppName;
// 
// 	TCHAR inBuff[MAX_PATH] = { 0, };
// 
// 	for (int iSeq = 0; iSeq < DEF_MOTION_SEQ_MAX; iSeq++)
// 	{
// 		strAppName.Format(_T("%s #%d"), MotorSequenceAppName, iSeq);
// 
// 		strSeq.Format(_T("Sequence %d"), iSeq);
// 
// 		GetPrivateProfileString(strAppName, _T("SequenceName"), strSeq, inBuff, MAX_PATH, szPath);
// 		stMotionInfo.MotionSeq[iSeq].szSequenceName = inBuff;
// 
// 		GetPrivateProfileString(strAppName, _T("StepCount"), _T("0"), inBuff, MAX_PATH, szPath);
// 		stMotionInfo.MotionSeq[iSeq].nStepCount = _ttoi(inBuff);
// 		
// 		for (int iStep = 0; iStep < DEF_SEQUENCE_STEP_MAX; iStep++)
// 		{
// 			strStep.Format(_T("Step_%d_"), iStep);
// 
// 			for (int iAxis = 0; iAxis < MotorAxisNum; iAxis++)
// 			{
// 				strAxis.Format(_T("Axis_%d_"), iAxis);
// 
// 				GetPrivateProfileString(strAppName, strStep + strAxis + _T("Use"), _T("0"), inBuff, MAX_PATH, szPath);
// 				stMotionInfo.MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].bAxisUse = _ttof(inBuff);
// 
// 				GetPrivateProfileString(strAppName, strStep + strAxis + _T("Pos"), _T("0"), inBuff, MAX_PATH, szPath);
// 				stMotionInfo.MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbPos = _ttof(inBuff);
// 
// 				GetPrivateProfileString(strAppName, strStep + strAxis + _T("Vel"), _T("0"), inBuff, MAX_PATH, szPath);
// 				stMotionInfo.MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbVel = _ttof(inBuff);
// 
// 				GetPrivateProfileString(strAppName, strStep + strAxis + _T("Acc"), _T("0"), inBuff, MAX_PATH, szPath);
// 				stMotionInfo.MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbAcc = _ttof(inBuff);
// 
// 				GetPrivateProfileString(strAppName, strStep + strAxis + _T("Dec"), _T("0"), inBuff, MAX_PATH, szPath);
// 				stMotionInfo.MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbDec = _ttof(inBuff);
// 			}
// 		}
// 	}
// 
// 
// 
// 	return TRUE;
// }
// 
// BOOL CFile_Motor::SaveMotionSequenceFiles(__in LPCTSTR szPath, __in const ST_AllMotorData* pstMotionInfo)
// {
// 	if ((szPath == NULL) && (pstMotionInfo == NULL))
// 		return FALSE;
// 
// 	CString strAppName;
// 	CString strSeq;
// 	CString strStep;
// 	CString strAxis;
// 
// 	CString strValue;
// 
// 	for (int iSeq = 0; iSeq < DEF_MOTION_SEQ_MAX; iSeq++)
// 	{
// 		strAppName.Format(_T("%s #%d"), MotorSequenceAppName, iSeq);
// 
// 		strValue = pstMotionInfo->MotionSeq[iSeq].szSequenceName;
// 		WritePrivateProfileString(strAppName, _T("SequenceName"), strValue, szPath);
// 
// 		strValue.Format(_T("%d"), pstMotionInfo->MotionSeq[iSeq].nStepCount);
// 		WritePrivateProfileString(strAppName, _T("StepCount"), strValue, szPath);
// 
// 		for (int iStep = 0; iStep < DEF_SEQUENCE_STEP_MAX; iStep++)
// 		{
// 			strStep.Format(_T("Step_%d_"), iStep);
// 
// 			for (int iAxis = 0; iAxis < MotorAxisNum; iAxis++)
// 			{
// 				strAxis.Format(_T("Axis_%d_"), iAxis);
// 
// 				strValue.Format(_T("%d"), pstMotionInfo->MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].bAxisUse);
// 				WritePrivateProfileString(strAppName, strStep + strAxis + _T("Use"), strValue, szPath);
// 
// 				strValue.Format(_T("%0.0f"), pstMotionInfo->MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbPos);
// 				WritePrivateProfileString(strAppName, strStep + strAxis + _T("Pos"), strValue, szPath);
// 
// 				strValue.Format(_T("%0.0f"), pstMotionInfo->MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbVel);
// 				WritePrivateProfileString(strAppName, strStep + strAxis + _T("Vel"), strValue, szPath);
// 
// 				strValue.Format(_T("%0.0f"), pstMotionInfo->MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbAcc);
// 				WritePrivateProfileString(strAppName, strStep + strAxis + _T("Acc"), strValue, szPath);
// 
// 				strValue.Format(_T("%0.0f"), pstMotionInfo->MotionSeq[iSeq].stSeqStep[iStep].stAxisParam[iAxis].dbDec);
// 				WritePrivateProfileString(strAppName, strStep + strAxis + _T("Dec"), strValue, szPath);
// 			}
// 		}
// 	}
// 
// 
// 	return TRUE;
// }
