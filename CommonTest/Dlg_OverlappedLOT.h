﻿//*****************************************************************************
// Filename	: 	Dlg_OverlappedLOT.h
// Created	:	2017/1/9 - 15:31
// Modified	:	2017/1/9 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_OverlappedLOT_h__
#define Dlg_OverlappedLOT_h__

#pragma once

#include "resource.h"
#include "VGStatic.h"

//-----------------------------------------------------------------------------
// CDlg_OverlappedLOT dialog
//-----------------------------------------------------------------------------
class CDlg_OverlappedLOT : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_OverlappedLOT)

public:
	CDlg_OverlappedLOT(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_OverlappedLOT();

// Dialog Data
	enum { IDD = IDD_DLG_LOT_OVERLAP };

protected:
	virtual void	DoDataExchange			(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	virtual BOOL	OnInitDialog			();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	afx_msg void	OnBnClickedBnRename		();
	afx_msg void	OnBnClickedBnNew		();
	afx_msg void	OnBnClickedBnContinue	();

	INT			m_iReturnCode;

	CVGStatic	m_st_Message;
	CVGStatic	m_st_RenameMsg;
	CVGStatic	m_st_NewMsg;
	CVGStatic	m_st_ContinueMsg;

	CButton		m_bn_RenameMsg;
	CButton		m_bn_NewMsg;
	CButton		m_bn_ContinueMsg;

public:

	enum LotReturnCode
	{
		Rename,
		DeleteAndNew,
		Continue,
	};

	INT		GetReturnCode()
	{
		return m_iReturnCode;
	};


	
};
#endif // Dlg_OverlappedLOT_h__
