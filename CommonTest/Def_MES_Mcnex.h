﻿//*****************************************************************************
// Filename	: 	Def_MES_Mcnex.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_MES_Mcnex_h__
#define Def_MES_Mcnex_h__

#include <afxwin.h>

static enum enHeader_FOC_Item_MES
{
	enHeader_FOC_LOT_NO,
	enHeader_FOC_LOT_CARD_NO,
	enHeader_FOC_Channel,
	enHeader_FOC_EqpCode,
	enHeader_FOC_ItemNumber,
	//enHeader_FOC_Barcode,
	enHeader_FOC_Operator,
	enHeader_FOC_TestDate,
	enHeader_FOC_TestTime,

	enHeader_FOC_TestResult,

	enHeader_FOC_Current,
	enHeader_FOC_CurrentValue,

	enHeader_FOC_CenterPoint,
	enHeader_FOC_CenterPoint_X,
	enHeader_FOC_CenterPoint_Y,

	enHeader_FOC_SFR,
	enHeader_FOC_SFR_S1,
	enHeader_FOC_SFR_S2,
	enHeader_FOC_SFR_S3,
	enHeader_FOC_SFR_S4,
	enHeader_FOC_SFR_S5,
	enHeader_FOC_SFR_S6,
	enHeader_FOC_SFR_S7,
	enHeader_FOC_SFR_S8,
	enHeader_FOC_SFR_S9,
	enHeader_FOC_SFR_S10,
	enHeader_FOC_SFR_S11,
	enHeader_FOC_SFR_S12,
	enHeader_FOC_SFR_S13,
	enHeader_FOC_SFR_S14,
	enHeader_FOC_SFR_S15,
	enHeader_FOC_SFR_S16,
	enHeader_FOC_SFR_S17,
	enHeader_FOC_SFR_S18,
	enHeader_FOC_SFR_S19,
	enHeader_FOC_SFR_S20,
	enHeader_FOC_SFR_S21,
	enHeader_FOC_SFR_S22,
	enHeader_FOC_SFR_S23,
	enHeader_FOC_SFR_S24,
	enHeader_FOC_SFR_S25,
	enHeader_FOC_SFR_S26,
	enHeader_FOC_SFR_S27,
	enHeader_FOC_SFR_S28,
	enHeader_FOC_SFR_S29,
	enHeader_FOC_SFR_S30,

// 	enHeader_FOC_LED,
// 	enHeader_FOC_LED_Current,

	enHeader_FOC_Item_Max
};

static LPCTSTR g_lpszHeader_FOC_Item_MES[enHeader_FOC_Item_Max] =
{
	_T("LOT_NO"),
	_T("LOT_CARD_NO"),
	_T("채널번호"),
	_T("설비코드"),
	_T("지시번호"),
//	_T("품번"),
	_T("작업자"),
	_T("처리일자"),
	_T("처리일시"),
	_T("최종결과"),

	// test item
	_T("전류"),
	_T("전류 값"),

	_T("보임량 광축"),
	_T("광축 X"),
	_T("광축 Y"),

	_T("해상력 SFR"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),
	_T("S10"),
	_T("S11"),
	_T("S12"),
	_T("S13"),
	_T("S14"),
	_T("S15"),
	_T("S16"),
	_T("S17"),
	_T("S18"),
	_T("S19"),
	_T("S20"),
	_T("S21"),
	_T("S22"),
	_T("S23"),
	_T("S24"),
	_T("S25"),
	_T("S26"),
	_T("S27"),
	_T("S28"),
	_T("S29"),
	_T("S30"),

// 	_T("LED"),
// 	_T("LED 전류"),
};



//-----------------------------------------------------------------------------
// Worklist 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_MES_TotalResult
{
	CString szLotNo;
	CString szLotCardNo;
	CString szChannel;
	CString szEqpCode;
	CString szItemNumber;
	//CString szBarcode;
	CString szOperator;
	CString szTestDate;
	CString szTestTime;

	CStringArray	ItemHeaderz;
	CStringArray	Itemz;

	void Reset()
	{
		szLotNo		.Empty();
		szLotCardNo	.Empty();
		szChannel	.Empty();
		szEqpCode	.Empty();
		szItemNumber.Empty();
		//szBarcode	.Empty();
		szOperator	.Empty();
		szTestDate	.Empty();
		szTestTime	.Empty();
	};

}ST_MES_TotalResult;


#endif // Def_MES_Mcnex_h__
