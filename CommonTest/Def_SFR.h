﻿#ifndef Def_SFR_h__
#define Def_SFR_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

typedef enum enSFRRegion
{
	SFR_Center,
	SFR_Middle,
	SFR_Side,
	SFR_MaxEnum
};

static LPCTSTR g_szSFRType[] =
{
	_T("CENTER"),
	_T("MIDDEL"),
	_T("SIDE"),
	NULL
};

typedef enum enSFRRegionIdex
{
	Region_SFR_1,
	Region_SFR_2,
	Region_SFR_3,
	Region_SFR_4,
	Region_SFR_5,
	Region_SFR_6,
	Region_SFR_7,
	Region_SFR_8,
	Region_SFR_9,
	Region_SFR_10,
	Region_SFR_11,
	Region_SFR_12,
	Region_SFR_13,
	Region_SFR_14,
	Region_SFR_15,
	Region_SFR_16,
	Region_SFR_17,
	Region_SFR_18,
	Region_SFR_19,
	Region_SFR_20,
	Region_SFR_21,
	Region_SFR_22,
	Region_SFR_23,
	Region_SFR_24,
	Region_SFR_25,
	Region_SFR_26,
	Region_SFR_27,
	Region_SFR_28,
	Region_SFR_29,
	Region_SFR_30,
	Region_SFR_MaxEnum
};

static LPCTSTR g_szSFRMode[] =
{
	_T("◁▶"),
	_T("◀▷"),
	_T("△▼"),
	_T("▲▽"),
	_T("NULL"),
	NULL
};

typedef enum enSFRMode
{
	SFR_Mode_LR_WB = 0,
	SFR_Mode_LR_BW,
	SFR_Mode_TB_WB,
	SFR_Mode_TB_BW,
	SFR_Mode_NO
};

static LPCTSTR g_szSFR_FontMode[] =
{
	_T("◁"),
	_T("▷"),
	_T("△"),
	_T("▽"),
	NULL
};

typedef enum enSFR_FontMode
{
	SFR_Mode_F_Left = 0,
	SFR_Mode_F_Right,
	SFR_Mode_F_Up,
	SFR_Mode_F_Down,
	SFR_Mode_F_Max,
};

// SFR 영역에 대한 데이터
typedef struct _tag_RegionSFRDa
{
	double	dbResultValue;
	UINT	nResult;

	_tag_RegionSFRDa()
	{
		dbResultValue = 0;
		nResult = TR_Init;
	};


	void Reset()
	{
		dbResultValue = 0;
		nResult = TR_Init;
	}

	_tag_RegionSFRDa& operator= (_tag_RegionSFRDa& ref)
	{
		dbResultValue = ref.dbResultValue;
		nResult		  = ref.nResult;
		return *this;
	};

}ST_RegionSFRDa, *PST_RegionSFRDa;

// SFR 영역에 대한 옵션
typedef struct _tag_RegionSFROp
{
	BOOL bEnable;

	double dbField;

	int nPos_X;
	int nPos_Y;

	int nWidth;
	int nHeight;
	int nType;

	double dbThreshold;
	double dbOffset;
	double dbLinePair;
	double dbRoi_X;
	double dbRoi_Y;

	int nFont;
	
	_tag_RegionSFROp()
	{
		bEnable = FALSE;
		dbField = 0;
		nPos_X = 360;
		nPos_Y = 240;
		nWidth = 30;
		nHeight = 20;
		nType = 0;

		dbThreshold = 0;
		dbOffset = 0;

		dbLinePair = 45;
		dbRoi_X = 1.0;
		dbRoi_Y = 1.0;
		nFont = 0;
	};


	void Reset()
	{
		bEnable = FALSE;
		dbField = 0;
		nPos_X = 360;
		nPos_Y = 240;
		nWidth = 30;
		nHeight = 20;
		nType = 0;

		dbThreshold = 0;
		dbOffset = 0;

		dbLinePair = 45;
		dbRoi_X = 1.0;
		dbRoi_Y = 1.0;
		nFont = 0;
	}

	_tag_RegionSFROp& operator= (_tag_RegionSFROp& ref)
	{
		bEnable		= ref.bEnable;
		dbField		= ref.dbField;
		nPos_X		= ref.nPos_X;
		nPos_Y		= ref.nPos_Y;
		nWidth		= ref.nWidth;
		nHeight		= ref.nHeight;
		nType		= ref.nType;
		dbThreshold = ref.dbThreshold;
		dbOffset	= ref.dbOffset;

		dbLinePair	= ref.dbLinePair;
		dbRoi_X		= ref.dbRoi_X;
		dbRoi_Y		= ref.dbRoi_Y;
		nFont		= ref.nFont;
		return *this;
	};

}ST_RegionSFROp, *PST_RegionSFROp;

	
typedef struct _tag_SFR_Op
{
	ST_RegionSFROp	stSFR_Region[Region_SFR_MaxEnum];
	ST_RegionSFROp	stSFR_InitRegion[Region_SFR_MaxEnum];

	BOOL	bSmoothMode;
	BOOL	bField;
	BOOL	bEdge;
	BOOL	bDistortion;
	double	dbPixelSizeW;

	UINT nDataType;

	int iOffsetX;
	int iOffsetY;


	_tag_SFR_Op()
	{
		for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		{
			stSFR_Region[nIdx].Reset();
			stSFR_InitRegion[nIdx].Reset();
		}

		bSmoothMode = FALSE;
		bField = FALSE;
		bEdge = FALSE;
		bDistortion = FALSE;
		dbPixelSizeW = 0.0;

		nDataType = 0;

		iOffsetX = 0;
		iOffsetY = 0;

	};
	void Reset()
	{
		for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		{
			stSFR_Region[nIdx].Reset();
			stSFR_InitRegion[nIdx].Reset();
		}

		bSmoothMode = FALSE;
		bField = FALSE;
		bEdge = FALSE;
		bDistortion = FALSE;
		dbPixelSizeW = 0.0;

		nDataType = 0;

	}

	_tag_SFR_Op& operator= (_tag_SFR_Op& ref)
	{
		for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		{
			stSFR_Region[nIdx] = ref.stSFR_Region[nIdx];
			stSFR_Region[nIdx] = ref.stSFR_Region[nIdx];
		}

		bSmoothMode = ref.bSmoothMode;
		bField = ref.bField;
		bEdge = ref.bEdge;
		bDistortion = ref.bDistortion;
		dbPixelSizeW	= ref.dbPixelSizeW;

		iOffsetX = ref.iOffsetX;
		iOffsetY = ref.iOffsetY;

		nDataType = ref.nDataType;
		return *this;
	};

}ST_SFR_Op, *PST_SFR_Op;

//각각 영역에 대한 Data정보
typedef struct _tag_SFR_Data
{
	UINT	nResult;

	UINT    nEachResult[Region_SFR_MaxEnum];
	double	dbResultValue[Region_SFR_MaxEnum];

	// 알고리즘 시 사용할 변수
	BOOL	bEnable[Region_SFR_MaxEnum];

	UINT	nPassCnt;
	UINT	nTotalCnt;

	_tag_SFR_Data()
	{
		nResult = TR_Init;
		nPassCnt = 0;
		nTotalCnt = 0;
		for (UINT nIdex = 0; nIdex < Region_SFR_MaxEnum; nIdex++)
		{
			nEachResult[nIdex] = 0;
			dbResultValue[nIdex] = 0;
			bEnable[nIdex] = 0;
		}
	};

	void reset()
	{
		nResult = TR_Init;
		nPassCnt = 0;
		nTotalCnt = 0;

		for (UINT nIdex = 0; nIdex < Region_SFR_MaxEnum; nIdex++)
		{
			nEachResult[nIdex] = 0;
			dbResultValue[nIdex] = 0;
			bEnable[nIdex] = 0;
		}
	};

	_tag_SFR_Data& operator= (_tag_SFR_Data& ref)
	{
		nResult = ref.nResult;
		nPassCnt = ref.nPassCnt;
		nTotalCnt = ref.nTotalCnt;

		for (UINT nIdex = 0; nIdex < Region_SFR_MaxEnum; nIdex++)
		{
			nEachResult[nIdex] = ref.nEachResult[nIdex];
			dbResultValue[nIdex] = ref.dbResultValue[nIdex];
			bEnable[nIdex] = ref.bEnable[nIdex];
		}

		return *this;
	};

}ST_SFR_Data, *PST_SFR_Data;

#endif // Def_SFR_h__