//*****************************************************************************
// Filename	: 	LGIT_EVMS.h
// Created	:	2016/11/8 - 16:28
// Modified	:	2016/11/8 - 16:28
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef LGIT_EVMS_h__
#define LGIT_EVMS_h__

#pragma once

#include "Def_EVMS_Cm.h"

class CLGIT_EVMS
{
public:
	CLGIT_EVMS();
	~CLGIT_EVMS();
	
protected:

	// log (Header -> List)
	CString		Conv_SYSTEMTIME2String	(__in SYSTEMTIME* pTime);
	CString		Conv_FileVer2EVMSVer	();
	
public:

	BOOL		VerifyDirectory			(__in enEVMS_PGM_Type nPgmType);
	BOOL		VerifyDirectory			(__in enEVMS_PGM_Type nPgmType, __in LPCTSTR szDirectory);
};

#endif // LGIT_EVMS_h__

