﻿//*****************************************************************************
// Filename	: 	TestItem.h
// Created	:	2016/12/30 - 12:31
// Modified	:	2016/12/30 - 12:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestItem_h__
#define TestItem_h__

#pragma once

#include "Def_TestItem.h"

class CTestItem
{
public:
	CTestItem();
	~CTestItem();


protected:
	
	UINT		m_nTestItem_ID;
	CString		m_szTestItemName;
	CString		m_szTestItemDesc;
	LRESULT		m_lResult;

	CWnd*		m_pwnd_UI_Option;		// 검사 기준 설정
	CWnd*		m_pwnd_UI_Inspection;	// 검사 실시간 정보
	CWnd*		m_pwnd_UI_Worklist;		// CListCtrl

public:

	void			SetID(__in UINT nID);
	UINT			GetID();

	void			SetName(__in LPCTSTR szName, __in LPCTSTR szDesc = NULL);
	CString			GetName();
	CString			GetDesc();

	void			SetResult(__in LRESULT lResult);
	LRESULT			GetResult();

	void			SetTestItemInfo(__in ST_LT_TestItem* pstTestItem);
	ST_LT_TestItem	GetTestItemInfo();

};

#endif // TestItem_h__
