﻿//*****************************************************************************
// Filename	: 	Def_MES_Cm.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_MES_Cm_h__
#define Def_MES_Cm_h__

#include <afxwin.h>

#define	 LOT_ID_LENGTH		12

typedef enum enMES_ImgFormat
{
	EVMS_IMG_RAW,
	EVMS_IMG_BMP,
	EVMS_IMG_JPG,
};

static LPCTSTR g_szMES_ImgFormat[] =
{
	_T("raw"),
	_T("bmp"),
	_T("jpg"),
};

//-----------------------------------------------------------------------------
// Worklist 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_MES_FinalResult
{
	CString		Time;
	CString		Equipment;
	CString		Model;
	CString		SWVersion;
	CString		LOTName;
	//CString		Barcode;
	CString		Operator;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_FinalResult;

typedef struct _tag_MES_TestItemLog
{
	CString		Time;
	CString		Equipment;
	CString		Model;
	CString		SWVersion;
	CString		LOTName;
	CString		Operator;
	int			nRetryTestCnt;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_TestItemLog;


//-----------------------------------------------------------------------------
// LOT ID 규칙
//-----------------------------------------------------------------------------
typedef struct _tag_LOTIDRule
{
	char	Location;
	char	Type;
	char	ProductCode[5];
	char	Year;
	char	Month;
	char	Day;
	char	SerialNo[2];
}ST_LOTIDRule, *PST_LOTIDRule;


#endif // Def_MES_Cm_h__
