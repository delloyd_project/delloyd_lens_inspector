﻿#ifndef Def_Current_h__
#define Def_Current_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

typedef enum enListItemNum_CurrentOp
{
	CuOp_Channel1,
// 	CuOp_Channel2,
// 	CuOp_Channel3,
// 	CuOp_Channel4,
// 	CuOp_Channel5,
	CuOp_ItemNum,
};

typedef struct _tag_Current_Op
{
	double dVoltage;
	int nOffset;

	UINT nMinCurrent;
	UINT nMaxCurrent;
	

	/*변수 초기화 함수*/
	_tag_Current_Op()
	{
		dVoltage = 5.5;
		nOffset = 0;
		nMinCurrent = 50;
		nMaxCurrent = 80;
		
	};

	/*변수 교환 함수*/
	_tag_Current_Op& operator= (_tag_Current_Op& ref)
	{
		
		dVoltage = ref.dVoltage;
		nOffset = ref.nOffset;
		nMinCurrent = ref.nMinCurrent;
		nMaxCurrent = ref.nMaxCurrent;
		return *this;
	};

}ST_Current_Op, *PST_Current_Op;

typedef struct _tag_Current_Data
{
	double	nCurrent;
	UINT	nResult;

	/*변수 초기화 함수*/
	_tag_Current_Data()
	{
		reset();
	};

	void reset()
	{
		nCurrent = 0;
		nResult = TR_Init;
	};

	/*변수 교환 함수*/
	_tag_Current_Data& operator= (_tag_Current_Data& ref)
	{
		nCurrent = ref.nCurrent;
		nResult = ref.nResult;

		nResult = ref.nResult;

		return *this;
	};

}ST_Current_Data, *PST_Current_Data;
#endif // Def_Current_h__