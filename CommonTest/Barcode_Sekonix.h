//*****************************************************************************
// Filename	: 	Barcode_Sekonix.h
// Created	:	2017/5/4 - 14:31
// Modified	:	2017/5/4 - 14:31
//
// Author	:	PiRing
//	
// Purpose	:	
//		공정이력번호		: 예) 95760H9000-17C220001
//		제품임시바코드	: 예) T20170405-000082
//		공정완료바코드	: COMPLETE
//
// (1)T (2)151222 (3)P1A1 (4)A (5)0014
// 
// (1) T		: 추적코드 구별자
// (2) 151222	: 생산일자
// (3) P1A1		: 평택공장 / 1번라인 / A조 / 재료
// (4) A		: 개별 Serial번호 관리
// (5) 0014		: Serial Count
//
//*****************************************************************************
#ifndef Barcode_Sekonix_h__
#define Barcode_Sekonix_h__

#pragma once

#include <afxwin.h>

typedef enum enBarcodeType
{
	Barcode_UnknownType,
	Barcode_Lot,
	Barcode_Camera,
	Barcode_LotEnd,
	Barcode_Socket,
	Barcode_PCB,
	Barcode_MaxEnum,
};

#define BARCODE_LOT_LEN		20
#define BARCODE_CAM_LEN		16
#define BARCODE_PCB_LEN		22
#define BARCODE_LOTEND_LEN	8
#define BARCODE_SOCKET_LEN	3	// "1-1", "1-2", "1-3", "1-4"
#define BARCODE_LOTEND		_T("COMPLETE")	//COMPLETE

//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
class CBarcode_Sekonix
{
public:
	CBarcode_Sekonix();
	~CBarcode_Sekonix();

protected:


public:

	enBarcodeType	CheckBarcodeType	(__in LPCTSTR szBarcode);

};

#endif // Barcode_Sekonix_h__

