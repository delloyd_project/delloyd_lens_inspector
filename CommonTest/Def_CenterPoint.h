﻿#ifndef Def_CenterPoint_h__
#define Def_CenterPoint_h__


#include <afxwin.h>
#include "Def_Test_Cm.h"


#define CP_No_Target -9999
#define CP_No_Target_Result -99999

enum enCenterPointTestMode{
	//조정
	//CP_Fine_tune_TestMode,
	//측정
	CP_Measurement_TestMode,
	CP_TestMode_MaxNum,
};

static LPCTSTR	g_szCenterPointTestMode[] =
{
	//_T("CenterPoint Fine Tuning Mode"),
	_T("CenterPoint Measuring Mode"),
	NULL
};

enum enCenterPointWriteMode{
	CP_FullWrite_Mode,
	CP_Sector_Mode,
	CP_None_Mode,
	CP_WriteMode_MaxNum,
};
static LPCTSTR	g_szCenterPointWriteMode[] =
{
	_T("Full Write Mode"),
	_T("Sector Write Mode"),
	_T("None Write Mode"),
	NULL
};

typedef struct _tag_CenterPoint_Op
{
	/*TestMode*/
	UINT nTestMode;
	/*카메라 상태*/
	UINT nCameraState;
	/*초기 Write 상태*/
	UINT nInitWriteMode;
	/*TEST Write 상태*/
	UINT nTestWriteMode;
	/*총 조정 카운트*/
	UINT nWriteCnt;

	/*측정기준*/
	UINT nStandard_X;
	UINT nStandard_Y;

	/*합격편차*/
	UINT nStandard_OffsetX;
	UINT nStandard_OffsetY;

	/*조정기준 Target*/
	UINT nTarget_OffsetX;
	UINT nTarget_OffsetY;

	/*적용 비율*/
	double dApplied_RatioX;
	double dApplied_RatioY;

	// 이미지센서 1pix 크기
	double dbImageSensorPix;

	/*초기값 Pix*/
	UINT nInit_PixX;
	UINT nInit_PixY;
	/*최소 조정 Pix*/
	UINT nMin_PixX;
	UINT nMin_PixY;
	/*최대 조정 Pix*/
	UINT nMax_PixX;
	UINT nMax_PixY;

	/*변수 초기화 함수*/
	_tag_CenterPoint_Op()
	{
		nTestMode = CP_Measurement_TestMode;
		nCameraState = CAM_STATE_MIRROR;
		nInitWriteMode = CP_FullWrite_Mode;
		nTestWriteMode = CP_FullWrite_Mode;
		nWriteCnt = 3;

		nStandard_X = CAM_IMAGE_WIDTH_HALF;
		nStandard_Y = CAM_IMAGE_HEIGHT_HALF;

		nStandard_OffsetX = 3;
		nStandard_OffsetY = 3;

		nTarget_OffsetX = 1;
		nTarget_OffsetY = 1;

		dApplied_RatioX = 1.0;
		dApplied_RatioY = 1.0;

		nInit_PixX = 15;
		nInit_PixY = 11;
		nMin_PixX = 0;
		nMin_PixY = 0;

		nMax_PixX=30;
		nMax_PixY=22;

		dbImageSensorPix = 0.0;
	};

	/*변수 교환 함수*/
	_tag_CenterPoint_Op& operator= (_tag_CenterPoint_Op& ref)
	{
		nTestMode = ref.nTestMode;
		nCameraState = ref.nCameraState;
		nInitWriteMode = ref.nInitWriteMode;
		nTestWriteMode = ref.nTestWriteMode;
		nWriteCnt = ref.nWriteCnt;

		nStandard_X = ref.nStandard_X;
		nStandard_Y = ref.nStandard_Y;

		nStandard_OffsetX = ref.nStandard_OffsetX;
		nStandard_OffsetY = ref.nStandard_OffsetY;

		nTarget_OffsetX = ref.nTarget_OffsetX;
		nTarget_OffsetY = ref.nTarget_OffsetY;

		dApplied_RatioX = ref.dApplied_RatioX;
		dApplied_RatioY = ref.dApplied_RatioY;


		nInit_PixX = ref.nInit_PixX;
		nInit_PixY = ref.nInit_PixY;
		nMin_PixX = ref.nMin_PixX;
		nMin_PixY = ref.nMin_PixY;

		nMax_PixX = ref.nMax_PixX;
		nMax_PixY = ref.nMax_PixY;
	
		dbImageSensorPix = ref.dbImageSensorPix;
		
		return *this;
	};

}ST_CenterPoint_Op, *PST_CenterPoint_Op;


typedef struct _tag_CenterPoint_Data
{
	/*결과 - 양,불*/
	UINT nResult;

	/*결과 - 이미지 상 광축 좌표 값*/
	int iResult_RealPos_X;
	int iResult_RealPos_Y;

	/*결과 - 카메라 상태값 반영 광축 좌표 값*/
	int iResult_Pos_X;
	int iResult_Pos_Y;

	/*결과 - 광축 Offset 값*/
	int iResult_Offset_X;
	int iResult_Offset_Y;

	/*결과 - 광축 적용 Offset 누적 값*/
	int iResult_Tuning_X;
	int iResult_Tuning_Y;
	int iResult_Tuning_SumX;
	int iResult_Tuning_SumY;

	/*변수 초기화 함수*/
	_tag_CenterPoint_Data()
	{
		nResult = TR_Init;

		iResult_RealPos_X = -9999;
		iResult_RealPos_Y = -9999;

		iResult_Pos_X = -9999;
		iResult_Pos_Y = -9999;

		iResult_Offset_X = -9999;
		iResult_Offset_Y = -9999;

		iResult_Tuning_X = -9999;
		iResult_Tuning_Y = -9999;
	};

	void reset(){
		nResult = TR_Init;

		iResult_RealPos_X = -9999;
		iResult_RealPos_Y = -9999;

		iResult_Pos_X = -9999;
		iResult_Pos_Y = -9999;

		iResult_Offset_X = -9999;
		iResult_Offset_Y = -9999;

		iResult_Tuning_X = 0;
		iResult_Tuning_Y = 0;

		iResult_Tuning_SumX = 0;
		iResult_Tuning_SumY = 0;
	};

	/*변수 교환 함수*/
	_tag_CenterPoint_Data& operator= (_tag_CenterPoint_Data& ref)
	{
		nResult = ref.nResult;

		iResult_RealPos_X = ref.iResult_RealPos_X;
		iResult_RealPos_Y = ref.iResult_RealPos_Y;

		iResult_Pos_X	 = ref.iResult_Pos_X;
		iResult_Pos_Y	 = ref.iResult_Pos_Y;

		iResult_Offset_X = ref.iResult_Offset_X;
		iResult_Offset_Y = ref.iResult_Offset_Y;

		iResult_Tuning_X = ref.iResult_Tuning_X;
		iResult_Tuning_Y = ref.iResult_Tuning_Y;

		iResult_Tuning_SumX = ref.iResult_Tuning_SumX;
		iResult_Tuning_SumY = ref.iResult_Tuning_SumY;
		return *this;
	};

}ST_CenterPoint_Data, *PST_CenterPoint_Data;

typedef struct _tag_CPChart_Op
{
	UINT nPosX;
	UINT nPosY;
	UINT nSize;

	/*변수 초기화 함수*/
	_tag_CPChart_Op()
	{
		nPosX = 0;
		nPosY = 0;
		nSize = 0;
	};

	/*변수 교환 함수*/
	_tag_CPChart_Op& operator= (_tag_CPChart_Op& ref)
	{
		nPosX = ref.nPosX;
		nPosY = ref.nPosY;
		nSize = ref.nSize;
		return *this;
	};

}ST_CPChart_Op, *PST_CPChart_Op;

typedef struct _tag_CPRect_Op
{
	CRect nTestRect;

	/*변수 초기화 함수*/
	_tag_CPRect_Op()
	{
		nTestRect.left = 50;
		nTestRect.right = CAM_IMAGE_WIDTH - 50;
		nTestRect.top = 50;
		nTestRect.bottom = CAM_IMAGE_HEIGHT - 50;
	};

	/*변수 교환 함수*/
	_tag_CPRect_Op& operator= (_tag_CPRect_Op& ref)
	{
		nTestRect = ref.nTestRect;
		return *this;
	};

}ST_CPRect_Op, *PST_CPRect_Op;
#endif // Def_CenterPoint_h__