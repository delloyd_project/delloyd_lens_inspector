﻿#ifndef Def_PatternNoise_h__
#define Def_PatternNoise_h__


#include <afxwin.h>
#include "Def_Test_Cm.h"

#define		MAX_Region_PATTERNOISE	9

typedef enum enRegion
{
	Region_Center,
	Region_Left,
	Region_Right,
	Region_Top,
	Region_Bottom,
	Region_LeftTop,
	Region_RightTop,
	Region_LeftBottom,
	Region_RightBottom,
	Region_MaxEnum,
};

static LPCTSTR g_szRegion[] =
{
	_T("Center"),
	_T("Left"),
	_T("Right"),
	_T("Top"),
	_T("Bottom"),
	_T("Left Top"),
	_T("Right Top"),
	_T("Left Bottom"),
	_T("Right Bottom"),
	NULL
};


typedef struct _tag_RegionPtNoise
{
	int		Center_PosX;
	int		Center_PosY;
	int		RectGap_X;
	int		RectGap_Y;
	int		Rect_Width;
	int		Rect_Height;

	_tag_RegionPtNoise()
	{
		Center_PosX = 360;
		Center_PosY = 240;
		RectGap_X = 220;
		RectGap_Y = 150;
		Rect_Width = 150;
		Rect_Height = 90;
	};

	_tag_RegionPtNoise& operator= (_tag_RegionPtNoise& ref)
	{
		Center_PosX = ref.Center_PosX;
		Center_PosY = ref.Center_PosY;
		RectGap_X = ref.RectGap_X;
		RectGap_Y = ref.RectGap_Y;
		Rect_Width = ref.Rect_Width;
		Rect_Height = ref.Rect_Height;

		return *this;
	};

}ST_RegionPtNoise, *PST_ZonePtNoise;

typedef struct _tag_PatternNoise_Op
{
	int		iThresholddB;
	int		iThresholdEnv;
	int		iBrightness;
	int		iContrast;

	BOOL	bSelect[Region_MaxEnum];	// 검사 여부
	CRect	RegionList[Region_MaxEnum];	// 검사 영역

	ST_RegionPtNoise	InitData;

	/*변수 초기화 함수*/
	_tag_PatternNoise_Op()
	{
		
		iThresholddB = 20;
		iThresholdEnv = 200;
		iBrightness = 110;
		iContrast = 110;

		for (int iIdx = 0; iIdx < Region_MaxEnum; iIdx++)
		{
			bSelect[iIdx] = TRUE;
		}
	};

	/*변수 교환 함수*/
	_tag_PatternNoise_Op& operator= (_tag_PatternNoise_Op& ref)
	{
		iThresholddB = ref.iThresholddB;
		iThresholdEnv = ref.iThresholdEnv;
		iBrightness = ref.iBrightness;
		iContrast = ref.iContrast;

		for (int iIdx = 0; iIdx < Region_MaxEnum; iIdx++)
		{
			bSelect[iIdx] = ref.bSelect[iIdx];
			RegionList[iIdx] = ref.RegionList[iIdx];
		}

		return *this;
	};

}ST_PatternNoise_Op, *PST_PatternNoise_Op;


typedef struct _tag_PatternNoise_Data
{
	/*결과 - 양,불*/
	UINT nResult;
	UINT nEachResult[9];
	BOOL bUse[9];
	double ddBResult[9];
	double dEnvironmentResult[9];

	

	/*변수 초기화 함수*/
	_tag_PatternNoise_Data()
	{
		nResult = 0;
		for (int t = 0; t < 9; t++)
		{
			nEachResult[t] = 0;
			ddBResult[t] = 0;
			dEnvironmentResult[t] = 0;
			bUse[t] = 0;
		}
		
		
	};

	void reset(){
		for (int t = 0; t < 9; t++)
		{
			nEachResult[t] = 0;
			ddBResult[t] = 0;
			dEnvironmentResult[t] = 0;
			bUse[t] = 0;
		}
		nResult = 0;
	};

	/*변수 교환 함수*/
	_tag_PatternNoise_Data& operator= (_tag_PatternNoise_Data& ref)
	{
		for (int t = 0; t < 9; t++)
		{
			nEachResult[t] = ref.nEachResult[t];
			ddBResult[t] = ref.ddBResult[t];
			dEnvironmentResult[t] = ref.dEnvironmentResult[t];
			bUse[t] = ref.bUse[t];
		}
		nResult = ref.nResult;

		return *this;
	};

}ST_PatternNoise_Data, *PST_PatternNoise_Data;
#endif // Def_PatternNoise_h__