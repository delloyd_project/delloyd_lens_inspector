﻿//*****************************************************************************
// Filename	: 	File_MES_Mcnex.h
// Created	:	2016/11/8 - 16:29
// Modified	:	2016/11/8 - 16:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_MES_Mcnex_h__
#define File_MES_Mcnex_h__

#pragma once

#include "Def_MES_Mcnex.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CFile_MES_Mcnex
{
public:
	CFile_MES_Mcnex();
	~CFile_MES_Mcnex();

	BOOL	Make_MES_Log_File		(__in CString szMESPath, __in CString Model, __in int iIndex, __in const ST_MES_TotalResult* pstWorklist);
	BOOL	Delete_MES_TextFile		(__in CString szMESPath);

protected:
	CString	Make_MES_Result_Header	(__in const ST_MES_TotalResult* pstWorklist);
	CString	Make_MES_Result			(__in const ST_MES_TotalResult* pstWorklist);

};

#endif // File_MES_Mcnex_h__

