﻿//*****************************************************************************
// Filename	: 	Grid_FailureRate_Socket.h
// Created	:	2016/11/14 - 20:13
// Modified	:	2016/11/14 - 20:13
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_FailureRate_Socket_h__
#define Grid_FailureRate_Socket_h__

#pragma once

#include "Grid_Base.h"
#include "Def_DataStruct_Cm.h"

class CGrid_FailureRate_Socket : public CGrid_Base
{
public:
	CGrid_FailureRate_Socket();
	virtual ~CGrid_FailureRate_Socket();

	virtual void	OnSetup			();
	virtual int		OnHint			(int col, long row, int section, CString *string);
	virtual void	OnGetCell		(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect	(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline	();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline	();

	// 헤더를 초기화
	void			InitHeader		();

	CFont		m_font_Header;
	CFont		m_font_Data;

	UINT		m_nSocketCTCount;

public:

	void		SetUseSocketCTCount	(__in UINT nCount);
	void		SetYield			(__in const ST_Yield* pstYield);

};

#endif // Grid_FailureRate_Socket_h__

