﻿#ifndef Def_RotateTest_h__
#define Def_RotateTest_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

enum enRotateTestMode
{
	RT_Fine_tune_TestMode,		//조정
	RT_Measurement_TestMode,	//측정
	RT_TestMode_MaxNum,
};

static LPCTSTR	g_szRotateTestMode[] =
{
	_T("Rotate Fine Tuning Mode"),
	_T("Rotate Measuring Mode"),
	NULL
};

typedef enum enRegionRotate
{
	RegionRotate_LeftTop = 0,
	RegionRotate_RightTop,
	RegionRotate_LeftBottom,
	RegionRotate_RightBottom,
	RegionRotate_MaxEnum,
};

static LPCTSTR g_szRegionRotate[] =
{
	_T("LeftTop"),
	_T("RightTop"),
	_T("LeftBottom"),
	_T("RightBottom"),
	NULL
};

// ROI
typedef struct _tag_RegionRotate
{
	CRect	RegionList;

	_tag_RegionRotate()
	{
		Reset();
	};

	void Reset()
	{
		RegionList.SetRectEmpty();
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top = Y - (int)(H / 2);
		RegionList.bottom = RegionList.top + H;

		RegionList.left = X - (int)(W / 2);
		RegionList.right = RegionList.left + W;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data = RegionList;
		int posX = data.CenterPoint().x;
		int posY = data.CenterPoint().y;

		int halfX = W / 2;
		int halfY = H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

	_tag_RegionRotate& operator= (_tag_RegionRotate& ref)
	{
		RegionList = ref.RegionList;

		return *this;
	};

}ST_RegionRotate, *PST_ZoneRotate;


typedef struct _tag_RegionRotate_Oper
{
	CRect	RegionList;

	_tag_RegionRotate_Oper()
	{
		Reset();
	};

	void Reset()
	{
		RegionList.SetRectEmpty();
	};

	_tag_RegionRotate_Oper& operator= (_tag_RegionRotate_Oper& ref)
	{
		RegionList = ref.RegionList;

		return *this;
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top = Y - (int)(H / 2);
		RegionList.bottom = RegionList.top + H - 1;

		RegionList.left = X - (int)(W / 2);
		RegionList.right = RegionList.left + W - 1;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data = RegionList;
		int posX = data.CenterPoint().x;
		int posY = data.CenterPoint().y;

		int halfX = W / 2;
		int halfY = H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

}ST_RegionRotate_Oper, *PST_RegionRotate_Oper;

typedef struct _tag_Rotate_Op
{
	/*ROI 정보*/
	ST_RegionRotate	StdrectData[RegionRotate_MaxEnum];
	ST_RegionRotate	rectData[RegionRotate_MaxEnum];

	// 카메라 상태
	UINT nCameraState;

	// TestMode
	UINT nTestMode;

	// 총 조정 카운트
	UINT nWriteCnt;

	// 이미지센서 1pix 크기
	double dbImageSensorPix;

	// 기준(마스터) 각도
	double dbMasterDegree;

	// 합격 기준 각도
	double dbDeviDegree;

	// 조정 목표 각도
	double dbTargetDegree;

	// 최대 이동 각도
	double dbMaxDegree;

	
	/*변수 초기화 함수*/
	_tag_Rotate_Op()
	{
		nTestMode		= RT_Measurement_TestMode;
		nCameraState	= CAM_STATE_MIRROR;
		dbMasterDegree	= 0.00;
		dbDeviDegree	= 0.00;
		dbImageSensorPix = 0.0;
		nWriteCnt		 = 0;
		dbMaxDegree		 = 5.0;
		dbTargetDegree	 = 0.5;

		for (UINT nIdx = 0; nIdx < RegionRotate_MaxEnum; nIdx++)
		{
			rectData[nIdx] = StdrectData[nIdx];
		}
	};

	/*변수 교환 함수*/
	_tag_Rotate_Op& operator= (_tag_Rotate_Op& ref)
	{
		nCameraState	= ref.nCameraState;
		dbMasterDegree	= ref.dbMasterDegree;
		dbDeviDegree	= ref.dbDeviDegree;
		dbImageSensorPix = ref.dbImageSensorPix;

		nWriteCnt		= ref.nWriteCnt;
		dbMaxDegree		= ref.dbMaxDegree;
		dbTargetDegree	= ref.dbTargetDegree;


		for (int t = 0; t < RegionRotate_MaxEnum; t++)
			rectData[t] = ref.rectData[t];

		return *this;
	};

}ST_Rotate_Op, *PST_Rotate_Op;

typedef struct _tag_Rotate_Data
{
	UINT nResult;
	double dbDegree;

	/*변수 초기화 함수*/
	_tag_Rotate_Data()
	{
		nResult = TR_Init;
		dbDegree = 0.0;
	};

	void reset()
	{
		nResult = TR_Init;
		dbDegree = 0.0;
	};

	/*변수 교환 함수*/
	_tag_Rotate_Data& operator= (_tag_Rotate_Data& ref)
	{
		nResult	 = ref.nResult;
		dbDegree = ref.dbDegree;

		return *this;
	};

}ST_Rotate_Data, *PST_Rotate_Data;

#endif // Def_RotateTest_h__

