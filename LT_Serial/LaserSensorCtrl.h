//*****************************************************************************
// Filename	: 	LaserSensorCtrl.h
// Created	:	2016/5/10 - 15:19
// Modified	:	2016/5/10 - 15:19
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef LaserSensorCtrl_h__
#define LaserSensorCtrl_h__

#pragma once

#include "SerialCom_Base.h"
#include "Define_LaserSensor.h"

using namespace LaserSensor;

//-----------------------------------------------------------------------------
// CLaserSensorCtrl
//-----------------------------------------------------------------------------
class CLaserSensorCtrl : public CSerialCom_Base
{
public:
	CLaserSensorCtrl();
	virtual ~CLaserSensorCtrl();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData(const char* szACK, DWORD dwAckSize);
	virtual void	OnRecvProtocol(const char* szACK, DWORD dwAckSize);

	ST_IL_Protocol_Base		m_stRecvProtocol_Base;

	ST_IL_Protocol_SR		m_stProtocol_SR;

	ST_IL_Protocol_SR_Res	m_stRecvProtocol_SR;

	ST_IL_Protocol_Error	m_stRecvError;

	WORD	m_wRecvCmd;
	BYTE	m_byErrorNo;

public:
	
	BOOL	IsRecvedError					(__out BYTE& byErrorNo);

	// 	센서 앰프 에러 상태
	BOOL	Read_SensorAmpErrorState		(__out WORD& wErrorBit);
	// 	판정 출력·알람 출력
	BOOL	Read_ControlOutput				(__out BYTE& byControlOutputBit);
	// 	측정값(P.V.값)
	BOOL	Read_ComparatorValue			(__out FLOAT& fPV_Value);
	// 	내부 측정값(R.V.값)
	BOOL	Read_RawValue					(__out FLOAT& fRV_Value);
	// 	홀드 시의 피크 홀드값(P.V.값)
	BOOL	Read_PeakValue_Hold				(__out FLOAT& fPV_Value);
	// 	홀드 시의 보텀 홀드값(P.V.값)
	BOOL	Read_BottomValue_Hold			(__out FLOAT& fPV_Value);
	// 	연산값(CALC값)
	BOOL	Read_CalculationDisplayValue	(__out FLOAT& fValue);
	// 	아날로그 출력값
	BOOL	Read_AnalogOutput				(__out FLOAT& fVoltCurr);
	// 	뱅크 상태
	BOOL	Read_BankState					(__out BYTE& byState);
	// 	타이밍 상태
	BOOL	Read_TimingStatus				(__out BYTE& byStatus);
	// 	투광 정지 상태
	BOOL	Read_ProjectionStatus			(__out BYTE& byStatus);
	// 	설정 이상
	BOOL	Read_SettingError				(__out BYTE& byStatus);
	// 	외부 입력 상태
	BOOL	Read_ExternalInputStatus		(__out BYTE& StatusBit);
	// 	EEPROM 입력 결과
	BOOL	Read_EEPROMInputStatus			(__out BYTE& byStatus);
	// 	제로 시프트·제로 시피트 리셋 실행 결과
	BOOL	Read_ZeroShiftResetResult		(__out BYTE& byStatus);
	// 	리셋 실행 결과
	BOOL	Read_ResetResult				(__out BYTE& byStatus);
	// 	시스템 파라미터 현재 상태
	BOOL	Read_SystemParameterStatus		(__out BYTE& StatusBit);
	// 	공차 튜닝, 2점 튜닝 실행 결과
	BOOL	Read_TuningResult				(__out BYTE& byStatus);
	// 	캘리브레이션 실행 결과
	BOOL	Read_CalibrationResult			(__out BYTE& byStatus);
	// 	프로덕트 코드
	BOOL	Read_ProductCode				(__out WORD& wCode);
	// 	접속 센서 헤드
	BOOL	Read_ConnectedSensorHead		(__out WORD& wHead);

	void	Test_Code();
};

#endif // LaserSensorCtrl_h__
