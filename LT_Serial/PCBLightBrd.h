﻿//*****************************************************************************
// Filename	: 	PCBLightBrd.h
// Created	:	2016/03/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PCBLightBrd_h__
#define PCBLightBrd_h__

#pragma once

#include "SerialCom_Base.h"
#include "Define_PCBLightBrd.h"

//=============================================================================
//
//=============================================================================
class CPCBLightBrd : public CSerialCom_Base
{
public:
	CPCBLightBrd();
	virtual ~CPCBLightBrd();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent					(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData		(const char* szACK, DWORD dwAckSize);
	virtual void	OnRecvProtocol			(const char* szACK, DWORD dwAckSize);

	ST_LightBrdProtocol		m_stProtocol;
	ST_LightBrdProtocol_A	m_stProtocol_A;
	ST_LightBrdRecvProtocol	m_stRecvProtocol;	

	void			ResetProtocol();

	FLOAT			m_fMinVoltage;
	FLOAT			m_fMaxVoltage;
	FLOAT			m_fUnitVolt;

public:

	ST_LightBrdRecvProtocol	GetAckProtocol()
	{
		return m_stRecvProtocol;
	};

	BOOL			Send_BoardCheck				(__out BYTE& byPortNo);
	BOOL			Send_OutputVolt				(__in UINT nSlotNo, __in float fVolt);
	BOOL			Send_DetectVolt				(__out ST_SlotVolt& stOutSlotVolt);
	BOOL			Send_OutputCurrent			(__in UINT nSlotNo, __in float fCurrent);

	BOOL			Send_AmbientLightCurrent	(__in UINT nSlotNo, __in WORD wValue);

};

#endif // PCBLightBrd_h__
