﻿//*****************************************************************************
// Filename	: 	Define_PCBLightBrd.h
// Created	:	2016/3/8 - 13:31
// Modified	:	2016/3/8 - 13:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_PCBLightBrd_h__
#define Define_PCBLightBrd_h__

//=============================================================================
// Auto Focus - Pogo pin 컨트롤 보드 통신 프로토콜
//=============================================================================
/*
Transmit from host
<STX><CMD><DATA><ETX>

Response from host
<STX><CMD><DATA><ETX>

Where:
<STX>		= 1 ASCII '!'
<CMD>		= 1 ASCII character
<DATA>		= Command Argument, up to 9 ASCII characters.
<ETX>		= 1 ASCII '@'
*/

#define	LightBrd_STX				0x21	//'!'
#define	LightBrd_ETX				0x40	//'@'
#define LightBrd_DummyChar			0x30	//'0'
#define LightBrd_ProtoLength		23
#define LightBrd_DataLength			20
#define LightBrd_MinCurrent			0		// 0A
#define LightBrd_MaxCurrent			3000	// 4A

// 고정된 통신 프로토콜 선언
static const char* g_szLightBrd_PortCheck		= "!000000000000000000000@";
static const char* g_szLightBrd_OutputVolt_A	= "!V00000000000000000000@";
static const char* g_szLightBrd_OutputVolt_U	= "!U00000000000000000000@";
static const char* g_szLightBrd_OutputCurrent_A	= "!300000000000000000000@";
static const char* g_szLightBrd_OutputCurrent_U	= "!500000000000000000000@";
static const char* g_szLightBrd_DetectVolt		= "!W00000000000000000000@";

// 프로토콜 커맨드 
typedef enum enLightBrdCmd
{
	CMD_PortCheck		= '0',
	CMD_OutputVolt_A	= 'V',
	CMD_OutputVolt_U	= 'U',
	CMD_OutputCurrent_A	= '3',
	CMD_OutputCurrent_U	= '5',
	CMD_DetectVolt		= 'W',
};

typedef enum enSlot
{
	Slot_All,
	Slot_A,
	Slot_B,
	Slot_C,
	Slot_D,
	Slot_E,
	Slot_Max = Slot_E,
};

typedef struct _tag_SlotVolt
{
	float	fVolt[Slot_Max];
	WORD	wCurrent[Slot_Max];

	_tag_SlotVolt()
	{
		for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
		{
			fVolt[nIdx]		= 0.0f;
			wCurrent[nIdx] = 0;
		}
	}
}ST_SlotVolt;

typedef struct _tag_LightBrdProtocol
{
	char		STX;
	char		CMD;
	char		Data[LightBrd_DataLength];
	char		ETX;

	_tag_LightBrdProtocol()
	{
		STX = LightBrd_STX;
		CMD = LightBrd_DummyChar;
		ETX = LightBrd_ETX;
		memset(Data, LightBrd_DummyChar, LightBrd_DataLength);
	};

	void MakeProtocol_OutputVolt(__in UINT nSlotNo, __in float fVolt)
	{
		ASSERT((Slot_All <= nSlotNo) && (nSlotNo <= Slot_E + 1));

		CMD = CMD_OutputVolt_U;
		Data[0] = nSlotNo + 0x30;
		sprintf_s(&(Data[1]), 5, "%04.0f", fVolt * 100.0f);
		Data[5] = LightBrd_DummyChar;
	};

	void MakeProtocol_OutputCurrent(__in UINT nSlotNo, __in float fCurrent)
	{
		ASSERT((Slot_All <= nSlotNo) && (nSlotNo <= Slot_E + 1));

		CMD = CMD_OutputCurrent_U;
		Data[0] = nSlotNo + 0x30;
		sprintf_s(&(Data[1]), 5, "%04.0f", fCurrent * 1.0f);
		Data[5] = LightBrd_DummyChar;
	};

}ST_LightBrdProtocol, *PST_LightBrdProtocol;

typedef struct _tag_LightBrdProtocol_A
{
	char		STX;
	char		CMD;
	char		Data[LightBrd_DataLength];
	char		ETX;

	_tag_LightBrdProtocol_A()
	{
		STX = LightBrd_STX;
		CMD = LightBrd_DummyChar;
		ETX = LightBrd_ETX;
		memset(Data, LightBrd_DummyChar, LightBrd_DataLength);
	};

}ST_LightBrdProtocol_A, *PST_LightBrdProtocol_A;


typedef struct _tag_LightBrdRecvProtocol
{
	char		STX;
	char		CMD;
	CStringA	Data;
	char		ETX;

	CStringA	Protocol;

	ST_SlotVolt RecvedSlotVolt;

	_tag_LightBrdRecvProtocol()
	{

	};

	_tag_LightBrdRecvProtocol& operator= (_tag_LightBrdRecvProtocol& ref)
	{
		STX		= ref.STX;
		CMD		= ref.CMD;
		Data	= ref.Data;
		ETX		= ref.ETX;

		Protocol = ref.Protocol;

		return *this;
	};

	BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
	{
		if (NULL == pszProtocol)
			return FALSE;

		if (nLength != LightBrd_ProtoLength)
			return FALSE;

		CStringA strProtocol = pszProtocol;

		INT_PTR nDataLength = nLength - 3; // STX, ETX, CMD
		INT_PTR nOffset = 0;

		STX		= pszProtocol[nOffset++];
		CMD		= pszProtocol[nOffset++];
		Data	= strProtocol.Mid((int)nOffset++, (int)nDataLength);
		ETX		= pszProtocol[nOffset];

		switch (CMD)
		{
		case CMD_PortCheck:
			AnalyzeData_PortCheck();
			break;

		case CMD_OutputVolt_A:
			break;

		case CMD_OutputVolt_U:
			//AnalyzeData_OutputVolt_U();
			break;

		case CMD_OutputCurrent_A:
			break;

		case CMD_OutputCurrent_U:
			//AnalyzeData_OutputVolt_U();
			break;

		case CMD_DetectVolt:
			AnalyzeData_SlotVolt();
			break;
		}

		// Check STX
		if (LightBrd_STX != STX)
			return FALSE;

		// Check ETX
		if (LightBrd_ETX != ETX)
			return FALSE;

		return TRUE;
	};

	void AnalyzeData_PortCheck()
	{
		;
	};

	void AnalyzeData_SlotVolt()
	{
		CStringA szBuff;

		UINT nOffset = 0;
		for (UINT nSlotIdx = 0; nSlotIdx < Slot_E; nSlotIdx++)
		{
			szBuff = Data.Mid(nOffset, nOffset + 4);

			RecvedSlotVolt.fVolt[nSlotIdx] = (float)atof(szBuff.GetBuffer());
			szBuff.ReleaseBuffer();
		}
	};
}ST_LightBrdRecvProtocol, *PST_LightBrdRecvProtocol;

#endif // Define_PCBLightBrd_h__
