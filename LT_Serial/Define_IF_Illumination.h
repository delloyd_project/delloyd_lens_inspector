﻿//*****************************************************************************
// Filename	: 	Define_IF_Illumination.h
// Created	:	2016/3/8 - 13:31
// Modified	:	2016/3/8 - 13:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_IF_Illumination_h__
#define Define_IF_Illumination_h__

//=============================================================================
// Sekonix - Camera Board 통신 프로토콜
//=============================================================================
/*
Transmit from host
<STX><CMD><DATA><ETX>

Response from host
<STX><CMD><DATA><ETX>

Where:
<STX>		= 1 ASCII '!'
<CMD>		= 1 ASCII character
<DATA>		= Command Argument, up to 9 ASCII characters.
<ETX>		= 1 ASCII '@'
*/
namespace IF_IlluminationBrd
{

#define	IF_IlluminationBrd_STX				0x02	//'!'
#define	IF_IlluminationBrd_ETX				0x03	//'@'
#define IF_IlluminationBrd_DummyChar		0x30	//'0'
#define IF_IlluminationBrd_RecvLength		3
#define IF_IlluminationBrd_ProtoLength		6
#define IF_IlluminationBrd_DataLength		3

	// 고정된 통신 프로토콜 선언
// 	static const char* g_szIF_IlluminationBrd_BoardCheck		= "!000000000@";
// 	static const char* g_szIF_IlluminationBrd_SetVoltage		= "!V00000000@";
// 	static const char* g_szIF_IlluminationBrd_SetVoltReset		= "!X00000000@";	// Voltage 입력
// 	static const char* g_szIF_IlluminationBrd_GetInCurrent		= "!400000000@";
// 	static const char* g_szIF_IlluminationBrd_ChkCanOpenShort	= "!200000000@";
// 	static const char* g_szIF_IlluminationBrd_OptCenter			= "!O00000000@";	// Offset X, Y 입력
// 	static const char* g_szIF_IlluminationBrd_OverlayOn			= "!E10000000@";
// 	static const char* g_szIF_IlluminationBrd_OverlayOff		= "!E00000000@";
// 	
// 	static const char* g_szIF_IlluminationBrd_ChkOpenShort		= "!Y00000000@";
// 	static const char* g_szIF_IlluminationBrd_ChkVoltage		= "!900000000@";

	// 프로토콜 커맨드 
	typedef enum enIF_IlluminationBrdCmd
	{

	};

	typedef struct _tag_IF_IlluminationBrdProtocol
	{
		char		STX;
		char		CMD;
		char		Data[IF_IlluminationBrd_DataLength];
		char		ETX;

		_tag_IF_IlluminationBrdProtocol()
		{
			STX = IF_IlluminationBrd_STX;
			CMD = IF_IlluminationBrd_DummyChar;
			ETX = IF_IlluminationBrd_ETX;
			memset(Data, IF_IlluminationBrd_DummyChar, IF_IlluminationBrd_DataLength);
		};
// 
		void MakeProtocol_SetBrightness(__in UINT nCH, __in UINT nBrightness)
		{
			CMD = '1' + nCH;

			sprintf_s(Data, 4, "%03d", nBrightness);
			Data[3] = IF_IlluminationBrd_DummyChar;
			ETX = IF_IlluminationBrd_ETX;
		};

		void MakeProtocol_SetOnOff(__in UINT nCH, __in BOOL bMode)
		{
			
			CMD = '1' + nCH;

			if (bMode == TRUE)
			{
				Data[0] = '1' + nCH;
				Data[1] = 'O';
				Data[2] = 'N';
			}
			else{
				Data[0] = 'O';
				Data[1] = 'F';
				Data[2] = 'F';
			}
		};

		void MakeProtocol_GetBrightness(__in UINT nCH)
		{

			CMD = '1' + nCH;

			
			Data[0] = 'R';
			Data[1] = 'E';
			Data[2] = 'T';
			
		};


		void MakeProtocol_GetOnOff(__in UINT nCH)
		{

			CMD = '1' + nCH;


			Data[0] = '1' + nCH;
			Data[1] = 'R';
			Data[2] = 'T';

		};



 	}ST_IF_IlluminationBrdProtocol, *PST_IF_IlluminationBrdProtocol;
 
 	typedef struct _tag_IF_IlluminationBrdRecvProtocol
 	{
		char		STX;
		char		CMD;
		CStringA	Data;
		char		ETX;

		CStringA	Protocol;
		long		lCurrent;
		BOOL		bPass;
	
		UINT		nCH;
		UINT		nBrightness;
		BOOL		bOnOffMode;

		_tag_IF_IlluminationBrdRecvProtocol()
		{
			lCurrent = 0;
			bPass	 = FALSE;
		};
 
		_tag_IF_IlluminationBrdRecvProtocol& operator= (_tag_IF_IlluminationBrdRecvProtocol& ref)
		{
			STX = ref.STX;
			CMD = ref.CMD;
			Data = ref.Data;
			ETX = ref.ETX;

			Protocol = ref.Protocol;

			return *this;
		};
// 
 		BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
 		{
 			if (NULL == pszProtocol)
 				return FALSE;
 
			if (!(nLength == IF_IlluminationBrd_ProtoLength || nLength == IF_IlluminationBrd_RecvLength))
 				return FALSE;
 
 			CStringA strProtocol = pszProtocol;
 

			INT_PTR nOffset =0;

			if (nLength == IF_IlluminationBrd_RecvLength)
			{

				STX = pszProtocol[nOffset++];
				CMD = pszProtocol[nOffset++];
				ETX = pszProtocol[nOffset];

				bPass = FALSE;
				if (CMD == 0x06)
				{
					bPass = TRUE;
				}
				if (CMD == 0x15)
				{
					bPass = FALSE;
				}
			}
			else{
				STX = pszProtocol[nOffset++];
				CMD = pszProtocol[nOffset++];
				Data = strProtocol.Mid((int)nOffset++, 3);
				ETX = pszProtocol[nOffset];


				nCH = CMD - '0';
				CStringA szdata;
				CStringA szBuff;
				szBuff = Data.Left(3);

				szdata.Format("%dON", nCH);
				if (szdata == szBuff)
				{
					bOnOffMode = TRUE;
					return TRUE;
				}
				szdata.Format("%dOF", nCH);
				if (szdata == szBuff)
				{
					bOnOffMode = FALSE;
					return TRUE;
				}


				
				nBrightness = atoi(szBuff);


				

			}

 			
 			
 
//  			switch (CMD)
//  			{
//  			case CMD_BoardCheck:
//  				AnalyzeData_BoardCheck();
//  				break;
//  
//  			case CMD_SetVoltage:
//  				AnalyzeData_SetVolt();
//  				break;
//  
//  			case CMD_GetInCurrent:
//  				AnalyzeData_Current();
//  				break;
//  
//  			case CMD_ChkCanOpenShort:
//  				AnalyzeData_ChkCanOpenShort();
//  				break;
//  
//  			case CMD_OptCenterOffset:
//  				AnalyzeData_SetOptCenterOffset();
//  				break;
//  
//  			case CMD_Overlay:
//  				AnalyzeData_Overlay();
//  				break;
//  
//  			case CMD_ChkOpenShort:
//  				AnalyzeData_ChkOpenShort();
//  				break;
//  
//  			case CMD_ChkVoltage:
//  				AnalyzeData_ChkVoltage();
//  				break;
//  			}	
 
 			// Check STX
 			if (IF_IlluminationBrd_STX != STX)
 				return FALSE;
 
 			// Check ETX
 			if (IF_IlluminationBrd_ETX != ETX)
 				return FALSE;
 
 			return TRUE;
 		};
// 
// 		void AnalyzeData_BoardCheck()
// 		{
// 			Data.ReleaseBuffer();
// 		};
// 
// 		void AnalyzeData_SetVolt()
// 		{
// 			Data.ReleaseBuffer();
// 		};
// 
// 		void AnalyzeData_Current()
// 		{
// 			CStringA szBuff;
// 
// 			szBuff = Data.Left(5);
// 			lCurrent = atol(szBuff);
// 		};
// 
// 		void AnalyzeData_ChkCanOpenShort()
// 		{
// 			CStringA szBuff;
// 			szBuff = Data.Left(1);
// 			if (1 == atoi(szBuff))
// 			{
// 				bPass = TRUE;
// 			}
// 			else
// 			{
// 				bPass = FALSE;
// 			}
// 		};
// 
// 		void AnalyzeData_SetOptCenterOffset()
// 		{
// 			CStringA szBuff;
// 			szBuff = Data.Left(1);
// 			if (1 == atoi(szBuff))
// 			{
// 				bPass = TRUE;
// 			}
// 			else
// 			{
// 				bPass = FALSE;
// 			}
// 		};
// 
// 		void AnalyzeData_Overlay()
// 		{
// 			CStringA szBuff;
// 			szBuff = Data.Left(1);
// 			if (1 == atoi(szBuff))
// 			{
// 				bPass = TRUE;
// 			}
// 			else
// 			{
// 				bPass = FALSE;
// 			}
// 		};
// 
// 		void AnalyzeData_ChkOpenShort()
// 		{
// 			CStringA szBuff;
// 			szBuff = Data.Left(1);
// 			if (1 == atoi(szBuff))
// 			{
// 				bPass = TRUE;
// 			}
// 			else
// 			{
// 				bPass = FALSE;
// 			}
// 		};
// 
// 		void AnalyzeData_ChkVoltage()
// 		{
// 			CStringA szBuff;
// 
// 			szBuff = Data.Left(4);
// 			lVpp = atol(szBuff);
// 
// 			szBuff = Data.Mid(4, 4);
// 			lVsync = atol(szBuff);
// 		};

	}ST_IF_IlluminationBrdRecvProtocol, *PST_IF_IlluminationBrdRecvProtocol;

};

#endif // Define_IF_Illumination_h__
