//*****************************************************************************
// Filename	: 	LaserSensorCtrl.cpp
// Created	:	2016/5/10 - 15:18
// Modified	:	2016/5/10 - 15:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "LaserSensorCtrl.h"
#include "ErrorCode.h"

using namespace PR_ErrorCode;

CLaserSensorCtrl::CLaserSensorCtrl()
{
	SetDelimeter(0, 0x0A, LaserSensor::MIN_Protocol_Length, LaserSensor::MAX_Protocol_Length);

	m_wRecvCmd = CMD_Unknown;
}

CLaserSensorCtrl::~CLaserSensorCtrl()
{

}

//=============================================================================
// Method		: OnFilterRecvData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: const char * szACK
// Parameter	: DWORD dwAckSize
// Qualifier	:
// Last Update	: 2016/5/11 - 16:33
// Desc.		:
//=============================================================================
LRESULT CLaserSensorCtrl::OnFilterRecvData(const char* szACK, DWORD dwAckSize)
{
	if (NULL == szACK)
	{
		ASSERT(_T("NULL == szACK"));
		m_dwErrCode = ERROR_INVALID_PARAMETER;
		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	ASSERT(0 != dwAckSize);

	DWORD_PTR	dwQueueSize = 0;
	DWORD_PTR	dwDataLength = 0;
	INT_PTR		iFindETXIndex = 0;

	//ResetAckBuffer();
	m_dwACKBufSize = 0;

	//-----------------------------------------------------
	// Data가 들어오면 우선 큐에 집어 넣는다?
	//-----------------------------------------------------
	m_SerQueue.Push(szACK, dwAckSize);

	//-----------------------------------------------------
	// LF 를 찾는다.
	//-----------------------------------------------------
	dwQueueSize = m_SerQueue.GetSize();

	// 프로토콜 사이즈보다 작으면 데이터가 들어오길 기다린다.
	if (dwQueueSize < (DWORD)m_nMinProtocolLength)
	{
//		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	iFindETXIndex = 0;
	while (0 < (iFindETXIndex = m_SerQueue.FindDelimiter(m_chETX, iFindETXIndex)))
	{
		// LF 위치가 프로토콜 마지막
		dwDataLength = (DWORD)iFindETXIndex + 1;
		m_SerQueue.PopData(m_szACKBuf, (DWORD)iFindETXIndex + 1);
		break;
	}

	if (iFindETXIndex <= 0) //LF 를 못찾으면 데이터가 중간에 나뉘어 들어온걸로 판단함
	{
		// LF 까지 검색한 데이터 사이즈가 프로토콜 사이즈보다 넘어가면 다음 데이터가 들어오면 처리
		if (dwQueueSize < (DWORD)m_nMaxProtocolLength)
		{
			//TRACE(_T("FilterAckData() : %d\n"), __LINE__);
			return FALSE;
		}
		else // 아니면 에러 처리
		{
			m_SerQueue.Empty();
			TRACE(_T("FilterAckData() : %d\n"), __LINE__);
			return FALSE;
		}
	}

	m_dwACKBufSize = dwDataLength;
	m_szACKBuf[m_dwACKBufSize] = 0x00;
	OnRecvProtocol(m_szACKBuf, m_dwACKBufSize);

	// 이벤트 핸들이 설정되어있다면 이벤트를 발생시킨다.
	if (NULL != m_hEvent_ACK)
		SetEvent(m_hEvent_ACK);

	TRACE(_T("FilterAckData : Complete Ack Protocol\n"));
// 	if (NULL != m_hOwnerWnd)
// 	{
// 		::SendNotifyMessage(m_hOwnerWnd, m_WM_Ack, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
// 	}

	//-----------------------------------------------------
	// 큐 비우기
	//-----------------------------------------------------
	m_SerQueue.Empty();
	m_dwErrCode = NO_ERROR;
	return TRUE;
}

void CLaserSensorCtrl::OnRecvProtocol(const char* szACK, DWORD dwAckSize)
{
	BOOL bReturn = TRUE;
	WORD wCmd = 0;
	memcpy(&wCmd, szACK, 2);
	m_wRecvCmd = wCmd;

	CStringA strErr;
	switch (wCmd)
	{
	case CMD_Read_SR:
		bReturn = m_stRecvProtocol_SR.SetRecvProtocol(szACK, dwAckSize);
		break;

	case CMD_Read_M0:
		break;

	case CMD_Read_MS:
		break;

	case CMD_Read_DRQinput:
		break;

	case CMD_Write_SW:
		break;

	case CMD_Write_AW:
		break;

	case CMD_Error:
		bReturn = m_stRecvError.SetRecvProtocol(szACK, dwAckSize);
		m_byErrorNo = m_stRecvError.byErrorNo;
// 		memcpy(&m_stRecvError, szACK, sizeof(ST_IL_Protocol_Error));
// 		strErr.Empty();
// 		strErr.AppendChar(m_stRecvError.ErrorNo[0]);
// 		strErr.AppendChar(m_stRecvError.ErrorNo[1]);
// 		m_byErrorNo = (BYTE)atoi(strErr.GetBuffer());
// 		strErr.ReleaseBuffer();
		
		break;

	default:	// Protocol Error
		bReturn = m_stRecvProtocol_Base.SetRecvProtocol(szACK, dwAckSize);
		break;
	}
}

//=============================================================================
// Method		: IsRecvedError
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byErrorNo
// Qualifier	:
// Last Update	: 2016/5/19 - 7:30
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::IsRecvedError(__out BYTE& byErrorNo)
{
	if (CMD_Error == m_wRecvCmd)
	{
		byErrorNo = m_byErrorNo;
		return TRUE;
	}
	else
	{
		byErrorNo = 0;
		return FALSE;
	}
}

//=============================================================================
// Method		: Read_SensorAmpErrorState
// Access		: public  
// Returns		: BOOL
// Parameter	: __out WORD & wErrorBit
// Qualifier	:
// Last Update	: 2016/5/11 - 16:30
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_SensorAmpErrorState(__out WORD& wErrorBit)
{
	TRACE(_T("CLaserSensorCtrl::Read_SensorAmpErrorState \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	memcpy (m_stProtocol_SR.DataNo, g_DataNumber[DN_SensorAmplifierErrorState].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				wErrorBit = atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				wErrorBit = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Sensor Amp Error State ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Sensor Amp Error State ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Sensor Amp Error State ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------
	return bReturn;
}

//=============================================================================
// Method		: Read_ControlOutput
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byControlOutputBit
// Qualifier	:
// Last Update	: 2016/5/19 - 7:28
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ControlOutput(__out BYTE& byControlOutputBit)
{
	TRACE(_T("CLaserSensorCtrl::Read_ControlOutput \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

// 	m_stProtocol_SR.DataNo[0] = '0';
// 	m_stProtocol_SR.DataNo[1] = '3';
// 	m_stProtocol_SR.DataNo[2] = '6';

	memcpy (m_stProtocol_SR.DataNo, g_DataNumber[DN_ControlOutput].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byControlOutputBit = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byControlOutputBit = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Control Output ACK : Error Command Recieved!\n"));
// 				CString strText;
// 				strText.Format(_T("Error : %d"), m_byErrorNo);
// 				AfxMessageBox(strText);
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Control Output ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Control Output ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ComparatorValue
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fPV_Value
// Qualifier	:
// Last Update	: 2016/5/19 - 7:28
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ComparatorValue(__out FLOAT& fPV_Value)
{
	TRACE(_T("CLaserSensorCtrl::Read_ComparatorValue \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '3';
	// 	m_stProtocol_SR.DataNo[2] = '7';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ComparatorValue].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fPV_Value = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);

				if (fPV_Value < -16)
				{
					fPV_Value = 0.0f;
				}
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fPV_Value = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Comparator Value ACK : Error Command Recieved!\n"));
// 				CString strText;
// 				strText.Format(_T("Error : %d"), m_byErrorNo);
// 				AfxMessageBox(strText);
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Comparator Value ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Comparator Value ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_RawValue
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fRV_Value
// Qualifier	:
// Last Update	: 2016/5/19 - 7:29
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_RawValue(__out FLOAT& fRV_Value)
{
	TRACE(_T("CLaserSensorCtrl::Read_RawValue \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '3';
	// 	m_stProtocol_SR.DataNo[2] = '8';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_RawValue].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fRV_Value = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fRV_Value = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Raw Value ACK : Error Command Recieved!\n"));
// 				CString strText;
// 				strText.Format(_T("Error : %d"), m_byErrorNo);
// 				AfxMessageBox(strText);
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Raw Value ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Raw Value ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_PeakValue_Hold
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fPV_Value
// Qualifier	:
// Last Update	: 2016/5/19 - 7:38
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_PeakValue_Hold(__out FLOAT& fPV_Value)
{
	TRACE(_T("CLaserSensorCtrl::Read_PeakValue_Hold \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '3';
	// 	m_stProtocol_SR.DataNo[2] = '9';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_PeakValue_DuringSamplingPeriod].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fPV_Value = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fPV_Value = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Peak Value for Hold ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Peak Value for Hold ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Peak Value for Hold ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_BottomValue_Hold
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fPV_Value
// Qualifier	:
// Last Update	: 2016/5/19 - 7:38
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_BottomValue_Hold(__out FLOAT& fPV_Value)
{
	TRACE(_T("CLaserSensorCtrl::Read_BottomValue_Hold \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '4';
	// 	m_stProtocol_SR.DataNo[2] = '0';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_BottomValue_DuringSamplingPeriod].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fPV_Value = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fPV_Value = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Bottom Value for Hold ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Bottom Value for Hold ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Bottom Value for Hold ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_CalculationDisplayValue
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fValue
// Qualifier	:
// Last Update	: 2016/5/19 - 7:38
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_CalculationDisplayValue(__out FLOAT& fValue)
{
	TRACE(_T("CLaserSensorCtrl::Read_CalculationDisplayValue \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '4';
	// 	m_stProtocol_SR.DataNo[2] = '1';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_CalculationDisplayValue].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fValue = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fValue = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Calculation Display Value ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Calculation Display Value ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Calculation Display Value ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_AnalogOutput
// Access		: public  
// Returns		: BOOL
// Parameter	: __out FLOAT & fVoltCurr
// Qualifier	:
// Last Update	: 2016/5/19 - 7:38
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_AnalogOutput(__out FLOAT& fVoltCurr)
{
	TRACE(_T("CLaserSensorCtrl::Read_AnalogOutput \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '4';
	// 	m_stProtocol_SR.DataNo[2] = '2';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_AnalogOutput].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				fVoltCurr = (FLOAT)atof(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				fVoltCurr = 0.0f;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Analog Output ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Analog Output ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Analog Output ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------
	return bReturn;
}

//=============================================================================
// Method		: Read_BankState
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byState
// Qualifier	:
// Last Update	: 2016/5/19 - 7:39
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_BankState(__out BYTE& byState)
{
	TRACE(_T("CLaserSensorCtrl::Read_BankState \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '4';
	// 	m_stProtocol_SR.DataNo[2] = '3';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_BankState].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byState = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byState = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Bank State ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Bank State ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Bank State ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_TimingStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_TimingStatus(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_TimingStatus \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '4';
	// 	m_stProtocol_SR.DataNo[2] = '4';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_TimingStatus].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Timing Status ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Timing Status ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Timing Status ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ProjectionStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ProjectionStatus(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_ProjectionStatus \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '0';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_StopStatus].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Projection Status ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Projection Status ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Projection Status ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_SettingError
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_SettingError(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_SettingError \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '1';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_SettingError].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Setting Error ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Setting Error ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Setting Error ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ExternalInputStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & StatusBit
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ExternalInputStatus(__out BYTE& StatusBit)
{
	TRACE(_T("CLaserSensorCtrl::Read_ExternalInputStatus \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '2';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ExternalInputStatus].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				StatusBit = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				StatusBit = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read External Input Status ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read External Input Status ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read External Input Status ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_EEPROMInputStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_EEPROMInputStatus(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_EEPROMInputStatus \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '3';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_EEPROMInputResult].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read EEPROM Input Status ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read EEPROM Input Status ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read EEPROM Input Status ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ZeroShiftResetResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ZeroShiftResetResult(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_ZeroShiftResetResult \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '4';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ZeroShiftResetResult].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Zero Shift Reset Result ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Zero Shift Reset Result ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Zero Shift Reset Result ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ResetResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:06
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ResetResult(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_ResetResult \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '5';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ResetResult].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Reset Result ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("ReadRead Reset Result ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Reset Result ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_SystemParameterStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & StatusBit
// Qualifier	:
// Last Update	: 2016/5/19 - 10:07
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_SystemParameterStatus(__out BYTE& StatusBit)
{
	TRACE(_T("CLaserSensorCtrl::Read_SystemParameterStatus \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '5';
	// 	m_stProtocol_SR.DataNo[2] = '6';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_SystemParameterStatus].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				StatusBit = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				StatusBit = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read System Parameter Status ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read System Parameter Status ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read System Parameter Status ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_TuningResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:07
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_TuningResult(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_TuningResult \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '6';
	// 	m_stProtocol_SR.DataNo[2] = '0';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_TuningResult].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Tuning Result ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Tuning Result ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Tuning Result ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_CalibrationResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __out BYTE & byStatus
// Qualifier	:
// Last Update	: 2016/5/19 - 10:07
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_CalibrationResult(__out BYTE& byStatus)
{
	TRACE(_T("CLaserSensorCtrl::Read_CalibrationResult \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '0';
	// 	m_stProtocol_SR.DataNo[1] = '6';
	// 	m_stProtocol_SR.DataNo[2] = '1';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_CalibrationResult].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				byStatus = (BYTE)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				byStatus = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Calibration Result ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Calibration Result ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Calibration Result ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ProductCode
// Access		: public  
// Returns		: BOOL
// Parameter	: __out WORD & wCode
// Qualifier	:
// Last Update	: 2016/5/19 - 10:07
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ProductCode(__out WORD& wCode)
{
	TRACE(_T("CLaserSensorCtrl::Read_ProductCode \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '1';
	// 	m_stProtocol_SR.DataNo[1] = '9';
	// 	m_stProtocol_SR.DataNo[2] = '3';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ProductCode].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				wCode = (WORD)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				wCode = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Product Code ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Product Code ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Product Code ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

//=============================================================================
// Method		: Read_ConnectedSensorHead
// Access		: public  
// Returns		: BOOL
// Parameter	: __out WORD & wHead
// Qualifier	:
// Last Update	: 2016/5/19 - 10:07
// Desc.		:
//=============================================================================
BOOL CLaserSensorCtrl::Read_ConnectedSensorHead(__out WORD& wHead)
{
	TRACE(_T("CLaserSensorCtrl::Read_ConnectedSensorHead \n"));
	if (!IsOpen())
		return FALSE;

	if (WAIT_OBJECT_0 != LockSemaphore()) //-----------------------------------
		return FALSE;

	BOOL	bReturn = FALSE;

	// 	m_stProtocol_SR.DataNo[0] = '1';
	// 	m_stProtocol_SR.DataNo[1] = '9';
	// 	m_stProtocol_SR.DataNo[2] = '5';

	memcpy(m_stProtocol_SR.DataNo, g_DataNumber[DN_ConnectedSensorHead].chDataNo, DataNo_Length);

	if (TransferStart((char*)&m_stProtocol_SR, sizeof(ST_IL_Protocol_SR)))
	{
		if (WaitACK(m_dwAckWaitTime))
		{
			if (CMD_Read_SR == m_wRecvCmd)
			{
				wHead = (WORD)atoi(m_stRecvProtocol_SR.szRecvData);
			}
			else if (CMD_Error == m_wRecvCmd)
			{
				wHead = 0;

				m_dwErrCode = ERR_SERIAL_ACK;
				bReturn = FALSE;
				TRACE(_T("Read Connected Sensor Head ACK : Error Command Recieved!\n"));
			}

			m_dwErrCode = NO_ERROR;
			bReturn = TRUE;
			TRACE(_T("Read Connected Sensor Head ACK OK\n"));
		}
		else
		{
			m_dwErrCode = ERR_SERIAL_ACK;
			bReturn = FALSE;
			TRACE(_T("Read Connected Sensor Head ACK ERROR\n"));
		}
	}
	else
	{
		m_dwErrCode = ERR_SERIAL_TRANSFER;
		bReturn = FALSE;
	}

	UnlockSemaphore(); //------------------------------------------------------

	return bReturn;
}

void CLaserSensorCtrl::Test_Code()
{
// 	char szTest[] = "ER,SR";
// 	DWORD dwSize = sizeof(szTest) - 1;
// 	
// 	OnFilterRecvData(szTest, dwSize);
// 
// 	char szTest2[] = ",01\r\n";
// 	DWORD dwSize2 = sizeof(szTest) - 1;
// 
// 	OnFilterRecvData(szTest2, dwSize2);



	ST_IL_Protocol_SR_Res stSRRes;




}
