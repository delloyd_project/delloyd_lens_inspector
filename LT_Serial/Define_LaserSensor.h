//*****************************************************************************
// Filename	: 	Define_LaserSensor.h
// Created	:	2016/5/10 - 14:47
// Modified	:	2016/5/10 - 14:47
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_LaserSensor_h__
#define Define_LaserSensor_h__

// Keyence IL Seriese (CMOS Multi-Function Analog Laser Sensor)

/*
Read ----------------------------------------------------------------
 * 설정한 센서 앰프로부터의 판독(SR 커맨드)
 S R , IDNo(2Byte) , DataNo(3Byte) CR LF
 S R , IDNo(2Byte) , DataNo(3Byte), Data(n Byte) CR LF
 E R , S R , ErrorNo(2byte) CR LF

 * 모든 앰프로부터의 데이터 일괄 판독(M0(엠제로) 커맨드)
 M O CR LF
 M O , ID:00의 센서 앰프 데이터 , ID:01의 센서 앰프 데이터 , ... , ID:Last의 센서 앰프 데이터 CR LF
 E R , M O , ErrorNo(2byte) CR LF

 * 모든 앰프로부터의 제어 출력 상태와 데이터 일제 판독(MS 커맨드)
 M S CR LF
 M S , 제어출력(2Byte) , ID:00의 센서 앰프 데이터 , .... , 제어출력(2Byte) , ID:Last의 센서 앰프 데이터 CR LF
 E R , M S , ErrorNo(2byte) CR LF

 * DRQ 단자로의 입력에 의한 판독(DRQ 판독)
 D R , 제어출력(2Byte) , ID:00의 센서 앰프 데이터 , .... , 제어출력(2Byte) , ID:Last의 센서 앰프 데이터 CR LF
 E R , D R , ErrorNo(2byte) CR LF

Write ---------------------------------------------------------------
 * 지정한 센서 앰프로의 입력(SW 커맨드)
 S W , IDNo(2Byte) , DataNo(3Byte) , 데이터(n Byte) CR LF
 S W , IDNo(2Byte) , DataNo(3Byte) CR LF
 E R , S W , ErrorNo(2byte) CR LF

 * 모든 센서 앰프로의 일괄 입력(AW 커맨드)
 A W , DataNo(3Byte) , 데이터(n Byte) CR LF
 A W , DataNo(3Byte) CR LF
 E R , A W , ErrorNo(2byte) CR LF

*/


namespace LaserSensor
{

	typedef enum ConstVar
	{
		Delimeter_Comma			= ',',
		Delimeter_CR			= 0x0D,
		Delimeter_LF			= 0x0A,

		MAX_SensorAmpID_Cnt		= 8,	// 0 ~ 7

		Delimeter_Length		= 1,
		Command_Length			= 2,
		DataNo_Length			= 3,
		IDNo_Length				= 2,
		ControlOutput_Length	= 2,
		ErrorNo_Length			= 2,
		EndDelimeter_Length		= 2,	// CR, LF

		MAX_SR_Cmd_Dataz_Length	= 16,	

		MAX_SensorAmpDataLength	= 10,	// 최대 10 문자
		MAX_ALLDatalenght		= MAX_SensorAmpDataLength * MAX_SensorAmpID_Cnt,
		MAX_AllCtrlOutputLength	= (ControlOutput_Length + MAX_SensorAmpDataLength) * MAX_SensorAmpID_Cnt,
		MAX_Protocol_Length		= MAX_AllCtrlOutputLength + 5, // 		
		MAX_WriteData_Length	= 7,

		MIN_Protocol_Length		= 4,	// Command(2Byte) + CR + LF
		MIN_RecvProtocol_Length	= 8,	// Command(2Byte) + , + DataNo(3Byte) + CR + LF

		ErrorProtocol_Lenght	= 10,	// Command(2Byte) + , + RecvCommand(2Byte) + , + Error No(2Byte) + CR + LF
	};

	typedef struct _tag_IL_Protocol_Base
	{
		char		Command[Command_Length];
		CStringA	Dataz;
		char		CR;						// 0x0D
		char		LF;						// 0x0A

		CStringA	Protocol;

		_tag_IL_Protocol_Base()
		{
			Command[0]	= 0x00;
			Command[1]	= 0x00;			
			CR			= 0x0D;
			LF			= 0x0A;
		};

		virtual void Init()
		{
			Protocol.Empty();

			Command[0] = 0x00;
			Command[1] = 0x00;
			CR = 0x0D;
			LF = 0x0A;
		};
		
		virtual CStringA MakeProtocol()
		{
			Init();

			Protocol.AppendChar(Command[0]);
			Protocol.AppendChar(Command[1]);
			Protocol.Append(Dataz);
			Protocol.AppendChar(CR);
			Protocol.AppendChar(LF);

			return Protocol;
		};

		virtual BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
		{
			if (NULL == pszProtocol)
				return FALSE;

			if (nLength < MIN_Protocol_Length)
				return FALSE;

			if (MAX_Protocol_Length < nLength)
				return FALSE;

			Init();

			Protocol = pszProtocol;

			INT_PTR nDataLength = nLength - 4; // Command(2Byte), CR, LF
			INT_PTR nOffset = 0;

			Command[0] = pszProtocol[nOffset++];
			Command[1] = pszProtocol[nOffset++];
			CR = pszProtocol[nLength - 2];
			LF = pszProtocol[nLength - 1];

			Dataz = Protocol.Mid(nOffset++, nDataLength);

			if (FALSE == OnAnalyzeRecvData())
				return FALSE;

			// Check CR
			if (0x0D != CR)
				return FALSE;

			// Check LF
			if (0x0A != LF)
				return FALSE;

			return TRUE;
		};

		virtual BOOL OnAnalyzeRecvData()
		{
			return TRUE;
		};

	}ST_IL_Protocol_Base;

	typedef struct _tag_IL_Protocol_Error : public ST_IL_Protocol_Base
	{
		//char	Command[Command_Length];	// "ER"
		char		Delimeter_1;			// 0x2C
		CStringA	szRecvCommand;
		char		Delimeter_2;			// 0x2C
		CStringA	szErrorNo;
		//char	CR;							// 0x0D
		//char	LF;							// 0x0A

		BYTE		byIDNo;
		BYTE		byErrorNo;

		virtual void Init()
		{
			__super::Init();

			szRecvCommand.Empty();
			szErrorNo.Empty();			
		};

		virtual BOOL OnAnalyzeRecvData()
		{
			if (FALSE == __super::OnAnalyzeRecvData())
				return FALSE;

			if (ErrorProtocol_Lenght < Dataz.GetLength())
				return FALSE;

			UINT nOffset = 0;
			Delimeter_1 = Dataz.GetAt(nOffset++);

			szRecvCommand.AppendChar(Dataz.GetAt(nOffset++));
			szRecvCommand.AppendChar(Dataz.GetAt(nOffset++));

			Delimeter_2 = Dataz.GetAt(nOffset++);

			szErrorNo.Empty();
			szErrorNo.AppendChar(Dataz.GetAt(nOffset++));
			szErrorNo.AppendChar(Dataz.GetAt(nOffset++));

			byIDNo = (BYTE)atoi(szRecvCommand.GetBuffer());
			szRecvCommand.ReleaseBuffer();

			byErrorNo = (BYTE)atoi(szErrorNo.GetBuffer());
			szErrorNo.ReleaseBuffer();

			return TRUE;
		}
	}ST_IL_Protocol_Error;

	typedef struct _tag_IL_Protocol_SR
	{
		char	Command[Command_Length];	// "SR"
		char	Delimeter_1;				// 0x2C
		char	ID_No[IDNo_Length];
		char	Delimeter_2;				// 0x2C
		char	DataNo[DataNo_Length];
		char	CR;							// 0x0D
		char	LF;							// 0x0A

		_tag_IL_Protocol_SR()
		{
			Command[0]	= 'S';
			Command[1]	= 'R';
			Delimeter_1 = Delimeter_Comma;
			ID_No[0]	= '0';
			ID_No[1]	= '0';
			Delimeter_2 = Delimeter_Comma;
			DataNo[0]	= '0';
			DataNo[1]	= '0';
			DataNo[2]	= '0';
			CR			= Delimeter_CR;
			LF			= Delimeter_LF;
		};
	}ST_IL_Protocol_SR;

	typedef struct _tag_IL_Protocol_SR_Res : public ST_IL_Protocol_Base
	{
		// Command "SR"
		char		Delimeter_1;				// 0x2C
		CStringA	szID_No;
		char		Delimeter_2;				// 0x2C
		CStringA	szDataNo;
		char		Delimeter_3;				// 0x2C
		CStringA	szRecvData;					// 최대 10문자

		BYTE		byIDNo;
		WORD		wDataNo;

		virtual void Init()
		{
			__super::Init();

			szID_No.Empty();
			szDataNo.Empty();
			szRecvData.Empty();
		};

		virtual BOOL OnAnalyzeRecvData()
		{
			if (FALSE == __super::OnAnalyzeRecvData())
				return FALSE;

			if (MAX_SR_Cmd_Dataz_Length < Dataz.GetLength())
				return FALSE;

			UINT nOffset = 0;
			Delimeter_1 = Dataz.GetAt(nOffset++);
			
			szID_No.AppendChar(Dataz.GetAt(nOffset++));
			szID_No.AppendChar(Dataz.GetAt(nOffset++));

			Delimeter_2 = Dataz.GetAt(nOffset++);

			szDataNo.AppendChar(Dataz.GetAt(nOffset++));
			szDataNo.AppendChar(Dataz.GetAt(nOffset++));
			szDataNo.AppendChar(Dataz.GetAt(nOffset++));

			Delimeter_3 = Dataz.GetAt(nOffset++);

			szRecvData = Dataz.Mid(nOffset, Dataz.GetLength() - nOffset);

			byIDNo = (BYTE)atoi(szID_No.GetBuffer());
			szID_No.ReleaseBuffer();

			wDataNo = (WORD)atoi(szDataNo.GetBuffer());
			szDataNo.ReleaseBuffer();

			return TRUE;
		};

	}ST_IL_Protocol_SR_Res;

	typedef struct _tag_IL_Protocol_M0_MS
	{
		char	Command[Command_Length];	// "M0", "MS"
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_M0_MS;

	typedef struct _tag_IL_Protocol_M0_MS_Res
	{
		char	Command[Command_Length];	// "M0", "MS"
		char	Delimeter_1;				// 0x2C
		char*	Data;
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_M0_MS_Res;

	typedef struct _tag_IL_Protocol_SW
	{
		char	Command[Command_Length];	// "SW"
		char	Delimeter_1;				// 0x2C
		char	ID_No[IDNo_Length];
		char	Delimeter_2;				// 0x2C
		char	DataNo[DataNo_Length];
		char	Delimeter_3;				// 0x2C
		char*	Dataz;						// 최대 7 Byte
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_SW;

	typedef struct _tag_IL_Protocol_SW_Res
	{
		char	Command[Command_Length];	// "SW"
		char	Delimeter_1;				// 0x2C
		char	ID_No[IDNo_Length];
		char	Delimeter_2;				// 0x2C
		char	DataNo[DataNo_Length];
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_SW_Res;

	typedef struct _tag_IL_Protocol_AW
	{
		char	Command[Command_Length];	// "AW"
		char	Delimeter_1;				// 0x2C
		char	DataNo[DataNo_Length];
		char	Delimeter_2;				// 0x2C
		char*	Dataz;						// 최대 7 Byte
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_AW;

	typedef struct _tag_IL_Protocol_AW_Res
	{
		char	Command[Command_Length];	// "AW"
		char	Delimeter_1;				// 0x2C
		char	DataNo[DataNo_Length];
		char	CR;							// 0x0D
		char	LF;							// 0x0A
	}ST_IL_Protocol_AW_Res;

	// Command ----------------------------------------------------------------
	typedef enum enCommandIndex
	{
		CMDI_Read_SR,		// 지정하는 ID 번호의 센서 앰프로부터 데이터 번호로 지정한 데이터를 판독합니다
		CMDI_Read_M0,		// DL-RS1A가 모든 센서 앰프로부터 정기적으로 받아들이는 판정값(P.V. 값)을 일괄하여 판독합니다
		CMDI_Read_MS,		// DL-RS1A가 모든 센서 앰프로부터 정기적으로 받아들이는 판정 출력과 알람 출력의 ON/OFF 상태 및 판정값(P.V.값)을 일괄하여 판독합니다
		CMDI_Read_DRQinput,	// DRQ 입력이 ON이 되면, DL-RS1A가 모든 센서 앰프로부터 정기적으로 받아들이는 판정 출력과 알람 출력의 ON/OFF 상태 및 판정값(P.V.값)을 일괄하여 판독합니다.
		CMDI_Write_SW,		// 지정하는 ID 번호의 센서 앰프에 데이터 번호로 지정한 설정 데이터를 입력합니다
		CMDI_Write_AW,		// 데이터 번호로 지정한 설정 데이터를 모든 센서 앰프에 기입합니다
		CMDI_Error,
	};

	static const char* g_szCommand[] =
	{
		"SR",
		"M0",
		"MS",
		"DR",
		"SW",
		"AW",
		"ER",
	};

	typedef enum enCommand_Code
	{
		CMD_Read_SR			= 0x5253,	// SR
		CMD_Read_M0			= 0x304D,	// M0
		CMD_Read_MS			= 0x534D,	// MS
		CMD_Read_DRQinput	= 0x5244,	// DR
		CMD_Write_SW		= 0x5753,	// SW
		CMD_Write_AW		= 0x5741,	// AW
		CMD_Error			= 0x5245,	// ER

		CMD_Unknown	= 0,
	};

	/* 판정 출력 - Control output ----------------------------------------------
	0 : HIGH 판정 출력 OFF,  1 : HIGH 판정 출력 ON
	0 : LOW 판정 출력 OFF,  1 : LOW 판정 출력 ON
	0 : GO 판정 출력 OFF,  1 : GO 판정 출력 ON
	0 : 알람 출력 ON,  1 : 알람 출력 OFF
	*/
	typedef enum ControlOutput_bit
	{
		CO_NO_HIGH_ON		= 0,
		CO_NO_LOW_ON,
		CO_NO_GO_ON,
		CO_NO_Alarm_OFF,

		CO_NC_HIGH_OFF		= 0,
		CO_NC_LOW_OFF,
		CO_NC_GO_OFF,
		CO_NC_Alarm_OFF,
	};

	//Sensor amplifier errors -------------------------------------------------
	typedef enum SensorAmplifierErrors_BitOffset
	{
		SAE_Overcurrent_Error,	//과전류 에러
		SAE_EEPROM_Error,		//EEPROM 에러
		SAE_Head_Error,			//헤드 에러
		SAE_NotUse_03,			//미사용
		SAE_NotUse_04,			//미사용
		SAE_NotUse_05,			//미사용
		SAE_NotUse_06,			//미사용
		SAE_Laser_Error,		//투광기 레이저 에러
		SAE_Format_Error,		//형식 불일치 에러
		SAE_NotUse_09,			//미사용
		SAE_NotUse_10,			//미사용
		SAE_Communication_Error,//앰프 간 통신 에러
		SAE_NumberOfUnits_Error,//유닛 대수 에러
		SAE_Calculation_Error,	//연산 에러
	};

	static LPCTSTR g_szSensorError[] =
	{
		_T("과전류 에러"),
		_T("EEPROM 에러"),
		_T("헤드 에러"),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T("투광기 레이저 에러"),
		_T("형식 불일치 에러"),
		_T(""),
		_T(""),
		_T("앰프 간 통신 에러"),
		_T("유닛 대수 에러"),
		_T("연산 에러"),
		NULL
	};

	// Error Number -----------------------------------------------------------
	typedef enum ErrorNumber_Index
	{
		EN_InvalidCommand_Error,
		EN_CommunicationTimeout_Error,
		EN_DataLength_Error,
		EN_NumberOfParameters_Error,
		EN_Parameter_Error,
		EN_Communication_Error,
		EN_IDNumber_Error,
		EN_ExpansionLine_Error,
		EN_WriteControl_Error,
	};

	typedef struct _tag_ErrorNumber
	{
		BYTE		byErrorNo;
		char		chErrorNo[ErrorNo_Length];
		LPCTSTR		szDesc_Ko;
		LPCTSTR		szDesc_En;
	}ST_ErrorNumber;
	
	static const ST_ErrorNumber g_ErrorNumber[] =
	{
		{ 00,	{'0', '0'},	_T("부정 커맨드 에러"),		_T("Invalid command error") },
		{ 02,	{'0', '2'},	_T("통신 타임 아웃 에러"),	_T("Communication timeout error") },
		{ 20,	{'2', '0'},	_T("데이터 길이 에러"),		_T("Data length error") },
		{ 21,	{'2', '1'},	_T("파라미터 수 에러"),		_T("Number of parameters error") },
		{ 22,	{'2', '2'},	_T("파라미터 에러"),			_T("Parameter error") },
		{ 29,	{'2', '9'},	_T("통신 에러"),				_T("Communication error") },
		{ 65,	{'6', '5'},	_T("ID 에러"),				_T("ID number error") },
		{ 66,	{'6', '6'},	_T("증설 라인 에러"),		_T("Expansion line error") },
		{ 67,	{'6', '7'},	_T("입력 제어 에러"),		_T("Write control error") },
		NULL
	};

	// Data Number ------------------------------------------------------------
	typedef enum DataNumber_ReadOnly
	{
		DN_SensorAmplifierErrorState		=  0,
		DN_ControlOutput					=  1,
		DN_ComparatorValue					=  2,
		DN_RawValue							=  3,
		DN_PeakValue_DuringSamplingPeriod	=  4,
		DN_BottomValue_DuringSamplingPeriod =  5,
		DN_CalculationDisplayValue			=  6,
		DN_AnalogOutput						=  7,
		DN_BankState						=  8,
		DN_TimingStatus						=  9,
		DN_StopStatus						= 10,
		DN_SettingError						= 11,
		DN_ExternalInputStatus				= 12,
		DN_EEPROMInputResult				= 13,
		DN_ZeroShiftResetResult				= 14,
		DN_ResetResult						= 15,
		DN_SystemParameterStatus			= 16,
		DN_TuningResult						= 17,
		DN_CalibrationResult				= 18,
		DN_ProductCode						= 19,
		DN_ConnectedSensorHead				= 20,
	};

	typedef struct _tag_DataNumber
	{
		BYTE		byDataNo;
		char		chDataNo[DataNo_Length];
		LPCTSTR		szDesc_Ko;
	}ST_DataNumber;

	static const ST_DataNumber g_DataNumber[] =
	{
		{  33, { '0', '3', '3' }, _T("센서 앰프 에러 상태") },
		{  36, { '0', '3', '6' }, _T("판정 출력·알람 출력") },
		{  37, { '0', '3', '7' }, _T("측정값(P.V.값)") },
		{  38, { '0', '3', '8' }, _T("내부 측정값(R.V.값)") },
		{  39, { '0', '3', '9' }, _T("홀드 시의 피크 홀드값(P.V.값)") },
		{  40, { '0', '4', '0' }, _T("홀드 시의 보텀 홀드값(P.V.값)") },
		{  41, { '0', '4', '1' }, _T("연산값(CALC값)") },
		{  42, { '0', '4', '2' }, _T("아날로그 출력값") },
		{  43, { '0', '4', '3' }, _T("뱅크 상태") },
		{  44, { '0', '4', '4' }, _T("타이밍 상태") },
		{  50, { '0', '5', '0' }, _T("투광 정지 상태") },
		{  51, { '0', '5', '1' }, _T("설정 이상") },
		{  52, { '0', '5', '2' }, _T("외부 입력 상태") },
		{  53, { '0', '5', '3' }, _T("EEPROM 입력 결과") },
		{  54, { '0', '5', '4' }, _T("제로 시프트·제로 시피트 리셋 실행 결과") },
		{  55, { '0', '5', '5' }, _T("리셋 실행 결과") },
		{  56, { '0', '5', '6' }, _T("시스템 파라미터 현재 상태") },
		{  60, { '0', '6', '0' }, _T("공차 튜닝, 2점 튜닝 실행 결과") },
		{  61, { '0', '6', '1' }, _T("캘리브레이션 실행 결과") },
		{ 193, { '1', '9', '3' }, _T("프로덕트 코드") },
		{ 195, { '1', '9', '5' }, _T("접속 센서 헤드") },
		NULL
	};

	

	/* Read
	033	센서 앰프 에러 상태 - Sensor amplifier error state
	036	판정 출력·알람 출력
	037	측정값(P.V.값) - Comparator value (P.V. value)
	038	내부 측정값(R.V.값) - Raw value (R.V. value)
	039	홀드 시의 피크 홀드값(P.V.값) - Peak value during sampling period
	040	홀드 시의 보텀 홀드값(P.V.값) - Bottom value during sampling period
	041	연산값(CALC값) - Calculation display value
	042	아날로그 출력값
	043	뱅크 상태
	044	타이밍 상태 - Timing status
	050	투광 정지 상태
	051	설정 이상
	052	외부 입력 상태
	053	EEPROM 입력 결과
	054	제로 시프트·제로 시피트 리셋 실행 결과
	055	리셋 실행 결과
	056	시스템 파라미터 현재 상태
	060	공차 튜닝, 2점 튜닝 실행 결과
	061	캘리브레이션 실행 결과
	193	프로덕트 코드
	195	접속 센서 헤드
	*/

	/*
	0 : 샘플링 중
	1 : 비 샘플링 중

	0 : 투광중
	1 : 투광 정지중

	0 : 설정 정상
	1 : 설정 이상

	0 : 실행 중
	1 : 정상 종료
	2 : 이상 종료

	기본 유닛 : 4022
	증설 유닛 : 4023

	0000 : 접속되어 있지 않음
	0001 : IL-030
	0002 : IL-065
	0003 : IL-100
	0004 : IL-300
	0005 : IL-600
	0106 : IL-S025
	0107 : IL-S065
	0208 : IL-S100
	0311 : IL-2000
	*/

	/* Read & Write
	001	제로 시프트 실행 요구
	002	제로 시프트 리셋 실행 요구
	003	리셋 요구
	005	이니셜 리셋 요구
	006	시스템 파라미터 세트 요구
	014	공차 튜닝 요구
	015	2점 튜닝, HIGH 측 1점째 결정 조작 요구
	016	2점 튜닝, HIGH 측 2점째 결정 조작 요구 (HIGH 측 설정값 결정)
	017	2점 튜닝, LOW 측 2점째 결정 조작 요구
	018	2점 튜닝, LOW 측 2점째 결정 조작 요구 (LOW 측 설정값 결정)
	019	캘리브레이션, SET1 결정 조작 요구
	020	캘리브레이션, SET2 결정 조작 요구, (캘리브레이션 실행)
	021	연산값 2점 캘리브레이션 SET1, 결정 조작 요구
	022	연산값 2점 캘리브레이션 SET2, 결정 조작 요구(연산값 2점 캘리브레이션 실행)
	023	연산값 3점 캘리브레이션 SET1, 결정 조작 요구
	024	연산값 3점 캘리브레이션 SET2, 결정 조작 요구
	025	연산값 3점 캘리브레이션 SET3, 결정 조작 요구(연산값 3점 캘리브레이션 실행)
	026 단차 카운트 필터 1점 튜닝 요구
	027	단차 카운트 필터 2점 튜닝, 1점째 결정 조작 요구
	028	단차 카운트 필터 2점 튜닝, 2점째 결정 조작 요구 (HIGH 측/LOW 측 설정값 결정)
	065	HIGH 측 설정값(BANK 0)
	066	LOW 측 설정값(BANK 0)
	067	시프트 목표값 (BANK 0)
	068	아날로그 출력 상한값(BANK 0)
	069	아날로그 출력 하한값(BANK 0)
	070	HIGH 측 설정값(BANK 1)
	071	LOW 측 설정값(BANK 1)
	072	시프트 목표값(BANK 1)
	073	아날로그 출력 상한값(BANK 1)
	074	아날로그 출력 하한값(BANK 1)
	075	HIGH 측 설정값(BANK 2)
	076	LOW 측 설정값(BANK 2)
	077	시프트 목표값(BANK 2)
	078	아날로그 출력 상한값(BANK 2)
	079	아날로그 출력 하한값(BANK 2)
	080	HIGH 측 설정값(BANK 3)
	081	LOW 측 설정값(BANK 3)
	082	시프트 목표값(BANK 3)
	083	아날로그 출력 상한값(BANK 3)
	084	아날로그 출력 하한값(BANK 3)
	097	키 록 기능
	098	뱅크 기능
	099	타이밍 입력
	100	레이저 투광 정지 입력
	104	서브 표시부의 표시 화면
	105	시스템 파라미터 설정
	106	공차 튜닝 공차의 설정폭
	107	캘리브레이션 기능
	108	캘리브레이션 기능 SET1
	109	캘리브레이션 기능 SET2
	110	연산값 캘리브레이션 기능
	111	연산값 2점 캘리브레이션 기능 SET1
	112	연산값 2점 캘리브레이션 기능 SET2
	113	연산값 3점 캘리브레이션 기능 SET1
	114	연산값 3점 캘리브레이션 기능 SET3
	129	연산 기능
	131	측정 방향
	132	샘플링 주기
	133	평균 횟수 · 단차 카운트 필터 ·하이패스 필터
	134	출력 양식
	136	홀드 기능의 설정
	137	오토 피크 홀드 또는 오토 보텀 트리거 레벨
	138	타이밍 설정
	139	딜레이 타이머
	140	타이머 시간
	141	히스테리시스
	142	아날로그 출력 스케일링
	143	아날로그 출력 상한값
	144	아날로그 출력 하한값
	145	외부 입력
	146	외부 입력1
	147	외부 입력2
	148	외부 입력3
	149	외부 입력4
	150	뱅크 전환 방법
	152	제로 시프트값 기억 기능
	153	상호 간섭 방지 기능
	154	표시 자릿수
	155	전력 절약 모드
	156	헤드 표시 모드
	157	표시색
	158	단차 카운트 필터의 원 숏 출력 시간
	159	하이패스 필터의 컷 오프 주파수
	161	알람 설정
	162	알람 횟수

	*/


	

	

	/*
	0 : 언록
	1 : 키 록

	0 : 타이밍 입력 OFF
	1 : 타이밍 입력 ON

	0 : 투광 정지 입력 OFF
	1 : 투광 정지 입력 ON

	0 : R.V.값
	1 : 아날로그값
	2 : HI 설정값
	3 : LO 설정값
	4 : 제로 시프트값
	5 : CALC값

	0 : 초기 설정
	1 : 유저 설정

	0 : 초기 설정
	1 : 연산 2점 캘리브레이션
	2 : 연산 3점 캘리브레이션

	0 : OFF
	1 : 가산
	2 : 감산

	0 : nor
	1 : rEv

	0 : 초기 설정
	1 : 0.33ms
	2 : 1ms
	3 : 2ms
	4 : 5ms

	0 : 1회
	1 : 2회
	2 : 4회
	3 : 8회
	4 : 16회
	5 : 32회
	6 : 64회
	7 : 128회
	8 : 256회
	9 : 512회
	10 : 1024회
	11 : 2048회
	12 : 4096회
	13 : 단차 카운트 필터
	14 : 하이패스 필터

	0 : N.O.
	1 : N.C.

	0 : 샘플 홀드
	1 : 피크 홀드
	2 : 보텀 홀드
	3 : 피크 to 피크 홀드
	4 : 오토 피크 홀드
	5 : 오토 보텀 홀드

	0 : 레벨
	1 : 에지

	0 : OFF
	1 : 온 딜레이
	2 : 오프 딜레이
	3 : 원숏

	0 : 초기 상태
	1 : 프리 레인지
	2 : 뱅크

	0 : 초기 상태
	1 : 유저 설정

	0 : 제로 시프트 입력
	1 : 뱅크A 입력
	2 : 뱅크B 입력
	3 : 레이저 투광 정지 입력
	4 : 사용하지 않는다

	0 : 리셋 입력
	1 : 뱅크A 입력
	2 : 뱅크B 입력
	3 : 레이저 투광 정지 입력
	4 : 사용하지 않는다

	0 : 타이밍 입력
	1 : 뱅크A 입력
	2 : 뱅크B 입력
	3 : 레이저 투광 정지 입력
	4 : 사용하지 않는다

	0 : 사용하지 않는다
	1 : 뱅크A 입력
	2 : 뱅크B 입력
	3 : 레이저 투광 정지 입력

	0 : 버튼
	1 : 외부 입력

	0 : OFF
	1 : ON

	0 : 간섭 방지 OFF
	1 : 간섭 방지 ON

	0 : 초기 설정
	2 : 0.001
	3 : 0.01
	4 : 0.1
	5 : 1

	0 : OFF
	1 : HALF
	2 : ALL

	0 : 초기 상태
	1 : OK/NG 표시
	2 : OFF

	0 : GO 녹색
	1 : GO 적색
	2 : 항상 적색
	
	0 : 0.1Hz
	1 : 0.2Hz
	2 : 0.5Hz
	3 : 1Hz
	4 : 2Hz
	5 : 5Hz
	6 : 10Hz
	7 : 20Hz
	8 : 50Hz
	9 : 100Hz

	0 : 초기 상태
	1 : 클램프
	2 : 유저 설정

	*/
};


#endif // Define_LaserSensor_h__
