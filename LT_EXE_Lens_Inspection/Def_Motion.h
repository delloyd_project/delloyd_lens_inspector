﻿// *****************************************************************************
// Filename	: 	Def_Motion.h
// Created	:	2016/10/03 - 16:40
// Modified	:	2016/10/03 - 16:40
// 
// Author	:	KHO
// 	
// Purpose	:	
// *****************************************************************************
// #ifndef Def_Motion_h__
// #define Def_Motion_h__
// 
// #include "Def_Motor.h"
// 
// #define DI_Port_FixedCount_Max	32
// #define DO_Port_FixedCount_Max	32
// 
// typedef enum enIO_TowerLamp
// {
// 	TowerLampRed = 0,
// 	TowerLampYellow,
// 	TowerLampGreen,
// 	TowerLampMax
// };
// 
// =============================================================================
// IO Table, Define Name
// =============================================================================
// typedef enum enIO_In_BitOffset
// {
// 	DI_EMO ,
// 	DI_MainPowerBtn,
// 	DI_NotUseBit_2,
// 	DI_DoorSensor,
// 	DI_AirSensor,
// 	DI_NotUseBit_5,
// 	DI_StartBtn,
// 	DI_StopBtn,
// 	DI_ModuleFixBtn,
// 	DI_ModuleUnFixBtn,
// 	DI_DriverInBtn,
// 	DI_DriverOutBtn,
// 	DI_PCBFixBtn,
// 	DI_PCBUnFixBtn,
// 	DI_NotUseBit_14,
// 	DI_NotUseBit_15,
// 	DI_ModuleFixCYLSensor,
// 	DI_ModuleUnfixCYLSensor,
// 	DI_DriverInCYLSensor,
// 	DI_DriverOutCYLSensor,
// 	DI_PCBUnFixCYLSensor,
// 	DI_PCBFixCYLSensor,
// 	DI_NotUseBit_22,
// 	DI_NotUseBit_23,
// 	DI_NotUseBit_24,
// 	DI_NotUseBit_25,
// 	DI_NotUseBit_26,
// 	DI_NotUseBit_27,
// 	DI_NotUseBit_28,
// 	DI_NotUseBit_29,
// 	DI_NotUseBit_30,
// 	DI_FailBoxSensor,
// 	DI_NotUseBit_Max,
// 
// 
// 	DI_EMO = 00,
// 	DI_MainPowerBtn,
// 	DI_ModuleFixBtn,
// 	DI_ModuleUnFixBtn,
// 	DI_PCBFixBtn,
// 	DI_PCBUnFixBtn,
// 	DI_DriverInBtn,
// 	DI_DriverOutBtn,
// 	DI_StartBtn,
// 	DI_StopBtn,
// 	DI_NotUseBit_10,
// 	DI_NotUseBit_11,
// 	DI_NotUseBit_12,
// 	DI_NotUseBit_13,
// 	DI_NotUseBit_14,
// 	DI_NotUseBit_15,
// 	DI_NotUseBit_16,
// 	DI_NotUseBit_17,
// 	DI_NotUseBit_18,
// 	DI_ModuleFixCYLSensor,
// 	DI_PCBUnFixCYLSensor,
// 	DI_PCBFixCYLSensor,
// 	DI_DriverInCYLSensor,
// 	DI_DriverOutCYLSensor,
// 	DI_NotUseBit_24,
// 	DI_NotUseBit_25,
// 	DI_NotUseBit_26,
// 	DI_NotUseBit_27,
// 	DI_NotUseBit_28,
// 	DI_NotUseBit_29,
// 	DI_NotUseBit_30,
// 	DI_NotUseBit_31,
// 	DI_NotUseBit_Max,
// };
// 
// =============================================================================
// IO Table, UI View
// =============================================================================
// static LPCTSTR g_lpszDI_bit_Desc[] =
// {
// 	_T(" 00. MAIN POWER			       "),		//01
// 	_T(" 01. EMO				       "),		//00
// 	_T(" 02.					       "),		//02
// 	_T(" 03. DOOR SENSOR		       "),		//03
// 	_T(" 04.					       "),		//04
// 	_T(" 05.					       "),		//05
// 	_T(" 06. START BUTTON			   "),		//08
// 	_T(" 07. STOP BUTTON			   "),		//09
// 	_T(" 08. PCB FIX BUTTON		       "),		//06
// 	_T(" 09. PCB UNFIX BUTTON	       "),		//07
// 	_T(" 10. DRIVER IN BUTTON	 	   "),		//10
// 	_T(" 11. DRIVER OUT BUTTON	 	   "),		//11
// 	_T(" 12. PCB FIX BUTTON	           "),		//12
// 	_T(" 13. PCB UNFIX BUTTON		   "),		//13
// 	_T(" 14. 					       "),		//14
// 	_T(" 15. 					       "),		//15
// 	_T(" 16. MODULE FIX CYL SENSOR 	   "),
// 	_T(" 17. MODULE UnFIX CYL SENSOR   "),
// 	_T(" 18. DRIVER IN CYL SENSOR	   "),
// 	_T(" 19. DRIVER OUT CYL SENSOR     "),
// 	_T(" 20. PCB UNFIX CYL SENSOR      "),
// 	_T(" 21. PCB FIX CYL SENSOR	       "),
// 	_T(" 22.						   "),
// 	_T(" 23.						   "),
// 	_T(" 24. 					       "),
// 	_T(" 25.					       "),
// 	_T(" 26.					       "),
// 	_T(" 27.					       "),
// 	_T(" 28. 					       "),
// 	_T(" 29.					       "),
// 	_T(" 30.					       "),
// 	_T(" 31. FAIL BOX SENSOR	       "),
// 
// 
// 	_T(" 00. EMO				       "),		//00
// 	_T(" 01. MAIN POWER			       "),		//01
// 	_T(" 02. MODULE FIX BUTTON	       "),		//02
// 	_T(" 03. MODULE UNFIX BUTTON       "),		//03
// 	_T(" 04. PCB FIX BUTTON		       "),		//04
// 	_T(" 05. PCB UNFIX BUTTON	       "),		//05
// 	_T(" 06. DRIVER IN BUTTON	       "),		//06
// 	_T(" 07. DRIVER OUT BUTTON	       "),		//07
// 	_T(" 08. START LEFT BUTTON	       "),		//08
// 	_T(" 09. START RIGHT / STOP BUTTON "),		//09
// 	_T(" 10. 					       "),		//10
// 	_T(" 11.					       "),		//11
// 	_T(" 12.            		       "),		//12
// 	_T(" 13.					       "),		//13
// 	_T(" 14.					       "),		//14
// 	_T(" 15.					       "),		//15
// 	_T(" 16. 					       "),
// 	_T(" 17. 					       "),
// 	_T(" 18.						   "),
// 	_T(" 19. MODULE FIX CYL SENSOR     "),
// 	_T(" 20. PCB UNFIX CYL SENSOR      "),
// 	_T(" 21. PCB FIX CYL SENSOR	       "),
// 	_T(" 22. DRIVER IN CYL SENSOR      "),
// 	_T(" 23. DRIVER OUT CYL SENSOR     "),
// 	_T(" 24. 					       "),
// 	_T(" 25.					       "),
// 	_T(" 26.					       "),
// 	_T(" 27.					       "),
// 	_T(" 28. 					       "),
// 	_T(" 29.					       "),
// 	_T(" 30.					       "),
// 	_T(" 31.					       "),
// 
// 	NULL
// };
// 
// =============================================================================
// IO Table, Define Name
// =============================================================================
// typedef enum enIO_Out_BitOffset
// {
// 	DO_NotUseBit_0 = 00,
// 	DO_NotUseBit_1,
// 	DO_ChartLight,
// 	DO_NotUseBit_3,
// 	DO_NotUseBit_4,
// 	DO_NotUseBit_5,
// 	DO_StartBtnLamp,
// 	DO_StopBtnLamp,
// 	DO_ModuleFixBtnLamp,
// 	DO_ModuleUnFixBtnLamp,
// 	DO_DriverInBtnLamp,
// 	DO_DriverOutBtnLamp,
// 	DO_PCBFixBtnLamp,
// 	DO_PCBUnFixBtnLamp,
// 	DO_NotUseBit_14,
// 	DO_NotUseBit_15,
// 	DO_ModuleFixCYL,
// 	DO_ModuleUnFixCYL,
// 	DO_DriverInCYL,
// 	DO_DriverOutCYL,
// 	DO_PCBUnFixCYL,
// 	DO_PCBFixCYL,
// 	DO_NotUseBit_22,
// 	DO_NotUseBit_23,
// 	DO_NotUseBit_24,
// 	DO_NotUseBit_25,
// 	DO_NotUseBit_26,
// 	DO_NotUseBit_27,
// 	DO_TowerLampRed,
// 	DO_TowerLampYellow,
// 	DO_TowerLampGreen,
// 	DO_TowerLampBuzzer,
// 	DO_NotUseBit_Max,
// 
// 	DO_NotUseBit_0 = 00,
// 	DO_NotUseBit_1,
// 	DO_ModuleFixBtnLamp,
// 	DO_ModuleUnFixBtnLamp,
// 	DO_PCBUnFixBtnLamp,
// 	DO_PCBFixBtnLamp,
// 	DO_DriverInBtnLamp,
// 	DO_DriverOutBtnLamp,
// 	DO_StartBtnLamp,
// 	DO_StopBtnLamp,
// 	DO_NotUseBit_10,
// 	DO_NotUseBit_11,
// 	DO_NotUseBit_12,
// 	DO_NotUseBit_13,
// 	DO_NotUseBit_14,
// 	DO_NotUseBit_15,
// 	DO_NotUseBit_16,
// 	DO_NotUseBit_17,
// 	DO_ModuleFixCYL,
// 	DO_ModuleUnFixCYL,
// 	DO_PCBUnFixCYL,
// 	DO_PCBFixCYL,
// 	DO_DriverInCYL,
// 	DO_DriverOutCYL,
// 	DO_NotUseBit_24,
// 	DO_NotUseBit_25,
// 	DO_NotUseBit_26,
// 	DO_NotUseBit_27,
// 	DO_TowerLampRed,
// 	DO_TowerLampYellow,
// 	DO_TowerLampGreen,
// 	DO_TowerLampBuzzer,
// 	DO_NotUseBit_Max,
// };
// 
// =============================================================================
// IO Table, UI View
// =============================================================================
// static LPCTSTR g_lpszDO_bit_Desc[] =
// {
// 	_T(" 00. 							 "),		//00
// 	_T(" 01. 							 "),		//01
// 	_T(" 02. CHART LIGHT				 "),		//02
// 	_T(" 03.							 "),		//03
// 	_T(" 04.							 "),		//05
// 	_T(" 05.							 "),		//04
// 	_T(" 06. START BTN LAMP				 "),		//06
// 	_T(" 07. STOP BTN LAMP				 "),		//07
// 	_T(" 08. MODULE FIX BTN LAMP		 "),		//08
// 	_T(" 09. MODULE UNFIX BTN LAMP		 "),		//10
// 	_T(" 10. DRIVER IN BTN LAMP			 "),		//09
// 	_T(" 11. DRIVER OUT BTN LAMP		 "),		//11
// 	_T(" 12. PCB FIX BTN LAMP			 "),		//12
// 	_T(" 13. PCB UNFIX BTN LAMP			 "),		//13
// 	_T(" 14.							 "),		//14
// 	_T(" 15. 							 "),		//15
// 
// 	_T(" 16. MODULE FIX CYLINDER		 "),
// 	_T(" 17. MODULE UNFIX CYLINDER		 "),
// 	_T(" 18. DRIVER IN CYLINDER			 "),
// 	_T(" 19. DRIVER OUT CYLINDER		 "),
// 	_T(" 20. PCB UNFIX CYLINDER			 "),
// 	_T(" 21. PCB FIX CYLINDER			 "),
// 	_T(" 22.							 "),
// 	_T(" 23.							 "),
// 	_T(" 24. 							 "),
// 	_T(" 25.							 "),
// 	_T(" 26.							 "),
// 	_T(" 27.							 "),
// 	_T(" 28. TOWER LAMP RED				 "),
// 	_T(" 29. TOWER LAMP YELLOW			 "),
// 	_T(" 30. TOWER LAMP GREEN			 "),
// 	_T(" 31. TOWER LAMP BUZZER			 "),
// 
// 	_T(" 00. 							 "),		//00
// 	_T(" 01. 							 "),		//01
// 	_T(" 02. MODULE FIX BTN LAMP		 "),		//02
// 	_T(" 03. MODULE UNFIX BTN LAMP		 "),		//03
// 	_T(" 04. PCB UNFIX BTN LAMP			 "),		//05
// 	_T(" 05. PCB FIX BTN LAMP			 "),		//04
// 	_T(" 06. DRIVER IN BTN LAMP			 "),		//06
// 	_T(" 07. DRIVER OUT BTN LAMP		 "),		//07
// 	_T(" 08. START BTN LAMP				 "),		//08
// 	_T(" 09. STOP BTN LAMP				 "),		//10
// 	_T(" 10.							 "),		//09
// 	_T(" 11.							 "),		//11
// 	_T(" 12.							 "),		//12
// 	_T(" 13.							 "),		//13
// 	_T(" 14.							 "),		//14
// 	_T(" 15. 							 "),		//15
// 
// 	_T(" 16. 							 "),
// 	_T(" 17. 							 "),
// 	_T(" 18. MODULE FIX CYLINDER		 "),
// 	_T(" 19. MODULE UNFIX CYLINDER		 "),
// 	_T(" 20. PCB UNFIX CYLINDER			 "),
// 	_T(" 21. PCB FIX CYLINDER			 "),
// 	_T(" 22. DRIVER IN CYLINDER			 "),
// 	_T(" 23. DRIVER OUT CYLINDER		 "),
// 	_T(" 24. 							 "),
// 	_T(" 25.							 "),
// 	_T(" 26.							 "),
// 	_T(" 27.							 "),
// 	_T(" 28. TOWER LAMP RED				 "),
// 	_T(" 29. TOWER LAMP YELLOW			 "),
// 	_T(" 30. TOWER LAMP GREEN			 "),
// 	_T(" 31. TOWER LAMP BUZZER			 "),
// 
// 	NULL
// };
// 
// TEACH
// typedef enum enMotor_TeachItem
// {
// 	MT_1 = 0,
// 	MT_MaxTeachItem, 
// };
// 
// static LPCTSTR g_lpszMotor_Teach[] =
// {
// 	_T(" 01. "),	
// 	NULL
// };
// 
// 
// 
// #endif // Def_Motion_h__
