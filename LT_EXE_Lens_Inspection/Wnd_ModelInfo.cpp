﻿//*****************************************************************************
// Filename	: 	Wnd_ModelInfo.cpp
// Created	:	2016/7/5 - 16:18
// Modified	:	2016/7/5 - 16:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_ModelInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ModelInfo.h"
#include "CommonFunction.h"

// CWnd_ModelInfo


enum enVideo_Id{
	IDC_STATIC_VIDEO = 1000,

};

IMPLEMENT_DYNAMIC(CWnd_ModelInfo, CWnd)

CWnd_ModelInfo::CWnd_ModelInfo()
{
}

CWnd_ModelInfo::~CWnd_ModelInfo()
{
}


BEGIN_MESSAGE_MAP(CWnd_ModelInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_ModelInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
int CWnd_ModelInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	if (!m_grid_ModelInfo.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 11))
	{
		TRACE("m_grid_ModelInfo 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_Group.SetTitle(L"MODEL INFO");

	if (!m_Group.Create(_T("MODEL INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
void CWnd_ModelInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 10;

	int iLeft = iSpacing;
	int iTop = 26;
	int iWidth = cx - iSpacing - iSpacing;
	int iHeight = cy - iTop - iSpacing;
	
	m_Group.MoveWindow(0, 0, cx, cy);

	m_grid_ModelInfo.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
BOOL CWnd_ModelInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRedrawControl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/17 - 19:30
// Desc.		:
//=============================================================================
void CWnd_ModelInfo::OnRedrawControl()
{
	
}
