﻿#ifndef List_RotateOp_h__
#define List_RotateOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_RtOp
{
	RtOp_Object,
	RtOp_PosX,
	RtOp_PosY,
	RtOp_Width,
	RtOp_Height,
	RtOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_RtOp[] =
{
	_T(""),
	_T("PosX"),
	_T("PosY"),
	_T("Width"),
	_T("Height"),
	NULL
};

static const TCHAR*	g_lpszItem_RtOp[] =
{
	_T("Left   ▲"),
	_T("Right ▲"),
	_T("Left   ▼"),
	_T("Right ▼"),
	NULL
};

const int	iListAglin_RtOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_RtOp[] =
{
	95,
	95,
	95,
	95,
	95,
};
// List_CurrentInfo

class CList_RotateOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_RotateOp)

public:
	CList_RotateOp();
	virtual ~CList_RotateOp();

	ST_LT_TI_Rotate		*m_pstRotate;

	void SetPtr_Rotate(ST_LT_TI_Rotate *pstRotate)
	{
		if (pstRotate == NULL)
			return;

		m_pstRotate = pstRotate;
	};

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);

protected:
	CFont	m_Font;

	DECLARE_MESSAGE_MAP()

	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;
	BOOL		UpdateCellData(UINT nRow, UINT nCol, int  iValue);

	BOOL		UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL		CheckRectValue(__in const CRect* pRegionz);

	afx_msg void	OnEnKillFocusERtOpellEdit();

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	BOOL Change_DATA_CHECK	(UINT nIdx);
};


#endif // List_CurrentInfo_h__
