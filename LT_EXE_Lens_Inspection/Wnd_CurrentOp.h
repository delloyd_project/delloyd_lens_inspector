﻿#ifndef Wnd_CurrentOp_h__
#define Wnd_CurrentOp_h__

#pragma once

#include "List_CurrentOp.h"
#include "Def_DataStruct.h"

// CWnd_CurrentOp

class CWnd_CurrentOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CurrentOp)

public:
	CWnd_CurrentOp();
	virtual ~CWnd_CurrentOp();

	CFont			m_font_Data;

	CList_CurrentOp m_ListCurrOp;
	ST_ModelInfo	*m_pstModelInfo;

	CButton			m_bn_Test;

	void SetUI		();
	void GetUI		();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

protected:

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnBnClickedBnTest();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg int	 OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	
};

#endif // Wnd_CurrentOp_h__
