﻿//*****************************************************************************
// Filename	: Wnd_MainView.cpp
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// Wnd_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MainView.h"
#include "resource.h"
#include "Dlg_ChkPassword.h"

//=============================================================================
// CWnd_MainView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MainView, CWnd_BaseView)

CWnd_MainView::CWnd_MainView()
{
	m_InspMode		= Permission_Operator;
	m_pstInspInfo	= NULL;
	m_pDevice		= NULL;
}

CWnd_MainView::~CWnd_MainView()
{
	TRACE(_T("<<< Start ~CWnd_MainView >>> \n"));
}

BEGIN_MESSAGE_MAP(CWnd_MainView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE	()
END_MESSAGE_MAP()


//=============================================================================
// CWnd_MainView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_MainView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	CStringW strText;

	m_wnd_SiteInfo.SetOwner(GetParent());
	m_wnd_SiteInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 100);

	m_wnd_ModelInfo.SetOwner(GetParent());
	m_wnd_ModelInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 101);

	m_wnd_LotInfo.SetOwner(GetParent());
	m_wnd_LotInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 102);

	m_wnd_TestInfo.SetOwner(GetParent());
	m_wnd_TestInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 103);

	m_wnd_MasterSet.SetOwner(GetParent());
	m_wnd_MasterSet.Create(NULL, _T(""), dwStyle, rectDummy, this, 104);

	return 0;
}

//=============================================================================
// Method		: CWnd_MainView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 5;
	int iSpacing		= 5;
	int iCateSpacing	= 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iHalfWidth	= (iWidth - iCateSpacing) * 75 / 100;
	int iHalfHeight = (iHeight - iCateSpacing) * 3 / 4; //5 / 7;
	int iTempHeight = 0;
	int iTempWidth	= 0;
			
	m_wnd_SiteInfo.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	iLeft = iMagrin;
	iTop  = iMagrin + iHalfHeight + 5;
	iTempHeight = iHeight - iHalfHeight - iCateSpacing + 3;
	
	iWidth = (iWidth - iMagrin - iMagrin) / 3;
	m_wnd_TestInfo.MoveWindow(iLeft, iTop, iWidth, iTempHeight);
	m_wnd_MasterSet.MoveWindow(iLeft, iTop, iWidth, iTempHeight);

	iLeft += iWidth + iMagrin;
	m_wnd_LotInfo.MoveWindow(iLeft, iTop, iWidth, iTempHeight);

	iLeft += iWidth + iMagrin;
	m_wnd_ModelInfo.MoveWindow(iLeft, iTop, iWidth, iTempHeight);
}

//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_MainView::SetInspectionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		m_InspMode = Permission_Operator;
		break;

	case Permission_MES:
		m_InspMode = Permission_MES;
		break;

	case Permission_Manager:
	case Permission_Administrator:
	case Permission_Engineer:
	case Permission_CNC:
		m_InspMode = Permission_Administrator;

		m_pstInspInfo->LotInfo.Reset();
		UpdateLotInfo();

		m_pstInspInfo->YieldInfo.Reset();
		m_pstInspInfo->CycleTime.Reset();
		UpdateYield();

		break;

	default:
		break;
	}

	m_wnd_TestInfo.PermissionMode(InspMode);
	m_wnd_MasterSet.PermissionMode(InspMode);
}

//=============================================================================
// Method		: SetMasterMode
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bMode
// Qualifier	:
// Last Update	: 2017/7/11 - 10:19
// Desc.		:
//=============================================================================
void CWnd_MainView::SetMasterMode(__in BOOL bMode)
{
	if (bMode == TRUE)
	{
		m_wnd_TestInfo.ShowWindow(SW_HIDE);
		m_wnd_LotInfo.ShowWindow(SW_SHOW);
		m_wnd_ModelInfo.ShowWindow(SW_SHOW);
		m_wnd_MasterSet.ShowWindow(SW_SHOW);
	}
	else
	{
		m_wnd_TestInfo.ShowWindow(SW_SHOW);
		m_wnd_LotInfo.ShowWindow(SW_SHOW);
		m_wnd_ModelInfo.ShowWindow(SW_SHOW);
		m_wnd_MasterSet.ShowWindow(SW_HIDE);
	}
}

//************************************
// Method:    ManualAutoMode
// FullName:  CWnd_MainView::ManualAutoMode
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in BOOL bMode
//************************************
// void CWnd_MainView::ManualAutoMode(__in BOOL bMode)
// {
// 	m_wnd_TestInfo.ManualAutoMode(bMode);
// }

//************************************
// Method:    ManualBtnMode
// FullName:  CWnd_MainView::ManualBtnMode
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in BOOL bMode
//************************************
void CWnd_MainView::ManualBtnEnable(__in BOOL bMode)
{
	m_wnd_TestInfo.ManualBtnEnable(bMode);
}

//=============================================================================
// Method		: UpdatSetResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult Result
// Qualifier	:
// Last Update	: 2017/2/21 - 9:56
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatSetResult(__in enTestResult Result)
{
	m_wnd_SiteInfo.SetResult(Result);
}

//=============================================================================
// Method		: UpdatSetResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult Result
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/11 - 9:58
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatSetResult(__in enTestResult Result, __in CString strText)
{
	m_wnd_SiteInfo.SetResult(Result, strText);
}

//=============================================================================
// Method		: UpdatSetErrorCode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szErrorCode
// Qualifier	:
// Last Update	: 2017/2/21 - 17:06
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatSetErrorCode(__in LPCTSTR szErrorCode)
{
	m_wnd_SiteInfo.SetErrorCode(szErrorCode);
}

//=============================================================================
// Method		: UpdateModelInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/12 - 16:03
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateModelInfo()
{
	// 검사항목 선택 처리
	m_wnd_SiteInfo.SetTestItem(&m_pstInspInfo->ModelInfo);
	m_wnd_ModelInfo.SetModelInfo(&m_pstInspInfo->ModelInfo);
	m_wnd_ModelInfo.UpdatePogoCnt(&m_pstInspInfo->PogoInfo);
	m_wnd_MasterSet.SetModelInfo(&m_pstInspInfo->ModelInfo);
}

//=============================================================================
// Method		: UpdateMasterInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 17:19
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateMasterInfo(__in int iOffsetX, __in int iOffsetY, __in double dbDegree)
{
	m_wnd_MasterSet.SetMasterData(iOffsetX, iOffsetY, dbDegree);
}

//=============================================================================
// Method		: UpdateLotInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateLotInfo()
{	
	if (m_pstInspInfo->LotInfo.bLotStatus == TRUE)
		m_wnd_TestInfo.ButtonEnable(FALSE);
	else
		m_wnd_TestInfo.ButtonEnable(TRUE);

	m_wnd_LotInfo.SetLotInifo(&m_pstInspInfo->LotInfo);
}


void CWnd_MainView::UpdateMESInfo()
{
	m_wnd_LotInfo.SetMESInifo(&m_pstInspInfo->MESInfo.stMESResult);
}

//=============================================================================
// Method		: UpdateYield
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateYield()
{
 	m_wnd_LotInfo.SetYield(&m_pstInspInfo->YieldInfo);
}

//=============================================================================
// Method		: UpdateCycleTime
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:55
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateCycleTime()
{

}

//=============================================================================
// Method		: UpdateTestProcess
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 16:21
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestProcess()
{
	if (TP_Run == m_pstInspInfo->TestStatus)
	{
		//m_st_TestResult.SetText(g_TestProcess[TP_Run].szText);
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_Yellow);
	}
	else if (TP_Stop == m_pstInspInfo->TestStatus)
	{
		//m_st_TestResult.SetText(g_TestProcess[TP_Idle].szText);
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	}

}

//=============================================================================
// Method		: UpdateTestResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 16:58
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestResult()
{
	enTestResult Result = m_pstInspInfo->Judgment_All;

	//m_st_TestResult.SetText(g_TestResult[Result].szText);

	switch (Result)
	{
	case TR_Pass:
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case TR_Fail:
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case TR_Empty:
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_White);
		break;

	case TR_Testing:
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_Violet);
		break;

	case TR_Skip:
		//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_Blue);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetTestResult_Unit
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult nResult
// Qualifier	:
// Last Update	: 2016/7/18 - 16:34
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestResult_Unit( __in enTestResult nResult)
{

}

//=============================================================================
// Method		: UpdateElapsedTime_All
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2017/2/18 - 14:01
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateElapsedTime_All(__in DWORD dwTime)
{
	m_wnd_SiteInfo.SetElapsedTime(dwTime);
}

//=============================================================================
// Method		: UpdateInputTime
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/8 - 18:50
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateInputTime()
{
	m_wnd_SiteInfo.SetInputTime(&m_pstInspInfo->CamInfo.tmInputTime);
}

//=============================================================================
// Method		: InitTestResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/21 - 21:40
// Desc.		:
//=============================================================================
void CWnd_MainView::InitTestResult()
{
	//m_st_TestResult.SetColorStyle(CVGStatic::ColorStyle_White);
	//m_st_TestResult.SetText(g_TestResult[TR_Empty].szText);
}

//=============================================================================
// Method		: InsertBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/1/4 - 10:56
// Desc.		:
//=============================================================================
// void CWnd_MainView::InsertBarcode(__in LPCTSTR szBarcode)
// {
// 	m_wnd_SiteInfo.SetBarcode(szBarcode);
// }

//=============================================================================
// Method		: ResetSiteInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:56
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateResetSiteInfo()
{
	m_wnd_SiteInfo.ResetZoneInfo();
}

//=============================================================================
// Method		: UpdatePogoCnt
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/10 - 16:24
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatePogoCnt()
{
	m_wnd_ModelInfo.UpdatePogoCnt(&m_pstInspInfo->PogoInfo);
}

//=============================================================================
// Method		: UpdateTestCurrent
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:41
// Desc.		:
//=============================================================================
// void CWnd_MainView::UpdateTestCurrent(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_wnd_SiteInfo.SetTestCurrent(EachResult, strText);
// }

//=============================================================================
// Method		: UpdateTestLED
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:41
// Desc.		:
//=============================================================================
// void CWnd_MainView::UpdateTestLED(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_wnd_SiteInfo.SetTestLED(EachResult, strText);
// }

//=============================================================================
// Method		: UpdateTestCenterAdjust
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:44
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestCenterAdjust(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestCenterAdjust(EachResult, strText);
}

//=============================================================================
// Method		: UpdateTestResolution
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:45
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestResolution(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestResolution(EachResult, strText);
}

//=============================================================================
// Method		: UpdateTestRotation
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:45
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestRotation(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestRotation(EachResult, strText);
}

//=============================================================================
// Method		: UpdateTestActiveAlgin
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/10 - 13:28
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestActiveAlgin(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestActiveAlgin(EachResult, strText);
}

void CWnd_MainView::UpdateTestFocus(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestFocus(EachResult, strText);
}

//=============================================================================
// Method		: UpdateTestParticle
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2017/2/21 - 9:45
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestParticle(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestParticle(EachResult, strText);
}

//************************************
// Method:    UpdateTestVision
// FullName:  CWnd_MainView::UpdateTestVision
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_MainView::UpdateTestVision(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_wnd_SiteInfo.SetTestVision(EachResult, strText);
// }

//************************************
// Method:    UpdateTestDisplace
// FullName:  CWnd_MainView::UpdateTestDisplace
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_MainView::UpdateTestDisplace(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_wnd_SiteInfo.SetTestDisplace(EachResult, strText);
// }

//************************************
// Method:    UpdateTestAFPostion
// FullName:  CWnd_MainView::UpdateTestAFPostion
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_MainView::UpdateTestAFPostion(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_wnd_SiteInfo.SetTestAFPosition(EachResult, strText);
// }

//=============================================================================
// Method		: UpdateTestInitialize
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/10 - 13:28
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestInitialize(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestInitialize(EachResult, strText);
}

//=============================================================================
// Method		: UpdateTestFinalize
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/10 - 13:28
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestFinalize(__in enTestEachResult EachResult, __in CString strText)
{
	m_wnd_SiteInfo.SetTestFinalize(EachResult, strText);
}

void CWnd_MainView::UpdateSetStartBtnChange(__in BOOL bMode)
{
	m_wnd_TestInfo.ChangeStartBtnState(bMode);
}

void CWnd_MainView::UpdateAngleColor(__in enTestResult nResult, __in int nRetryTestCnt, BOOL bInitmode /*= FALSE*/)
{
	m_wnd_SiteInfo.SetAngleColor(nResult, nRetryTestCnt, bInitmode);
}
