﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_StdInfo.h
// Created	:	2017/1/3 - 10:37
// Modified	:	2017/1/3 - 10:37
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_StdInfo_h__
#define Wnd_Cfg_StdInfo_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"

#include "Wnd_CurrentOp.h"
#include "Wnd_CenterPointOp.h"
#include "Wnd_RotateOp.h"
#include "Wnd_EIAJOp.h"
#include "Wnd_SFROp.h"
#include "Wnd_ParticleOp.h"


#include "Def_TestDevice.h"

class CWnd_Cfg_StdInfo : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_StdInfo)

public:
	CWnd_Cfg_StdInfo();
	virtual ~CWnd_Cfg_StdInfo();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);

	CFont		m_font_Default;
	CFont		m_font_Data;

	/*TabCtrl Option*/
	CMFCTabCtrl				m_tc_Option;

	CWnd_CurrentOp			m_wnd_CurrentOp;
	CWnd_CenterPointOp		m_wnd_CenterPointOp;
	CWnd_EIAJOp				m_wnd_EIAJOp;
	CWnd_SFROp				m_wnd_SFROp;
	CWnd_ParticleOp			m_wnd_ParticleOp;
	CWnd_RotateOp			m_wnd_RotateOp;
//	CWnd_LEDTestOp			m_wnd_LEDOp;

	ST_TestItemMode			m_stTestItem;

	ST_ModelInfo*			m_pstModelInfo;
	ST_ImageMode*		m_pstImageMode;
	ST_Device*				m_pstDevice;

public:

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};


	BOOL		IsTestAll				();

	// 모델 데이터를 UI에 표시
	void		SetModelInfo			(ST_ModelInfo* pstModelInfo);
	void SetImageMode			(ST_ImageMode* pstImageMode);
	void		GetModelInfo();

	void		SetStatusEngineerMode	(__in enPermissionMode InspMode);

};

#endif // Wnd_Cfg_StdInfo_h__
