﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_Particle : public CTestItem
{
public:
	CTI_Particle();
	virtual ~CTI_Particle();

public:

	BOOL	SetImageSize			(DWORD dwWidth, DWORD dwHeight);

	/*이물 측정*/
	UINT	Particle_Test			(ST_LT_TI_Particle *pstParticle, LPBYTE pImageBuf);

	/*측정알고리즘*/
	double	GetDistance				(int ix1, int iy1, int ix2, int iy2);

	// 영역분할, m_pstParticleData->AreaBuf[x][y] = {0: 미검사 , 1: Center, 2: Middle , 3: Side} 
	void	FullRegionSum			(ST_LT_TI_Particle *pstParticle);
	double	EllipseDistanceSum		(BOOL bMode, int iLeft, int iTop, double dbA, double dbB, int iX1, int iY1);
	void	LumpDetection			(ST_LT_TI_Particle *pstParticle, LPBYTE ImageBuf);

protected:

	UINT	m_nWidth;
	UINT	m_nHeight;

	UINT	m_nCounter;

	char	**m_cAreaBuf;
	float	**m_fData;
	CvRect *pRectArray;

	void	MemoryCreat		();
	void	MemoryDelete	();
};