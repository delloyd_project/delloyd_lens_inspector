﻿#ifndef Wnd_ImageView_h__
#define Wnd_ImageView_h__

#pragma once

#include "VGStatic.h"

// CWnd_ImageView

class CWnd_ImageView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ImageView)

public:
	CWnd_ImageView();
	virtual ~CWnd_ImageView();
	CVGStatic		m_stVideo;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL	OnEraseBkgnd(CDC* pDC);
};


#endif // Wnd_ImageView_h__
