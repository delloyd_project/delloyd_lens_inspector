﻿// TI_Processing.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TI_Processing.h"

CTI_Processing::CTI_Processing()
{
	m_pstDevice = NULL;
	m_pstCurrent = NULL;
	m_pstEIAJ = NULL;
	m_pstCenterPoint = NULL;
	m_pstRotate = NULL;
	m_pstModelInfo = NULL;
	//m_pstLED				= NULL;

	m_pImageBuf = NULL;
	m_dwImageBufSize = 0;

	m_pImageSourceBuf = NULL;
	m_wImageSourceBufSize = 0;

}

CTI_Processing::~CTI_Processing()
{
	DeleteMemory();
}

//=============================================================================
// Method		: DeleteMemory
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/4 - 20:18
// Desc.		:
//=============================================================================
void CTI_Processing::DeleteMemory()
{
	if (NULL != m_pImageBuf)
	{
		delete[] m_pImageBuf;
		m_pImageBuf = NULL;
	}

	m_TlEIAJ.DeleteMemory(m_pstEIAJ);
}

//=============================================================================
// Method		: SetPtr_Device
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Device * pDevice
// Qualifier	:
// Last Update	: 2017/2/13 - 14:51
// Desc.		:
//=============================================================================
void CTI_Processing::SetPtr_Device(__in ST_Device* pstDevice)
{
	if (pstDevice == NULL)
		return;

	m_pstDevice = pstDevice;

	m_TlCurrent.SetPtr_Device(m_pstDevice);
	//m_TILED.SetPtr_Device(m_pstDevice);
}

//=============================================================================
// Method		: SetPtr_ModelInfo
// Access		: public  
// Returns		: void
// Parameter	: ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/2/15 - 14:29
// Desc.		:
//=============================================================================
void CTI_Processing::SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
{
	if (pstModelInfo == NULL)
		return;

	m_pstModelInfo = pstModelInfo;

	m_pstRotate = &m_pstModelInfo->stRotate;
	m_pstCurrent = &m_pstModelInfo->stCurrent;
	m_pstEIAJ = &m_pstModelInfo->stEIAJ;
	m_pstSFR = &m_pstModelInfo->stSFR;
	//m_pstLED		 = &m_pstModelInfo->stLED;
	m_pstCenterPoint = &m_pstModelInfo->stCenterPoint;
	m_pstParticle = &m_pstModelInfo->stParticle;
	m_pstCustomTeach = &m_pstModelInfo->stCustomTeach;

	m_TIPicControl.SetPtr_ModelInfo(m_pstModelInfo);
}

//=============================================================================
// Method		: CameraConnect
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/15 - 16:19
// Desc.		:
//=============================================================================
BOOL CTI_Processing::CameraConnect()
{
	UINT nGrabType = m_pstModelInfo->nGrabType;

	if (m_pstImageMode->eImageMode == ImageMode_StillShotImage)
	{
		return TRUE;
	}

	// 영상 보드 연결 상태 확인
	switch (nGrabType)
	{
	case GrabType_NTSC:
		if (FALSE == m_pstDevice->Cat3DCtrl.IsConnect())
			return FALSE;

		if (FALSE == m_pstDevice->Cat3DCtrl.GetStatus(CAM_VIEW_NUM))
			return FALSE;
		break;
		// 	case GrabType_LVDS:
		// 		if (FALSE == m_pstDevice->DAQCtrl.GetSignalStatus(CAM_VIEW_NUM))
		// 			return FALSE;
		// 		break;
	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: GetTestImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/7/7 - 10:46
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetTestImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;


	LPBYTE pRGBz = NULL;

	UINT nGrabType = m_pstModelInfo->nGrabType;

	IplImage *TestImage;

	if (m_pstImageMode->eImageMode == ImageMode_LiveCam)
	{
		if (CameraConnect() == FALSE)
			return FALSE;

		ST_VideoRGB_NTSC* pNRGB = NULL;
		//	ST_VideoRGB*  pLRGB	= NULL;

		switch (nGrabType)
		{
		case GrabType_NTSC:
			pRGBz = (LPBYTE)(m_pstDevice->Cat3DCtrl.GetRecvRGBData(nCh))->m_pMatRGB[0];
			pNRGB = m_pstDevice->Cat3DCtrl.GetRecvRGBData(VideoView_Ch_1);

			m_dwWidth = pNRGB->m_dwWidth;
			m_dwHeight = pNRGB->m_dwHeight;
			break;
		default:
			break;
		}

	}
	else{
		TestImage = cvLoadImage((CStringA)m_pstImageMode->szImagePath);

		if (TestImage == NULL || TestImage->width < 1 || TestImage->height < 1)
		{
			return FALSE;
		}
		m_dwWidth = TestImage->width;
		m_dwHeight = TestImage->height;
	}

	DWORD dwSize = m_dwWidth * m_dwHeight * 3;

	if (m_dwImageBufSize != dwSize)
	{
		m_dwImageBufSize = dwSize;

		if (NULL != m_pImageBuf)
		{
			delete[] m_pImageBuf;
			m_pImageBuf = NULL;
		}

		m_pImageBuf = new BYTE[dwSize];
	}
	if (m_pstImageMode->eImageMode == ImageMode_LiveCam)
	{
		switch (nGrabType)
		{
		case GrabType_NTSC:
		{
							  int widthstep_x3 = m_dwWidth * 3;
							  int widthstep_x4 = m_dwWidth * 4;

							  for (int y = 0; y < (int)m_dwHeight; y++)
							  {
								  for (int x = 0; x < (int)m_dwWidth; x++)
								  {
									  m_pImageBuf[y*widthstep_x3 + 3 * x + 0] = pRGBz[y*(widthstep_x4)+x * 4];
									  m_pImageBuf[y*widthstep_x3 + 3 * x + 1] = pRGBz[y*(widthstep_x4)+x * 4 + 1];
									  m_pImageBuf[y*widthstep_x3 + 3 * x + 2] = pRGBz[y*(widthstep_x4)+x * 4 + 2];
								  }
							  }

		}

			break;

			// 	case GrabType_LVDS:
			//memcpy(m_pImageBuf, pRGBz, dwSize);

			// 		break;

		default:
			break;
		}
	}
	else{
		memcpy(m_pImageBuf, TestImage->imageData, m_dwWidth*m_dwHeight * 3);
		cvReleaseImage(&TestImage);
	}

	return TRUE;
}


//=============================================================================
// Method		: GetTestSourceImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/9/8 - 16:56
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetTestSourceImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	if (CameraConnect() == FALSE)
		return FALSE;

	//	ST_FrameData* pRGB = m_pstDevice->DAQCtrl.GetSourceFrameData_ST(VideoView_Ch_1);
	//	LPWORD pRGBDATA = (LPWORD)m_pstDevice->DAQCtrl.GetSourceFrameData(VideoView_Ch_1);

	DWORD dwSize = m_dwWidth * m_dwHeight;

	if (m_wImageSourceBufSize != dwSize)
	{
		m_wImageSourceBufSize = dwSize;

		if (NULL != m_pImageSourceBuf)
		{
			delete[] m_pImageSourceBuf;
			m_pImageSourceBuf = NULL;
		}

		m_pImageSourceBuf = new WORD[dwSize];
	}

	//	memcpy(m_pImageSourceBuf, pRGBDATA, pRGB->dwWidth * pRGB->dwHeight * sizeof(WORD));

	return TRUE;
}


//=============================================================================
// Method		: GetVisionTestImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/10/12 - 11:19
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetVisionTestImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	return TRUE;
}


UINT CTI_Processing::CameraRegToFunctionChange(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff)
{
	UINT nTestResult = RCA_OK;

	// Serialize Disable Setting
	BYTE SerializeDisable[1] = { 0x43 };
	// 	if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeDisable) == FALSE)
	// 	{
	// 		return FALSE;
	// 	}
	Sleep(100);

	// ISP Reset
	BYTE ISP_LOW[1] = { 0x0f };
	BYTE ISP_HIGH[1] = { 0x8f };
	// 	m_pstDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_LOW);
	// 	Sleep(100);
	// 	m_pstDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x0d, 1, ISP_HIGH);
	// 	Sleep(1000);

	// Distortion
	switch (nDistortion)
	{
	case 0:
		DistortionCorrection(FALSE);
		break;
	case 1:
		DistortionCorrection(TRUE);
		break;
	default:
		break;
	}

	// Edge
	switch (nEdgeOnOff)
	{
	case 0:
		EdgeOnOff(FALSE);
		break;
	case 1:
		EdgeOnOff(TRUE);
		break;
	default:
		break;
	}

	// Fps Setting
	switch (nfpsMode)
	{
	case 0: // 15
		OperationModeChange(0);
		break;
	case 1: // 30
		OperationModeChange(1);
		break;
	default:
		break;
	}

	Sleep(500);

	// Serialize Enable Setting
	BYTE SerializeEnable[1] = { 0x83 };
	// 	if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x40 << 1, 1, 0x04, 1, SerializeEnable) == FALSE)
	// 	{
	// 		return FALSE;
	// 	}

	Sleep(100);

	return nTestResult;
}

//=============================================================================
// Method		: ResolutionRun
// Access		: public  
// Returns		: UINT
// Parameter	: BOOL MODE
// Qualifier	:
// Last Update	: 2017/2/13 - 10:56
// Desc.		:
//=============================================================================
UINT CTI_Processing::EIAJRun(BOOL bMode)
{
	enTestEachResult enResult;

	if (NULL == m_pstEIAJ)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_EIAJ;

	// 영상 사이즈 
	if (m_TlEIAJ.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	//if (bMode)
	//	m_pstEIAJ->stEIAJData.reset();

	// 	IplImage *OriginImage = cvCreateImage(cvSize(m_dwWidth, m_dwHeight), IPL_DEPTH_8U, 3);
	// 
	// 	for (int y = 0; y < m_dwHeight; y++)
	// 	{
	// 		for (int x = 0; x < m_dwWidth; x++)
	// 		{
	// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 0] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3];
	// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 1] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3 + 1];
	// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 2] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3 + 2];
	// 		}
	// 	}
	// 
	// 	cvSaveImage("C:\\Testimage.bmp", OriginImage);
	// 	cvReleaseImage(&OriginImage);


	enResult = (enTestEachResult)m_TlEIAJ.EIAJ_Test(m_pstEIAJ, m_pImageBuf);


	return enResult;
}

//=============================================================================
// Method		: SFRRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/8/11 - 10:42
// Desc.		:
//=============================================================================
UINT CTI_Processing::SFRRun()
{
	enTestEachResult enResult;

	if (NULL == m_pstSFR)
		return TER_Fail;

	// UI 이미지
	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 원본 이미지
	if (GetTestSourceImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_SFR;


	// 	DistortionCorrection(FALSE);
	// 
	// 	for (int i = 0; i < 50; i++)
	// 	{
	// 		DoEvents(33);
	// 	}
	// 
	// 	EdgeOnOff(m_pstModelInfo->stSFR.stSFROp.bEdge);
	// 
	// 	for (int i = 0; i < 50; i++)
	// 	{
	// 		DoEvents(33);
	// 	}

	// 영상 사이즈 
	if (m_TlSFR.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return TER_Fail;

	enResult = (enTestEachResult)m_TlSFR.SFR_Test(m_pstSFR, m_pImageBuf, m_pImageSourceBuf);

	return enResult;
}


UINT CTI_Processing::CenterPointOffset(__out int& iOffsetX, __out int&iOffsetY)
{


	if (NULL == m_pstSFR)
		return TER_Fail;

	// UI 이미지
	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 원본 이미지
	if (GetTestSourceImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlSFR.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return TER_Fail;

	CvPoint CenterPoint = m_TlSFR.SearchCenterPoint(m_pImageBuf);

	iOffsetX = CenterPoint.x;
	iOffsetY = CenterPoint.y;

	return TER_Pass;
}


UINT CTI_Processing::EdgeOnOff(BOOL bOnOff)
{
	if (m_pstDevice == NULL)
		return TER_Fail;

	if (bOnOff == TRUE)
	{
		BYTE EdgeDataOn[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x01, 0x80 };

		//  		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOn) == FALSE)
		//  		{
		//  			//	nTestResult = RCA_NG;
		//  		}
	}
	else
	{
		BYTE EdgeDataOff[8] = { 0x01, 0x06, 0x02, 0x59, 0x00, 0x14, 0x00, 0x7f };

		//  		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, EdgeDataOff) == FALSE)
		//  		{
		//  			//	nTestResult = RCA_NG;
		//  		}
	}
	return TER_Pass;
}


UINT CTI_Processing::DistortionCorrection(BOOL bCorrect)
{
	if (m_pstDevice == NULL)
		return TER_Fail;

	if (bCorrect == TRUE)
	{
		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };

		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOff) == FALSE)
		//  		{
		//  			//	nTestResult = RCA_NG;
		//  		}
	}
	else
	{
		BYTE DistortionDataOff[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x01, 0x2f };
		BYTE DistortionDataOn[8] = { 0x01, 0x06, 0x02, 0x1c, 0x00, 0x00, 0x00, 0x2e };

		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x09, 8, DistortionDataOn) == FALSE)
		//  		{
		//  			//	nTestResult = RCA_NG;
		//  		}
	}
	return TER_Pass;
}


UINT CTI_Processing::OperationModeChange(__in UINT nfpsMode)
{

	UINT nTestResult = RCA_OK;

	// Fps Mode Output
	if (nfpsMode == 0) // 15fps
	{
		BYTE IspSetting_0[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x00, 0x01, 0x80, 0x96 };
		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x0a, 9, IspSetting_0) == FALSE)
		// 		{
		// 			return FALSE;
		// 		}

		Sleep(100);

		BYTE IspSetting_F_0[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x01, 0x01, 0x80, 0x96 };
		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x0a, 9, IspSetting_F_0) == FALSE)
		// 		{
		// 			return FALSE;
		// 		}

		Sleep(100);
	}
	else if (nfpsMode == 1) // 30fps
	{
		BYTE IspSetting_0[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x00, 0x30, 0x80, 0xc5 };
		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x0a, 9, IspSetting_0) == FALSE)
		// 		{
		// 			return FALSE;
		// 		}

		Sleep(100);

		BYTE IspSetting_F_0[9] = { 0x01, 0x07, 0x02, 0x01, 0x00, 0x01, 0x80, 0x30, 0xc6 };
		// 		if (m_pstDevice->DAQCtrl.I2C_Write(0, 0x18 << 1, 1, 0x0a, 9, IspSetting_F_0) == FALSE)
		// 		{
		// 			return FALSE;
		// 		}

		Sleep(100);
	}

	return nTestResult;
}

//=============================================================================
// Method		: CenterPointRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/13 - 10:54
// Desc.		:
//=============================================================================
UINT CTI_Processing::CenterPointRun(BOOL bPicMode /*= TRUE*/)
{
	enTestEachResult enResult;

	if (NULL == m_pstCenterPoint)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	if (bPicMode == TRUE)
		m_pstModelInfo->nPicViewMode = PIC_CenterAdjust;

	// 영상 사이즈 
	if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘

	enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: CenterPointTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
UINT CTI_Processing::CenterPointTuningRun()
{
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_CenterAdjust;

	if (NULL == m_pstCenterPoint)
		return FALSE;

	for (UINT nStep = 0; nStep < m_pstModelInfo->stCenterPoint.stCenterPointOp.nWriteCnt; nStep++)
	{
		DoEvents(33);

		if (GetTestImageBuffer() == FALSE)
			return TER_NoImage;

		// 영상 사이즈 
		if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
			return FALSE;

		// 알고리즘
		enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

		// 이동 제한 값에서 벗어난 경우
		if (abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_X) >= m_pstModelInfo->stCenterPoint.stCenterPointOp.nMax_PixX && abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_Y) >= m_pstModelInfo->stCenterPoint.stCenterPointOp.nMax_PixY)
			break;

		// 목표 타켓내에 들어가면 루틴 중지
		if (abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_X) <= m_pstModelInfo->stCenterPoint.stCenterPointOp.nTarget_OffsetX && abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_Y) <= m_pstModelInfo->stCenterPoint.stCenterPointOp.nTarget_OffsetY)
			break;

		// 광축 조정 루틴
		//		m_pstDevice->MotionSequence.CenterPointAdjustment(m_pstCenterPoint->stCenterPointData.iResult_Offset_X, m_pstCenterPoint->stCenterPointData.iResult_Offset_Y);
	}

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘
	enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: RotateRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/13 - 10:57
// Desc.		:
//=============================================================================
UINT CTI_Processing::RotateRun()
{
	enTestEachResult enResult;

	if (NULL == m_pstRotate)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_Rotation;

	// 영상 사이즈 
	if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: RotateTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
UINT CTI_Processing::RotateTuningRun()
{
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_Rotation;

	if (NULL == m_pstRotate)
		return FALSE;

	for (UINT nStep = 0; nStep < m_pstModelInfo->stRotate.stRotateOp.nWriteCnt; nStep++)
	{
		DoEvents(33);

		if (GetTestImageBuffer() == FALSE)
			return TER_NoImage;

		// 영상 사이즈 
		if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
			return FALSE;

		// 알고리즘
		enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

		// 이동 제한 값에서 벗어난 경우
		if (abs(m_pstRotate->stRotateData.dbDegree) >= m_pstRotate->stRotateOp.dbMaxDegree)
			break;

		// 목표 타켓내에 들어가면 루틴 중지
		if (abs(m_pstRotate->stRotateData.dbDegree) <= m_pstRotate->stRotateOp.dbTargetDegree)
			break;

		// 로테이트 조정 루틴
		//		m_pstDevice->MotionSequence.RotateAdjustment(m_pstRotate->stRotateData.dbDegree);
	}

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘
	enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: ParticleRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/8/13 - 15:13
// Desc.		:
//=============================================================================
UINT CTI_Processing::ParticleRun()
{
	enTestEachResult enResult;

	if (NULL == m_pstRotate)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_Particle;

	// 영상 사이즈 
	if (m_TlParticle.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	enResult = (enTestEachResult)m_TlParticle.Particle_Test(m_pstParticle, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: ActiveAlignRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/7/10 - 13:39
// Desc.		:
//=============================================================================
UINT CTI_Processing::ActiveAlignRun()
{
	if (NULL == m_pstCenterPoint)
		return FALSE;
	if (NULL == m_pstEIAJ)
		return FALSE;

	enTestEachResult enCenter;
	enTestEachResult enEIAJ;

	// 	if (GetTestSourceImageBuffer() == FALSE)
	// 		return TER_NoImage;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;
	// 영상 사이즈 
	if (m_pstModelInfo->bCenterPoint_Ck == TRUE){
		if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
			return FALSE;
	}
	if (m_TlEIAJ.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;


	m_pstModelInfo->nPicViewMode = PIC_ActiveAlgin;

	// 알고리즘
	if (m_pstModelInfo->bCenterPoint_Ck == TRUE)
	{
		enCenter = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf); //20181226
		enEIAJ = (enTestEachResult)m_TlEIAJ.EIAJ_Test(m_pstEIAJ, m_pImageBuf);

		if (enCenter == TER_Fail || 
			enEIAJ == TER_Fail)
		{
			return TER_Fail;
		}
	}
	else
	{
		enEIAJ = (enTestEachResult)m_TlEIAJ.EIAJ_Test(m_pstEIAJ, m_pImageBuf);
		if (enEIAJ == TER_Fail)
		{
			return TER_Fail;
		}

	}
	return TER_Pass;
}


//=============================================================================
// Method		: ActiveAlignTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:49
// Desc.		:
//=============================================================================
UINT CTI_Processing::ActiveAlignTuningRun()
{
	if (NULL == m_pstCenterPoint)
		return FALSE;

	if (NULL == m_pstRotate)
		return FALSE;

	enTestEachResult enCenter;
	enTestEachResult enRotate;

	DoEvents(33);

	if (m_pstCenterPoint->stCenterPointOp.nTestMode == CP_Measurement_TestMode && m_pstModelInfo->bCenterPoint_Ck == TRUE)
		enCenter = (enTestEachResult)CenterPointRun();

	// 이미지가 없을 경우에 Fail
	if (enCenter == TER_NoImage)
		return enCenter;

	if (m_pstRotate->stRotateOp.nTestMode == RT_Fine_tune_TestMode)
		enRotate = (enTestEachResult)RotateTuningRun();

	if (m_pstRotate->stRotateOp.nTestMode == RT_Measurement_TestMode)
		enRotate = (enTestEachResult)RotateRun();

	// 이미지가 없을 경우에 Fail
	if (enRotate == TER_NoImage)
		return enRotate;

	// 	if (m_pstCenterPoint->stCenterPointOp.nTestMode == CP_Fine_tune_TestMode)
	// 		enCenter = (enTestEachResult)CenterPointTuningRun();

	// 이미지가 없을 경우에 Fail
	if (enCenter == TER_NoImage)
		return enCenter;

	if (enCenter == TER_Fail || enRotate == TER_Fail)
	{
		return TER_Fail;
	}

	return TER_Pass;
}


UINT CTI_Processing::ManualBtnCtrl(enManual_Commend enCmd)
{
	UINT nResult = TER_Pass;

	if (m_pstDevice == NULL)
		return nResult = TER_Fail;

	if (m_pstModelInfo == NULL)
		return nResult = TER_Fail;

	// 	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
	// 		return nResult = TER_Fail;

	// 	switch (enCmd)
	// 	{
	// 	case Man_Cmd_ModuleFix:
	// 		if (!m_pstDevice->MotionSequence.ModuleFixUnFixCYLMotion(FIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_ModuleUnFix:
	// 		if (!m_pstDevice->MotionSequence.ModuleFixUnFixCYLMotion(UNFIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_PCBFix:
	// 		if (!m_pstDevice->MotionSequence.PCBFixUnFixCYLMotion(FIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_PCBUnFix:
	// 		if (!m_pstDevice->MotionSequence.PCBFixUnFixCYLMotion(UNFIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_DriverIn:
	// 		if (!m_pstDevice->MotionSequence.DriverInOutCYLMotion(FIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_DriverOut:
	// 		if (!m_pstDevice->MotionSequence.DriverInOutCYLMotion(UNFIX))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_LampRed:
	// 		if (!m_pstDevice->MotionSequence.TowerLamp(TowerLampRed, ON))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_LampYellow:
	// 		if (!m_pstDevice->MotionSequence.TowerLamp(TowerLampYellow, ON))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_LampGreen:
	// 		if (!m_pstDevice->MotionSequence.TowerLamp(TowerLampGreen, ON))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	case Man_Cmd_LampBuzzer:
	// 		if (!m_pstDevice->MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON))
	// 		{
	// 			nResult = TER_Fail;
	// 		}
	// 		break;
	// 
	// 	default:
	// 		break;
	// 	}

	return nResult;
}

//=============================================================================
// Method		: ImageSaveOriginal
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/7 - 13:12
// Desc.		:
//=============================================================================
BOOL CTI_Processing::ImageSaveOriginal()
{
	// 영상 보드 연결 상태 확인
	if (!CameraConnect())
		return FALSE;

	CString strPath;

	if (GetSaveImageFilePath(strPath))
	{
		strPath = strPath + _T("_Original.png");

		ST_VideoRGB_NTSC* pRGB = m_pstDevice->Cat3DCtrl.GetRecvRGBData(VideoView_Ch_1);
		RGBQUAD** pRGBItr = (m_pstDevice->Cat3DCtrl.GetRecvRGBData(VideoView_Ch_1))->m_pMatRGB;
		LPBYTE pRGBDATA = (LPBYTE)((m_pstDevice->Cat3DCtrl.GetRecvRGBData(VideoView_Ch_1))->m_pMatRGB[0]);
		IplImage *Copyimage = cvCreateImage(cvSize(pRGB->m_dwWidth, pRGB->m_dwHeight), IPL_DEPTH_8U, 3);

		for (int y = 0; y < (int)pRGB->m_dwHeight; y++)
		{
			for (int x = 0; x < (int)pRGB->m_dwWidth; x++)
			{
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 0] = pRGBDATA[(y)*(pRGB->m_dwWidth * 4) + x * 4 + 0];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 1] = pRGBDATA[(y)*(pRGB->m_dwWidth * 4) + x * 4 + 1];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 2] = pRGBDATA[(y)*(pRGB->m_dwWidth * 4) + x * 4 + 2];
			}
		}

		cvSaveImage((CStringA)strPath, Copyimage);
		cvReleaseImage(&Copyimage);

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ImageSavePic
// Access		: public  
// Returns		: BOOL
// Parameter	: CString Path
// Qualifier	:
// Last Update	: 2017/9/7 - 13:12
// Desc.		:
//=============================================================================
BOOL CTI_Processing::ImageSavePic()
{
	if (m_hImageWnd == NULL)
		return FALSE;

	// 영상 보드 연결 상태 확인
	if (CameraConnect() == FALSE)
		return FALSE;

	CString strPath;

	// 비트맵(DIB) 데이터 추출
	//	GetTestImageBuffer();

	if (GetSaveImageFilePath(strPath))
	{
		strPath = strPath + _T("_Pic.bmp");
		m_pstPicImageCaptureMode->szImagePath = strPath;
		m_pstPicImageCaptureMode->nImageCaptureMode = TRUE;

		for (int t = 0; t < 100; t++)
		{
			if (m_pstPicImageCaptureMode->nImageCaptureMode == FALSE)
			{
				break;
			}
			DoEvents(10);
		}

		return TRUE;
	}

	return FALSE;
}


BOOL CTI_Processing::ReportImageSaveOriginal(__in CString szReportPath)
{
	// 영상 보드 연결 상태 확인
	if (m_pImageBuf == NULL)
		return FALSE;

	CString strPath;

	if (!szReportPath.IsEmpty())
	{
		strPath = szReportPath + _T("_Original.bmp");

		IplImage *Copyimage = cvCreateImage(cvSize(m_dwWidth, m_dwHeight), IPL_DEPTH_8U, 3);

		for (int y = 0; y < m_dwHeight; y++)
		{
			for (int x = 0; x < m_dwWidth; x++)
			{
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 0] = m_pImageBuf[(y)*(m_dwWidth * 3) + x * 3 + 0];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 1] = m_pImageBuf[(y)*(m_dwWidth * 3) + x * 3 + 1];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 2] = m_pImageBuf[(y)*(m_dwWidth * 3) + x * 3 + 2];
			}
		}

		cvSaveImage((CStringA)strPath, Copyimage);
		cvReleaseImage(&Copyimage);

		return TRUE;
	}

	return FALSE;
}


BOOL CTI_Processing::ReportImageSavePic(__in CString szReportPath)
{
	if (m_hImageWnd_Main == NULL)
		return FALSE;

	// 영상 보드 연결 상태 확인
	if (m_pImageBuf == NULL)
		return FALSE;

	Sleep(100);

	CString strPath;

	// 비트맵(DIB) 데이터 추출
	if (!szReportPath.IsEmpty())
	{
		strPath = szReportPath + _T("_Pic.bmp");

		
		m_pstPicImageCaptureMode->szImagePath = strPath;
		m_pstPicImageCaptureMode->nImageCaptureMode = TRUE;

		for (int t = 0; t < 100; t++)
		{
			if (m_pstPicImageCaptureMode->nImageCaptureMode == FALSE)
			{
				break;
			}
			DoEvents(10);
		}


		return TRUE;
	}

	return FALSE;
}


BOOL CTI_Processing::ReportImageSave(__in CString szReportPath)
{
	return TRUE;
}

//=============================================================================
// Method		: GetSaveImageFilePath
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szOutPath
// Qualifier	:
// Last Update	: 2016/7/27 - 16:31
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetSaveImageFilePath(__out CString& szOutPath)
{
	CString szPath = m_pstOption->Inspector.szPath_Image;
	CString szPathTime;
	SYSTEMTIME tmLocal;

	GetLocalTime(&tmLocal);

	szPathTime.Format(_T("%04d-%02d-%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);

	CString strModel = m_pstModelInfo->szModelCode;
	if (m_pstModelInfo->szModelCode.IsEmpty())
		strModel = _T("DEFAULT");

	szPath += +_T("\\") + szPathTime + _T("\\") + strModel;

	MakeDirectory(szPath);

	szOutPath.Format(_T("%04d%02d%02d-%02d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	szOutPath = szPath + _T("\\") + szOutPath;

	return TRUE;
}
void CTI_Processing::GetSaveImageFileModelPath(__out CString& szOutPath)
{
	CString szPath = m_pstOption->Inspector.szPath_Image;
	CString szPathTime;
	SYSTEMTIME tmLocal;

	GetLocalTime(&tmLocal);
	CString strModel = m_pstModelInfo->szModelCode;
	if (m_pstModelInfo->szModelCode.IsEmpty())
		strModel = _T("DEFAULT");

	szPathTime.Format(_T("%04d-%02d-%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	szPath += +_T("\\") + szPathTime + _T("\\") + strModel;

	MakeDirectory(szPath);
	szOutPath = szPath;

}



//=============================================================================
// Method		: InterlockRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/10/19 - 14:21
// Desc.		:
//=============================================================================
// UINT CTI_Processing::InterlockRun()
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
// 		return TER_Fail;
// 
// 	if (m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockIn) == TRUE
// 		&& m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockOut) == FALSE)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(OFF))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 	else if (m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockIn) == FALSE
// 		&& m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockOut) == TRUE)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(ON))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 
// 	return TER_Fail;
// }
// 
// 
// UINT CTI_Processing::InterlockFixedRun(__in BOOL bOnOff)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
// 		return TER_Fail;
// 
// 	if (bOnOff == ON)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(ON))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 	else if (bOnOff == OFF)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(OFF))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 
// 	return TER_Fail;
// }
// 
// //=============================================================================
// // Method		: UnloadingRun
// // Access		: public  
// // Returns		: UINT
// // Qualifier	:
// // Last Update	: 2017/10/19 - 14:21
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::UnloadingRun()
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 	// 비전 조명 OFF
// 	if (!m_pstDevice->IF_Illumination.Send_SetOnOff(0, OFF))
// 		return TER_Fail;
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ = 0;
// 	double dOffsetX = 0;
// 	double dOffsetY = 0;
// 
// 	int	iSeq = 0;
// 	int	iStepCount = 0;
// 
// 	dOffsetZ = m_pstCustomTeach->dUnloadingMotion_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dUnloadingMotion_OffsetX * 1000;
// 	dOffsetY = m_pstCustomTeach->dUnloadingMotion_OffsetY * 1000;
// 
// 	iSeq = m_pstCustomTeach->iUnloadingMotionSeq;
// 	ST_MotionSeqTeach stMotionSeq = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeq.nStepCount;
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 
// 	if (!m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeq) == TRUE)
// 		return TER_Fail;
// 
// 	// 콜렛 초기 위치
// 	if (!m_pstDevice->MotionSequence.ColletDegreeMove(POS_ABS_MODE, 0))
// 		return TER_Fail;
// 
// 	// 인터락 해제
// 	//if (!m_pstDevice->MotionSequence.InterlockMove(OFF))
// 	//	return TER_Fail;
// 
// 	return TER_Pass;
// }

// //=============================================================================
// // Method		: AFPositionRun
// // Access		: public  
// // Returns		: UINT
// // Parameter	: __in double dDisplace
// // Qualifier	:
// // Last Update	: 2017/10/19 - 11:29
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::AFPositionRun(__in double dDisplace, __out double& dInitPosY)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 	// 비전 이미지 버퍼
// 	LPBYTE	lpImage = NULL;
// 	DWORD	dwWidth = 0;	// 비전 이미지 가로 크기
// 	DWORD	dwHeight = 0;	// 비전 이미지 세로 크기
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ = 0;
// 	double dOffsetX = 0;
// 	double dOffsetY = 0;
// 	double dMasterDev = m_pstModelInfo->stAFInfo.dSTD_Displace - dDisplace;
// 
// 	int	iSeq = 0;
// 	int	iStepCount = 0;
// 
// 	dOffsetZ = m_pstCustomTeach->dAFPosition_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dAFPosition_OffsetX * 1000;
// 	dOffsetY = (m_pstCustomTeach->dAFPosition_OffsetY + dMasterDev) * 1000;
// 
// 	iSeq = m_pstCustomTeach->iAFPositionSeq;
// 	ST_MotionSeqTeach stMotionSeq = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeq.nStepCount;
// 
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 	
// 	dInitPosY = stMotionSeq.stSeqStep[iStepCount - 1].stAxisParam[MotorAxisY].dbPos;
// 
// 	// 비전 측정 위치
// 	if (m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeq) == TRUE)
// 		return TER_Pass;
// 
// 	return TER_Fail;
// }
// 
// 
// UINT CTI_Processing::AutoFocusingRun(__in double dDisplace)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstEIAJ == NULL)
// 		return TER_Fail;
// 
// 	double dStepDeg[3] =
// 	{
// 		m_pstModelInfo->stAFInfo.dAFStepDegree1, // 1차 스텝 단위
// 		m_pstModelInfo->stAFInfo.dAFStepDegree2, // 2차 스텝 단위
// 		m_pstModelInfo->stAFInfo.dAFStepDegree3, // 3차 스텝 단위
// 	};
// 
// 	double	dSTDDisplace = m_pstModelInfo->stAFInfo.dSTD_Displace;
// 	double	dDetDisplace = dDisplace;
// 	double	dLensConchoid = m_pstModelInfo->stAFInfo.dLensConchoid;
// 	BOOL	bDirection = m_pstModelInfo->stAFInfo.bLensDirection;
// 	double	dRotateCount = m_pstModelInfo->stAFInfo.dAFRotateCnt;
// 
// 	double dConchoid[3] =
// 	{
// 		dStepDeg[0] / 360.0 * dLensConchoid,
// 		dStepDeg[1] / 360.0 * dLensConchoid,
// 		dStepDeg[2] / 360.0 * dLensConchoid,
// 	};
// 
// // 	if (m_pstEIAJ->stEIAJData.nResult == TER_Pass)
// // 		return TER_Pass;
// 
// 	int	iDegDirection = 0;
// 
// 	if (bDirection == Reverse)
// 	{
// 		iDegDirection = -1;
// 	}
// 	else if (bDirection == Foward)
// 	{
// 		iDegDirection = 1;
// 	}
// 	
// 	double	dCurrentDegPuls = 0;
// 	int		iStepCnt = 0;
// 
// 
// 	while (1)
// 	{
// 		if (m_pstEIAJ->stEIAJData.nResult == TER_Pass)
// 			return TER_Pass;
// 
// 		int iDevCount = m_pstEIAJ->stEIAJData.nTotal_cnt - m_pstEIAJ->stEIAJData.nResult_cnt;
// 
// 		if (bDirection == Reverse)
// 		{
// 			iDegDirection = -1;
// 			
// 			if (dCurrentDegPuls >= (360.0 * dRotateCount * 100))
// 			{
// 				break;
// 			}
// 		}
// 		else if (bDirection == Foward)
// 		{
// 			iDegDirection = 1;
// 
// 			// 변위 위치에 대한 리미트
// 			double dLimitDev = abs(m_pstModelInfo->stAFInfo.dSTD_Displace - dDetDisplace);
// 			double dLimitPuls = dLimitDev / dLensConchoid * 36000;
// 			
// 			if (dCurrentDegPuls >= dLimitPuls)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		if (iDevCount >= 2 && iDevCount < 4)
// 			iStepCnt = 1;
// 		else if (iDevCount >= 0 && iDevCount < 2)
// 			iStepCnt = 2;
// 		else
// 			iStepCnt = 0;
// 
// 		if (!m_pstDevice->MotionSequence.FocusingDegreeMove(dStepDeg[iStepCnt] * iDegDirection, dConchoid[iStepCnt] * iDegDirection))
// 			break;
// 
// 		dCurrentDegPuls += dStepDeg[iStepCnt] * 100;
// 
// 		DoEvents(150);
// 
// 	}
// 	
// 	return TER_Fail;
// }
// 
// UINT CTI_Processing::ManualFocusingRun(__in double dDegree)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	double dLensConchoid	= m_pstModelInfo->stAFInfo.dLensConchoid;
// 	double dConchoid		= dDegree / 360.0 * dLensConchoid;
// 
// 	if (m_pstDevice->MotionSequence.FocusingDegreeMove(dDegree, dConchoid) == TRUE)
// 	{
// 		return TER_Pass;
// 	}
// 
// 	return TER_Fail;
// }
// 
// 
// //************************************
// // Method:    InitFocusingRun
// // FullName:  CTI_Processing::InitFocusingRun
// // Access:    public 
// // Returns:   UINT
// // Qualifier:
// // Parameter: __in double dInitY
// // Parameter: __in double dInitR
// //************************************
// UINT CTI_Processing::InitFocusingRun(__in double dInitY, __in double dInitR)
// {
// 	if (m_pstDevice->MotionSequence.FocusingInitMove(dInitY, dInitR) == TRUE)
// 	{
// 		return TER_Pass;
// 	}
// 
// 	return TER_Fail;
// }

//=============================================================================
// Method		: CameraPowerOnOff
// Access		: public  
// Returns		: UINT
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/2/15 - 14:20
// Desc.		:
//=============================================================================
UINT CTI_Processing::CameraPowerOnOff(BOOL bMode, UINT nCh /*= 0*/)
{
	if (m_pstModelInfo == NULL)
		return FALSE;

	BOOL bResult = FALSE;
	float fVolt = 0;
	CString strText;
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_Standby;

	if (bMode == TRUE)
	{
		float volt[2] = { m_pstModelInfo->fVoltage[0], 0 };

		for (int t = 0; t < 5; t++)
		{
			bResult = m_pstDevice->PCBCamBrd[0].Send_SetVolt(volt);
			if (bResult  == TRUE)
			{
				break;
			}
			Sleep(100);
		}
		//bResult = TRUE;

		if (bResult == TRUE)
		{
			if (m_pstModelInfo->nGrabType == GrabType_NTSC)
			{
				bResult = TRUE;
			}

			DoEvents(m_pstModelInfo->nCameraDelay);

			if (CameraConnect() == FALSE)
				bResult = FALSE;

			if (bResult)
				m_Log.LogMsg(_T("Camera Power ON"));
			else
				m_Log.LogMsg_Err(_T("Camera Power ON : Signal Err"));

			if (bResult)
			{
				::SendNotifyMessage(m_hOwnerWnd, WM_INCREASE_POGO_CNT, 0, 0);
			}
		}
	}
	else
	{
		float volt[1] = { 0 };

		for (int t = 0; t < 5; t++)
		{
			bResult = m_pstDevice->PCBCamBrd[0].Send_SetVolt(volt);
			if (bResult == TRUE)
			{
				break;
			}
			Sleep(100);
		}

		DoEvents(500);

		if (bResult == TRUE)
		{
			if (bResult)
				m_Log.LogMsg(_T("Camera Power OFF"));
			else
				m_Log.LogMsg_Err(_T("Camera Power Off : Signal Err"));
		}
	}

	if (bResult)
		enResult = TER_Pass;
	else
		enResult = TER_Fail;

	return enResult;
}


//=============================================================================
// Method		: CameraCurrent
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/15 - 16:15
// Desc.		:
//=============================================================================
UINT CTI_Processing::CameraCurrent()
{
	enTestEachResult enResult = TER_Fail;

	if (NULL == m_pstCurrent)
		return TER_Fail;

	// 알고리즘
	enResult = (enTestEachResult)m_TlCurrent.Current_Test(m_pstCurrent);

	m_pstCurrent->stCurrentData.nCurrent += m_pstCurrent->stCurrentOp[CuOp_Channel1].nOffset;

	if (m_pstCurrent->stCurrentData.nCurrent >= m_pstModelInfo->stCurrent.stCurrentOp[CuOp_Channel1].nMinCurrent
		&& m_pstCurrent->stCurrentData.nCurrent <= m_pstModelInfo->stCurrent.stCurrentOp[CuOp_Channel1].nMaxCurrent)
	{
		enResult = TER_Pass;
	}

	m_pstCurrent->stCurrentData.nResult = enResult;

	return enResult;
}
