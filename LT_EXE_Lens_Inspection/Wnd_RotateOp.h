﻿#ifndef Wnd_RotateOp_h__
#define Wnd_RotateOp_h__

#pragma once

#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "List_RotateOp.h"
#include "List_RotateSubOp.h"

enum enRotate_Static
{
	STI_RT_TESTMODE = 0,
	STI_RT_CAMERASTATE,
	STI_RT_IMAGESENSOR,
	STI_RT_TRYCOUNT,
	STI_RT_MAXNUM,
};

static LPCTSTR	g_szRotate_Static[] =
{
	_T("TEST MODE"),
	_T("CAMERA STATE"),
	_T("IMAGE SENSOR PIXSL"),
	_T("TRY COUNT"),
	NULL
};

enum enRotate_Buttonbox
{
	Btn_RT_ROTATE_TEST = 0,
	Btn_CT_ROTATE_TEST_STOP,
	Btn_RT_DEFAULT_SET,
	Btn_RT_MAXNUM,
};

static LPCTSTR	g_szRotate_Button[] =
{
	_T("ROTATE TEST"),
	_T("ROTATE STOP"),
	_T("DEFAULT SET"),
	NULL
};

enum enRotate_Combobox{
	ComboI_RT_TESTMODE = 0,
	ComboI_RT_CAMERASTATE,
	ComboI_RT_MAXNUM,
};

enum enRotate_Editbox{
	Edt_RT_IMAGESENSOR = 0,
	Edt_RT_TRYCOUNT,
	Edt_RT_MAXNUM,
};

// CWnd_RotateOp

class CWnd_RotateOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_RotateOp)

public:
	CWnd_RotateOp();
	virtual ~CWnd_RotateOp();

	CFont			m_font_Data;
	ST_ModelInfo	*m_pstModelInfo;

	/*UI*/
	CVGStatic			m_st_Item[STI_RT_MAXNUM];
	CComboBox			m_Cb_Item[ComboI_RT_MAXNUM];
	CMFCMaskedEdit		m_ed_Item[Edt_RT_MAXNUM];
	CMFCButton			m_bn_Item[Btn_RT_MAXNUM];
	CList_RotateOp		m_ListRtOp;
	CList_RotateSubOp	m_ListRtSubOp;

protected:

	DECLARE_MESSAGE_MAP()
	BOOL m_bTest_Flag;

public:
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnCbnSelendTestMode();
	afx_msg void	OnBnClickedBnTest();
	afx_msg void	OnBnClickedBnTestStop();
	afx_msg void	OnBnClickedBnDefaultSet();
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	void SetUI();
	void GetUI();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

};

#endif // Wnd_RotateOp_h__
