﻿//*****************************************************************************
// Filename	: 	Def_Test.h
// Created	:	2016/06/30
// Modified	:	2016/08/08
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Test_h__
#define Def_Test_h__

#include "Def_Enum.h"
#include "Def_Test_Cm.h"
#include "Def_TestItem.h"

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#define CAM_MAX_NUM			1

// 결과 코드 전체 검사
enum enResultCode_AF
{
	RCAF_UnknownError = 0,
	RCAF_OK,
	RCAF_Exception,
	RCAF_TestRunning,
	RCAF_Model_Err,
	RCAF_EMO,
	RCAF_MaxPogoCount,
	RCAF_AreaSensor,
	RCAF_Model_Empty_Err,
	RCAF_LotID_Empty_Err,
	RCAF_Operator_Empty_Err,
	RCAF_Barcode_Empty_Err,
	RCAF_ForcedStop,
	RCAF_PowerOn_Err,
	RCAF_PowerOff_Err,
	RCAF_Current_Err,
	RCAF_CameraBd_Err,
	RCAF_TimeOut_Err,
	RCAF_NoImage_Err,
	RCAF_UserStop,

	RCAF_VisionErr,
	RCAF_DisplaceErr,
	RCAF_AFPositionErr,
	RCAF_FocusErr,

	RCAF_ActiveAlignErr,

	RCAF_Max,
};

static LPCTSTR g_szResultCode_AF[] =
{
	_T("[ERR] Unknown error."),												
	_T("[ERR] OK"),																	
	_T("[ERR] Exception Err."),											
	_T("[ERR] TestRunning Err."),								
	_T("[ERR] Model Setting Err."),													
	_T("[ERR] The E.M.O button is working."),																
	_T("[ERR] Pogo Pin count exceeded."),						
	_T("[ERR] AreaSensor Detected."),								
	_T("[ERR] Model File Empty Err."),
	_T("[ERR] LOT ID Empty Err."),
	_T("[ERR] Operator Empty Err."),
	_T("[ERR] Barcode Empty Err."),
	_T("[ERR] Test Forced Stop."),
	_T("[ERR] Cam Power On Err."),
	_T("[ERR] Cam Power Off Err."),
	_T("[ERR] Current Err."),
	_T("[ERR] Camera Board Err."),
	_T("[ERR] TimeOut Err."),
	_T("[ERR] NoImage Err."),
	_T("[ERR] User Stop."),

	_T("[ERR] Vision measurement error."),
	_T("[ERR] Displacement measurement error."),
	_T("[ERR] Failed to move Focusing position."),
	_T("[ERR] Focus adjustment and measurement failure."),

	_T("[ERR] Lens Inspection failure."),
	
	NULL
};

// 
// static LPCTSTR g_szResultCode[] =
// {
// 	_T("[ERR] 无法预知的错误!\n[ERR] Unknown Err!"),										// RCA_UnknownError
// 	_T("[ERR] OK"),																		// RCA_OK
// 	_T("[ERR] Exception Err!"),															// RCA_Exception
// 	_T("[ERR] TestRunning Err!"),														// RCA_TestRunning
// 	_T("[ERR] Model Setting Err!"),														// RCA_Model_Err
// 	_T("[ERR] EMO State!"),																// RCA_EMO
// 	_T("[ERR] Pogo Pin 使用超过了最大值!\n[ERR] Max Pogo Count!"),						// RCA_MaxPogoCount
// 	_T("[ERR] 安全传感器感知\n[ERR] Area Sensor Detect"),								// RCA_AreaSensor
// 	_T("[ERR] Cylinder Move Err!"),														// RC3_Cylinder_Err
// 	_T("[ERR] Model Check Empty!"),														// RCA_Model_Empty_Err
// 	_T("[ERR] 没有输入LOT ID!\n[ERR] Lot ID Check Empty!"),								// RCA_LotID_Empty_Err
// 	_T("[ERR] 没有输入作业者工号!\n[ERR] Operator Check Empty!"),						// RCA_Operator_Empty_Err
// 	_T("[ERR] 没有输入MES LOT ID!\n[ERR] Barcode Number Check Empty!"),					// RCA_Barcode_Empty_Err
// 	_T("[ERR] Operator Test Stop!"),													// RCA_ForcedStop
// 	_T("[ERR] No initial instrument settings!"),										// RAC_InitSetting_Err
// 	_T("[ERR] 摄像头电源无法ON!\n[ERR] Camera Power On Fail!"),							// RC3_PowerOn_Err
// 	_T("[ERR] 摄像头电源无法OFF!\n[ERR] Camera Power Off Fail!"),						// RC3_PowerOff_Err
// 	_T("[ERR] Camera Current Fail!"),													// RC3_Current_Err
// 	_T("[ERR] Particle Fail!"),															// RC3_Particle_Err
// 	_T("[ERR] Camera Board Err!"),														// RC3_CameraBd_Err
// 	_T("[ERR] Test Time Out Err!"),														// RC3_TimeOut_Err
// 	_T("[ERR] Camera Video No Signal Err!"),											// RC3_NoImage_Err
// 	_T("[ERR] Camera Algorithm Err!"),													// RC3_Algorithm_Err
// 	_T("[ERR] 没有测试 Master Sample!\n[ERR] Fail Master Set Err!"),						// RC3_MasterSet_Err
// 	_T("[ERR] USER Stop!"),																// RC3_UserStop
// 	_T("[ERR] 3 Axis Stage Machine Err!"),												// RC3_MachineCheck
// 	NULL
// };

typedef enum enIndicatorNum
{
	IndicatorX,
	IndicatorY,
	Indicator_Max,
};

static UINT g_nEqp_TestItemIDList[LT_EnumMax] =
{
//	TIID_Current,
//	TIID_LEDTest,
	TIID_ActiveAlgin,
	
};

typedef enum enTestEachResult
{
	TER_Fail,
	TER_Pass,
	TER_Empty,
	TER_SKIP,
	TER_Init,
	TER_UserStop,
	TER_NotTest,
	TER_MachineCheck,
	TER_Timeout,
	TER_NoImage,
};

static ST_StaticInf g_TestEachResult[] =
{
	{ _T("Fail"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("Pass"),	RGB(255, 255, 255), RGB(0, 122, 204),	RGB(20, 86, 150) },
	{ _T("Empty"),	RGB(0, 0, 0),		RGB(255, 255, 255), RGB(62, 20, 110) },
	{ _T("TEST SKIP"), RGB(255, 255, 255), RGB(255, 255, 255), RGB(62, 20, 110) },
	{ _T("Init"),	RGB(0, 0, 0),		RGB(255, 255, 255), RGB(0, 0, 0),	 },
	{ _T("USER STOP"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("NOT TEST"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("Machine Check"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("Timeout"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("No Image"),	RGB(255, 255, 255), RGB(192, 0, 0),		RGB(174, 90, 33) },

	NULL
};

//-----------------------------------------------------------------------------
// 카메라 검사 정보 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_CamInfo
{
	CString				szReportFilePath;	// 이미지 저장 경로
	CString				szIndex;
	CString				szLotID;			// Lot ID	
	CString				szModelName;		// 모델 이름
	CString				szOperatorName;		// 작업자 이름
	int					nRetryTestCnt;		// 각도별 Retry Cnt
	CString				szEquipment;		
	CString				szSWVersion;		

	SYSTEMTIME			tmInputTime;		// 제품 투입시간
	DWORD				dwInputTime;		// CycleTime 계산용 제품 투입시간
	SYSTEMTIME			tmOutputTime;		// 제품 배출시간
	DWORD				dwOutputTime;		// CycleTime 계산용 제품 배출시간
	DWORD				dwCycleTime;		// 제품 투입에서 배출까지 시간
	DWORD				dwTactTime;			// Tact Time
	//DWORD				dwTestTime;			// 각 Site별 테스트 시간 누적
	CString				szDay;				// ex)20161008
	CString				szTime;				// ex)180000
	CString				szFullDate;			//ex)20190108112732

	UINT				nSocketIndex;		// 테이블 위치 (Socket)
	enTestProcess		nProgressStatus;	// 각 카메라의 검사 진행 상태

	enTestResult		nJudgment;			// 최종 결과, 제품 유무
	LRESULT				ResultCode;			// 결과 코드 (오류 코드)	
	
	enTestEachResult	nJudgment_TestItem[USE_TEST_ITEM_CNT];	// 검사 항목별 결과
	INT					FailTestItem;		// 불량이 발생한 검사 항목

	
	enTestEachResult	nJudgmentInitial;
	enTestEachResult	nJudgmentFinalize;

	enTestEachResult	nJudgmentVision;
	enTestEachResult	nJudgmentDisplace;
	enTestEachResult	nJudgmentAFPosition;

	double				dVColletDegree;		// 비전 콜렛 측정 데이터
	double				dDisplaceAvg;		// 변위 평균 측정 데이터 
	double				dInitPosY;
	double				dInitPosR;
	BOOL				bTestMode;

	ST_Current_Data		stCurrentData;
	ST_CenterPoint_Data	stCenterPointData;
	ST_EIAJ_Data		stEIAJData;
	ST_SFR_Data			stSFRData;
	ST_Rotate_Data		stRotateData;
	ST_Particle_Data	stParticleData;
	ST_Current_Data		stLEDData;
	

	UINT				CurrentTestItem;	// 현재 검사 중인 TestItem	
	DWORD				dwTestItemST[USE_TEST_ITEM_CNT];	// TestItem 검사 시간

	_tag_CamInfo()
	{

		ZeroMemory(&tmInputTime, sizeof(SYSTEMTIME));
		ZeroMemory(&tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= 0;
		dwOutputTime	= 0;
		dwCycleTime		= 0;
		dwTactTime		= 0;
		nRetryTestCnt	= -1;
		//dwTestTime		= 0;

		nSocketIndex	= 0;
		nProgressStatus = enTestProcess::TP_Idle;
		nJudgment		= enTestResult::TR_Empty;
		ResultCode		= RCC_OK;
		nJudgmentInitial = enTestEachResult::TER_Init;
		nJudgmentFinalize = enTestEachResult::TER_Init;
		nJudgmentVision = enTestEachResult::TER_Init;
		nJudgmentDisplace = enTestEachResult::TER_Init;
		nJudgmentAFPosition = enTestEachResult::TER_Init;
		FailTestItem = -1;

		dVColletDegree	= 0;
		dDisplaceAvg	= 0;

		dInitPosY = 0;
		dInitPosR = 0;

		bTestMode = FALSE;
	};

	void Reset()
	{
		szReportFilePath.Empty();
		szIndex.Empty();
		szLotID.Empty();
		szModelName.Empty();
		szOperatorName.Empty();
		nRetryTestCnt = -1;
		//szBarcode.Empty();

		ZeroMemory(&tmInputTime, sizeof(SYSTEMTIME));
		ZeroMemory(&tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= 0;
		dwOutputTime	= 0;
		dwCycleTime		= 0;
		dwTactTime		= 0;
		szDay.Empty();
		szTime.Empty();
		nProgressStatus = enTestProcess::TP_Idle;
		nJudgment		= enTestResult::TR_Init;
		nJudgmentInitial = enTestEachResult::TER_Init;
		nJudgmentFinalize = enTestEachResult::TER_Init;
		nJudgmentVision = enTestEachResult::TER_Init;
		nJudgmentDisplace = enTestEachResult::TER_Init;
		nJudgmentAFPosition = enTestEachResult::TER_Init;
		ResultCode		= RCC_OK;
		FailTestItem	= -1;
		
		stCurrentData.reset();
		stCenterPointData.reset();
		stEIAJData.reset();
		stRotateData.reset();
		stParticleData.reset();

		dVColletDegree	= 0;
		dDisplaceAvg	= 0;

		dInitPosY = 0;
		dInitPosR = 0;

		bTestMode = FALSE;
	};

	_tag_CamInfo& operator= (_tag_CamInfo& ref)
	{
		szReportFilePath = ref.szReportFilePath;
		szIndex			= ref.szIndex;
		szLotID			= ref.szLotID;		
		szModelName		= ref.szModelName;
		szOperatorName	= ref.szOperatorName;
		nRetryTestCnt	= ref.nRetryTestCnt;
		//szBarcode		= ref.szBarcode;

		memcpy(&tmInputTime, &ref.tmInputTime, sizeof(SYSTEMTIME));
		memcpy(&tmOutputTime, &ref.tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= ref.dwInputTime;
		dwOutputTime	= ref.dwOutputTime;
		dwCycleTime		= ref.dwCycleTime;
		dwTactTime		= ref.dwTactTime;
		//dwTestTime		= ref.dwTestTime;
		szDay			= ref.szDay;
		szTime			= ref.szTime;
		nSocketIndex	= ref.nSocketIndex;
		nProgressStatus = ref.nProgressStatus;
		nJudgment = ref.nJudgment;
		
		nJudgment = ref.nJudgment;
		nJudgmentInitial = ref.nJudgmentInitial;
		nJudgmentFinalize = ref.nJudgmentFinalize;
		nJudgmentVision = ref.nJudgmentVision;
		nJudgmentDisplace = ref.nJudgmentDisplace;
		nJudgmentAFPosition = ref.nJudgmentAFPosition;


		ResultCode		= ref.ResultCode;
		FailTestItem	= ref.FailTestItem;

		dVColletDegree	= ref.dVColletDegree;
		dDisplaceAvg	= ref.dDisplaceAvg;
		dInitPosY		= ref.dInitPosY;
		dInitPosR		= ref.dInitPosR;
		bTestMode		= ref.bTestMode;

		stCurrentData	= ref.stCurrentData;
		stCenterPointData = ref.stCenterPointData;
		stEIAJData = ref.stEIAJData;
		stRotateData = ref.stRotateData;
		stParticleData = ref.stParticleData;
		stLEDData = ref.stLEDData;

		return *this;
	};

	void SetTestProgress(__in enTestProcess nProcess)
	{
		nProgressStatus = nProcess;
	};

	void SetInformation(__in LPCTSTR szIn_LotName, __in LPCTSTR szIn_Barcode, __in LPCTSTR szIn_ModelName, __in LPCTSTR szIn_OperatorName)
	{
		szLotID			= szIn_LotName;
		//szBarcode		= szIn_Barcode;
		szModelName		= szIn_ModelName;		// 모델 이름
		szOperatorName	= szIn_OperatorName;
	};

	void SetInputTime()
	{		
		GetLocalTime(&tmInputTime);
		dwInputTime = timeGetTime();
		szDay.Format(_T("%04d/%02d/%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay);
		szTime.Format(_T("%02d:%02d:%02d"), tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
		szFullDate.Format(_T("%04d/%02d/%02d-%02d:%02d:%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay, tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
	};

	void SetInputTime(__in SYSTEMTIME tmIn, __in DWORD dwIn)
	{
		memcpy(&tmInputTime, &tmIn, sizeof(SYSTEMTIME));
		dwInputTime = dwIn;
		szDay.Format(_T("%04d/%02d/%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay);
		szTime.Format(_T("%02d:%02d:%02d"), tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
		szFullDate.Format(_T("%04d/%02d/%02d-%02d:%02d:%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay, tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
	};

 	void SetOutputTime()
 	{
 		GetLocalTime(&tmOutputTime);
		dwOutputTime = timeGetTime();

		if (dwOutputTime < dwInputTime)
		{
			dwCycleTime = 0xFFFFFFFF - dwInputTime + dwOutputTime;
		}
		else
		{
			dwCycleTime = dwOutputTime - dwInputTime;
		}
 	};

	void SetOutputTime(__in SYSTEMTIME tmOut, __in DWORD dwOut)
	{
		memcpy(&tmOutputTime, &tmOut, sizeof(SYSTEMTIME));
		dwOutputTime = dwOut;

		if (dwOutputTime < dwInputTime)
		{
			dwCycleTime = 0xFFFFFFFF - dwInputTime + dwOutputTime;
		}
		else
		{
			dwCycleTime = dwOutputTime - dwInputTime;
		}
	};

	void SetSocketIndex(__in UINT nIdx)
	{
		nSocketIndex = nIdx;
	};

}ST_CamInfo;


#endif // Def_Test_h__
