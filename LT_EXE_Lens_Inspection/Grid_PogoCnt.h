﻿//*****************************************************************************
// Filename	: 	Grid_PogoCnt.h
// Created	:	2016/6/21 - 22:19
// Modified	:	2016/6/21 - 22:19
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_PogoCnt_h__
#define Grid_PogoCnt_h__

#pragma once
#include "Grid_Base.h"
#include "Def_DataStruct.h"

class CGrid_PogoCnt : public CGrid_Base
{
public:
	CGrid_PogoCnt();
	virtual ~CGrid_PogoCnt();

protected:
	virtual void	OnSetup			();
	virtual int		OnHint			(int col, long row, int section, CString *string);
	virtual void	OnGetCell		(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect	(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline();

	// 헤더를 초기화
	void			InitHeader();

	CFont		m_font_Header;
	CFont		m_font_Data;

	const ST_PogoInfo* m_pstPogoCnt;

public:

	void	SetPtr_PogoCnt				(__in const ST_PogoInfo* pstPogoCnt)
	{
		m_pstPogoCnt = pstPogoCnt;
	};

	void	UpdatePogoCnt_Socket		();
	
	void	ShowResetButton				(__in BOOL bShow);

};

#endif // Grid_PogoCnt_h__

