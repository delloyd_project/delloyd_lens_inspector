﻿//*****************************************************************************
// Filename	: 	Wnd_SiteInfo.h
// Created	:	2016/5/13 - 11:39
// Modified	:	2016/5/13 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_SiteInfo_h__
#define Wnd_SiteInfo_h__

#pragma once

#include "VGGroupWnd.h"
#include "Def_ErrorCode.h"
#include "Def_Test.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "VGProgressBar.h"
#include "Wnd_ImageView.h"

#include "Wnd_ImgProc.h"

//-----------------------------------------------------------------------------
// CWnd_SiteInfo
//-----------------------------------------------------------------------------
class CWnd_SiteInfo : public CWnd
{
	DECLARE_DYNAMIC(CWnd_SiteInfo)

public:
	CWnd_SiteInfo();
	virtual ~CWnd_SiteInfo();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	virtual void	OnRedrawControl			();

	DECLARE_MESSAGE_MAP()

	enum enZoneItem
	{
		SI_InputTime,			// 제품 투입 시간
		SI_Test_Init,			// Test Initialize
		SI_Test_ActiveAlign,	// Focus
		SI_Test_Final,			// Test Finalize
		SI_TestTime,			// 검사 시간
		SI_ErrorCode,			// 불량 사유
		SI_AngleResult,			// 각도별 불량
		SI_MaxEnum,
	};

	CFont			m_Font;	
	
	// Item 명칭
	CVGStatic		m_stItem_T[SI_MaxEnum];
	// Item 데이터
	CVGStatic		m_stItem_V[SI_MaxEnum];
	// Angle 각도별 검사 결과
	CVGStatic		m_stAngleResult[4];
	
	// 검사 결과
	CVGStatic		m_stJudgment;

	CVGStatic		m_st_BackColor;
	CVGGroupWnd		m_Group;

	// 데이터
	ST_CamInfo*		m_pCamObj;

	UINT			m_nSocketIdx;
	UINT			m_nUseTestCount;
	BOOL			m_bUseTestItem[USE_TEST_ITEM_CNT];

	DWORD			m_nWidth;
	DWORD			m_nHeight;

	void			RedrawWindow();

	CString			GetErrorCodeText(__in INT ErrorCode);

public:
	// 영상 화면 출력용
	CWnd_ImgProc		m_wnd_ImgProc;
	//CWnd_ImageView	m_wnd_Video;

	// 비전 영상 화면 출력용B
	CWnd_ImageView	m_wnd_VVideo;

	void	SetImageSize		(__in UINT nWidth, __in UINT nHeight);

	void	SetUnitIndex		(__in UINT nUnitIdx);
	
	void	SetPtr_CamObject	(__in ST_CamInfo* pCamObj)
	{
		m_pCamObj = pCamObj;
	};

	void	ResetZoneInfo		();
	void	ResetResult			();
	void	UpdateCamObjetInfo	(__in const ST_CamInfo* pCamObj);

	//-------------------------------------------------------------------
	// 검사 결과 정보 UI 업데이트
	//-------------------------------------------------------------------
	//void	SetBarcode			(__in LPCTSTR szBarcode);		// BARCODE
	void	SetInputTime		(__in SYSTEMTIME* ptmInput);	// 투입시간
	void	SetInputTime		(__in LPCTSTR szInputTime);		// 투입시간
	void	SetResult			(__in enTestResult Result);		// 검사결과
	void	SetResult			(__in enTestResult Result, __in CString szResult);		// 검사결과
	void	SetErrorCode		(__in INT ErrorCode);			// ERROR 원인
	void	SetErrorCode		(__in LPCTSTR szErrorCode);		// ERROR 원인
	void	SetElapsedTime		(__in DWORD dwTime);			// 진행시간

	void	SetTestInitialize	(__in enTestEachResult EachResult, __in CString strText);	// 검사 초기화
	void	SetTestFinalize		(__in enTestEachResult EachResult, __in CString strText);	// 검사 마무리

// 	void	SetTestVision		(__in enTestEachResult EachResult, __in CString strText);	// 비전
// 	void	SetTestDisplace		(__in enTestEachResult EachResult, __in CString strText);	// 변위
// 	void	SetTestAFPosition	(__in enTestEachResult EachResult, __in CString strText);	// AF 위치


	//void	SetTestLED			(__in enTestEachResult EachResult, __in CString strText);	// 소비전류 검사
	//void	SetTestCurrent		(__in enTestEachResult EachResult, __in CString strText);	// 소비전류 검사
	void	SetTestCenterAdjust	(__in enTestEachResult EachResult, __in CString strText);	// 광축조정 검사
	void	SetTestResolution	(__in enTestEachResult EachResult, __in CString strText);	// 해상력 검사
	void	SetTestRotation		(__in enTestEachResult EachResult, __in CString strText);	// Rotation 검사
	void	SetTestParticle		(__in enTestEachResult EachResult, __in CString strText);	// 이물 검사
	void	SetTestActiveAlgin	(__in enTestEachResult EachResult, __in CString strText);	// ActiveAlgin
	void	SetTestFocus		(__in enTestEachResult EachResult, __in CString strText);	// Focus	

	void	SetTableIndex		(__in UINT nIndex);
	//--------------------------------------------------------------------

	void	SetTestItem			(__in const ST_ModelInfo* pstModelInfo);
	void	ShowVideo			(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void SetAngleColor (__in enTestResult nResult, __in int nRetryTestCnt, BOOL bInitmode = FALSE);
};

#endif // Wnd_SiteInfo_h__


