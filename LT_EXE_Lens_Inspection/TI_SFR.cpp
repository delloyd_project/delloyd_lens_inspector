﻿#include "stdafx.h"
#include "Def_Test.h"
#include "TI_SFR.h"

#define PI	3.1415926536

CTI_SFR::CTI_SFR()
{
	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
	{
		m_bLimit[nIdx] = FALSE;
	}

	m_nWidth	= 720;
	m_nHeight	= 480;
}


CTI_SFR::~CTI_SFR()
{
}

// =============================================================================
// Method		: SetImageSize
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2017/8/8 - 14:25
// Desc.		:
// =============================================================================
BOOL CTI_SFR::SetImageSize(DWORD dwWidth, DWORD dwHeight)
{
	if (dwWidth <= 0 || dwHeight <= 0)
		return FALSE;

	m_nWidth	= dwWidth;
	m_nHeight	= dwHeight;

	return TRUE;
}

//=============================================================================
// Method		: SFR_Test
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_SFR * pstSFR
// Parameter	: LPBYTE pImageBuf
// Qualifier	:
// Last Update	: 2017/8/9 - 19:49
// Desc.		:
//=============================================================================
UINT CTI_SFR::SFR_Test(ST_LT_TI_SFR *pstSFR, LPBYTE pImageBuf, LPWORD pImageSourceBuf)
{
	if (pstSFR == NULL)
		return TER_MachineCheck;

	for (int q = 0; q < Region_SFR_MaxEnum; q++)
	{
		m_bLimit[q] = FALSE;
	}


	//// Edge on /off
	//if (EdgeOnOff(m_stInspInfo.ModelInfo.stSFR.stSFROp.bEdge) == TER_Fail)
	//{
	//	;
	//}


	CvPoint CenterPoint = SearchCenterPoint(pImageBuf);
	
// 	CRect rcRect;
// 
// 	rcRect.left = m_nWidth / 3;
// 	rcRect.right = m_nWidth - (m_nWidth / 3);
// 
// 	rcRect.top = m_nHeight / 4;
// 	rcRect.bottom = m_nHeight - (m_nHeight / 4);
// 
// 	CPoint ptCenter;
// 	CvPoint CenterPoint;
// 
// 	IplImage* pImageCV = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
// 
// 	for (int y = 0; y< m_nHeight; y++)
// 	{
// 		for (int x = 0; x < m_nWidth; x++)
// 		{
// 			pImageCV->imageData[y * pImageCV->widthStep + (x * 3) + 0] = pImageBuf[y * (m_nWidth * 3) + x * 3 + 0];
// 			pImageCV->imageData[y * pImageCV->widthStep + (x * 3) + 1] = pImageBuf[y * (m_nWidth * 3) + x * 3 + 1];
// 			pImageCV->imageData[y * pImageCV->widthStep + (x * 3) + 2] = pImageBuf[y * (m_nWidth * 3) + x * 3 + 2];
// 		}
// 	}
// 
// 	FiducialMark_Test(pImageCV, m_nWidth, m_nHeight, rcRect, ptCenter);
	
//	if (ptCenter.x > -1 && ptCenter.y > -1)
	{
// 		CenterPoint.x = ptCenter.x;
// 		CenterPoint.y = ptCenter.y;

		SetROI_CenterRef(pImageBuf, pstSFR, CenterPoint);

		

		SFR_8Bit(pstSFR, pImageBuf);
	}

//	cvReleaseImage(&pImageCV);

	return	pstSFR->stSFRData.nResult;
}

//=============================================================================
// Method		: SFR_8Bit
// Access		: public  
// Returns		: BOOL
// Parameter	: LPBYTE pImageBuf
// Qualifier	:
// Last Update	: 2017/9/8 - 17:09
// Desc.		:
//=============================================================================
void CTI_SFR::SFR_8Bit(ST_LT_TI_SFR *pstSFR, LPBYTE pImageBuf)
{
	DWORD R0, G0, B0, Value;
	BYTE *pTemp;

	int iWidth = 0;
	int iHeight = 0;
	int iLeft = 0;
	int iTop = 0;

	int iResultPassCnt = 0;
	int iResultTotalCnt = 0;

	for (int i = 0; i < Region_SFR_MaxEnum; i++)
	{
		pstSFR->stSFRData.bEnable[i] = pstSFR->stSFROp.stSFR_Region[i].bEnable;

		if (pstSFR->stSFROp.stSFR_Region[i].bEnable == TRUE && m_bLimit[i] == TRUE)
		{
			iWidth	= pstSFR->stSFROp.stSFR_InitRegion[i].nWidth;
			iHeight = pstSFR->stSFROp.stSFR_InitRegion[i].nHeight;
			iLeft	= pstSFR->stSFROp.stSFR_InitRegion[i].nPos_X - (iWidth / 2);
			iTop	= pstSFR->stSFROp.stSFR_InitRegion[i].nPos_Y - (iHeight / 2);

			int CntX = 0;
			int CntY = 0;

			pTemp = new BYTE[(iWidth - 1) * (iHeight - 1)];

			if (iWidth > iHeight)
			{
				int iLimitW = iLeft + (iWidth - 1);
				int iLimitH = iTop + (iHeight - 1);

				if (iLimitW > m_nWidth)
				{
					iLimitW = m_nWidth;
				}
				else if (iLimitH > m_nHeight)
				{
					iLimitH = m_nHeight;
				}

				for (int x = iLeft; x < iLimitW; x++)
				{
					CntY = 0;
					for (int y = iTop; y < iLimitH; y++)
					{
						B0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 0];
						G0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 1];
						R0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 2];
						Value = (BYTE)((0.29900*R0) + (0.58700*G0) + (0.11400*B0));

						pTemp[(CntX * (iHeight - 1)) + CntY] = (BYTE)Value;

						CntY++;
					}
					CntX++;
				}

				double dbPixelSz = pstSFR->stSFROp.dbPixelSizeW;
				double dbLinePerPixel = pstSFR->stSFROp.stSFR_Region[i].dbLinePair;

				IplImage *test = cvCreateImage(cvSize(iWidth - 1, iHeight - 1), IPL_DEPTH_8U, 1);
				for (int y = 0; y < iHeight - 1; y++)
				{
					for (int x = 0; x < iWidth - 1; x++)
					{
						test->imageData[y * (test->widthStep) + x] = pTemp[x *(iHeight - 1) + y];

					}
				}
				//cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

				for (int y = 0; y < iHeight - 1; y++)
				{
					for (int x = 0; x < iWidth - 1; x++)
					{
						pTemp[x * (iHeight - 1) + y] = test->imageData[y * (test->widthStep) + x];

					}
				}
				cvReleaseImage(&test);

				pstSFR->stSFRData.dbResultValue[i] = GetSFRValue(pTemp, iHeight - 1, iWidth - 1, dbPixelSz, dbLinePerPixel, pstSFR->stSFROp.nDataType);
			}
			else
			{
				int LimitW = iLeft + (iWidth - 1);
				int LimitH = iTop + (iHeight - 1);

				if (LimitW > m_nWidth)
				{
					LimitW = m_nWidth;
				}
				else if (LimitH > m_nHeight)
				{
					LimitH = m_nHeight;
				}

				for (int y = iTop; y < LimitH; y++)
				{
					CntX = 0;
					for (int x = iLeft; x < LimitW; x++)
					{
						B0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 0];
						G0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 1];
						R0 = pImageBuf[y * (m_nWidth * 3) + x * 3 + 2];
						Value = (BYTE)((0.29900*R0) + (0.58700*G0) + (0.11400*B0));

						pTemp[(CntY * (iWidth - 1)) + CntX] = (BYTE)Value;

						CntX++;
					}
					CntY++;
				}

				double dbPixelSz = pstSFR->stSFROp.dbPixelSizeW;
				double dbLinePerPixel = pstSFR->stSFROp.stSFR_Region[i].dbLinePair;

				IplImage *test = cvCreateImage(cvSize(iWidth - 1, iHeight - 1), IPL_DEPTH_8U, 1);
				for (int y = 0; y < iHeight - 1; y++)
				{
					for (int x = 0; x < iWidth - 1; x++)
					{
						test->imageData[y * (test->widthStep) + x] = pTemp[y *((iWidth - 1)) + x];

					}
				}
				//cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

				for (int y = 0; y < iHeight - 1; y++)
				{
					for (int x = 0; x < iWidth - 1; x++)
					{
						pTemp[y * (iWidth - 1) + x] = test->imageData[y * (test->widthStep) + x];

					}
				}
				cvReleaseImage(&test);

				pstSFR->stSFRData.dbResultValue[i] = GetSFRValue(pTemp, iWidth - 1, iHeight - 1, dbPixelSz, dbLinePerPixel, pstSFR->stSFROp.nDataType);
			}

			pstSFR->stSFRData.dbResultValue[i] += pstSFR->stSFROp.stSFR_Region[i].dbOffset;

			if (pstSFR->stSFRData.dbResultValue[i] >= pstSFR->stSFROp.stSFR_Region[i].dbThreshold)
			{
				pstSFR->stSFRData.nEachResult[i] = TRUE;

				iResultPassCnt++;
			}
			else
			{
				pstSFR->stSFRData.nEachResult[i] = FALSE;
			}
			iResultTotalCnt++;


			pstSFR->stSFRData.nResult &= pstSFR->stSFRData.nEachResult[i];

			delete[] pTemp;
		}
	}

	pstSFR->stSFRData.nTotalCnt = iResultTotalCnt;
	pstSFR->stSFRData.nPassCnt = iResultPassCnt;

	if (pstSFR->stSFRData.nPassCnt == pstSFR->stSFRData.nTotalCnt)
	{
		pstSFR->stSFRData.nResult = TRUE;
	}
	else
	{
		pstSFR->stSFRData.nResult = FALSE;
	}
}

//=============================================================================
// Method		: SFR_16Bit
// Access		: public  
// Returns		: BOOL
// Parameter	: ST_LT_TI_SFR * pstSFR
// Parameter	: LPWORD pImageSourceBuf
// Qualifier	:
// Last Update	: 2017/9/8 - 17:19
// Desc.		:
//=============================================================================
void CTI_SFR::SFR_16Bit(ST_LT_TI_SFR *pstSFR, LPWORD pImageSourceBuf)
{
	DWORD R0, G0, B0, Value;
	WORD *pTemp;
	UINT nTotalCnt = 0;
	UINT nPassCnt = 0;
	pstSFR->stSFRData.nResult = TR_Pass;

	for (int i = 0; i < Region_SFR_MaxEnum; i++)
	{
		if (pstSFR->stSFROp.stSFR_Region[i].bEnable == TRUE)
		{
			if (m_bLimit[i] == TRUE)
			{
			nTotalCnt++;
			int CntX = 0;
			int CntY = 0;

				int	iWidth = pstSFR->stSFROp.stSFR_InitRegion[i].nWidth;
				int	iHeight = pstSFR->stSFROp.stSFR_InitRegion[i].nHeight;
				int	iLeft = pstSFR->stSFROp.stSFR_InitRegion[i].nPos_X - iWidth / 2;
				int	iTop = pstSFR->stSFROp.stSFR_InitRegion[i].nPos_Y - iHeight / 2;

				if (iWidth > 0 && iHeight > 0 && iLeft > 0 && iTop > 0)
				{
			pTemp = new WORD[iWidth * iHeight];

			int iLimitW = iLeft + iWidth;
			int iLimitH = iTop + iHeight;

			if (iLimitW > m_nWidth)
			{
				iLimitW = m_nWidth;
			}
			else if (iLimitH > m_nHeight)
			{
				iLimitH = m_nHeight;
			}

			if (iLimitH < 0 || iLimitW < 0)
			{
				pstSFR->stSFRData.nResult = TER_Fail;
				return;
			}

			if (iWidth > iHeight)
			{

				for (int x = iLeft; x < iLimitW; x++)
				{
					CntY = 0;
					for (int y = iTop; y < iLimitH; y++)
					{
						pTemp[(CntX * (iHeight)) + CntY] = pImageSourceBuf[y * m_nWidth + x];
						CntY++;
					}
					CntX++;
				}

				pstSFR->stSFRData.dbResultValue[i] = GetSFRValue_16bit(pTemp, iHeight, iWidth, pstSFR->stSFROp.dbPixelSizeW, pstSFR->stSFROp.stSFR_Region[i].dbLinePair, 2);//Ver.3
			}
 			else
			{
				for (int y = iTop; y < iLimitH; y++)
				{
					CntX = 0;
					for (int x = iLeft; x < iLimitW; x++)
					{
						pTemp[(CntY * (iWidth)) + CntX] = pImageSourceBuf[y * m_nWidth + x];
						CntX++;
					}
					CntY++;
				}
				pstSFR->stSFRData.dbResultValue[i] = GetSFRValue_16bit(pTemp, iWidth, iHeight, pstSFR->stSFROp.dbPixelSizeW, pstSFR->stSFROp.stSFR_Region[i].dbLinePair, 2);//Ver.3
			}

			pstSFR->stSFRData.dbResultValue[i] += pstSFR->stSFROp.stSFR_Region[i].dbOffset;

			if (pstSFR->stSFRData.dbResultValue[i] >= pstSFR->stSFROp.stSFR_Region[i].dbThreshold)
			{
				nPassCnt++;
				pstSFR->stSFRData.nEachResult[i] = TER_Pass;
			}
			else
			{
				pstSFR->stSFRData.nEachResult[i] = TER_Fail;
			}

			pstSFR->stSFRData.nResult &= pstSFR->stSFRData.nEachResult[i];

			delete[] pTemp;
		}
	}
		}
	}

	pstSFR->stSFRData.nTotalCnt = nTotalCnt;
	pstSFR->stSFRData.nPassCnt = nPassCnt;
}

//=============================================================================
// Method		: MulMatrix
// Access		: public  
// Returns		: void
// Parameter	: double * * A
// Parameter	: double * * B
// Parameter	: double * * C
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: unsigned int n
// Parameter	: unsigned int Mode
// Qualifier	:
// Last Update	: 2017/8/10 - 11:25
// Desc.		:
//=============================================================================
void CTI_SFR::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{
	UINT i, j, k;

	for (i = 0; i < Row; i++)
	{
		for (j = 0; j < Col; j++)
		{
			C[i][j] = 0;
			for (k = 0; k < n; k++)
			{
				if (Mode == 0)	    C[i][j] += A[i][k] * B[k][j];   // Mode 0 : A  x B
				else if (Mode == 1) C[i][j] += A[k][i] * B[k][j];   // Mode 1 : A' x B
				else if (Mode == 2) C[i][j] += A[i][k] * B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

//=============================================================================
// Method		: Inverse
// Access		: public  
// Returns		: void
// Parameter	: double * * dataMat
// Parameter	: unsigned int n
// Parameter	: double * * MatRtn
// Qualifier	:
// Last Update	: 2017/8/10 - 11:25
// Desc.		:
//=============================================================================
void CTI_SFR::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{
	unsigned int i, j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	dataA = new double[n*n];

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			dataA[i*n + j] = dataMat[i][j];
		}
	}

	matA = cvMat(n, n, CV_64F, dataA);

	cvInvert(&matA, pMatB);

	for (i = 0; i < n; i++)
	for (j = 0; j < n; j++)
		MatRtn[i][j] = cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

	cvReleaseMat(&pMatB);
	delete[]dataA;
}

//=============================================================================
// Method		: pInverse
// Access		: public  
// Returns		: void
// Parameter	: double * * A
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: double * * RtnMat
// Qualifier	:
// Last Update	: 2017/8/10 - 11:26
// Desc.		:
//=============================================================================
void CTI_SFR::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{
	unsigned int i;//, j;
	double **AxA, **InvAxA;

	AxA = new double *[Col];
	for (i = 0; i < Col; i++)
	{
		AxA[i] = new double[Col];
	}

	InvAxA = new double *[Col];
	for (i = 0; i < Col; i++)
	{
		InvAxA[i] = new double[Col];
	}

	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	for (i = 0; i < Col; i++)
	{
		delete[]InvAxA[i];
	}
	delete[]InvAxA;

	for (i = 0; i < Col; i++)
	{
		delete[]AxA[i];
	}
	delete[]AxA;
}

//=============================================================================
// Method		: GetSFRValue
// Access		: public  
// Returns		: double
// Parameter	: BYTE * atemp
// Parameter	: int width
// Parameter	: int height
// Parameter	: double dbPixelSz
// Parameter	: double LinePerPixel
// Qualifier	:
// Last Update	: 2017/8/10 - 11:45
// Desc.		:
//=============================================================================
double CTI_SFR::GetSFRValue(BYTE *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair, UINT nMode)
{
	int imgWidth = iWidth;
	int imgHeight = iHeight;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double dbResultSFR = 0;


	//rotatev2가 들어가야 함.

	for (int y = 0; y < imgHeight; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			tleft += (int)pBuf[y * imgWidth + x];
		}

		for (int x = imgWidth - 6; x < imgWidth; x++)
		{
			tright += (int)pBuf[y * imgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if (tleft + tright == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.1)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = imgWidth;
	double mid = (imgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[imgWidth];

	for (int i = 0; i < n; i++)
	{
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((CV_PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];
	double *c = new double[imgWidth * imgHeight];

	for (int k = 0; k < imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = 0; k < imgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j > -1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					sum += (double)(pBuf[y * imgWidth + (k - d_j)]) * fil1[d_j];
			}
			Deriv_c[y * imgWidth + k] = sum;
		}
	}

	for (int y = 0; y < imgHeight; y++)
	{
		for (int k = (d_n - 1); k < imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[imgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += ((double)c[y * imgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * imgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);

	double *place = new double[imgHeight];
	double *win2 = new double[imgWidth];

	for (j = 0; j<imgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			win2[i] = cos(CV_PI*arg / wid);
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		loc_v = 0;
		total_v = 0;

		for (int x = 0; x < imgWidth; x++)
			total_v += c[j * imgWidth + x] * win2[x];

		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += (c[j * imgWidth + x] * win2[x])*(x + 1); // 수정
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[j] = 0;
		else
			loc[j] = loc_v / total_v - 0.5;
	}

	delete[]win2;
	delete[]place;
	delete[]c;

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = imgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//===================================================================================================//
	//여기까진 OK!
	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * imgWidth];

	//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((CV_PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / CV_PI;

	if (slope_deg < 2.00)
	{
		delete[]freq;
		delete[]win;

		for (int i = 0; i < cN; i++)
			delete[]C[i];
		delete[]C;

		delete[]loc;

		return 0;
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//===========================================================================2018-02-20 주석처리하고!!
	/*	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;*/
	//============================================================================

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i < nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((CV_PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(CV_PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	imgHeight = cvRound((double)(cvFloor((double)imgHeight * fabs(C[0][0]))) / fabs(C[0][0]));	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	//==========================================================================2018-02-20 여기로!!!
	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;
	//============================================================================

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 100];

	for (i = 0; i < 2; i++)
		for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
			barray[i][j] = 0;

	int tmp_x, tmp_y;

	for (tmp_x = 0; tmp_x < imgWidth; tmp_x++)
	{
		for (tmp_y = 0; tmp_y < imgHeight; tmp_y++)
		{
			double ttt = ((double)(tmp_x)-((double)tmp_y / slope))*fac;
			double ling = cvCeil(ttt) - (double)offset;
			barray[0][(int)ling] = barray[0][(int)ling] + 1;
			barray[1][(int)ling] = barray[1][(int)ling] + (double)pBuf[tmp_y * imgWidth + tmp_x];
		}
	}

	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status != 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{
		if (barray[0][i + start] != 0)
		{
			if (barray[0][(i + 1) + start] == 0)
			{
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];
	delete[]barray;

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j > -1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001)
	{
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else
	{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}
	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		//Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]); //-> 0으로 나눗셈이 진행되는 문제가 있음
		Resultmtf[count_j] = (0.0f != ConveredResultFFT->data.db[0]) ? (fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0])) : 0;
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);


	// 값 산출
	double nPixel = 1000.0 / dbPixelSize;
	double CyclePerPixel = dbLinePair / nPixel;

	double	dbBefore = 0, dbNext = 0;
	double dbBeforeFreq = 0;
	double dbNextFreq = 0;

	int nBefore = 0, nNext = 0;
	int half_sampling_index = -1;
	double data, old_data = 1.0;

	// 산출 방식 분기
	switch (nMode)
	{
	case enSFRDataType_MTF:

		for (int q = 0; q < inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - CyclePerPixel);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			dbResultSFR = 0.0;
		else
			dbResultSFR = Resultmtf[half_sampling_index];

		break;

	case enSFRDataType_CyPx:

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			TRACE(_T("Result[%d] Freq.:MTF--- %f,%f\n"), i, freq[i], dbNext);
			if (dbNext < CyclePerPixel)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		dbBeforeFreq = freq[nBefore];
		dbNextFreq = freq[nNext];

		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		dbResultSFR = dbNextFreq - (CyclePerPixel - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		break;

	case enSFRDataType_Cymm:

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			TRACE(_T("Result[%d] Freq.:MTF--- %f,%f\n"), i, freq[i], dbNext);
			if (dbNext < CyclePerPixel)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		dbBeforeFreq = freq[nBefore];
		dbNextFreq = freq[nNext];

		dbResultSFR = dbNextFreq - (CyclePerPixel - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		dbResultSFR = (dbResultSFR / dbPixelSize) * 1000;

		break;

	default:
		break;
	}

	delete[]freq;
	delete[]Resultmtf;

	return dbResultSFR;
}

//=============================================================================
// Method		: GetSFRValue_16bit
// Access		: public  
// Returns		: double
// Parameter	: WORD * atemp
// Parameter	: int width
// Parameter	: int height
// Parameter	: double dbPixelSz
// Parameter	: double LinePerPixel
// Parameter	: UINT nItem
// Qualifier	:
// Last Update	: 2017/9/8 - 18:07
// Desc.		:
//=============================================================================
double CTI_SFR::GetSFRValue_16bit(WORD *atemp, int width, int height, double dbPixelSz, double LinePerPixel, UINT nItem)
{
	int imgWidth = width;
	int imgHeight = height;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double resultSFR = 0.0;

#define adjcont		0
#if adjcont == 1
	WORD	wMaxValue = 0;
	for (int y = 0; y < imgHeight; y++)
	for (int x = 0; x < imgWidth; x++)
	if (wMaxValue < atemp[y * imgWidth + x])
		wMaxValue = atemp[y * imgWidth + x];

	WORD	nAdjValue = 0xFFFF / (wMaxValue + 1);
	for (int y = 0; y < imgHeight; y++)
	for (int x = 0; x < imgWidth; x++)
		atemp[y * imgWidth + x] *= nAdjValue;
#endif

	//rotatev2가 들어가야 함.

	for (int y = 0; y < imgHeight; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			tleft += (int)atemp[y * imgWidth + x];
		}

		for (int x = imgWidth - 6; x<imgWidth; x++)
		{
			tright += (int)atemp[y * imgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if ((tleft + tright) == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = imgWidth;
	//	double mid = (imgWidth) / 2.0;
	double mid = (imgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	//	double wid1 = mid;
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[imgWidth];

	for (int i = 0; i < n; i++)
	{
		//		arg = i - mid;
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];
	double *c = new double[imgWidth * imgHeight];

	for (int k = 0; k < imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = 0; k<imgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j>-1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					sum += (double)(atemp[y * imgWidth + (k - d_j)]) * fil1[d_j];
				//				sum += (double)((unsigned char)atemp[y * imgWidth + (k - d_j)]) * fil1[d_j];
			}

			Deriv_c[y * imgWidth + k] = sum;

		}
	}

	for (int y = 0; y < imgHeight; y++)
	{
		for (int k = (d_n - 1); k < imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[imgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += ((double)c[y * imgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * imgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);


	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);


	double *place = new double[imgHeight];

	//	for (i = 0; i < imgHeight; i++)
	//		place[i] = C[1][0] + C[0][0] * (double)(i+1);

	double *win2 = new double[imgWidth];

	for (j = 0; j<imgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			//	win2[i] = 0.54 + 0.46*cos((PI*arg / wid));
			win2[i] = cos((PI*arg / wid));
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		//	for(int y=0; y<imgHeight; y++)
		{
			loc_v = 0;
			total_v = 0;

			for (int x = 0; x<imgWidth; x++)
				total_v += c[j * imgWidth + x] * win2[x];

			for (int x = 0; x < imgWidth; x++)
			{
				loc_v += (c[j * imgWidth + x] * win2[x])*(x + 1); // 수정
				//	total_v += c[j * imgWidth + x] * win2[x];
			}

			if (total_v == 0 || total_v < 0.0001)
				loc[j] = 0;
			else
				loc[j] = loc_v / total_v - 0.5;
		}
	}



	delete[]win2;
	delete[]place;
	delete[]c;
	/*
	for (i = 0; i < rM; i++)
	B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);
	*/
	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = imgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;
	//===================================================================================================//

	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * imgWidth];

	//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;


	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / PI;

	if (slope_deg < 3.5)
	{
		delete[]freq;
		delete[]win;
		return 0.0;
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i<nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	imgHeight = cvRound(cvFloor(imgHeight * fabs(C[0][0]))) / fabs(C[0][0]);	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];



	for (i = 0; i < 2; i++)
	for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
		barray[i][j] = 0;

	for (int x = 0; x < imgWidth; x++)
	{
		for (int y = 0; y < imgHeight; y++)
		{
			double ttt = ((double)x - (double)y / slope)*fac;
			int ling = cvCeil(ttt) - offset;
			barray[0][ling]++;
			//	double kkk = barray[0][ling];
			//	barray[0][ling] = kkk;
			double kk = barray[1][ling];
			//	barray[1][ling] = kk + (unsigned char)atemp[y * imgWidth + x];
			barray[1][ling] = kk + atemp[y * imgWidth + x];
		}
	}




	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{

		if (barray[0][i + start] != 0){
			if (barray[0][(i + 1) + start] == 0){
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];

	delete[]barray;

	//	IplImage *esf_image = cvCreateImage(cvSize(inProject_nn, 255), IPL_DEPTH_8U, 1);
	//	
	//	cvSetZero(esf_image);
	//
	//	for(i=0; i<(int)inProject_nn-1; i++)
	//		cvLine(esf_image, cvPoint(i, point[i]), cvPoint(i+1, point[i+1]), CV_RGB(255, 255, 255), 1, 8);
	//	
	//	cvFlip(esf_image);
	//
	////	cvShowImage("esf", esf_image);
	////	cvSaveImage("d:\\test\\esf_image.bmp", esf_image);
	//	
	//	cvReleaseImage(&esf_image);

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j>-1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001){
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;


	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;


	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}

	//	CvMat *sourceFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *resultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *ConveredResultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	////	for(i=0; i<(int)inProject_nn; i++)
	////		sourceFFT->data.db[i] = temp[i];
	//	sourceFFT->data.db[0] = 0.0001;
	//	sourceFFT->data.db[1] = 0;
	//	sourceFFT->data.db[2] = -0.2222;
	//	sourceFFT->data.db[3] = 0;
	//	sourceFFT->data.db[4] = 0.1111;
	//	sourceFFT->data.db[5] = 0;
	//	sourceFFT->data.db[6] = 0.4444;
	//	sourceFFT->data.db[7] = 0;
	//	sourceFFT->data.db[8] = 0.2211;
	//	sourceFFT->data.db[9] = 0;
	//	sourceFFT->data.db[10] = -1.0000;
	//	sourceFFT->data.db[11] = 0;
	//	
	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
	//
	//	for(i=0; i<6; i++)
	//	{
	//		if( i != 0)
	//		{
	//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
	//		}
	//		else
	//		{
	//		//	resultFFT->data.db[i*2] = fabs(resultFFT->data.db[i*2]);
	//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
	//		}
	//	}

	//	delete []temp;
	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	
	//	double k1 = sqrt ( pow (resultFFT->data.db[5], 2) + pow (resultFFT->data.db[6], 2) ) ;

	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	// 	double a[200];
	// 	for (int i = 0; i <124; i++){
	// 		a[i]=dcorr[i];
	// 	}
	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]);
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	/*	ofstream outFile("c:\\output.txt");		//mtf결과 파일로 저장
	for(i=0; i<(int)inProject_nn/2; i++)
	{
	outFile << Resultmtf[i] << endl;
	}
	outFile.close();*/

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);

	// SFRGraph_image
#define SAVE_SFR_GRAPH	0
#if SAVE_SFR_GRAPH
	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn * 10, 110 * 10), IPL_DEPTH_8U, 3);
	cvSetZero(SFRGraph_image);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		cvLine(SFRGraph_image, cvPoint(q * 10, cvRound(Resultmtf[q] * 1000.0)), cvPoint((q + 1) * 10, cvRound(Resultmtf[q + 1] * 1000.0)), CV_RGB(255, 255, 255), 1, 8);
	}
	cvFlip(SFRGraph_image);

	char	szText[64];
	CvFont* font = (CvFont*)malloc(sizeof(CvFont));
	cvInitFont(font, CV_FONT_VECTOR0, 0.5, 0.5, 0, 1);

	FILE*	logFile;
	sprintf(szText, "d:\\test\\SFR_Result_%03d.txt", ROI_NUM);
	logFile = fopen(szText, "w+");

	sprintf(szText, "SFR Result\t%d\nFrequency\tSFR\n", ROI_NUM);
	fwrite(szText, 1, strlen(szText), logFile);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		sprintf(szText, "%.04f", Resultmtf[q]);
		cvPutText(SFRGraph_image, szText, cvPoint(q * 10 - 5, 1100 - cvRound(Resultmtf[q] * 1000.0)), font, CV_RGB(255, 0, 0));

		sprintf(szText, "%3.4f\t%3.4f\n", freq[q], Resultmtf[q]);

		if (freq[q] < 1.01)
			fwrite(szText, 1, strlen(szText), logFile);
		//		printf("Result[%d] Freq.:MTF--- %f,%f\n", q, freq[q], Resultmtf[q]);
	}
	fclose(logFile);

	sprintf(szText, "d:\\test\\SFR_GRAPH_%03d.png", ROI_NUM);
	cvSaveImage(szText, SFRGraph_image);

	cvReleaseImage(&SFRGraph_image);
#endif
	// SFRGraph_image 


	double nPixel = 1000.0 / dbPixelSz;
	double cyPx = LinePerPixel / nPixel;
	//cyPx *=2.23;


	if (nItem == 0){

		// MTF를 결과로 하는 방식
		int half_sampling_index = -1;
		double data, old_data = 1.0;

		for (int q = 0; q<inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - cyPx);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			resultSFR = 0.0;
		else
			resultSFR = Resultmtf[half_sampling_index];
	}
	else{

		// cypx를 결과로 하는 방식
		// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
		double	dbBefore, dbNext;
		int		nBefore, nNext;

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			//		printf("Result[%d] Freq.:MTF--- %f,%f\n", i, freq[i], dbNext);
			if (dbNext < 0.5)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		double dbBeforeFreq = freq[nBefore];
		double dbNextFreq = freq[nNext];

		//	resultSFR = (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq) + dbBeforeFreq;
		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		resultSFR = dbNextFreq - (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		printf("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
			, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

		if (resultSFR > 0.5)	// 왜? 0.5보다 크면 안 됨으로
		{
			resultSFR = 0.0;
		}
		//////////////////////// 끝

		// cymm를 결과로 하는 방식
		if (nItem == 2){
			resultSFR = (resultSFR / dbPixelSz) * 1000;
		}
	}

	delete[]freq;
	delete[]Resultmtf;

	return resultSFR;
}

//=============================================================================
// Method		: SearchROIArea
// Access		: public  
// Returns		: void
// Parameter	: LPBYTE IN_RGB
// Parameter	: int tempWidth
// Parameter	: int tempHeight
// Parameter	: int FieldCenterX
// Parameter	: int FieldCenterY
// Parameter	: ST_LT_TI_SFR * pstSFR
// Qualifier	:
// Last Update	: 2017/8/11 - 15:19
// Desc.		:
//=============================================================================
void CTI_SFR::SearchROIArea(LPBYTE IN_RGB, int tempWidth, int tempHeight, int FieldCenterX, int FieldCenterY, ST_LT_TI_SFR *pstSFR)
{
  	IplImage *OriginImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 1);
  	IplImage *CannyImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 1);
  	IplImage *DilateImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 1);
  	IplImage *SmoothImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 1);
  	IplImage *RGBResultImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 3);
  	IplImage *temp_PatternImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 1);
  
  	IplImage *tempOriginImage = cvCreateImage(cvSize(tempWidth, tempHeight), IPL_DEPTH_8U, 3);
  
  	BYTE R, G, B;
  	double Sum_Y;
  
  	for (int y = 0; y < tempHeight; y++)
  	{
  		for (int x = 0; x < tempWidth; x++)
  		{
  			B = IN_RGB[y*(tempWidth * 3) + x * 3];
  			G = IN_RGB[y*(tempWidth * 3) + x * 3 + 1];
  			R = IN_RGB[y*(tempWidth * 3) + x * 3 + 2];
  
  			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3] = B;
  			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 1] = G;
  			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 2] = R;
  
  			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
  
  			OriginImage->imageData[y*OriginImage->widthStep + x] = (char)Sum_Y;
  		}
  	}
  
  	cvReleaseImage(&tempOriginImage);
  
  	cvThreshold(OriginImage, DilateImage, 0, 255, CV_THRESH_OTSU);
  	cvNot(DilateImage, DilateImage);
  
  	cvCopyImage(DilateImage, temp_PatternImage);
  
  	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);
  
  	CvMemStorage* contour_storage = cvCreateMemStorage(0);
  	CvSeq *contour = 0;
  	CvSeq *temp_contour = 0;
  
  	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
  
  	temp_contour = contour;
  
  	int counter = 0;
  
  	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
  		counter++;
  
  	CvRect *rectArray = new CvRect[counter];
  	CvRect rect;
  	double area = 0, arcCount = 0;
  	counter = 0;
  	double old_dist = 999999;
  
  	for (; contour != 0; contour = contour->h_next)
  	{
  		area = cvContourArea(contour, CV_WHOLE_SEQ);
  		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
  
  		rect = cvContourBoundingRect(contour, 1);
  		double circularity = (4.0*3.14*area) / (arcCount*arcCount);
  
  		rect = cvContourBoundingRect(contour, 1);
  
  		int center_x, center_y;
  		center_x = rect.x + rect.width / 2;
  		center_y = rect.y + rect.height / 2;
  
  
  		if (rect.width < (tempWidth / 4) && rect.height < (tempHeight / 4))
  		{
  			if (rect.width > 20 && rect.height > 20)
  			{
  				rectArray[counter] = rect;
  				counter++;
  
  				double dist = GetDistance(center_x, center_y, FieldCenterX, FieldCenterY);
  
  				if (old_dist > dist)
  				{
  					m_currPt.x = center_x - FieldCenterX;
  					m_currPt.y = center_y - FieldCenterY;
  
  					m_currPt.x = m_currPt.x / 2;
  					m_currPt.y = m_currPt.y / 2;
  
  					old_dist = dist;
  				}
  
  				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
  				//cvSaveImage("D:\\RGBResult.bmp", RGBResultImage);
  			}
  		}
  	}
  
  	int rect_cnt = 0;
  
  	if (counter > 0 && abs(m_currPt.x) < 50 && abs(m_currPt.y) < 50)
  	{
  		for (int q = 0; q < Region_SFR_MaxEnum; q++)
  		{
  			double old_dist = 9999999;
  
  			int shortest_index = -1;
  
  			if (pstSFR->stSFROp.stSFR_Region[q].bEnable == TRUE)
  			{
  				shortest_index = -1;
  
  				for (int k = 0; k < counter; k++)
  				{
  					rect = rectArray[k];
  					double WHRatio = (double)rect.width / (double)rect.height;
  					double HWRatio = (double)rect.height / (double)rect.width;
  					int White_area_cnt = 0;
  					unsigned char pixel_v = 0;
  
  					int curr_rect_center_x = rect.x + rect.width / 2;
  					int curr_rect_center_y = rect.y + rect.height / 2;
  					int roi_x = pstSFR->stSFROp.stSFR_Region[q].nPos_X;
  					int roi_y = pstSFR->stSFROp.stSFR_Region[q].nPos_Y;
  
  					double dist = GetDistance(curr_rect_center_x, curr_rect_center_y, roi_x, roi_y);
  
  					if (old_dist > dist && dist < 150)
  					{
  						shortest_index = k;
  						old_dist = dist;
  					}
  				}
  				if (shortest_index != -1)
  				{
  
  					int initW = pstSFR->stSFROp.stSFR_InitRegion[q].nWidth;
  					int initH = pstSFR->stSFROp.stSFR_InitRegion[q].nHeight;
  					int initLeft = pstSFR->stSFROp.stSFR_InitRegion[q].nPos_X - (initW / 2);
  					int initTop = pstSFR->stSFROp.stSFR_InitRegion[q].nPos_Y - (initH / 2);
  
  					int TestW = pstSFR->stSFROp.stSFR_Region[q].nWidth;
  					int TestH = pstSFR->stSFROp.stSFR_Region[q].nHeight;
  					int TestLeft = pstSFR->stSFROp.stSFR_Region[q].nPos_X - (initW / 2);
  					int TestTop = pstSFR->stSFROp.stSFR_Region[q].nPos_Y - (initH / 2);
  
  					double ROI_OffsetX = pstSFR->stSFROp.stSFR_Region[q].dbRoi_X;
  					double ROI_OffsetY = pstSFR->stSFROp.stSFR_Region[q].dbRoi_Y;
  
  
  					if (pstSFR->stSFROp.stSFR_Region[q].nType == 0)
  					{
  						if (rectArray[shortest_index].x - initW / 2 < 0)
  							TestLeft = 0;
  						else
  						{
  							int newPos = (rectArray[shortest_index].x - initW / 2);
  							int dev_x = abs(initLeft - newPos);
  
  							if (dev_x < 100)
  								TestLeft = (int)((double)rectArray[shortest_index].x - (initW / ROI_OffsetX));
  						}
  
  						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - initH / 2 < 0)
  							TestTop = 0;
  						else
  						{
  							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - initH / 2;
  							int dev_y = abs(initTop - newPos);
  
  							if (dev_y < 100)
  								TestTop = (int)((double)(rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - ((double)initH / ROI_OffsetY));
  						}
  					}
  					else if (pstSFR->stSFROp.stSFR_Region[q].nType == 1)
  					{
  						if (rectArray[shortest_index].x + rectArray[shortest_index].width - initW / 2 < 0)
  							TestLeft = 0;
  						else
  						{
  							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width - initW / 2;
  							int dev_x = abs(initLeft - newPos);
  
  							if (dev_x < 100)
  								TestLeft = (int)((double)rectArray[shortest_index].x + rectArray[shortest_index].width - initW / ROI_OffsetX);
  						}
  
  						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - initH / 2 < 0)
  							TestTop = 0;
  						else
  						{
  							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - initH / 2;
  							int dev_y = abs(initTop - newPos);
  
  							if (dev_y < 100)
  								TestTop = (int)((double)(rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - ((double)initH / ROI_OffsetY));
  						}
  					}
  					else if (pstSFR->stSFROp.stSFR_Region[q].nType == 2)
  					{
  						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - initW / 2 < 0)
  							TestLeft = 0;
  						else
  						{
  							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - initW / 2;
  							int dev_x = abs(initLeft - newPos);
  
  							if (dev_x < 100)
  								TestLeft = (int)((double)(rectArray[shortest_index].x + rectArray[shortest_index].width / 2) - ((double)initW / ROI_OffsetX));
  						}
  
  						if ((rectArray[shortest_index].y) - initH / 2 < 0)
  							TestTop = 0;
  						else
  						{
  							int newPos = (rectArray[shortest_index].y) - initH / 2;
  							int dev_y = abs(initTop - newPos);
  
  							if (dev_y < 100)
  								TestTop = (rectArray[shortest_index].y) - (int)((double)initH / ROI_OffsetY);
  						}
  					}
  					else if (pstSFR->stSFROp.stSFR_Region[q].nType == 3)
  					{
  						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - initW / 2 < 0)
  							TestLeft = 0;
  						else
  						{
  							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - initW / 2;
  							int dev_x = abs(initLeft - newPos);
  
  							if (dev_x < 100)
  								TestLeft = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - (int)((double)initW / ROI_OffsetX);
  						}
  
  						if ((rectArray[shortest_index].y + rectArray[shortest_index].height) - initH / 2 < 0)
  							TestTop = 0;
  						else
  						{
  							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height) - initH / 2;
  							int dev_y = abs(initTop - newPos);
  
  							if (dev_y < 100)
  								TestTop = (int)((rectArray[shortest_index].y + rectArray[shortest_index].height) - initH / ROI_OffsetY);
  						}
  					}
  
  					pstSFR->stSFROp.stSFR_Region[q].nPos_X = TestLeft + (TestW / 2);
  					pstSFR->stSFROp.stSFR_Region[q].nPos_Y = TestTop + (TestH / 2);
  				}
  			}
  		}
  	}
  
  	// 	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
  
  	cvReleaseMemStorage(&contour_storage);
  	cvReleaseImage(&OriginImage);
  	cvReleaseImage(&CannyImage);
  	cvReleaseImage(&DilateImage);
  	cvReleaseImage(&SmoothImage);
  	cvReleaseImage(&RGBResultImage);
  	cvReleaseImage(&temp_PatternImage);
  
	delete[]rectArray;
 
}

void CTI_SFR::SetROI_CenterRef(LPBYTE IN_RGB, ST_LT_TI_SFR *pstSFR, CvPoint CenterPoint)
{
	UINT ResultX = 0;
	UINT ResultY = 0;

	int Dis_CenterX = 0;
	int DIs_CenterY = 0;

	UINT Helf_X = pstSFR->stSFROp.iOffsetX;
	UINT Helf_Y = pstSFR->stSFROp.iOffsetY;

	for (int q = 0; q < Region_SFR_MaxEnum; q++)
	{
		Dis_CenterX = pstSFR->stSFROp.stSFR_Region[q].nPos_X - Helf_X; // 영상중심의 광축 (320,240)
		DIs_CenterY = pstSFR->stSFROp.stSFR_Region[q].nPos_Y - Helf_Y;
		
		pstSFR->stSFROp.stSFR_InitRegion[q].nPos_X = CenterPoint.x + Dis_CenterX;
		pstSFR->stSFROp.stSFR_InitRegion[q].nPos_Y = CenterPoint.y + DIs_CenterY;

		if (pstSFR->stSFROp.stSFR_InitRegion[q].nPos_X < 0 || pstSFR->stSFROp.stSFR_InitRegion[q].nPos_X > 1920 || pstSFR->stSFROp.stSFR_InitRegion[q].nPos_Y < 0 || pstSFR->stSFROp.stSFR_InitRegion[q].nPos_Y > 1080)
		{
			m_bLimit[q] = FALSE;
		}
		else
		{
			m_bLimit[q] = TRUE;
		}
	}
}

//=============================================================================
// Method		: SearchCenterPoint
// Access		: public  
// Returns		: CvPoint
// Parameter	: LPBYTE IN_RGB
// Qualifier	:
// Last Update	: 2017/8/10 - 20:34
// Desc.		:
//=============================================================================
CvPoint CTI_SFR::SearchCenterPoint(LPBYTE IN_RGB)
{
	//
	//CvPoint resultPt = cvPoint((-1) * m_nWidth, (-1) * m_nHeight);

	CvPoint resultPt = cvPoint(960, 540);

	IplImage *OriginImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage	= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBOrgImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	//double Sum_Y;

	for (int y = 0; y < m_nHeight; y++)
	{
		for (int x = 0; x < m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3 + 0];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			if (R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep + x] = (char)255;
			else
				OriginImage->imageData[y*OriginImage->widthStep + x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvCopyImage(OriginImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}


	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if (circularity > 0.8)
		{

			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			if (center_x > m_nWidth / 4 && center_x < m_nWidth * 3 / 4 && center_y > m_nHeight / 4 && center_y < m_nHeight * 3 / 4)
			{
				if (rect.width < m_nWidth / 2 && rect.height < m_nHeight / 2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, m_nWidth / 2, m_nHeight / 2);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

				}
			}
		}

		counter++;
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete[]rectArray;
	delete[]areaArray;


 	return resultPt;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2017/8/10 - 20:36
// Desc.		:
//=============================================================================
double CTI_SFR::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}


int CTI_SFR::FiducialMark_Test(IN IplImage* pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN CRect rtROI, OUT POINT& ptOUT_Center)
{
	if (nullptr == pImageBuf)
		return -1;

	POINT ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	int ROI_Left = rtROI.left;
	int ROI_Top = rtROI.top;

	int ROI_W = rtROI.right - rtROI.left;
	int ROI_H = rtROI.bottom - rtROI.top;

	IplImage *OriginImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	if (pImageBuf->nChannels == 3)
		cvCvtColor(pImageBuf, OriginImage, CV_RGB2GRAY);
	else
		cvCopy(pImageBuf, OriginImage);


	int Offset_X = 1, Offset_Y = 1;



	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 501, 3.0);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvRect *rectArray = new CvRect[2];

	int iCounter = 0;
	CvRect Rect;
	CvSeq *contour = 0;

	cvNot(DilateImage, DilateImage);

	cvErode(DilateImage, DilateImage);

	IplImage *RGBResultImage = cvCreateImage(cvSize(ROI_W, ROI_H), IPL_DEPTH_8U, 3);

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double Rate;
	double old_dist = 99999.0;
	double old_dist2 = 99999.0;
	for (; contour != 0; contour = contour->h_next)
	{
		Rect = cvContourBoundingRect(contour, 1);
		Rate = (double)Rect.width / (double)Rect.height;

		if ((Rect.width > 4) && (Rect.width <= (ROI_W / 2)) && Rect.height > 4 && (Rect.height <= (ROI_H / 2)) && Rate > 0.5 && Rate < 2.5)
		{
			double distance = GetDistance(Rect.x + Rect.width / 2, Rect.y + Rect.height / 2, dwWidth / 2, dwHeight / 2);

			if (distance < old_dist || distance < old_dist2)
			{
				if (distance >= old_dist && distance < old_dist2)
				{ // 두 거리 사이의 거리가 들어오면 
					rectArray[1] = Rect;		//두번째 거리에 있는 Rect 에 입력
					old_dist2 = distance;
				}
				else // 최소 거리가 들어오면 
				{
					rectArray[1] = rectArray[0];// 가지고 있던 Rect 저장 ( 두번째로 가까운 Rect )
					rectArray[0] = Rect;		// 새로운 Rect 저장		 ( 제일 가까운 Rect )
					old_dist2 = old_dist;
					old_dist = distance;
				}

				iCounter++;
			}
		}
	}

	if (iCounter < 2)
	{
		ptCenter.x = -1;
		ptCenter.y = -1;

		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&OriginImage);
		delete[]rectArray;

		ptOUT_Center = ptCenter;

		return 1;
	}

	ptCenter.x = (int)((double)(rectArray[0].x + (rectArray[0].x + rectArray[0].width) + rectArray[1].x + (rectArray[1].x + rectArray[1].width)) / 4.0);
	ptCenter.y = (int)((double)(rectArray[0].y + (rectArray[0].y + rectArray[0].height) + rectArray[1].y + (rectArray[1].y + rectArray[1].height)) / 4.0);

	// 	ptCenter.x += rtROI.left;
	// 	ptCenter.y += rtROI.top;

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&OriginImage);

	delete[]rectArray;

	ptOUT_Center = ptCenter;

	return 1;
}
