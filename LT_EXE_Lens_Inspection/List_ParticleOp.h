﻿#ifndef List_ParticleOp_h__
#define List_ParticleOp_h__

#pragma once

#include "Def_DataStruct.h"


typedef enum enListNum_PtOp
{
	PtOp_Object,
	PtOp_PosX,
	PtOp_PosY,
	PtOp_Width,
	PtOp_Height,
	PtOp_BruiseConc,
	PtOp_BruiseSize,
	PtOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_PtOp[] =
{
	_T(""),
	_T("PosX"),
	_T("PosY"),
	_T("Width"),
	_T("Height"),
	_T("Conc"),
	_T("Size"),
	NULL
};

typedef enum enListItemNum_PtOp
{
	PtOp_Center,
	PtOp_Middle,
	PtOp_Side,
	PtOp_ItemNum,
};

static const TCHAR*	g_lpszItem_PtOp[] =
{
	_T("Center"),
	_T("Middle"),
	_T("Side"),
	NULL
};

const int	iListAglin_PtOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_PtOp[] =
{
	60,
	50,
	50,
	50,
	50,
	50,
	50,
};
// CList_ParticleOp

class CList_ParticleOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ParticleOp)

public:
	CList_ParticleOp();
	virtual ~CList_ParticleOp();
	
	
	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	ST_LT_TI_Particle*	m_pstParticle;

	void SetPtr_Particle(ST_LT_TI_Particle *pstParticle)
	{
		if (pstParticle == NULL)
			return;

		m_pstParticle = pstParticle;
	};

protected:
	CFont		m_Font;

	DECLARE_MESSAGE_MAP()

	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL			UpdateCellData_double	(UINT nRow, UINT nCol, double dbValue);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg void	OnEnKillFocusEPtOpellEdit();

	BOOL			Change_DATA_CHECK		(UINT nIdx);
};

#endif // List_Particle_h__


