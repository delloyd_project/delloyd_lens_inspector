﻿#include "stdafx.h"
#include "TI_Rotate.h"

#define _USE_MATH_DEFINES
#include <math.h>

CTI_Rotate::CTI_Rotate()
{
	m_nWidth = 720;
	m_nHeight = 480;
}

CTI_Rotate::~CTI_Rotate()
{
}

//=============================================================================
// Method		: SetImageSize
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2017/7/7 - 10:49
// Desc.		:
//=============================================================================
BOOL CTI_Rotate::SetImageSize(DWORD dwWidth, DWORD dwHeight)
{
	if (dwWidth <= 0 || dwHeight <= 0)
		return FALSE;

	m_nWidth = dwWidth;
	m_nHeight = dwHeight;

	return TRUE;
}

//=============================================================================
// Method		: SearchCenterPoint
// Access		: public  
// Returns		: CvPoint
// Parameter	: LPBYTE IN_RGB
// Qualifier	:
// Last Update	: 2017/1/14 - 13:00
// Desc.		:
//=============================================================================
CvPoint CTI_Rotate::SearchCenterPoint(LPBYTE IN_RGB)
{
	CvPoint resultPt = cvPoint((-1) * m_nWidth, (-1) * m_nHeight);

	IplImage *OriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	//double Sum_Y;

	for (UINT y = 0; y < m_nHeight; y++)
	{
		for (UINT x = 0; x < m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			if (R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep + x] = (char)255;
			else
				OriginImage->imageData[y*OriginImage->widthStep + x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvCopyImage(OriginImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}


	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if (circularity > 0.7)
		{

			rect = cvContourBoundingRect(contour, 1);

			UINT center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			if (center_x > m_nWidth / 4 && center_x < m_nWidth * 3 / 4 && center_y > m_nHeight / 4 && center_y < m_nHeight * 3 / 4)
			{
				if (rect.width < m_nWidth / 2 && rect.height < m_nHeight / 2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, m_nWidth / 2, m_nHeight / 2);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

				}
			}
		}

		counter++;
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete[]rectArray;
	delete[]areaArray;

	return resultPt;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int ix1
// Parameter	: int iy1
// Parameter	: int ix2
// Parameter	: int iy2
// Qualifier	:
// Last Update	: 2017/1/14 - 13:02
// Desc.		:
//=============================================================================
double CTI_Rotate::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	double result;

	result = sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));

	return result;
}

//=============================================================================
// Method		: Cam_State_Offset
// Access		: public  
// Returns		: CvPoint
// Parameter	: int iCamState
// Parameter	: CvPoint CenterPoint
// Qualifier	:
// Last Update	: 2017/1/14 - 13:01
// Desc.		:
//=============================================================================
CvPoint CTI_Rotate::Cam_State_Offset(int iCamState, CvPoint CenterPoint)
{
	CvPoint buf_Point;

	buf_Point.x = -99999;
	buf_Point.y = -99999;

	if (iCamState == CAM_STATE_ORIGINAL)
	{
		buf_Point.x = CenterPoint.x;
		buf_Point.y = CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_MIRROR)
	{
		buf_Point.x = m_nWidth - CenterPoint.x;
		buf_Point.y = CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_FLIP)
	{
		buf_Point.x = CenterPoint.x;
		buf_Point.y = m_nHeight - CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_ROTATE)
	{
		buf_Point.x = m_nWidth - CenterPoint.x;
		buf_Point.y = m_nHeight - CenterPoint.y;
	}

	return buf_Point;
}

//=============================================================================
// Method		: Rotate_Test
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_Rotate * pstRotate
// Parameter	: LPBYTE pImageBuf
// Qualifier	:
// Last Update	: 2017/7/7 - 10:48
// Desc.		:
//=============================================================================
UINT CTI_Rotate::Rotate_Test(ST_LT_TI_Rotate *pstRotate, LPBYTE pImageBuf)
{
	CvPoint SearchPoint = SearchCenterPoint(pImageBuf);
	Sleep(1);

	if (SearchPoint.x > -1 && SearchPoint.y > -1)
	{
		for (int lop = 0; lop < RegionRotate_MaxEnum; lop++)
		{
			int Center_x = SearchPoint.x - (SearchPoint.x - pstRotate->stRotateOp.rectData[lop].RegionList.CenterPoint().x);
			int Center_y = SearchPoint.y - (SearchPoint.y - pstRotate->stRotateOp.rectData[lop].RegionList.CenterPoint().y);

			pstRotate->stRotateOp.rectData[lop]._Rect_Position_Sum(Center_x, Center_y, pstRotate->stRotateOp.rectData[lop].RegionList.Width(), pstRotate->stRotateOp.rectData[lop].RegionList.Height());
		}
	}

	GetCircleCoordinate(pImageBuf, pstRotate, RegionRotate_MaxEnum);
	Sleep(1);

	double Degree = Rotate_Calculation_Data(pstRotate);

	pstRotate->stRotateData.dbDegree = pstRotate->stRotateOp.dbMasterDegree - Degree;

	int nDegree = (int)(pstRotate->stRotateData.dbDegree * 100.0);
	int nDevDegree = (int)(pstRotate->stRotateOp.dbDeviDegree * 100.0);

	int nAbsDegree = abs(nDegree);

	if (nAbsDegree <= nDevDegree)
		pstRotate->stRotateData.nResult = TER_Pass;
	else
		pstRotate->stRotateData.nResult = TER_Fail;

	return pstRotate->stRotateData.nResult;
}

//=============================================================================
// Method		: GetCircleCoordinate
// Access		: public  
// Returns		: void
// Parameter	: LPBYTE IN_RGB
// Parameter	: ST_LT_TI_Rotate * pstRotate
// Parameter	: int nRectCnt
// Qualifier	:
// Last Update	: 2017/1/14 - 13:25
// Desc.		:
//=============================================================================
void CTI_Rotate::GetCircleCoordinate(LPBYTE IN_RGB, ST_LT_TI_Rotate *pstRotate, int nRectCnt)
{
	CRect *RectData = (CRect *)pstRotate->stRotateOp.rectData;
	CRect *StdRectData = (CRect *)pstRotate->stRotateOp.StdrectData;

	IplImage *OriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *voidImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);

	BYTE R, G, B;
	double Sum_Y;
	BOOL bRectReset;

	for (int y = 0; y<m_nHeight; y++)
	{
		for (int x = 0; x<m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));	
			OriginImage->imageData[y*OriginImage->widthStep + x] = (unsigned char)Sum_Y;
		}
	}

	cvSetZero(voidImage);
	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1);

	for (int k = 0; k<nRectCnt; k++)
	{
		if (RectData[k].left > 0 
			&& RectData[k].top > 0
			&& RectData[k].right < m_nWidth
			&& RectData[k].bottom < m_nHeight)
		{
			IplImage *ROIImage = cvCreateImage(cvSize(RectData[k].Width(), RectData[k].Height()), IPL_DEPTH_8U, 1);

			cvSetImageROI(SmoothImage, cvRect(RectData[k].left, RectData[k].top, RectData[k].Width(), RectData[k].Height()));
			cvSetImageROI(voidImage, cvRect(RectData[k].left, RectData[k].top, RectData[k].Width(), RectData[k].Height()));

			cvCopyImage(SmoothImage, ROIImage);

			cvThreshold(ROIImage, ROIImage, 0, 255, CV_THRESH_OTSU);
			cvNot(ROIImage, ROIImage);

			cvCopyImage(ROIImage, voidImage);

			cvResetImageROI(SmoothImage);
			cvResetImageROI(voidImage);

			cvReleaseImage(&ROIImage);
		}
	}

	cvCopyImage(voidImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 100);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;
	CvRect rect;
	double area = 0, arcCount = 0;
	double min_dist[255];
	int center_x = 0, center_y = 0;

	for (int k = 0; k<nRectCnt; k++)
	{
		bRectReset = TRUE;

		min_dist[k] = 99999.0;

		temp_contour = contour;

		for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		{
			area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
			arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);

			rect = cvContourBoundingRect(temp_contour, 1);

			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			if (RectData[k].Width() > rect.width && RectData[k].Height() > rect.height)
			{
				if (RectData[k].left < center_x && (RectData[k].left + RectData[k].Width()) > center_x)
				{
					if (RectData[k].top < center_y && (RectData[k].top + RectData[k].Height()) > center_y)
					{
						double dist = GetDistance(center_x, center_y, RectData[k].left + RectData[k].Width() / 2, RectData[k].top + RectData[k].Height() / 2);

						if (dist < min_dist[k])
						{
							if (rect.width > 10 && rect.height > 10)
							{
								pstRotate->stRotateOp.rectData[k]._Rect_Position_Sum(center_x, center_y, RectData[k].Width(), RectData[k].Height());

								cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

								min_dist[k] = dist;
								bRectReset = FALSE;
							}
						}
					}
				}
			}

			if (bRectReset)
			{
				RectData[k].top		= StdRectData[k].top;
				RectData[k].left	= StdRectData[k].left;
				RectData[k].right	= StdRectData[k].right;
				RectData[k].bottom	= StdRectData[k].bottom;
			}
		}
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&voidImage);
}

//=============================================================================
// Method		: Rotate_Calculation_Data
// Access		: public  
// Returns		: double
// Parameter	: ST_LT_TI_Rotate * pRectData
// Qualifier	:
// Last Update	: 2017/1/14 - 13:26
// Desc.		:
//=============================================================================
double CTI_Rotate::Rotate_Calculation_Data(ST_LT_TI_Rotate *pRectData)
{
	double Left_Line_center_x = 0.0;
	double Left_Line_center_y = 0.0;
	double Right_Line_center_x = 0.0;
	double Right_Line_center_y = 0.0;

	Left_Line_center_x = (pRectData->stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().x + pRectData->stRotateOp.rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().x) / 2.0f;
	Left_Line_center_y = (pRectData->stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().y + pRectData->stRotateOp.rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().y) / 2.0f;

	Right_Line_center_x = (pRectData->stRotateOp.rectData[RegionRotate_RightTop].RegionList.CenterPoint().x + pRectData->stRotateOp.rectData[RegionRotate_RightBottom].RegionList.CenterPoint().x) / 2.0f;
	Right_Line_center_y = (pRectData->stRotateOp.rectData[RegionRotate_RightTop].RegionList.CenterPoint().y + pRectData->stRotateOp.rectData[RegionRotate_RightBottom].RegionList.CenterPoint().y) / 2.0f;

	double rad = atan2((Left_Line_center_y - Right_Line_center_y), (Left_Line_center_x - Right_Line_center_x));

	double degree = -99999.0;

	if (rad < 0)
	{
		degree = rad * 180.0 / M_PI + 180.0;
	}
	else
	{
		degree = rad * 180.0 / M_PI - 180.0;
	}

	return Cam_State_Degree(pRectData->stRotateOp.nCameraState, degree);
}

//=============================================================================
// Method		: Cam_State_Degree
// Access		: public  
// Returns		: double
// Parameter	: int iCamState
// Parameter	: double dbDegree
// Qualifier	:
// Last Update	: 2017/1/14 - 13:34
// Desc.		:
//=============================================================================
double CTI_Rotate::Cam_State_Degree(int iCamState, double dbDegree)
{
	double result = dbDegree;

	switch (iCamState)
	{
		case CAM_STATE_ORIGINAL:
			result *= -1;
			break;

		case CAM_STATE_FLIP:
			result *= -1;
			break;

		default:
			break;
	}

	return result;
}