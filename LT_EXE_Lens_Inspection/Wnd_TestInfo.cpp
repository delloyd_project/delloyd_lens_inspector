﻿//*****************************************************************************
// Filename	: 	Wnd_TestInfo.cpp
// Created	:	2016/7/5 - 16:18
// Modified	:	2016/7/5 - 16:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_TestInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_TestInfo.h"
#include "CommonFunction.h"
#include "resource.h"

// CWnd_TestInfo

typedef enum TestInfoBtn_ID
{
	IDC_BTN_START = 1001,
	IDC_BTN_STOP,
	IDC_BTN_LOTSTART,
	IDC_BTN_LOTSTOP,
	IDC_BTN_MODEL,
	IDC_BTN_LED_ON,
	IDC_BTN_LED_OFF,
	IDC_BTN_IMAGESAVE,
	IDC_BTN_MASTER,
	IDC_BTN_MANUAL,
	IDC_BTN_AUTO,
	IDC_BTN_MANUAL_DEG,
	IDC_BTN_MAX_NUM,
};

typedef enum TestInfoEdit_ID
{
	IDC_ED_DEGREE = 2000,
};

IMPLEMENT_DYNAMIC(CWnd_TestInfo, CWnd)

CWnd_TestInfo::CWnd_TestInfo()
{
	VERIFY(m_Font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestInfo::~CWnd_TestInfo()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_START, IDC_BTN_MAX_NUM, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_TestInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
int CWnd_TestInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < TB_MaxEnum; nIdx++)
	{
		if (nIdx == TB_Manual || nIdx == TB_Auto)
		{
			m_bn_Test[nIdx].Create(g_szTestButton[nIdx], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_START + nIdx);
			m_bn_Test[nIdx].SetImage(IDB_UNCHECKED_16);
			m_bn_Test[nIdx].SetCheckedImage(IDB_CHECKED_16);
			m_bn_Test[nIdx].SizeToContent();
			m_bn_Test[nIdx].SetCheck(BST_UNCHECKED);
		}
		else
		{
			m_bn_Test[nIdx].Create(g_szTestButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_START + nIdx);
			m_bn_Test[nIdx].SetFont(&m_Font);
		}
	}

	for (UINT nIdx = 0; nIdx < TS_MaxEnum; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szTestStatic[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}
	
	// Edit
	for (UINT nIdx = 0; nIdx < TE_MaxEnum; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_ED_DEGREE + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T(""));
	}

	m_Group.SetTitle(L"TEST INFO");
	if (!m_Group.Create(_T("TEST INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (USE_MASTER_SET == 0)
	{
		m_bn_Test[TB_MasterSet].ShowWindow(FALSE);
	} 
	else
	{
		m_bn_Test[TB_MasterSet].ShowWindow(TRUE);
	}

	m_bn_Test[TB_MasterSet].EnableWindow(FALSE);
	//ManualAutoMode(FALSE);
	m_bn_Test[TB_Start].EnableWindow(FALSE);
	m_bn_Test[TB_Stop].EnableWindow(FALSE);
	m_bn_Test[TB_LotStop].EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
void CWnd_TestInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 10;
	int iMagrin  = 5;

	int iLeft = iSpacing;
	int iTop = 26;
	int iWidth = cx - iSpacing - iSpacing;
	int iHeight = cy - iMagrin - iMagrin - 26;
	int ibnWidth = (iWidth - (iMagrin * 1)) / 2;
	int ibnWidth2 = (iWidth - (iMagrin * 3)) / 4;
	int ibnWidth3 = (iWidth - (iMagrin * 7)) / 8;
	int ibnHeight = (iHeight - (iMagrin * 2)) / 3;
	int ibnHeight2 = (iHeight - (iMagrin * 5)) / 6;

	m_Group.MoveWindow(0, 0, cx, cy);

	for (UINT nIdx = 0; nIdx < TB_MaxEnum; nIdx++)
	{
		m_bn_Test[nIdx].MoveWindow(0, 0, 0, 0);
	}

	iLeft = iSpacing;
	m_bn_Test[TB_ModelChange].MoveWindow(iLeft, iTop, ibnWidth*2, ibnHeight/2);

	iLeft = iSpacing;
	iTop += ibnHeight/2 + iMagrin;
	ibnHeight += ibnHeight / 4;
	m_bn_Test[TB_Start].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

	iLeft += ibnWidth + iMagrin;
	m_bn_Test[TB_Stop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight );

	iLeft = iSpacing;
	iTop += ibnHeight  + iMagrin;
	m_bn_Test[TB_LotStart].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

	iLeft += ibnWidth + iMagrin;
	m_bn_Test[TB_LotStop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight );



//	iLeft += ibnWidth + iMagrin;
//	m_bn_Test[TB_LED_On].MoveWindow(iLeft, iTop, ibnWidth / 2 - iMagrin, ibnHeight);

// 	iLeft += ibnWidth / 2 + iMagrin;
// 	m_bn_Test[TB_LED_Off].MoveWindow(iLeft, iTop, ibnWidth / 2 - iMagrin, ibnHeight);

	//iLeft += ibnWidth + iMagrin;
	//m_bn_Test[TB_MasterSet].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

// 	iLeft += ibnWidth + iMagrin;
// 	m_bn_Test[TB_Auto].MoveWindow(iLeft, iTop, ibnWidth2, ibnHeight2);
// 
// 	iLeft += ibnWidth2 + iMagrin;
// 	m_bn_Test[TB_Manual].MoveWindow(iLeft, iTop, ibnWidth2, ibnHeight2);
// 
// 	iLeft = (ibnWidth + iMagrin + iSpacing);
// 	iTop += ibnHeight2 + iMagrin;
// 	m_ed_Item[TE_Manual].MoveWindow(iLeft, iTop, ibnWidth2, ibnHeight2);
// 
// 	iLeft += ibnWidth2 + iMagrin;
// 	m_bn_Test[TB_ManualDeg].MoveWindow(iLeft, iTop, ibnWidth3, ibnHeight2);
// 
// 	iLeft += ibnWidth3 + iMagrin;
// 	m_bn_Test[TB_ManualDeg1].MoveWindow(iLeft, iTop, ibnWidth3, ibnHeight2);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
BOOL CWnd_TestInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: ButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/6/21 - 17:25
// Desc.		:
//=============================================================================
void CWnd_TestInfo::PermissionMode(enPermissionMode InspMode)
{
	m_mode = InspMode;
	switch (InspMode)
	{
	case Permission_Operator:
		m_bn_Test[TB_Start].EnableWindow(FALSE);
		m_bn_Test[TB_Stop].EnableWindow(FALSE);
		m_bn_Test[TB_LotStart].EnableWindow(TRUE);
		m_bn_Test[TB_LotStop].EnableWindow(FALSE);
		m_bn_Test[TB_ModelChange].EnableWindow(TRUE);
		m_bn_Test[TB_MasterSet].EnableWindow(FALSE);
		break;

	case Permission_Manager:
	case Permission_Engineer:
	case Permission_Administrator:
	case Permission_CNC:
		m_bn_Test[TB_Start].EnableWindow(TRUE);
		m_bn_Test[TB_Stop].EnableWindow(TRUE);
		m_bn_Test[TB_LotStart].EnableWindow(TRUE);
		m_bn_Test[TB_LotStop].EnableWindow(FALSE);
		m_bn_Test[TB_ModelChange].EnableWindow(TRUE);
		m_bn_Test[TB_MasterSet].EnableWindow(FALSE);
		break;
	case Permission_MES:
		m_bn_Test[TB_Start].EnableWindow(TRUE);
		m_bn_Test[TB_LotStart].EnableWindow(FALSE);
		m_bn_Test[TB_LotStop].EnableWindow(FALSE);
		m_bn_Test[TB_ModelChange].EnableWindow(TRUE);
		m_bn_Test[TB_MasterSet].EnableWindow(TRUE);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: ButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/7/11 - 10:07
// Desc.		:
//=============================================================================
void CWnd_TestInfo::ButtonEnable(BOOL bMode)
{
	m_bn_Test[TB_LotStart].EnableWindow(bMode);
	m_bn_Test[TB_LotStop].EnableWindow(!bMode);
	m_bn_Test[TB_ModelChange].EnableWindow(bMode);
	if (m_mode != Permission_Administrator || bMode==FALSE)	bMode = (bMode == TRUE) ? FALSE : TRUE;
	m_bn_Test[TB_Start].EnableWindow(bMode);
	m_bn_Test[TB_Stop].EnableWindow(bMode);
}

//************************************
// Method:    ManualAutoMode
// FullName:  CWnd_TestInfo::ManualAutoMode
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL bMode
//************************************
// void CWnd_TestInfo::ManualAutoMode(BOOL bMode)
// {
// 	if (bMode == TRUE)	// Manual
// 	{
// 		m_bn_Test[TB_Manual].SetCheck(BST_CHECKED);
// 		m_bn_Test[TB_Auto].SetCheck(BST_UNCHECKED);
// // 		m_bn_Test[TB_ManualDeg].EnableWindow(TRUE);
// // 		m_bn_Test[TB_ManualDeg1].EnableWindow(TRUE);
// // 		m_ed_Item[TE_Manual].EnableWindow(TRUE);
// 	}
// 	else				// Auto
// 	{
// 		m_bn_Test[TB_Auto].SetCheck(BST_CHECKED);
// 		m_bn_Test[TB_Manual].SetCheck(BST_UNCHECKED);
// 		ManualBtnEnable(bMode);
// 	}
// }

//************************************
// Method:    ManualBtnEnable
// FullName:  CWnd_TestInfo::ManualBtnEnable
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL bMode
//************************************
void CWnd_TestInfo::ManualBtnEnable(BOOL bMode)
{
	if (bMode == TRUE)	// Manual
	{
		m_bn_Test[TB_ManualDeg].EnableWindow(TRUE);
		m_bn_Test[TB_ManualDeg1].EnableWindow(TRUE);
		m_ed_Item[TE_Manual].EnableWindow(TRUE);
	}
	else				// Auto
	{
		m_bn_Test[TB_ManualDeg].EnableWindow(FALSE);
		m_bn_Test[TB_ManualDeg1].EnableWindow(FALSE);
		m_ed_Item[TE_Manual].EnableWindow(FALSE);
	}
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/6/21 - 14:33
// Desc.		:
//=============================================================================
void CWnd_TestInfo::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_START;
	CString strData;
	double dDegreePuls = 0;
	
	switch (nIndex)
	{
	case TB_Start:
		GetOwner()->SendNotifyMessage(WM_TEST_START, 0, 0);
		break;
	case TB_Stop:
	{
		m_bn_Test[TB_Start].EnableWindow(FALSE);
		m_bn_Test[TB_Stop].EnableWindow(FALSE);
		
		GetOwner()->SendMessage(WM_TEST_STOP, 0, 0);
		Sleep(50);
		m_bn_Test[TB_Start].EnableWindow(TRUE);
		m_bn_Test[TB_Stop].EnableWindow(TRUE);
		ChangeBtnState(TRUE);
		break;
	}
	case TB_LotStart:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, LOT_Start, 0);
		break;
	case TB_LotStop:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, LOT_End, 0);
		break;
	case TB_ModelChange:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, Model_Change, 0);
		break; 
// 	case TB_LED_On:
// 		GetOwner()->SendNotifyMessage(WM_MANUAL_CONTROL, 0, Man_Cmd_LED_On);
// 		break;
// 	case TB_LED_Off:
// 		GetOwner()->SendNotifyMessage(WM_MANUAL_CONTROL, 0, Man_Cmd_LED_Off);
// 		break;

	case TB_MasterSet:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, MASTER_Start, 0);
		break;
	case TB_Manual:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, ManualMode, 0);
		break;
	case TB_Auto:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, AutoMode, 0);
		break;
	case TB_ManualDeg://WM_MANUAL_DEGREE
		m_ed_Item[TE_Manual].GetWindowTextW(strData);
		dDegreePuls = _ttof(strData) * 100;

		if (dDegreePuls < 0)
		{
			dDegreePuls = 0;
		}
		else if (dDegreePuls > 4500)
		{
			dDegreePuls = 4500;
		}

		GetOwner()->SendNotifyMessage(WM_MANUAL_DEGREE, TB_ManualDeg, dDegreePuls);
		break;
	case TB_ManualDeg1://WM_MANUAL_DEGREE
		m_ed_Item[TE_Manual].GetWindowTextW(strData);
		dDegreePuls = _ttof(strData) * 100;

		if (dDegreePuls < 0)
		{
			dDegreePuls = 0;
		}
		else if (dDegreePuls > 4500)
		{
			dDegreePuls = 4500;
		}

		GetOwner()->SendNotifyMessage(WM_MANUAL_DEGREE, TB_ManualDeg1, dDegreePuls);
		break;
	default:
		break;
	}
}

void CWnd_TestInfo::ChangeBtnState(BOOL bMode){

	if (m_bn_Test[TB_LotStop].IsWindowEnabled() == FALSE)
	{
		m_bn_Test[TB_ModelChange].EnableWindow(bMode);
		m_bn_Test[TB_LotStart].EnableWindow(bMode);
	}	
}

void CWnd_TestInfo::ChangeStartBtnState(BOOL bMode)
{
	m_bn_Test[TB_Start].EnableWindow(bMode);
}
