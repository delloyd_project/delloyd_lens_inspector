﻿// Wnd_CenterPointOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_RotateOp.h"
#include "resource.h"
#include "CommonFunction.h"

// CWnd_RotateOp

typedef enum CenterPointOp_ID
{
	IDC_COMBO_CP_TESTMODE = 1000,
	IDC_COMBO_CP_CAMERASTATE,
	IDC_EDIT_IMAGE_SENSOR,
	IDC_EDIT_TRY_COUNT,
	IDC_LIST_ROTATEOP,
	IDC_LIST_ROTATESUBOP,
	IDC_BTN_ROTATE_TEST,
	IDC_BTN_ROTATE_TEST_STOP,
	IDC_BTN_DEFAULT_SET,
};

IMPLEMENT_DYNAMIC(CWnd_RotateOp, CWnd)

CWnd_RotateOp::CWnd_RotateOp()
{
	m_pstModelInfo = NULL;
	m_bTest_Flag = FALSE;

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_RotateOp::~CWnd_RotateOp()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_RotateOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_ROTATE_TEST, OnBnClickedBnTest)
	ON_BN_CLICKED(IDC_BTN_ROTATE_TEST_STOP, OnBnClickedBnTestStop)
	ON_BN_CLICKED(IDC_BTN_DEFAULT_SET, OnBnClickedBnDefaultSet)
	ON_CBN_SELENDOK(IDC_COMBO_CP_TESTMODE, OnCbnSelendTestMode)
END_MESSAGE_MAP()

// CWnd_RotateOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_RotateOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < ComboI_RT_MAXNUM; nIdex++)
		m_Cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_COMBO_CP_TESTMODE + nIdex);

	for (UINT nIdex = 0; nIdex < CAM_STATE_MAXNUM; nIdex++)
		m_Cb_Item[ComboI_RT_CAMERASTATE].InsertString(nIdex, g_szCamState[nIdex]);

	for (UINT nIdex = 0; nIdex < RT_TestMode_MaxNum; nIdex++)
		m_Cb_Item[ComboI_RT_TESTMODE].InsertString(nIdex, g_szRotateTestMode[nIdex]);

	for (UINT nIdex = 0; nIdex < STI_RT_MAXNUM; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Item[nIdex].Create(g_szRotate_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < Edt_RT_MAXNUM; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, CRect(0, 0, 0, 0), this, IDC_EDIT_IMAGE_SENSOR + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0.00"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	m_ListRtOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ROTATEOP);
	m_ListRtSubOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ROTATESUBOP);

	for (UINT nIdex = 0; nIdex < Btn_RT_MAXNUM; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szRotate_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ROTATE_TEST + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font_Data);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (m_pstModelInfo == NULL)
		return;

	int iMargin = 10;
	int iSpacing = 4;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int Static_W = iWidth / 4;
	int Static_H = 26;
	int Combo_W = iWidth / 3;
	int Combo_H = 26;
	int iBtnWidth = iWidth / 7;
	int iBtnHeight = 26;
	m_st_Item[STI_RT_IMAGESENSOR].MoveWindow(0, 0, 0, 0);

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Item[Btn_RT_ROTATE_TEST].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[Btn_CT_ROTATE_TEST_STOP].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[Btn_RT_DEFAULT_SET].MoveWindow(iLeft, iTop, iBtnWidth, Static_H);

	iTop += iBtnHeight + iSpacing;
	iLeft = (Static_W + Combo_W + iSpacing) + iMargin + iMargin;
	m_ListRtSubOp.MoveWindow(iLeft, iTop, iWidth - iLeft + iMargin, iHeight - iTop);

	iTop = iMargin;
	iLeft = iMargin;
	m_st_Item[STI_RT_TESTMODE].MoveWindow(iLeft, iTop, Static_W, Static_H);
	iLeft += Static_W + iSpacing;
	m_Cb_Item[ComboI_RT_TESTMODE].MoveWindow(iLeft, iTop, Combo_W, Static_H);
	iTop += Static_H + iSpacing;

	iLeft = iMargin;
	m_st_Item[STI_RT_CAMERASTATE].MoveWindow(iLeft, iTop, Static_W, Static_H);
	iLeft += Static_W + iSpacing;
	m_Cb_Item[ComboI_RT_CAMERASTATE].MoveWindow(iLeft, iTop, Combo_W, Static_H);
	iTop += Static_H + iSpacing;

	if (m_pstModelInfo->stRotate.stRotateOp.nTestMode == RT_Fine_tune_TestMode)
	{
		iLeft = iMargin;
		m_st_Item[STI_RT_IMAGESENSOR].MoveWindow(iLeft, iTop, Static_W, Static_H);
		iLeft += Static_W + iSpacing;
		m_ed_Item[Edt_RT_IMAGESENSOR].MoveWindow(iLeft, iTop, Combo_W, Static_H);
		iTop += Static_H + iSpacing;

	iLeft = iMargin;
		m_st_Item[STI_RT_TRYCOUNT].MoveWindow(iLeft, iTop, Static_W, Static_H);
	iLeft += Static_W + iSpacing;
		m_ed_Item[Edt_RT_TRYCOUNT].MoveWindow(iLeft, iTop, Combo_W, Static_H);
	iTop += Static_H + iSpacing;
	}
	else
	{
	iLeft = iMargin;
		m_st_Item[STI_RT_IMAGESENSOR].MoveWindow(0, 0, 0, 0);
		m_ed_Item[Edt_RT_IMAGESENSOR].MoveWindow(0, 0, 0, 0);
		m_st_Item[STI_RT_TRYCOUNT].MoveWindow(0, 0, 0, 0);
		m_ed_Item[Edt_RT_TRYCOUNT].MoveWindow(0, 0, 0, 0);
	}

	iLeft = iMargin;
	int List_W = (Static_W + Combo_W + iSpacing);
	int List_H = iHeight - iTop;
	m_ListRtOp.MoveWindow(iLeft, iTop, List_W, List_H);
	
}

//=============================================================================
// Method		: OnCbnSelendTestMode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/13 - 11:05
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnCbnSelendTestMode()
{
	m_pstModelInfo->stRotate.stRotateOp.nTestMode = m_Cb_Item[ComboI_RT_TESTMODE].GetCurSel();

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	SetUI();
}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/13 - 9:20
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnBnClickedBnTest()
{
	m_bn_Item[Btn_RT_ROTATE_TEST].EnableWindow(FALSE);

	UINT nItem = m_Cb_Item[ComboI_RT_TESTMODE].GetCurSel();

	if (nItem == RT_Fine_tune_TestMode)
	{
		GetOwner()->SendNotifyMessage(WM_MANUAL_TEST, RT_Fine_tune_TestMode, MT_Cmd_Rotate);
	}

	if (nItem == RT_Measurement_TestMode)
	{
		m_bTest_Flag = TRUE;

	while (m_bTest_Flag == TRUE)
	{
		DoEvents(50);
			GetOwner()->SendNotifyMessage(WM_MANUAL_TEST, RT_Measurement_TestMode, MT_Cmd_Rotate);
	}
}

	m_bn_Item[Btn_RT_ROTATE_TEST].EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnBnClickedBnTestStop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/13 - 11:01
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnBnClickedBnTestStop()
{
	m_bTest_Flag = FALSE;
	m_bn_Item[Btn_RT_ROTATE_TEST].EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnBnClickedBnDefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/14 - 10:29
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnBnClickedBnDefaultSet()
{
	if (m_pstModelInfo == NULL)
		return;
	
	int PosX[RegionRotate_MaxEnum] = { 0, };
	int PosY[RegionRotate_MaxEnum] = { 0, };
	int Width[RegionRotate_MaxEnum] = { 0, };
	int Height[RegionRotate_MaxEnum] = { 0, };

	int RectWidth = 100;
	int RectHeight = 100;
	int Gap_Width = 150;
	int Gap_Height = 150;

	int RectPosX = (m_pstModelInfo->dwWidth / 2) - (Gap_Width / 2);
	int RectPosY = (m_pstModelInfo->dwHeight / 2) - (Gap_Height / 2);

	int buf_PosX = 0;
	int buf_PosY = 0;
	int buf_Width = 0;
	int buf_Height = 0;

	for (int iRg = 0; iRg < RegionRotate_MaxEnum; iRg++)
	{
		buf_PosX = 0;
		buf_PosY = 0;
		buf_Width = 0;
		buf_Height = 0;

		if (iRg == 0)
		{
			PosX[iRg] = RectPosX;
			PosY[iRg] = RectPosY;
		}
		else if (iRg == 1)
		{
			PosX[iRg] = RectPosX + Gap_Width;
			PosY[iRg] = RectPosY;
		}
		else if (iRg == 2)
		{
			PosX[iRg] = RectPosX;
			PosY[iRg] = RectPosY + Gap_Height;
		}
		else if (iRg == 3)
		{
			PosX[iRg] = RectPosX + Gap_Width;
			PosY[iRg] = RectPosY + Gap_Height;
		}

		Width[iRg] = RectWidth;
		Height[iRg] = RectHeight;

		m_pstModelInfo->stRotate.stRotateOp.rectData[iRg]._Rect_Position_Sum(PosX[iRg], PosY[iRg], Width[iRg], Height[iRg]);
	}

	m_ListRtOp.InsertFullData();
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:00
// Desc.		:
//=============================================================================
void CWnd_RotateOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Rotation, 0);
	}
	else
	{
		OnBnClickedBnTestStop();
		DoEvents(100);
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_RotateOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_RotateOp::SetUI()
{
	CString str;

	if (m_pstModelInfo == NULL)
		return;

	m_ListRtOp.SetPtr_Rotate(&m_pstModelInfo->stRotate);
	m_ListRtOp.InsertFullData();

	m_ListRtSubOp.SetTestModeChange(m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode);
	m_ListRtSubOp.SetPtr_Rotate(&m_pstModelInfo->stRotate);
	m_ListRtSubOp.InsertFullData();

	m_Cb_Item[ComboI_RT_TESTMODE].SetCurSel(m_pstModelInfo->stRotate.stRotateOp.nTestMode);

	m_Cb_Item[ComboI_RT_CAMERASTATE].SetCurSel(m_pstModelInfo->stRotate.stRotateOp.nCameraState);

	str.Format(_T("%.2f"), m_pstModelInfo->stRotate.stRotateOp.dbImageSensorPix);
	m_ed_Item[Edt_RT_IMAGESENSOR].SetWindowText(str);

	str.Format(_T("%d"), m_pstModelInfo->stRotate.stRotateOp.nWriteCnt);
	m_ed_Item[Edt_RT_TRYCOUNT].SetWindowText(str);

}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_RotateOp::GetUI()
{
	CString str;

	if (m_pstModelInfo == NULL)
		return;

	m_pstModelInfo->stRotate.stRotateOp.nTestMode = m_Cb_Item[ComboI_RT_TESTMODE].GetCurSel();

	m_pstModelInfo->stRotate.stRotateOp.nCameraState = m_Cb_Item[ComboI_RT_CAMERASTATE].GetCurSel();

	m_ed_Item[Edt_RT_IMAGESENSOR].GetWindowText(str);
	m_pstModelInfo->stRotate.stRotateOp.dbImageSensorPix = _ttof(str);

	m_ed_Item[Edt_RT_TRYCOUNT].GetWindowText(str);
	m_pstModelInfo->stRotate.stRotateOp.nWriteCnt = _ttoi(str);
}