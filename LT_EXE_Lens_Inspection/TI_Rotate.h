﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_Rotate
{
public:
	CTI_Rotate();
	~CTI_Rotate();

public:

	BOOL	SetImageSize			(DWORD dwWidth, DWORD dwHeight);

	/*측정알고리즘*/
	CvPoint SearchCenterPoint		(LPBYTE IN_RGB);
	double	GetDistance				(int ix1, int iy1, int ix2, int iy2);
	CvPoint Cam_State_Offset		(int iCamState, CvPoint CenterPoint);
	
	/*로테이트 측정*/
	UINT	Rotate_Test				(ST_LT_TI_Rotate *pstRotate, LPBYTE pImageBuf);

	/*측정알고리즘*/
	void	GetCircleCoordinate		(LPBYTE IN_RGB, ST_LT_TI_Rotate *stpRectData, int nRectCnt);
	double	Rotate_Calculation_Data	(ST_LT_TI_Rotate *pRectData);
	double  Cam_State_Degree		(int iCamState, double dbDegree);

protected:
	UINT m_nWidth;
	UINT m_nHeight;
};