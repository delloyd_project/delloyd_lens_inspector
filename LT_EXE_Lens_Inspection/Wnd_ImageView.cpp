﻿// Wnd_ImageView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ImageView.h"


// CWnd_ImageView

enum enVideo_Id
{
	IDC_STATIC_VIDEO = 1000,
};
IMPLEMENT_DYNAMIC(CWnd_ImageView, CWnd)

CWnd_ImageView::CWnd_ImageView()
{

}

CWnd_ImageView::~CWnd_ImageView()
{
}


BEGIN_MESSAGE_MAP(CWnd_ImageView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CWnd_ImageView 메시지 처리기입니다.




int CWnd_ImageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_stVideo.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_stVideo.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_stVideo.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | WS_TABSTOP, rectDummy, this, IDC_STATIC_VIDEO);

	return 0;
}


BOOL CWnd_ImageView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CWnd::PreCreateWindow(cs);
}


void CWnd_ImageView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (cx == 0 || cy == 0)
	{
		return;
	}
	m_stVideo.MoveWindow(0, 0, cx, cy);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2017/2/27 - 15:02
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnEraseBkgnd(CDC* pDC)
{
	m_stVideo.Invalidate();
	return TRUE;

}
