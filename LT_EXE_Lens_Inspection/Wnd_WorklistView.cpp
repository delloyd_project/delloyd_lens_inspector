﻿//*****************************************************************************
// Filename	: Wnd_WorklistView.cpp
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_WorklistView.h"

#define	IDC_LIST_ARRAY			1201
#define	IDC_LIST_CHANNEL		1202


enum enWorklist{
	IDC_LIST_TOTAL = 1000,
	//IDC_LIST_CURRENT,
	//IDC_LIST_LED,
	IDC_LIST_EIAJ,
	IDC_LIST_CENTERPOINT,
	//IDC_LIST_SFR,
	//IDC_LIST_ROTATE,
	//IDC_LIST_PARTICLE,
};

//=============================================================================
// CWnd_WorklistView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_WorklistView, CWnd)

//=============================================================================
//
//=============================================================================
CWnd_WorklistView::CWnd_WorklistView()
{
	VERIFY(m_font_Default.CreateFont(
		32,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		12,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("굴림체")));			// lpszFacename

}

CWnd_WorklistView::~CWnd_WorklistView()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_WorklistView, CWnd)
	ON_WM_CREATE	()
	ON_WM_SIZE		()
	ON_NOTIFY		(NM_CLICK, IDC_LIST_ARRAY, OnNMClickListArray)
END_MESSAGE_MAP()


//=============================================================================
// CWnd_WorklistView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_WorklistView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
int CWnd_WorklistView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_list_Array.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ARRAY);
	m_tc_Worklist.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_TOP);

	m_List_Total.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_TOTAL);
	//m_List_Current.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_CURRENT);
	m_List_EIAJ.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_EIAJ);
 	m_List_CenterPoint.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_CENTERPOINT);

// 	m_List_Rotate.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_ROTATE);
// 	m_List_Particle.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_PARTICLE);
 //	m_List_SFR.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_SFR);
///	m_List_LED.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_LED);
	
	m_tc_Worklist.AddTab(&m_List_Total,			_T("Total		"), 0, FALSE);
	//m_tc_Worklist.AddTab(&m_List_Current,		_T("Current		"), 1, FALSE);
	//m_tc_Worklist.AddTab(&m_List_LED,		 _T("LED		"), 1, FALSE);
 	m_tc_Worklist.AddTab(&m_List_CenterPoint,	_T("CenterPoint "),	2, FALSE);
//	m_tc_Worklist.AddTab(&m_List_SFR,			_T("Focus (SFR)"), 3, FALSE);
	m_tc_Worklist.AddTab(&m_List_EIAJ, _T("Focus (EIAJ)"), 4, FALSE);
	m_tc_Worklist.EnableTabSwap(FALSE);

// 	m_tc_Worklist.AddTab(&m_List_SFR,			_T("SFR			"), 3, FALSE);
// 	m_tc_Worklist.AddTab(&m_List_Rotate,		_T("Rotate		"), 4, FALSE);
// 	m_tc_Worklist.AddTab(&m_List_Particle,		_T("Particle	"), 5, FALSE);
	m_tc_Worklist.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: CWnd_WorklistView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
void CWnd_WorklistView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	iTop = iMagrin;
	m_tc_Worklist.MoveWindow(iLeft, iTop, iWidth, iHeight);

}

//=============================================================================
// Method		: CWnd_WorklistView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_WorklistView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClickListArray
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * result
// Qualifier	:
// Last Update	: 2016/8/12 - 16:05
// Desc.		:
//=============================================================================
void CWnd_WorklistView::OnNMClickListArray(NMHDR * pNMHDR, LRESULT * result)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;

	int index = pNMView->iItem;

	if (0 <= index)
	{
		ST_Worklist stWorklist;
		
	}
	else
	{
	}
}

//=============================================================================
// Method		: InsertWorklist
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 16:56
// Desc.		:
//=============================================================================
void CWnd_WorklistView::InsertWorklist(__in const ST_Worklist* pstWorklist)
{
	m_list_Array.InsertWorklist(pstWorklist);

	int iCount = m_list_Array.GetItemCount();
	
	if (0 < iCount)
	{
		m_list_Array.SetSelectionMark(iCount - 1);
		m_list_Array.SetItemState(iCount - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_list_Array.SetFocus();

		//m_list_Channel.SetWorklistChInfo(pstWorklist);
	}
}

//=============================================================================
// Method		: GetPtr_Worklist
// Access		: public  
// Returns		: void
// Parameter	: ST_Worklist & pWorklist
// Qualifier	:
// Last Update	: 2017/2/22 - 14:05
// Desc.		:
//=============================================================================
void CWnd_WorklistView::GetPtr_Worklist(ST_Worklist& pWorklist)
{
	pWorklist.pList_Total		= &m_List_Total;
	//pWorklist.pList_Current		= &m_List_Current;
	pWorklist.pList_EIAJ		= &m_List_EIAJ;
 	pWorklist.pList_CenterPoint	= &m_List_CenterPoint;
 //	pWorklist.pList_SFR			= &m_List_SFR;
//	pWorklist.pList_LED			= &m_List_LED;
// 	pWorklist.pList_Rotate		= &m_List_Rotate;
// 	pWorklist.pList_Particle	= &m_List_Particle;
}