﻿#ifndef List_Work_CenterPoint_h__
#define List_Work_CenterPoint_h__

#pragma once


#include "Def_Test.h"

typedef enum enListNum_CP_Worklist
{
	CP_W_Time,
	CP_W_Equipment,
	CP_W_Model,
	CP_W_SWVersion,
	CP_W_LOTNum,
	CP_W_Operator,
	CP_W_RetryCnt,
	CP_W_Result,
	CP_W_PosX,
	CP_W_PosY,
	CP_W_OffsetX,
	CP_W_OffsetY,
	CP_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_CP_Worklist[] =
{
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SWVersion"),
	_T("LOTNum"),
	_T("Operator"),
	_T("TestCount"),
	_T("Result"),
	_T("PosX"),
	_T("PosY"),
	_T("OffsetX"),
	_T("OffsetY"),
	NULL,
};

const int	iListAglin_CP_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CP_Worklist[] =
{
	130,
	120,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,

};
// CList_Work_CenterPoint

class CList_Work_CenterPoint : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_CenterPoint)

public:
	CList_Work_CenterPoint();
	virtual ~CList_Work_CenterPoint();
	CFont		m_Font;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo, BOOL m_bCheck);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo,BOOL m_bCheck);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);
};


#endif // List_Work_CenterPoint_h__
