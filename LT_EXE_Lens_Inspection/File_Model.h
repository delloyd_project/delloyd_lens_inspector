﻿#ifndef File_Model_h__
#define File_Model_h__

#pragma once

#include "Def_DataStruct.h"

#define		Model_AppName		_T("Model")
#define		Common_AppName		_T("Common")
#define		ModelCode_KeyName	_T("ModelCode")
#define		DaqGrab_AppName		_T("DAQ")

class CFile_Model
{
public:
	CFile_Model();
	~CFile_Model();

	BOOL	LoadModelFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveModelFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	BOOL	Load_Common				(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	Save_Common				(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// 전류
	BOOL	LoadCurrentOpFile		(__in LPCTSTR szPath, __out ST_LT_TI_Current& stCurrentInfo);
	BOOL	SaveCurrentOpFile		(__in LPCTSTR szPath, __in const ST_LT_TI_Current* pstCurrentInfo);

	// LED
// 	BOOL	LoadLEDOpFile			(__in LPCTSTR szPath, __out ST_LT_TI_LED& stLEDInfo);
// 	BOOL	SaveLEDOpFile			(__in LPCTSTR szPath, __in const ST_LT_TI_LED* pstLEDInfo);

	// 광축
	BOOL	LoadCenterPointOpFile	(__in LPCTSTR szPath, __out ST_LT_TI_CenterPoint& stCenterPointInfo);
	BOOL	SaveCenterPointOpFile	(__in LPCTSTR szPath, __in const ST_LT_TI_CenterPoint* pstCenterPointInfo);

	// 로테이션
	BOOL	SaveRotateFile			(__in LPCTSTR szPath, __in const ST_LT_TI_Rotate* pstRotateInfo);
	BOOL	LoadRotateFile			(__in LPCTSTR szPath, __out ST_LT_TI_Rotate& stRotateInfo);

	// EIAJ
	BOOL	LoadEIAJFile			(__in LPCTSTR szPath, __out ST_LT_TI_EIAJ& stResolutionInfo);
	BOOL	SaveEIAJFile			(__in LPCTSTR szPath, __in const ST_LT_TI_EIAJ* pstResolutionInfo);

	// SFR
	BOOL	LoadSFRFile				(__in LPCTSTR szPath, __out ST_LT_TI_SFR& stSFRInfo);
	BOOL	SaveSFRlFile			(__in LPCTSTR szPath, __in const ST_LT_TI_SFR* pstSFRInfo);

	// Pogo File
	BOOL	LoadPogoIniFile			(__in LPCTSTR szPath, __out ST_PogoInfo& stPogoInfo);
	BOOL	SavePogoIniFile			(__in LPCTSTR szPath, __in const ST_PogoInfo* pstPogoInfo);

	BOOL	LoadPogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __out DWORD& dwCount);
	BOOL	SavePogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __in DWORD dwCount);

	// LVDS 세팅
	//BOOL	LoadDaqSetFile			(__in LPCTSTR szPath, __out ST_LVDSInfo& stLVDSInfo);
//	BOOL	SaveDaqSetFile			(__in LPCTSTR szPath, __out const ST_LVDSInfo* pstLVDSInfo);

	// Vision Test
//	BOOL	LoadVisionOptFile		(__in LPCTSTR szPath, __out ST_VisionOptInfo& stVisionOptInfo);
//	BOOL	SaveVisionOptFile		(__in LPCTSTR szPath, __out const ST_VisionOptInfo* pstVisionOptInfo);

	// AF 옵션
	BOOL	LoadAFInfoFile			(__in LPCTSTR szPath, __out ST_AFInfo& stAFInfo);
	BOOL	SaveAFInfoFile			(__in LPCTSTR szPath, __out const ST_AFInfo* pstAFInfo);

	// Custom Teach
	BOOL	LoadCustomTeachFile		(__in LPCTSTR szPath, __out ST_CustomTeach& stCustomTeach);
	BOOL	SaveCustomTeachFile		(__in LPCTSTR szPath, __out const ST_CustomTeach* pstCustomTeach);

	// 광원
	BOOL	LoadLightBrdFile		(__in LPCTSTR szPath, __out ST_SlotVolt& stLightInfo);
	BOOL	SaveLightBrdFile		(__in LPCTSTR szPath, __in const ST_SlotVolt* pstLightInfo);

	// 이물	
	BOOL	LoadParticleOpFile		(__in LPCTSTR szPath, __out ST_LT_TI_Particle& stParticleInfo);
	BOOL	SaveParticleOpFile		(__in LPCTSTR szPath, __in const ST_LT_TI_Particle* pstParticleInfo);

	// MES
// 	BOOL	SaveMESFile				(__in LPCTSTR szPath, __in const ST_InspectionInfo* pstInspectInfo, __in LPCTSTR szEqpId);
// 	BOOL	MakeMESCamData			(__out CString& szCamData, __in const ST_InspectionInfo* pstInspectInfo);


};

#endif // File_Model_h__
