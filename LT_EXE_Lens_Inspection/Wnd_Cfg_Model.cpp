﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Model.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_Model.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum Model_ID
{
	IDC_ED_ModelName = 1001,
	IDC_ED_Voltage,
	IDC_ED_Cam_Delay,
	IDC_CB_Retry_Test_Cnt,
	IDC_BTN_CENTERPOINT
};

// CWnd_Cfg_Model
IMPLEMENT_DYNAMIC(CWnd_Cfg_Model, CWnd_BaseView)

CWnd_Cfg_Model::CWnd_Cfg_Model()
{
	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Model::~CWnd_Cfg_Model()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Model, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_Cfg_Model message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_Model::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_ML_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szModel_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);	
	}

	//  [1/28/2019 ysJang] Retry Test Count ComboBox 추가
	for (UINT nIdex = 0; nIdex < ComboI_ML_MAXNUM; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_Retry_Test_Cnt + nIdex);
		m_ed_Item[nIdex].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; NULL != g_szModel_Retry_Test_Cnt[nIdx]; nIdx++)
		m_cb_Item[ComboI_ML_RetryTestCnt].AddString(g_szModel_Retry_Test_Cnt[nIdx]);

	m_cb_Item[ComboI_ML_RetryTestCnt].SetCurSel(0);


	for (UINT nIdx = 0; nIdx < Edit_ML_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_ModelName + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
		
		if (nIdx != Edit_ML_ModelName)
			m_ed_Item[nIdx].SetValidChars(_T("0123456789."));
	}

	m_bn_Check[BTN_CT_CenterPoint].Create(g_CTPoint_Button[BTN_CT_CenterPoint], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_CENTERPOINT);
	m_bn_Check[BTN_CT_CenterPoint].SetFont(&m_font_Data);

	m_bn_Check[BTN_CT_CenterPoint].SetImage(IDB_UNCHECKED_16);
	m_bn_Check[BTN_CT_CenterPoint].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Check[BTN_CT_CenterPoint].SizeToContent();
	m_bn_Check[BTN_CT_CenterPoint].SetCheck(BST_UNCHECKED);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlWidth	= 0;
	int iCtrlHeight = 25;
	int iHalfWidth	= (iWidth - iCateSpacing- iCateSpacing) / 3;
	int iLeftSub  	= 0;
	int iTempWidth  = 0;

	int iCapWidth	= 140;

	iCtrlWidth = (iHalfWidth - iCateSpacing) / 2;

	for (UINT nIdx = 0; nIdx < STI_ML_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		
		iLeftSub = iLeft + iCapWidth + iCateSpacing;

		switch (nIdx)
		{
		case STI_ML_ModelName:
			m_ed_Item[Edit_ML_ModelName].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		case STI_ML_Cam_Delay:
			m_ed_Item[Edit_ML_CameraDelay].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		case STI_ML_Cam_Volt:
			m_ed_Item[Edit_ML_Voltage].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		case STI_ML_Retry_Test_Cnt:
			m_cb_Item[ComboI_ML_RetryTestCnt].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		default:
			break;
		}	
		iTop += iCtrlHeight + iCateSpacing;
	}
	m_bn_Check[BTN_CT_CenterPoint].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnBnClickedBnApply
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 14:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnBnClickedBnApply()
{
// 	CString strValue;
// 
// 	m_ed_Item[Edit_ML_CameraAlign].GetWindowText(strValue);
// 	UINT nAlign = _ttoi(strValue.GetBuffer());
// 
// 	strValue.ReleaseBuffer();
// 
// 	GetOwner()->SendNotifyMessage(WM_ALIGN, (WPARAM)nAlign, 0);
}


//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetUIData(__out ST_ModelInfo& stModelInfo)
{
	CString strValue;	
	UINT	nData = 0;

	// 모델명
	m_ed_Item[Edit_ML_ModelName].GetWindowText(strValue);
	stModelInfo.szModelCode = strValue;

	m_ed_Item[Edit_ML_Voltage].GetWindowText(strValue);
	stModelInfo.fVoltage[0] = (FLOAT)_ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_ML_CameraDelay].GetWindowText(strValue);
	stModelInfo.nCameraDelay = _ttoi(strValue.GetBuffer());
	strValue.ReleaseBuffer();
	
	stModelInfo.nRetryTestCnt = m_cb_Item[ComboI_ML_RetryTestCnt].GetCurSel();

	stModelInfo.bCenterPoint_Ck = m_bn_Check[BTN_CT_CenterPoint].GetCheck();
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetUIData(__in const ST_ModelInfo* pModelInfo)
{
	CString strValue;
	UINT	nData = 0;

	// 모델명
	m_ed_Item[Edit_ML_ModelName].SetWindowText(pModelInfo->szModelCode);

	strValue.Format(_T("%.1f"), pModelInfo->fVoltage[0]);
	m_ed_Item[Edit_ML_Voltage].SetWindowText(strValue);

	strValue.Format(_T("%d"), pModelInfo->nCameraDelay);
	m_ed_Item[Edit_ML_CameraDelay].SetWindowText(strValue);

	m_cb_Item[ComboI_ML_RetryTestCnt].SetCurSel(pModelInfo->nRetryTestCnt);

	m_bn_Check[BTN_CT_CenterPoint].SetCheck(pModelInfo->bCenterPoint_Ck);
}

//=============================================================================
// Method		: RefreshFileList
// Access		: protected  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/7/6 - 9:47
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::RefreshFileList(__in const CStringList* pFileList)
{
// 	m_cb_Item[ComboI_ML_I2CFile].ResetContent();
// 
// 	INT_PTR iFileCnt = pFileList->GetCount();
// 
// 	POSITION pos;
// 	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
// 	{
// 		m_cb_Item[ComboI_ML_I2CFile].AddString(pFileList->GetNext(pos));
// 	}
// 
// 	// 이전에 선택되어있는 파일 다시 선택
// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		int iSel = m_cb_Item[ComboI_ML_I2CFile].FindStringExact(0, m_szI2CPath);
// 
// 		if (0 <= iSel)
// 		{
// 			m_cb_Item[ComboI_ML_I2CFile].SetCurSel(iSel);
// 		}
// 	}
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	SetUIData(pModelInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo);
}
