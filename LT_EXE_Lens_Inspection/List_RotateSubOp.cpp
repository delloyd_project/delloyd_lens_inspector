﻿// List_RotateSubOp.Rtp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_RotateSubOp.h"

#define IRtSOp_ED_CELLEDIT		5001

// CList_RotateSubOp

IMPLEMENT_DYNAMIC(CList_RotateSubOp, CListCtrl)

CList_RotateSubOp::CList_RotateSubOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstRotate = NULL;
	m_nTestMode = RT_Measurement_TestMode;
}

CList_RotateSubOp::~CList_RotateSubOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_RotateSubOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_RotateSubOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_RotateSubOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtSOp_ED_CELLEDIT, &CList_RotateSubOp::OnEnKillFocusERtOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_RotateSubOp 메시지 처리기입니다.
int CList_RotateSubOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtSOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

void CList_RotateSubOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[RtSOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < RtSOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_RtSOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < RtSOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_RtSOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_RtSOp[RtSOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
 	SetColumnWidth(RtSOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/6/26 - 14:59
// Desc.		:
//=============================================================================
BOOL CList_RotateSubOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_RotateSubOp::InitHeader()
{
	for (int nCol = 0; nCol < RtSOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_RtSOp[nCol], iListAglin_RtSOp[nCol], iHeaderWidth_RtSOp[nCol]);

	for (int nCol = 0; nCol < RtSOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_RtSOp[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LT_TI_Rotate * pstRotate
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateSubOp::InsertFullData()
{
	if (m_pstRotate == NULL)
		return;

 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < RegionRotate_MaxEnum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateSubOp::SetRectRow(UINT nRow)
{
	if (m_nTestMode == RT_Measurement_TestMode)
	{
		if (nRow == RtSOp_TargetOffset || nRow == RtSOp_MaxOffset)
			return;
	}

	CString strText;

	strText.Format(_T("%s"), g_lpszItem_RtSOp[nRow]);
	SetItemText(nRow, RtSOp_Object, strText);

	switch (nRow)
	{
	case RtSOp_Standard:
		strText.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbMasterDegree);
		SetItemText(nRow, RtSOp_Degree, strText);
		break;

	case RtSOp_StandardOffset:
		strText.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbDeviDegree);
		SetItemText(nRow, RtSOp_Degree, strText);
		break;
	case RtSOp_TargetOffset:
		strText.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbTargetDegree);
		SetItemText(nRow, RtSOp_Degree, strText);
		break;
	case RtSOp_MaxOffset:
		strText.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbMaxDegree);
		SetItemText(nRow, RtSOp_Degree, strText);
		break;
	default:
		break;
	}
 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateSubOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_RotateSubOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < RtSOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusERtSOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_RotateSubOp::OnEnKillFocusERtOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_RotateSubOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (iValue < 0)
		iValue = 0;

	CString str;

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:43
// Desc.		:
//=============================================================================
BOOL CList_RotateSubOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	CString str;

	switch (nRow)
	{
	case RtSOp_Standard:
		m_pstRotate->stRotateOp.dbMasterDegree = dValue;
		str.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbMasterDegree);
		break;

	case RtSOp_StandardOffset:
		m_pstRotate->stRotateOp.dbDeviDegree = dValue;
		str.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbDeviDegree);
		break;

	case RtSOp_TargetOffset:
		if (m_pstRotate->stRotateOp.dbTargetDegree < 0.0)
			m_pstRotate->stRotateOp.dbTargetDegree = 0;
		else
			m_pstRotate->stRotateOp.dbTargetDegree = dValue;

		str.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbTargetDegree);
		break;

	case RtSOp_MaxOffset:
		if (m_pstRotate->stRotateOp.dbMaxDegree < 0.0)
			m_pstRotate->stRotateOp.dbMaxDegree = 0;
		else
			m_pstRotate->stRotateOp.dbMaxDegree = dValue;
		
		str.Format(_T("%.2f"), m_pstRotate->stRotateOp.dbMaxDegree);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/6/26 - 14:51
// Desc.		:
//=============================================================================
BOOL CList_RotateSubOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		int casenum = pWndFocus->GetDlgCtrlID();

		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dValue = dValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dValue)
				dValue = dValue + ((zDelta / 120)*0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dValue > 10.0)
			dValue = 10.0;

		UpdateCellData_double(m_nEditRow, m_nEditCol, dValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
