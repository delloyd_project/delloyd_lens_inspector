﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Base.h
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Base_h__
#define Wnd_Cfg_Base_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"


//-----------------------------------------------------------------------------
// CWnd_Cfg_Base
//-----------------------------------------------------------------------------
class CWnd_Cfg_Base : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Base)

public:
	CWnd_Cfg_Base();
	virtual ~CWnd_Cfg_Base();

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	

	CFont m_font_Default;
	CFont m_font_Data;

	//CVGStatic	m_st_Frame;
	//CVGStatic	m_st_Title;
	

public:

	
	
};
#endif // Wnd_Cfg_Base_h__


