﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_AF.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_AF_h__
#define Wnd_Cfg_AF_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

enum enAF_Static
{
	STI_AF_STD_Displace,		// 기준 변위 값
	STI_AF_STD_DisplaceDev,		// 기준 변위 차
	STI_AF_LensConchoid,		// 회전 당 높이
	STI_AF_LensDirection,		// 진행 방향
	STI_AF_StepDegree_1,		// AF 단위 각도 1
	STI_AF_StepDegree_2,		// AF 단위 각도 2
	STI_AF_StepDegree_3,		// AF 단위 각도 3
	STI_AF_RotateCnt,			// AF 단위 각도 3

	STI_AF_MAXNUM,				
};

static LPCTSTR	g_szAF_Static[] =
{
	_T("Standard Displace"),
	_T("Displace Deviation"),
	_T("Lens Conchoid"),
	_T("Lens Direction"),
	_T("Step Degree 1"),
	_T("Step Degree 2"),
	_T("Step Degree 3"),
	_T("Rotate Count"),

	NULL
};

enum enAF_Edit
{
	Edit_AF_STD_Displace,
	Edit_AF_STD_DisplaceDev,
	Edit_AF_LensConchoid,
	Edit_AF_StepDegree_1,
	Edit_AF_StepDegree_2,
	Edit_AF_StepDegree_3,
	Edit_AF_RotateCnt,

	Edit_AF_MAXNUM,
};

enum enAF_Combo
{
	Combo_AF_LensDirection,

	Combo_AF_MAXNUM,
};


static LPCTSTR	g_szAF_LensDirCombo[] =
{
	_T("Reverse"),
	_T("Foward"),

	NULL
};


//-----------------------------------------------------------------------------
// CWnd_Cfg_VIsion
//-----------------------------------------------------------------------------
class CWnd_Cfg_AF : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_AF)

public:
	CWnd_Cfg_AF();
	virtual ~CWnd_Cfg_AF();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_AF_MAXNUM];
	CComboBox		m_cb_Item[Combo_AF_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[Edit_AF_MAXNUM];

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_ModelInfo& stModelInfo);

	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_ModelInfo* pModelInfo);

public:
	
	// 모델 데이터를 UI에 표시
	void		SetModelInfo		(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetModelInfo		(__out ST_ModelInfo& stModelInfo);
};

#endif // Wnd_Cfg_AF_h__


