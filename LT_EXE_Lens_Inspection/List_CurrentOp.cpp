﻿// List_CurrentOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_CurrentOp.h"

#define ICurrOp_ED_CELLEDIT		5001

// CList_CurrentOp

IMPLEMENT_DYNAMIC(CList_CurrentOp, CListCtrl)

CList_CurrentOp::CList_CurrentOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstCurrent = NULL;
}

CList_CurrentOp::~CList_CurrentOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_CurrentOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_CurrentOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_CurrentOp::OnNMDblclk)
	ON_EN_KILLFOCUS(ICurrOp_ED_CELLEDIT, &CList_CurrentOp::OnEnKillFocusECurrOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_CurrentOp 메시지 처리기입니다.
int CList_CurrentOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER, CRect(0, 0, 0, 0), this, ICurrOp_ED_CELLEDIT);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:36
// Desc.		:
//=============================================================================
void CList_CurrentOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[CurrOp_MaxCol] = { 0, };
	int iColDivide	= 0;
	int iUnitWidth	= 0;
	int iMisc		= 0;

	for (UINT nCol = 0; nCol < CurrOp_MaxCol; nCol++)
		iColDivide += iHeaderWidth_CurrOp[nCol];

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < CurrOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_CurrOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_CurrOp[CurrOp_Channel]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(CurrOp_Channel, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:55
// Desc.		:
//=============================================================================
BOOL CList_CurrentOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_CurrentOp::InitHeader()
{
	for (UINT nCol = 0; nCol < CurrOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_CurrOp[nCol], iListAglin_CurrOp[nCol], iHeaderWidth_CurrOp[nCol]);

	for (UINT nCol = 0; nCol < CurrOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_CurrOp[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_CurrentOp::InsertFullData()
{
	if (m_pstCurrent == NULL)
		return;

	DeleteAllItems();

	for (UINT nIndx = 0; nIndx < CurrOp_ItemNum; nIndx++)
	{
		InsertItem(nIndx, _T(""));
		SetRectRow(nIndx);
	}

}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_CurrentOp::SetRectRow(UINT nRow)
{
	CString strText;

	if (m_pstCurrent->stCurrentOp[nRow].nMinCurrent >= m_pstCurrent->stCurrentOp[nRow].nMaxCurrent)
	{
		m_pstCurrent->stCurrentOp[nRow].nMaxCurrent = m_pstCurrent->stCurrentOp[nRow].nMinCurrent + 1;
	}

	strText.Format(_T("%s"), g_lpszItem_CurrOp[nRow]);
	SetItemText(nRow, CurrOp_Channel, strText);

// 	strText.Format(_T("%.1f"), m_pstCurrent->stCurrentOp[nRow].dVoltage);
// 	SetItemText(nRow, CurrOp_Voltage, strText);

	strText.Format(_T("%d"), m_pstCurrent->stCurrentOp[nRow].nMinCurrent);
	SetItemText(nRow, CurrOp_Min, strText);

	strText.Format(_T("%d"), m_pstCurrent->stCurrentOp[nRow].nMaxCurrent);
	SetItemText(nRow, CurrOp_Max, strText);

	strText.Format(_T("%d"), m_pstCurrent->stCurrentOp[nRow].nOffset);
	SetItemText(nRow, CurrOp_Offset, strText);
 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_CurrentOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_CurrentOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem <5 && pNMItemActivate->iSubItem >0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECurrOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_CurrentOp::OnEnKillFocusECurrOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (0 != strText.Compare(GetItemText(m_nEditRow, m_nEditCol)))
	{
		if (UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText)))
		{

		}
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
BOOL CList_CurrentOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	switch (nCol)
	{
		case CurrOp_Min:
			m_pstCurrent->stCurrentOp[nRow].nMinCurrent = iValue;
			break;

		case CurrOp_Max:
			if (m_pstCurrent->stCurrentOp[nRow].nMinCurrent >= iValue)
				iValue = m_pstCurrent->stCurrentOp[nRow].nMinCurrent + 1;
			
			m_pstCurrent->stCurrentOp[nRow].nMaxCurrent = iValue;
			break;

		case CurrOp_Offset:
			m_pstCurrent->stCurrentOp[nRow].nOffset = iValue;
			break;

		default:
			break;
	}
 
	CString str;
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_CurrentOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	switch (nCol)
	{
// 	case CurrOp_Voltage:
// 		if (dValue > 12.0)
// 			dValue = m_pstCurrent->stCurrentOp[nRow].dVoltage;
// 
// 		m_pstCurrent->stCurrentOp[nRow].dVoltage = dValue;
// 		break;

	case CurrOp_Offset:
		if (dValue > 2.0)
			dValue = m_pstCurrent->stCurrentOp[nRow].nOffset;

		m_pstCurrent->stCurrentOp[nRow].nOffset = dValue;
		break;

	default:
		break;
	}

	CString str;
	str.Format(_T("%d"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_CurrentOp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;


	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_Current & stCurrent
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
void CList_CurrentOp::GetCellData()
{
	if (m_pstCurrent == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_CurrentOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dValue = dValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + ((zDelta / 120));
				

			}

			if (0 < dValue)
			{
				dValue = dValue + ((zDelta / 120)*0.1);
			}
		}

		if (UpdateCellData(m_nEditRow, m_nEditCol, iValue))
		{
			strText.Format(_T("%d"), iValue);
			m_ed_CellEdit.SetWindowText(strText);
		}
	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);

}
