﻿#ifndef List_SFROp_h__
#define List_SFROp_h__

#pragma once

#include "Def_DataStruct.h"

// List_CurrentInfo

// 헤더

typedef enum enListNum_Combo
{
	SfrCombo_Type,
	SfrCombo_Font,
	SfrCombo_MaxCol,
};

typedef enum enListNum_SfrOp
{
	SfrOp_Object,
	SfrOp_X,
	SfrOp_Y,
	SfrOp_W,
	SfrOp_H,
	SfrOp_TestType,
	SfrOp_Threshold,
	SfrOp_Offset,
	SfrOp_Linepare,
	SfrOp_Font,
	SfrOp_MaxCol,
};

static const TCHAR*	g_lpszHeader_Sfr[] =
{
	_T(""),
	_T("X"),
	_T("Y"),
	_T("W"),
	_T("H"),
	_T("Type"),
	_T("Thr"),
	_T("Offset"),
	_T("Linepare"),
	_T("Font"),
	NULL
};

const int	iListAglin_SfrOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SfrCOp[] =
{
	60,
	70,
	70,
	70,
	70,
	75,
	85,
	75,
	95,
	95,
};


class CList_SFROp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_SFROp)

public:
	CList_SFROp();
	virtual ~CList_SFROp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);

	void SetPtr_SFR(ST_LT_TI_SFR *pstSFR)
	{
		if (pstSFR == NULL)
			return;

		m_pstSFR = pstSFR;
	};

protected:
	
	ST_LT_TI_SFR		*m_pstSFR;

	DECLARE_MESSAGE_MAP()

	CFont			m_Font;
	CComboBox		m_cb_Mode[SfrCombo_MaxCol];
	CEdit			m_ed_CellEdit;
	UINT			m_nEditCol;
	UINT			m_nEditRow;

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL			UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);
	BOOL			CheckRectValue			(__in const CRect* pRegionz);

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void	OnEnKillFocusESfrCOpellEdit	();
	afx_msg void	OnEnKillFocusESfrOpellCombo ();
	afx_msg void	OnEnSelectESfrOpellCombo	();

	afx_msg void	OnEnKillFocusESfrFontOpellCombo ();
	afx_msg void	OnEnSelectESfrFontOpellCombo	();

	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
};


#endif // List_CurrentInfo_h__
