﻿//*****************************************************************************
// Filename	: Def_Enum.h
// Created	: 2010/11/23
// Modified	: 2016/06/30
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Enum_h__
#define Def_Enum_h__

#include "Def_Enum_Cm.h"

//-------------------------------------------------------------------
// UI 관련
//-------------------------------------------------------------------

typedef struct _tag_TestItemMode
{
	BOOL	bCurrent;
	BOOL	bCenterPoint;
	BOOL	bEIAJ;
	BOOL	bSFR;
	BOOL	bRotate;
	BOOL	bPatternNoise;
	BOOL	bSteeringLinkage;
	BOOL	bWarningPhrase;
	BOOL	bParticle;
	BOOL	bIR_Filter;
	BOOL	bVideoSignal;
	BOOL	bFOV;
	BOOL	bImageReversal;
	BOOL	bBrightnessRatio;
	BOOL	bColor;
//	BOOL	bLED;

	_tag_TestItemMode()
	{
		bCurrent			= TRUE;
		bCenterPoint		= TRUE;
		bEIAJ				= TRUE;
//		bLED				= TRUE;
		bSFR				= TRUE;
		bRotate				= FALSE;
		bPatternNoise		= FALSE;
		bSteeringLinkage	= FALSE;
		bWarningPhrase		= FALSE;
		bParticle			= FALSE;
		bIR_Filter			= FALSE;
		bVideoSignal		= FALSE;
		bFOV				= FALSE;
		bImageReversal		= FALSE;
		bBrightnessRatio	= FALSE;
		bColor				= FALSE;
	};

}ST_TestItemMode;

// 통신 Device
typedef enum 
{
	COMM_DEV_BASE		= 100,
	COMM_DEV_FTDI		= 200,
	COMM_DEV_MCTRL		= 300,
	COMM_DEV_LPRNT		= 400,
	COMM_DEV_LAST		= COMM_DEV_LPRNT,// 마지막으로 설정할것.
	COMM_DEV_MAX,
}enumCommStatus_Device;

typedef enum enMasterMode
{
	Master_Set,
	Master_Test,
	Master_MaxEnum,
};

typedef enum enLightCtrl
{
	LightChat_Center,
	LightChat_Left,
	LightChat_Right,
	LightChat_Particle,
	LightChat_MaxEnum,
};

typedef enum enChangeView
{
	VideoView_Ch_1,
	//VideoView_Ch_All,
	VideoView_MaxEnum,
};

typedef enum enSocketIndex
{
	Socket_1,
	Socket_MaxEnum,
};

typedef enum enEqp_TestItem
{
	//TI3Axis_Current,		// 소비전류
	//TI3Axis_SFR,			// SFR
	//TI3Axis_LEDTest,		// LED 검사
	TI3Axis_ActiveAlgin,	// 조정
	

	//TI3Axis_CenterAdjust,	// 광축 조정
 	//TI3Axis_EIAJ,			// EIAJ
	//TI3Axis_Rotation,		// Rotation
	//TI3Axis_Particle,		// 이물
	TI3Axis_MaxEnum,
};

typedef enum enPic_TestItem
{
	PIC_Standby,
	PIC_Current,				//	Current
	PIC_CenterAdjust,			// 광축 조정
	PIC_EIAJ,					// Resolution
	PIC_SFR,					// Resolution
	PIC_Particle,				// Particle
	PIC_Rotation,				// Rotation
	PIC_ActiveAlgin,			// 조정
	PIC_Center_EIAJ,			// 광축 조정 + Resolution
	PIC_Center_SFR,				// 광축 조정 + Resolution
	PIC_Center_Rotation,		// 광축 조정 + Rotation
	PIC_EIAJ_Rotation,			// Rotation  + Resolution
	PIC_SFR_Rotation,			// Rotation  + Resolution
	PIC_SFR_Rotation_Center,	// Rotation  + Resolution
	PIC_TotalPic,
	PIC_Align,
//	PIC_LED,
	PIC_MaxEnum,
};

typedef enum enEqp_TotalTestItem
{
	TTAF_Initial,		
	TTAF_Current,		// 소비전류
	TTAF_CenterAdjust,	// 광축 조정
	TTAF_EIAJ,			// 헤상력
	TTAF_SFR,			// 헤상력
	TTAF_Rotation,		// Rotation
	TTAF_Particle,		// 이물
	TTAF_Finalize,
	TTAF_MaxEnum,
};

enum enManual_Commend
{
	Man_Cmd_Power_On = 0,
	Man_Cmd_Power_Off,
// 	Man_Cmd_LED_On,
// 	Man_Cmd_LED_Off,
// 	Man_Cmd_ModuleFix,
// 	Man_Cmd_ModuleUnFix,
// 	Man_Cmd_PCBFix,
// 	Man_Cmd_PCBUnFix,
// 	Man_Cmd_DriverIn,
// 	Man_Cmd_DriverOut,
//  	Man_Cmd_LampRed,
//  	Man_Cmd_LampYellow,
//  	Man_Cmd_LampGreen,
//  	Man_Cmd_LampBuzzer,
	Man_Cmd_Image_Save,
	Man_Cmd_Pic_Image_Save,
	Man_Cmd_ImageLoad,
	Man_Cmd_Total,
};

enum enManual_TESTCommend
{
	MT_Cmd_Current = 0,
	MT_Cmd_CenterPoint,
	MT_Cmd_Rotate,
	MT_Cmd_EIAJ,
	MT_Cmd_SFR,
	MT_Cmd_Particle,
	MT_Cmd_Total,
};


static LPCTSTR g_szManual_TESTCommend[] =
{
	_T("Current"),
	_T("CenterPoint"),
	_T("Rotate"),
	_T("EIAJ"),
	_T("SFR"),
	_T("Particle"),
	NULL
};


enum enSFRDataType
{
	enSFRDataType_MTF,
	enSFRDataType_CyPx,
	enSFRDataType_Cymm,
	enSFRDataType_Max,
};

static LPCTSTR g_szSFRDataType[] =
{
	_T("/ MTF"),
	_T("/ Px"),
	_T("/ mm"),
	NULL
};

typedef enum enImageMode
{
	ImageMode_LiveCam,
	ImageMode_StillShotImage,
	ImageMode_MaxNum,
};


#ifdef MAX_CHANNEL_CNT
#undef MAX_CHANNEL_CNT
#define 	MAX_CHANNEL_CNT			6	// 장비에서 실제 사용하는 채널 수 (소켓, 사이트 등)
#endif

#ifdef MAX_OPERATION_THREAD
#undef MAX_OPERATION_THREAD
#define 	MAX_OPERATION_THREAD	6	// 최대 개별 검사용 쓰레드 개수
#endif

#ifdef MAX_MODULE_CNT
#undef MAX_MODULE_CNT
#define 	MAX_MODULE_CNT			6	// 최대 제품 투입 개수 (25ch다운: 25, 6ch다운: 6, 4ch보임량: 4)
#endif

typedef enum _ConstVar_Eqp
{
	USE_CHANNEL_CNT = 1,	// 장비에서 실제 사용하는 채널 수 (소켓, 사이트 등)
	USE_TEST_ITEM_CNT = TI3Axis_MaxEnum,
};

#endif // Def_Enum_h__

