﻿//*****************************************************************************
// Filename	: 	Wnd_SiteInfo.cpp
// Created	:	2016/7/5 - 16:18
// Modified	:	2016/7/5 - 16:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_SiteInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_SiteInfo.h"
#include "CommonFunction.h"

// CWnd_SiteInfo

static LPCTSTR g_szSiteItem[] =
{
	_T("Input"),		// 제품 투입 시간
	_T("Initialize"),	// 
	_T("Lens Inspection"),	// Resolution
	_T("Finalize"),		// 
	_T("Time"),			// 검사 시간
	_T("ErrorCode"),	// 불량 사유
	_T("Angle Result"),	// 불량 사유
	NULL
};

enum enVideo_Id{
	IDC_STATIC_VIDEO = 1000,
	IDC_STATIC_VVIDEO,
};

IMPLEMENT_DYNAMIC(CWnd_SiteInfo, CWnd)

CWnd_SiteInfo::CWnd_SiteInfo()
{
	VERIFY(m_Font.CreateFont(
		12,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pCamObj		= NULL;

	m_nSocketIdx	= 0;
	m_nUseTestCount	= USE_TEST_ITEM_CNT;
	m_nWidth = 1280;
	m_nHeight = 720;

	for (UINT nIdx = 0; nIdx < USE_TEST_ITEM_CNT; nIdx++)
	{
		m_bUseTestItem[nIdx] = FALSE;
	}

}

CWnd_SiteInfo::~CWnd_SiteInfo()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_SiteInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_SiteInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
int CWnd_SiteInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < SI_Test_Init; nIdx++)
	{
		m_stItem_T[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stItem_T[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		m_stItem_T[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	}

	for (UINT nIdx = SI_Test_Init; nIdx < SI_TestTime; nIdx++)
	{
		m_stItem_T[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stItem_T[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		//m_stItem_T[nIdx].SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
		m_stItem_T[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	}

	for (UINT nIdx = SI_TestTime; nIdx < SI_MaxEnum; nIdx++)
	{
		m_stItem_T[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stItem_T[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		m_stItem_T[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
	}

	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_stItem_T[nIdx].Create(g_szSiteItem[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stItem_V[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_stAngleResult[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_wnd_ImgProc.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_VIDEO);
	//m_wnd_VVideo.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_VVIDEO);
	
	m_st_BackColor.SetBackColor(RGB(160, 160, 160));
	m_st_BackColor.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_stJudgment.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Green);
	m_stJudgment.SetFont_Gdip(L"Arial", 32.0F);
	m_stJudgment.Create(_T("STAND BY"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_Group.SetTitle(L"CAMERA VIEW_TEST INFO");

	if (!m_Group.Create(_T("CAMERA VIEW_TEST INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iLeftMargin = 7;
	int iRightMargin = 7;
	int iTopMargin = 27;
	int iBottomMargin = 7;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;
	int iBottom = cy - iBottomMargin;
	int iWidth = (int)(cx * 0.70) - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;
	int iHeightVideo = (int)(cy * 0.85) - iTopMargin - iBottomMargin;

	// 영상 (720 x 480)
	int iVideoWidth = m_nWidth; // VIDEO_WIDTH;//(iWidth - iSpacing) * 3 / 5;
	int iVideoHeight = m_nHeight; // VIDEO_HEIGHT;//VIDEO_HEIGHT * iVideoWidth / VIDEO_WIDTH;

	if (m_nWidth <= 0 || m_nHeight <= 0)
		return;

	int iWidthTemp = iVideoWidth;
	int iHeightTemp = iVideoHeight;

	if (iWidthTemp > iVideoWidth)
		iWidthTemp = iVideoWidth;
	else
		iWidthTemp = iWidth;

	iHeightTemp = iVideoHeight * iWidthTemp / iVideoWidth;

	if (iHeightTemp > iHeightVideo)
	{
		iHeightTemp = iHeightVideo;
		iWidthTemp = iVideoWidth * iHeightTemp / iVideoHeight;
	}

	iWidthTemp = 720;
	iHeightTemp = 480;

	m_Group.MoveWindow(0, 0, cx, cy);

	m_st_BackColor.MoveWindow(iLeft, iTop, iWidth, iHeightVideo);
	m_wnd_ImgProc.MoveWindow(iLeft + (abs(iWidthTemp - iWidth) / 2), iTop + (abs(iHeightTemp - iHeightVideo) / 2), iWidthTemp, iHeightTemp);

	m_stJudgment.MoveWindow(iLeft, iTop + iHeightVideo + iBottomMargin, iWidth, iHeight - iHeightVideo - 8);

	iTop += iVideoHeight + iSpacing;
	int iTempHeight = iHeight - iVideoHeight - iSpacing;

 	// 검사 정보
	iLeft += iWidth + iSpacing;
	
 	int iSubWidth = 100;
 	int iSubLeft = iLeft + iSubWidth - 1;
	int iCtrlHeight = (iHeight) / ((SI_MaxEnum - SI_InputTime) - USE_TEST_ITEM_CNT + m_nUseTestCount) + 1;
	int iFullWidth = (cx - iWidth - iSpacing - iSpacing) - iSpacing - 2;
	int iHalfWidth = (cx - iWidth - iSpacing - iSpacing) - iSubWidth - iSpacing - 1;
 	
	iTop = iTopMargin;
	//m_stItem_T[SI_Vision].MoveWindow(iLeft, iTop, iFullWidth, iHeight * 1 / 20 - iSpacing);

	//iTop += iHeight * 1 / 20 - iSpacing - 1;
	//m_wnd_VVideo.MoveWindow(iLeft, iTop, iFullWidth, iHeight * 7 / 20 + iSpacing);

	//iTop = iHeight * 2 / 5 + iTopMargin + 3;

	for (UINT nIdx = SI_InputTime; nIdx <= SI_Test_Init; nIdx++)
 	{
 		m_stItem_T[nIdx].MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
 		m_stItem_V[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);
 
 		iTop += iCtrlHeight - 1;
 	}
 
	for (UINT nIdx = SI_Test_ActiveAlign; nIdx < SI_Test_Final; nIdx++)
 	{
		
		if (m_bUseTestItem[nIdx - SI_Test_ActiveAlign])
		{
		m_stItem_T[nIdx].MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
		m_stItem_V[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);

		iTop += iCtrlHeight - 1;
		}
		else
		{
		m_stItem_T[nIdx].MoveWindow(0, 0, 0, 0);
		m_stItem_V[nIdx].MoveWindow(0, 0, 0, 0);
		}
		
 	}
 
	for (UINT nIdx = SI_Test_Final; nIdx < SI_MaxEnum; nIdx++)
 	{
		m_stItem_T[nIdx].MoveWindow(iLeft, iTop, iSubWidth, iCtrlHeight);
		if (nIdx == SI_AngleResult)
		{
			m_stItem_V[SI_AngleResult].MoveWindow(0, 0, 0, 0);
		}
		else
			m_stItem_V[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth, iCtrlHeight);

		iTop += iCtrlHeight - 1;
 	}

	iTop -= iCtrlHeight;
	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_stAngleResult[nIdx].MoveWindow(iSubLeft, iTop, iHalfWidth / 4, iCtrlHeight);
		iSubLeft += (iHalfWidth / 4);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
BOOL CWnd_SiteInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRedrawControl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/17 - 19:30
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::OnRedrawControl()
{
	m_st_BackColor.Invalidate();
	m_wnd_ImgProc.Invalidate();
	m_wnd_VVideo.Invalidate();

	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_stItem_T[nIdx].Invalidate();
		m_stItem_V[nIdx].Invalidate();
	}

	m_stJudgment.Invalidate();
}

//=============================================================================
// Method		: RedrawWindow
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/16 - 19:26
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::RedrawWindow()
{
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: GetErrorCodeText
// Access		: protected  
// Returns		: CString
// Parameter	: __in INT ErrorCode
// Qualifier	:
// Last Update	: 2016/6/8 - 17:31
// Desc.		:
//=============================================================================
CString CWnd_SiteInfo::GetErrorCodeText(__in INT ErrorCode)
{
	CString szReturn;

// 	if (ERR_Pass == ErrorCode)
// 		return szReturn;
// 
// 	szReturn = GetLGErrorCodeText(ErrorCode);

	// 불량 원인
	return szReturn;
}

void CWnd_SiteInfo::SetImageSize(__in UINT nWidth, __in UINT nHeight)
{
	m_nWidth = nWidth;
	m_nHeight = nHeight;
}

//=============================================================================
// Method		: SetUnitIndex
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/17 - 19:30
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetUnitIndex(__in UINT nUnitIdx)
{
	m_nSocketIdx = nUnitIdx;

	RedrawWindow();
}

//=============================================================================
// Method		: ResetZoneInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/27 - 10:07
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::ResetZoneInfo()
{
	for (UINT nIdx = SI_InputTime; nIdx < SI_MaxEnum; nIdx++)
	{
		m_stItem_V[nIdx].SetText(_T(""));
		m_stItem_V[nIdx].SetBackColor(RGB(255, 255, 255));
	}
}

//=============================================================================
// Method		: ResetResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/9 - 14:51
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::ResetResult()
{
	m_stJudgment.SetBackColor(g_TestResult[TR_Init].BackColor);
	m_stJudgment.SetTextColor(g_TestResult[TR_Init].TextColor);
	m_stJudgment.SetText(_T("STAND BY"));
}

//=============================================================================
// Method		: UpdateCamObjetInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo * pCamObj
// Qualifier	:
// Last Update	: 2016/10/24 - 23:04
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::UpdateCamObjetInfo(__in const ST_CamInfo* pCamObj)
{
// 	if (NULL != pCamObj)
// 	{
// 		//SetBarcode		(pCamObj->szBarcode);
// 
// 		if ((TR_Init != pCamObj->nJudgment) && (TR_Empty != pCamObj->nJudgment))
// 			SetInputTime((SYSTEMTIME*)&(pCamObj->tmInputTime));
// 		else
// 			m_stItem_V[SI_InputTime].SetText(_T(""));
// 
// 		SetResult		(pCamObj->nJudgment);
// 	}
}

//=============================================================================
// Method		: SetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/16 - 23:44
// Desc.		:
//=============================================================================
// void CWnd_SiteInfo::SetBarcode(__in LPCTSTR szBarcode)
// {
// 	if (NULL != szBarcode)
// 		m_stItem_V[SI_InputTime].SetText(szBarcode);
/*}*/

//=============================================================================
// Method		: SetInputTime
// Access		: public  
// Returns		: void
// Parameter	: __in SYSTEMTIME * ptmInput
// Qualifier	:
// Last Update	: 2016/11/16 - 23:44
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetInputTime(__in SYSTEMTIME* ptmInput)
{
	if (NULL != ptmInput)
	{
		CString strTime;

		strTime.Format(_T("%02d/%02d %02d:%02d:%02d"), ptmInput->wMonth, ptmInput->wDay, ptmInput->wHour, ptmInput->wMinute, ptmInput->wSecond);

		if (0 == strTime.Compare(_T("00/00 00:00:00")))
		{
			m_stItem_V[SI_InputTime].SetText(_T(""));
		}
		else
		{
			m_stItem_V[SI_InputTime].SetText(strTime);
		}
	}
}

//=============================================================================
// Method		: SetInputTime
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szInputTime
// Qualifier	:
// Last Update	: 2016/11/16 - 23:43
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetInputTime(__in LPCTSTR szInputTime)
{
	if (NULL != szInputTime)
		m_stItem_V[SI_InputTime].SetText(szInputTime);
}

//=============================================================================
// Method		: SetResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult Result
// Qualifier	:
// Last Update	: 2016/11/16 - 23:43
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetResult(__in enTestResult Result)
{
	m_stJudgment.SetFont_Gdip(L"Arial", 40.0F);

	switch (Result)
	{
	case TR_UserStop:
	case TR_Fail:
		m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	case TR_Pass:
		m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Green);
		break;
	default:
		break;
	}

	if (TR_Init == Result)
		m_stJudgment.SetText(_T(""));
	else
		m_stJudgment.SetText(g_TestResult[Result].szText);
}

//=============================================================================
// Method		: SetResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult Result
// Parameter	: __in CString szResult
// Qualifier	:
// Last Update	: 2017/2/18 - 14:45
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetResult(__in enTestResult Result, __in CString szResult)
{
	m_stJudgment.SetFont_Gdip(L"Arial", 30.0F);

	switch (Result)
	{
		case TR_Fail:
			m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Red);
			break;
		case TR_Pass:
			m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Green);
			break;
		case TR_Init:
			m_stJudgment.SetColorStyle(CVGStatic::ColorStyle_Blue);
			break;
		default:
			break;
	}

	m_stJudgment.SetText(szResult);
}

//=============================================================================
// Method		: SetErrorCode
// Access		: public  
// Returns		: void
// Parameter	: __in INT ErrorCode
// Qualifier	:
// Last Update	: 2016/11/11 - 19:11
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetErrorCode(__in INT ErrorCode)
{
	if (Err_Pass != ErrorCode)
	{
		SetErrorCode(GetErrorCodeText(ErrorCode));
	}
	else
	{
		m_stItem_V[SI_ErrorCode].SetText(_T(""));
		m_stItem_V[SI_ErrorCode].SetBackColor(RGB(255, 255, 255));
	}
}

//=============================================================================
// Method		: SetErrorCode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szErrorCode
// Qualifier	:
// Last Update	: 2016/11/11 - 19:11
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetErrorCode(__in LPCTSTR szErrorCode)
{
	m_stItem_V[SI_ErrorCode].SetText(szErrorCode);
	m_stItem_V[SI_ErrorCode].SetBackColor(g_TestResult[TR_Fail].BackColor);
}

// //=============================================================================
// // Method		: SetTestStatus
// // Access		: public  
// // Returns		: void
// // Parameter	: __in enTestProcess TestStatus
// // Qualifier	:
// // Last Update	: 2016/11/16 - 23:42
// // Desc.		:
// //=============================================================================
// void CWnd_SiteInfo::SetTestStatus(__in enTestProcess TestStatus)
// {
// 	// 윈도우 배경색 변경?
// 	m_stItem_V[SI_ProgressStatus].SetText(g_TestProcess[TestStatus].szText);
// 	//m_stItem_V[SI_ProgressStatus].SetBackColor(g_TestProcess[TestStatus].BackColor);
// 
// 	switch (TestStatus)
// 	{
// 	case TP_TableRotation:
// 		SetCheck(BST_CHECKED);
// 		break;
// 
// 	case TP_Run:		
// 		SetCheck(BST_CHECKED);
// 		break;
// 
// 	case TP_Idle:
// 	case TP_Stop:
// 	case TP_Completed:
// 		SetCheck(BST_UNCHECKED);
// 		break;
// 
// 	//case TP_Hold:
// 	case TP_Error:
// 	case TP_ForcedStop:
// 	default:
// 		SetCheck(BST_UNCHECKED);
// 		break;
// 	}
// }

//=============================================================================
// Method		: SetElapsedTime
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2016/11/16 - 23:42
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetElapsedTime(__in DWORD dwTime)
{
	CString szTime;

	szTime.Format(_T("%.1f Sec"), dwTime / 1000.0f);

	m_stItem_V[SI_TestTime].SetText(szTime.GetBuffer());
	szTime.ReleaseBuffer();
}

//=============================================================================
// Method		: SetTestInitialize
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/1/8 - 15:31
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestInitialize(__in enTestEachResult EachResult, __in CString strText)
{
	m_stItem_V[SI_Test_Init].SetBackColor(g_TestEachResult[EachResult].BackColor);
	m_stItem_V[SI_Test_Init].SetTextColor(g_TestEachResult[EachResult].TextColor);
	m_stItem_V[SI_Test_Init].SetText(strText);
}

//=============================================================================
// Method		: SetTestFinalize
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/1/8 - 15:31
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestFinalize(__in enTestEachResult EachResult, __in CString strText)
{
	m_stItem_V[SI_Test_Final].SetBackColor(g_TestEachResult[EachResult].BackColor);
	m_stItem_V[SI_Test_Final].SetTextColor(g_TestEachResult[EachResult].TextColor);
	m_stItem_V[SI_Test_Final].SetText(strText);
}

//************************************
// Method:    SetTestVision
// FullName:  CWnd_SiteInfo::SetTestVision
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_SiteInfo::SetTestVision(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_stItem_V[SI_Test_Vision].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Vision].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Vision].SetText(strText);
// }

//************************************
// Method:    SetTestDisplace
// FullName:  CWnd_SiteInfo::SetTestDisplace
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_SiteInfo::SetTestDisplace(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_stItem_V[SI_Test_Displace].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Displace].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Displace].SetText(strText);
// }

//************************************
// Method:    SetTestAFPosition
// FullName:  CWnd_SiteInfo::SetTestAFPosition
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __in enTestEachResult EachResult
// Parameter: __in CString strText
//************************************
// void CWnd_SiteInfo::SetTestAFPosition(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_stItem_V[SI_Test_AFPosition].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_AFPosition].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_AFPosition].SetText(strText);
// }

//=============================================================================
// Method		: SetTestCurrent
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2016/12/13 - 10:51
// Desc.		:
//=============================================================================
// void CWnd_SiteInfo::SetTestCurrent(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_stItem_V[SI_Test_Current].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Current].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Current].SetText(strText);
// }

//=============================================================================
// Method		: SetTestLED
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2016/12/13 - 10:51
// Desc.		:
//=============================================================================
// void CWnd_SiteInfo::SetTestLED(__in enTestEachResult EachResult, __in CString strText)
// {
// 	m_stItem_V[SI_Test_LED].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_LED].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_LED].SetText(strText);
// }

//=============================================================================
// Method		: SetTestCenterAdjust
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/1/2 - 14:04
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestCenterAdjust(__in enTestEachResult EachResult, __in CString strText)
{
// 	m_stItem_V[SI_Test_CenterPoint].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_CenterPoint].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_CenterPoint].SetText(strText);
}

//=============================================================================
// Method		: SetTestResolution
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/14 - 14:10
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestResolution(__in enTestEachResult EachResult, __in CString strText)
{
// 	m_stItem_V[SI_Test_Resolution].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Resolution].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Resolution].SetText(strText);
}

//=============================================================================
// Method		: SetTestRotation
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/16 - 10:11
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestRotation(__in enTestEachResult EachResult, __in CString strText)
{
// 	m_stItem_V[SI_Test_Rotation].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Rotation].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Rotation].SetText(strText);
}

//=============================================================================
// Method		: SetTestParticle
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2017/2/14 - 14:08
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestParticle(__in enTestEachResult EachResult, __in CString strText)
{
// 	m_stItem_V[SI_Test_Particle].SetBackColor(g_TestEachResult[EachResult].BackColor);
// 	m_stItem_V[SI_Test_Particle].SetTextColor(g_TestEachResult[EachResult].TextColor);
// 	m_stItem_V[SI_Test_Particle].SetText(strText);
}

//=============================================================================
// Method		: SetTestActiveAlgin
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/9/11 - 21:11
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestActiveAlgin(__in enTestEachResult EachResult, __in CString strText)
{
	m_stItem_V[SI_Test_ActiveAlign].SetBackColor(g_TestEachResult[EachResult].BackColor);
	m_stItem_V[SI_Test_ActiveAlign].SetTextColor(g_TestEachResult[EachResult].TextColor);
	m_stItem_V[SI_Test_ActiveAlign].SetText(strText);
}

//=============================================================================
// Method		: SetTestFocus
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/9/11 - 21:11
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestFocus(__in enTestEachResult EachResult, __in CString strText)
{
	m_stItem_V[SI_Test_ActiveAlign].SetBackColor(g_TestEachResult[EachResult].BackColor);
	m_stItem_V[SI_Test_ActiveAlign].SetTextColor(g_TestEachResult[EachResult].TextColor);
	m_stItem_V[SI_Test_ActiveAlign].SetText(strText);
}

//=============================================================================
// Method		: SetTestItem
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 11:21
// Desc.		:
//=============================================================================
void CWnd_SiteInfo::SetTestItem(__in const ST_ModelInfo* pstModelInfo)
{
	INT_PTR iCnt = pstModelInfo->TestItemz.GetCount();

	m_nWidth  = pstModelInfo->dwWidth;
	m_nHeight = pstModelInfo->dwHeight;

	if (iCnt <= USE_TEST_ITEM_CNT)
	{
		m_nUseTestCount = (UINT)iCnt;	
			
		UINT nTestItemID = 0;
		memset(m_bUseTestItem, FALSE, sizeof(BOOL) * USE_TEST_ITEM_CNT);

		for (UINT nIdx = 0; nIdx < m_nUseTestCount; nIdx++)
		{
			nTestItemID = pstModelInfo->TestItemz.GetAt(nIdx);

			switch (nTestItemID)
			{
// 			case TIID_Current:
// 				m_bUseTestItem[TI3Axis_Current] = TRUE;
// 				break;

			case TIID_CenterPointAdj:
//				m_bUseTestItem[TI3Axis_CenterAdjust] = FALSE;
				break;

			case TIID_EIAJ:
//				m_bUseTestItem[TI3Axis_EIAJ] = TRUE;
				break;

			case TIID_SFR:
//				m_bUseTestItem[TI3Axis_SFR] = FALSE;
				break;

			case TIID_Rotation:
//				m_bUseTestItem[TI3Axis_Rotation] = FALSE;
				break;
				
			case TIID_Particle:
//				m_bUseTestItem[TI3Axis_Particle] = FALSE;
				break;

			case TIID_ActiveAlgin:
				m_bUseTestItem[TI3Axis_ActiveAlgin] = TRUE;
				break;
// 
// 			case TIID_LEDTest:
// 				m_bUseTestItem[TI3Axis_LEDTest] = TRUE;
// 				break;

			default:
				break;
			}
		}
	}
	else
	{
		// 에러
	}

	RedrawWindow();
}


void CWnd_SiteInfo::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if ((0 <= iChIdx) && (iChIdx < USE_CHANNEL_CNT))
	{

		m_wnd_ImgProc.Render(lpVideo, dwWidth, dwHeight);

	}
}

void CWnd_SiteInfo::SetAngleColor(__in enTestResult nResult, __in int nRetryTestCnt, BOOL bInitmode /*= FALSE */)
{
	// init
	if (bInitmode == TRUE && nResult == TR_Init)
	{
		for (int t = 0; t < 4; t++)
		{
			m_stAngleResult[t].SetTextColor(RGB(255, 255, 255));
			if (t < nRetryTestCnt+1)
			{
				m_stAngleResult[t].SetBackColor(RGB(255, 255, 255));
				m_stAngleResult[t].SetText(_T(""));
			}
			else{
				m_stAngleResult[t].SetBackColor(RGB(86, 86, 86));
				m_stAngleResult[t].SetText(_T("X"));
			}
		}
		return;
	}

	// set
	m_stAngleResult[nRetryTestCnt].SetTextColor(g_TestEachResult[nResult].TextColor);
	m_stAngleResult[nRetryTestCnt].SetBackColor(g_TestEachResult[nResult].BackColor);
	if (nResult == TR_Pass)
	{
		m_stAngleResult[nRetryTestCnt].SetText(_T("P"));
	}
	else
	{
		m_stAngleResult[nRetryTestCnt].SetText(_T("F"));
	}
}