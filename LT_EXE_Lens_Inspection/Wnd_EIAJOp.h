﻿#ifndef Wnd_EIAJOp_h__
#define Wnd_EIAJOp_h__

#pragma once

#include "List_EIAJOp.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "CommonFunction.h"

// CWnd_EIAJOp

enum enEIAJ_Buttonbox
{
	Btn_EJ_EIAJ_TEST = 0,
	Btn_EJ_EIAJ_TEST_STOP,
	Btn_EJ_DEFAULT_SET,
	Btn_EJ_EIAJ_GRAPH,
	Btn_EJ_MAXNUM,
};

static LPCTSTR	g_szEIAJ_Button[] =
{
	_T("TEST"),
	_T("STOP"),
	_T("DEFAULT SET"),
	_T("EIAJ Graph"),
	NULL
};

class CWnd_EIAJOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_EIAJOp)

public:
	CWnd_EIAJOp();
	virtual ~CWnd_EIAJOp();

	void	SetUI	();
	void	GetUI	();
	BOOL	IsTest	();

	void	SetStatusEngineerMode	(__in enPermissionMode InspMode);

	void	SetPtr_ModelInfo		(__in ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void	SetPtr_ImageMode(__in ST_ImageMode *stImageMode)
	{
		if (stImageMode == NULL)
			return;

		m_pst_ImageMode = stImageMode;
	};//2018cws

protected:

	CFont		m_font_Data;

	CList_EIAJOp	m_ListEIAJOp;
	ST_ModelInfo	*m_pstModelInfo;
	ST_ImageMode		* m_pst_ImageMode;//2018cws

	CMFCButton		m_bn_Item[Btn_EJ_MAXNUM];


	DECLARE_MESSAGE_MAP()
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnBnClickedBnTest		();
	afx_msg void	OnBnClickedBnTestStop	();
	afx_msg void	OnBnClickedBnDefaultSet	();
	afx_msg void	OnBnClickedBnGraph		();
	
	BOOL m_bTest_Flag;

};

#endif // Wnd_EIAJOp_h__
