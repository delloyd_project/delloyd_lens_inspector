﻿#ifndef Wnd_PaticleOp_h__
#define Wnd_PaticleOp_h__

#pragma once

#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "List_ParticleOp.h"

enum enPaticle_Static
{
	STI_PT_SENSITIVITY = 0,
	STI_PT_MAXNUM,
};

static LPCTSTR	g_szPaticle_Static[] =
{
	_T("SENSITIVITY"),
	NULL
};

static LPCTSTR	g_szPaticle_Button[] =
{
	_T("PARTICLE TEST"),
	_T("FAIL DESPLAY"),
	_T("DEFAULT SET"),
	NULL
};

enum enPaticle_Editbox
{
	Edt_PT_SENSITIVITY = 0,
	Edt_PT_MAXNUM,
};

enum enPaticle_Buttonbox
{
	Btn_PT_TEST = 0,
	Btn_PT_FAIL_DISPLAY,
	Btn_PT_DEFAULT_SET,
	Btn_PT_MAXNUM,
};

// CWnd_ParticleOp

class CWnd_ParticleOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ParticleOp)

public:
	CWnd_ParticleOp();
	virtual ~CWnd_ParticleOp();

	void SetUI();
	void GetUI();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

protected:
	DECLARE_MESSAGE_MAP()

	CFont				m_font_Data;
	CVGStatic			m_st_Item[STI_PT_MAXNUM];
	CMFCMaskedEdit		m_ed_Item[Edt_PT_MAXNUM];
	CMFCButton			m_bn_Item[Btn_PT_MAXNUM];

	ST_ModelInfo		*m_pstModelInfo;
	CList_ParticleOp	m_ListPtOp;

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnTest();
	afx_msg void	OnBnClickedBnDefaultSet();
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
};

#endif // Wnd_RotateOp_h__
