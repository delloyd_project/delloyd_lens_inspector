﻿// Wnd_CenterPointOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CenterPointOp.h"


// CWnd_CenterPointOp

typedef enum CenterPointOp_ID
{
	IDC_BTN_CENTERPOINT = 1001,
	IDC_BTN_CENTERPOINT_TEST_STOP,
	IDC_COMBO_CP_TESTMODE,
	IDC_COMBO_CP_CAMERASTATE,
	IDC_LIST_CENTERPOINTOP,
	IDC_EDIT_TRYCOUNT,
};

IMPLEMENT_DYNAMIC(CWnd_CenterPointOp, CWnd)

CWnd_CenterPointOp::CWnd_CenterPointOp()
{
	m_pstModelInfo = NULL;
	m_bTest_Flag = FALSE;
	//m_CT_TIProcessing.SetPtr_ImageMode(m_pstImageMode);//2018cws

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_CenterPointOp::~CWnd_CenterPointOp()
{
	m_bTest_Flag = FALSE;
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_CenterPointOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_CENTERPOINT, OnBnClickedBnTest)
	ON_BN_CLICKED(IDC_BTN_CENTERPOINT_TEST_STOP, OnBnClickedBnTestStop)
	ON_CBN_SELENDOK(IDC_COMBO_CP_TESTMODE, OnCbnSelendTestMode)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

// CWnd_CenterPointOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_CenterPointOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < ComboI_CP_MAXNUM; nIdex++)
		m_Cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_COMBO_CP_TESTMODE + nIdex);
		
	for (UINT nIdex = 0; nIdex < CP_TestMode_MaxNum; nIdex++)
		m_Cb_Item[ComboI_CP_TESTMODE].InsertString(nIdex, g_szCenterPointTestMode[nIdex]);

	for (UINT nIdex = 0; nIdex < CAM_STATE_MAXNUM; nIdex++)
		m_Cb_Item[ComboI_CP_CAMERASTATE].InsertString(nIdex, g_szCamState[nIdex]);
	
	for (UINT nIdex = 0; nIdex < STI_CP_MAXNUM; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szCenterPoint_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < Edit_CP_MAXNUM; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_TRYCOUNT + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789"));
	}

	m_ed_Item[Edit_CP_IMAGESENSOR].SetValidChars(_T("0123456789."));

	m_ListCPOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_CENTERPOINTOP);

	for (UINT nIdex = 0; nIdex < Btn_CT_MAXNUM; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szCenter_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_CENTERPOINT + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font_Data);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int Static_W = iWidth / 4;
	int Static_H = 26;
	int Combo_W = iWidth / 3;
	int Combo_H = 26;

	int TempLeft = iLeft;

	int iBtnWidth = iWidth / 7;
	int iBtnHeight = 26;

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Item[Btn_CT_CENTER_TEST].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[Btn_CT_CENTER_TEST_STOP].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop = iMargin;
	for (UINT nIdx = ComboI_CP_CAMERASTATE; nIdx < ComboI_CP_MAXNUM; nIdx++)
	{
		iLeft = TempLeft;
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, Static_W, Static_H);
		iLeft += Static_W + iSpacing;
		m_Cb_Item[nIdx].MoveWindow(iLeft, iTop, Combo_W, Static_H);
		iTop += Static_H + iSpacing;
	}

// 	if (m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode == CP_Fine_tune_TestMode)
// 	{
// 		iLeft = TempLeft;
// 		m_st_Item[STI_CP_IMAGESENSOR].MoveWindow(iLeft, iTop, Static_W, Static_H);
// 		iLeft += Static_W + iSpacing;
// 		m_ed_Item[Edit_CP_IMAGESENSOR].MoveWindow(iLeft, iTop, Combo_W, Static_H);
// 		iTop += Static_H + iSpacing;
// 
// 		iLeft = TempLeft;
// 		m_st_Item[STI_CP_TRYCOUNT].MoveWindow(iLeft, iTop, Static_W, Static_H);
// 		iLeft += Static_W + iSpacing;
// 		m_ed_Item[Edit_CP_TRYCOUNT].MoveWindow(iLeft, iTop, Combo_W, Static_H);
// 		iTop += Static_H + iSpacing;
// 	}
// 	else
	{
		m_st_Item[STI_CP_IMAGESENSOR].MoveWindow(iLeft, iTop, 0, 0);
		m_ed_Item[Edit_CP_IMAGESENSOR].MoveWindow(iLeft, iTop, 0, 0);

		m_st_Item[STI_CP_TRYCOUNT].MoveWindow(iLeft, iTop, 0, 0);
		m_ed_Item[Edit_CP_TRYCOUNT].MoveWindow(iLeft, iTop, 0, 0);
	}

	iLeft = TempLeft;
	int List_W = (Static_W + Combo_W + iSpacing);
	int List_H = iHeight / 2;

	m_ListCPOp.MoveWindow(iLeft, iTop, List_W, List_H);

}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
	if (bShow)
	{
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_CenterAdjust, 0);
	}
	else
	{
		OnBnClickedBnTestStop();
		DoEvents(100);
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
	}
}

//=============================================================================
// Method		: OnCbnSelendTestMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:59
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnCbnSelendTestMode()
{
	m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode = m_Cb_Item[ComboI_CP_TESTMODE].GetCurSel();
	
	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	SetUI();
}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:01
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnBnClickedBnTest()
{
	m_bn_Item[Btn_CT_CENTER_TEST].EnableWindow(FALSE);

	UINT nItem = m_Cb_Item[ComboI_CP_TESTMODE].GetCurSel();

// 	if (nItem == CP_Fine_tune_TestMode)
// 	{
// 		GetOwner()->SendNotifyMessage(WM_MANUAL_TEST, CP_Fine_tune_TestMode, MT_Cmd_CenterPoint);
// 	}

	if (nItem == CP_Measurement_TestMode)
	{
		m_bTest_Flag = TRUE;

	while (m_bTest_Flag == TRUE)
	{
		DoEvents(50);
				GetOwner()->SendNotifyMessage(WM_MANUAL_TEST, CP_Measurement_TestMode, MT_Cmd_CenterPoint);
				
				if (m_pstImageMode->eImageMode == ImageMode_StillShotImage)
				{
					//  [1/17/2019 Admin]
					GetOwner()->SendNotifyMessage(WM_MANUAL_TEST_IMAGESAVE, CP_Measurement_TestMode, MT_Cmd_CenterPoint);
					OnBnClickedBnTestStop();
				}
	}
	//  [1/17/2019 Admin]
	GetOwner()->SendNotifyMessage(WM_MANUAL_TEST_IMAGESAVE, CP_Measurement_TestMode, MT_Cmd_CenterPoint);


}

	m_bn_Item[Btn_CT_CENTER_TEST].EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnBnClickedBnTestStop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:06
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::OnBnClickedBnTestStop()
{
	m_bTest_Flag = FALSE;
	m_bn_Item[Btn_CT_CENTER_TEST].EnableWindow(TRUE);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_CenterPointOp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::SetUI()
{
	if (m_pstModelInfo == NULL)
		return;

	CString str;

	m_ListCPOp.SetTestModeChange(m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode);

	m_ListCPOp.SetPtr_CenterPoint(&m_pstModelInfo->stCenterPoint);
	m_ListCPOp.InsertFullData();

	m_Cb_Item[ComboI_CP_CAMERASTATE].SetCurSel(m_pstModelInfo->stCenterPoint.stCenterPointOp.nCameraState);
	m_Cb_Item[ComboI_CP_TESTMODE].SetCurSel(m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode = CP_Measurement_TestMode);

	str.Format(_T("%.2f"), m_pstModelInfo->stCenterPoint.stCenterPointOp.dbImageSensorPix);
	m_ed_Item[Edit_CP_IMAGESENSOR].SetWindowText(str);

	str.Format(_T("%d"), m_pstModelInfo->stCenterPoint.stCenterPointOp.nWriteCnt);
	m_ed_Item[Edit_CP_TRYCOUNT].SetWindowText(str);
}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CenterPointOp::GetUI()
{
	if (m_pstModelInfo == NULL)
		return;

	CString str;

	m_ListCPOp.GetCellData();

	m_pstModelInfo->stCenterPoint.stCenterPointOp.nCameraState = m_Cb_Item[ComboI_CP_CAMERASTATE].GetCurSel();
	m_pstModelInfo->stCenterPoint.stCenterPointOp.nTestMode = CP_Measurement_TestMode; //m_Cb_Item[ComboI_CP_TESTMODE].GetCurSel();

	m_ed_Item[Edit_CP_IMAGESENSOR].GetWindowText(str);
	m_pstModelInfo->stCenterPoint.stCenterPointOp.dbImageSensorPix = _ttof(str);

	m_ed_Item[Edit_CP_TRYCOUNT].GetWindowText(str);
	m_pstModelInfo->stCenterPoint.stCenterPointOp.nWriteCnt = _ttoi(str);
}

//=============================================================================
// Method		: IsTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:40
// Desc.		:
//=============================================================================
BOOL CWnd_CenterPointOp::IsTest()
{
	return m_bTest_Flag;
}
