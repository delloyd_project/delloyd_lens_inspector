﻿// Wnd_PaticleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ParticleOp.h"
#include "resource.h"

// CWnd_ParticleOp

typedef enum Paticle_ID
{
	IDC_EDIT_SENSITIVITY = 1000,
	IDC_BTN_TEST,
	IDC_BTN_ROI_DISPLAY,
	IDC_BTN_DEFAULT_SET,
	IDC_LIST_PARTICLEOP,
};

IMPLEMENT_DYNAMIC(CWnd_ParticleOp, CWnd)

CWnd_ParticleOp::CWnd_ParticleOp()
{
	m_pstModelInfo = NULL;

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_ParticleOp::~CWnd_ParticleOp()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ParticleOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_DEFAULT_SET, OnBnClickedBnDefaultSet)
	ON_BN_CLICKED(IDC_BTN_TEST, OnBnClickedBnTest)
END_MESSAGE_MAP()

// CWnd_ParticleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/12 - 18:30
// Desc.		:
//=============================================================================
int CWnd_ParticleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_PT_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Item[nIdx].Create(g_szPaticle_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < Edt_PT_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, CRect(0, 0, 0, 0), this, IDC_EDIT_SENSITIVITY + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0.00"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789"));
	}

	m_ListPtOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_PARTICLEOP);

	for (UINT nIdx = 0; nIdx < Btn_PT_MAXNUM; nIdx++)
	{
		if (nIdx == Btn_PT_FAIL_DISPLAY)
			m_bn_Item[nIdx].Create(g_szPaticle_Button[nIdx], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_TEST + nIdx);
		else
			m_bn_Item[nIdx].Create(g_szPaticle_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_TEST + nIdx);

		m_bn_Item[nIdx].SetFont(&m_font_Data);
	}

	m_bn_Item[Btn_PT_FAIL_DISPLAY].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[Btn_PT_FAIL_DISPLAY].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[Btn_PT_FAIL_DISPLAY].SizeToContent();
	m_bn_Item[Btn_PT_FAIL_DISPLAY].SetCheck(BST_CHECKED);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/12 - 18:33
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int Static_W = iWidth / 4;
	int Static_H = 26;
	int Combo_W = iWidth / 3;
	int Combo_H = 26;

	int TempLeft = iLeft;
	int iBtnWidth = iWidth / 7;

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Item[Btn_PT_TEST].MoveWindow(iLeft, iTop, iBtnWidth, Combo_H);

	iTop += Combo_H + iSpacing;
	m_bn_Item[Btn_PT_DEFAULT_SET].MoveWindow(iLeft, iTop, iBtnWidth, Combo_H);

	iTop += Combo_H + iSpacing;
	m_bn_Item[Btn_PT_FAIL_DISPLAY].MoveWindow(iLeft, iTop, iBtnWidth, Combo_H);

	iTop = iMargin;
	for (UINT nIdx = 0; nIdx < STI_PT_MAXNUM; nIdx++)
	{
		iLeft = TempLeft;
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, Static_W, Static_H);
		iLeft += Static_W + iSpacing;
		m_ed_Item[nIdx].MoveWindow(iLeft, iTop, Combo_W, Static_H);
		iTop += Static_H + iSpacing;
	}

	iLeft = TempLeft;
	int List_W = (Static_W + Combo_W + iSpacing);
	int List_H = iHeight / 2;

	m_ListPtOp.MoveWindow(iLeft, iTop, List_W, List_H);


}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/13 - 14:58
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnBnClickedBnTest()
{
	m_bn_Item[Btn_PT_TEST].EnableWindow(FALSE);
		
	GetOwner()->SendMessage(WM_MANUAL_TEST, 0, MT_Cmd_Particle);
	
	m_bn_Item[Btn_PT_TEST].EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnBnClickedBnDefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/14 - 10:29
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnBnClickedBnDefaultSet()
{
	for (UINT nRig = 0; nRig < Particle_Region_MaxEnum; nRig++)
	{
		m_pstModelInfo->stParticle.stParticleOp.rectData[nRig].RegionList.left = 10;
		m_pstModelInfo->stParticle.stParticleOp.rectData[nRig].RegionList.top = 10;
		m_pstModelInfo->stParticle.stParticleOp.rectData[nRig].RegionList.right = 640;
		m_pstModelInfo->stParticle.stParticleOp.rectData[nRig].RegionList.bottom = 480;
	}

 	m_ListPtOp.InsertFullData();
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:00
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Particle, 0);
	else
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_ParticleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::SetUI()
{
	CString str;

	m_ListPtOp.SetPtr_Particle(&m_pstModelInfo->stParticle);
	m_ListPtOp.InsertFullData();

	str.Format(_T("%d"), m_pstModelInfo->stParticle.stParticleOp.iDustDis);
	m_ed_Item[Edt_PT_SENSITIVITY].SetWindowText(str);

	m_bn_Item[Btn_PT_FAIL_DISPLAY].SetCheck(m_pstModelInfo->stParticle.stParticleOp.bFailCheck);
}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_ParticleOp::GetUI()
{
	CString str;
	int	iDust  = 0;

	m_ListPtOp.GetCellData();

	m_ed_Item[Edt_PT_SENSITIVITY].GetWindowText(str);
	iDust = _ttoi(str);

	if (iDust > 10)
	{
		iDust = 10;
		str.Format(_T("%d"), iDust);
		m_ed_Item[Edt_PT_SENSITIVITY].SetWindowText(str);
	}

	m_pstModelInfo->stParticle.stParticleOp.iDustDis = iDust;

	m_pstModelInfo->stParticle.stParticleOp.bFailCheck = m_bn_Item[Btn_PT_FAIL_DISPLAY].GetCheck();
}