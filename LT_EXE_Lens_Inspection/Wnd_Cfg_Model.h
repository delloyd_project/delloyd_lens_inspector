﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Model_h__
#define Wnd_Cfg_Model_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

enum enModel_Static
{
	STI_ML_ModelName = 0,
	STI_ML_Cam_Volt,
	STI_ML_Cam_Delay,
	STI_ML_Retry_Test_Cnt,
	STI_ML_MAXNUM,
};


static LPCTSTR	g_szModel_Static[] =
{
	_T("MODEL"),
	_T("Voltage Cam"),
	_T("Camera Delay [ms]"),
	_T("Test Count"),

	NULL
};

enum enCenterPoint_Button
{
	BTN_CT_CenterPoint,
	BTN_CT_MAXNUM
};


static LPCTSTR	g_CTPoint_Button[] =
{
	_T("Center Point"),
	NULL
};


enum enModel_Combobox{
	ComboI_ML_RetryTestCnt = 0,
	ComboI_ML_MAXNUM = 1,
};

// 검사 각도 0, 90, 180, 270 별 검사
static LPCTSTR g_szModel_Retry_Test_Cnt[] =
{
	_T("1"),
	_T("2"),
	_T("3"),
	_T("4"),
	NULL
};

enum enModel_Edit{
	Edit_ML_ModelName = 0,
	Edit_ML_Voltage,
	Edit_ML_CameraDelay,

	Edit_ML_MAXNUM,
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_Model
//-----------------------------------------------------------------------------
class CWnd_Cfg_Model : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Model)

public:
	CWnd_Cfg_Model();
	virtual ~CWnd_Cfg_Model();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnApply();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Data;
	CString			m_szI2CPath;

	CVGStatic		m_st_Item[STI_ML_MAXNUM];
	CMFCButton		m_bn_Check[BTN_CT_MAXNUM];
	CComboBox		m_cb_Item[ComboI_ML_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[Edit_ML_MAXNUM];
	CFile_WatchList	m_IniWatch;
	CMFCButton		m_bn_Item;
	CMFCButton		m_bn_Distortion;

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_ModelInfo& stModelInfo);
	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_ModelInfo* pModelInfo);

	void		RefreshFileList		(__in const CStringList* pFileList);

public:
	
	// 파일이 있는 경로 설정
	void		SetPath				(__in LPCTSTR szI2CPath)
	{
		m_szI2CPath = szI2CPath;
	};

	// 모델 데이터를 UI에 표시
	void		SetModelInfo		(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetModelInfo		(__out ST_ModelInfo& stModelInfo);
};

#endif // Wnd_Cfg_Model_h__


