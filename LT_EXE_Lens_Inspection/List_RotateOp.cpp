﻿// List_RotateOp.Rtp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_RotateOp.h"

#define IRtOp_ED_CELLEDIT		5001

// CList_RotateOp

IMPLEMENT_DYNAMIC(CList_RotateOp, CListCtrl)

CList_RotateOp::CList_RotateOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstRotate = NULL;
}

CList_RotateOp::~CList_RotateOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_RotateOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_RotateOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_RotateOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_RotateOp::OnEnKillFocusERtOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_RotateOp 메시지 처리기입니다.
int CList_RotateOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

void CList_RotateOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[RtOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < RtOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_RtOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < RtOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_RtOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_RtOp[RtOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
 	SetColumnWidth(RtOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/6/26 - 14:59
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_RotateOp::InitHeader()
{
	for (int nCol = 0; nCol < RtOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_RtOp[nCol], iListAglin_RtOp[nCol], iHeaderWidth_RtOp[nCol]);

	for (int nCol = 0; nCol < RtOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_RtOp[nCol]);
	}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LT_TI_Rotate * pstRotate
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateOp::InsertFullData()
{
	if (m_pstRotate == NULL)
		return;

 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < RegionRotate_MaxEnum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateOp::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("%s"), g_lpszItem_RtOp[nRow]);
	SetItemText(nRow, RtOp_Object, strText);

	strText.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.CenterPoint().x);
	SetItemText(nRow, RtOp_PosX, strText);

	strText.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.CenterPoint().y);
	SetItemText(nRow, RtOp_PosY, strText);

	strText.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.Width());
	SetItemText(nRow, RtOp_Width, strText);

	strText.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height());
	SetItemText(nRow, RtOp_Height, strText);
 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:40
// Desc.		:
//=============================================================================
void CList_RotateOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/14 - 14:41
// Desc.		:
//=============================================================================
void CList_RotateOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < RtOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusERtOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnKillFocusERtOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText)))
	{
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (iValue < 0)
		iValue = 0;

	CString str;
	CRect Data = m_pstRotate->stRotateOp.rectData[nRow].RegionList;

	switch (nCol)
	{
	case RtOp_PosX:
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum((int)iValue, Data.CenterPoint().y, Data.Width(), Data.Height());
		break;
	case RtOp_PosY:
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum(Data.CenterPoint().x, (int)iValue, Data.Width(), Data.Height());
		break;
	case RtOp_Width:
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2((int)iValue, Data.Height());
		break;
	case RtOp_Height:
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2(Data.Width(), (int)iValue);
		break;
	default:
		break;
	}

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.left < 0)
	{
		CRect DataTemp = m_pstRotate->stRotateOp.rectData[nRow].RegionList;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.left = 0;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.right = m_pstRotate->stRotateOp.rectData[nRow].RegionList.left + DataTemp.Width();
	}

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.right > CAM_IMAGE_WIDTH)
	{
		CRect DataTemp = m_pstRotate->stRotateOp.rectData[nRow].RegionList;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.right = CAM_IMAGE_WIDTH;

		if (CAM_IMAGE_WIDTH - DataTemp.Width() >= 0)
			m_pstRotate->stRotateOp.rectData[nRow].RegionList.left = CAM_IMAGE_WIDTH - DataTemp.Width();
	}

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.top < 0)
	{
		CRect DataTemp = m_pstRotate->stRotateOp.rectData[nRow].RegionList;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.top = 0;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.bottom = DataTemp.Height() + m_pstRotate->stRotateOp.rectData[nRow].RegionList.top;
	}

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.bottom > CAM_IMAGE_HEIGHT)
	{
		CRect DataTemp = m_pstRotate->stRotateOp.rectData[nRow].RegionList;
		m_pstRotate->stRotateOp.rectData[nRow].RegionList.bottom = CAM_IMAGE_HEIGHT;
		
		if (CAM_IMAGE_HEIGHT - DataTemp.Height() >= 0)
			m_pstRotate->stRotateOp.rectData[nRow].RegionList.top = CAM_IMAGE_HEIGHT - DataTemp.Height();
	}

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height() <= 0)
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2(m_pstRotate->stRotateOp.rectData[nRow].RegionList.Width(), 1);

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height() >= CAM_IMAGE_HEIGHT)
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2(m_pstRotate->stRotateOp.rectData[nRow].RegionList.Width(), CAM_IMAGE_HEIGHT);

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.Width() <= 0)
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2(1, m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height());

	if (m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height() >= CAM_IMAGE_HEIGHT)
		m_pstRotate->stRotateOp.rectData[nRow]._Rect_Position_Sum2(CAM_IMAGE_HEIGHT, m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height());

	switch (nCol)
	{
	case RtOp_PosX:
		str.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.CenterPoint().x);
		break;
	case RtOp_PosY:
		str.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.CenterPoint().y);
		break;
	case RtOp_Width:
		str.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.Width());
		break;
	case RtOp_Height:
		str.Format(_T("%d"), m_pstRotate->stRotateOp.rectData[nRow].RegionList.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:43
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.1f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/6/26 - 14:43
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/6/26 - 14:51
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
	int casenum = pWndFocus->GetDlgCtrlID();

		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dValue = dValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dValue)
				dValue = dValue + ((zDelta / 120)*0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dValue < 0.0)
			dValue = 0.0;

		if (dValue > 10.0)
			dValue = 10.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: Change_DATA_CHECK
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2017/6/26 - 14:57
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::Change_DATA_CHECK(UINT nIdx)
{
	// 상한
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.Width() > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.Height() > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.CenterPoint().x > CAM_IMAGE_WIDTH) 
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.CenterPoint().y > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.top > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.bottom > CAM_IMAGE_HEIGHT)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.left > CAM_IMAGE_WIDTH)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.right > CAM_IMAGE_WIDTH)
		return FALSE;

	// 하한
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.Width() < 1) 
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.Height() < 1)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.CenterPoint().x < 0)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.CenterPoint().y < 0)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.top < 0)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.bottom < 0)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.left < 0)
		return FALSE;
	if (m_pstRotate->stRotateOp.rectData[nIdx].RegionList.right < 0)
		return FALSE;

	return TRUE;
}
