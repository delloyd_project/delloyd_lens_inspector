﻿//*****************************************************************************
// Filename	: 	Wnd_TestInfo.h
// Created	:	2016/5/13 - 11:39
// Modified	:	2016/5/13 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_TestInfo_h__
#define Wnd_TestInfo_h__

#pragma once

#include "VGGroupWnd.h"
#include "VGStatic.h"
#include "Def_Enum.h"


enum enTestButton
{
	TB_Start,
	TB_Stop,
	TB_LotStart,
	TB_LotStop,
	TB_ModelChange,
// 	TB_LED_On,
// 	TB_LED_Off,
	TB_MasterSet,
	TB_Manual,
	TB_Auto,
	TB_ManualDeg,
	TB_ManualDeg1,

	TB_MaxEnum,
};

enum enTestStatic
{
	TS_Manual,

	TS_MaxEnum,
};

enum enTestEdit
{
	TE_Manual,

	TE_MaxEnum,
};

static LPCTSTR g_szTestButton[] =
{
	_T("START"),
	_T("STOP"),
	_T("LOT START"),
	_T("LOT STOP"),
	_T("MODEL CHANGE"),
// 	_T("LED ON"),
// 	_T("LED OFF"),
	_T("MASTER SET"),
	_T("Manual"),
	_T("Auto"),
	_T("+"),
	_T("-"),

	NULL
};


static LPCTSTR g_szTestStatic[] =
{
	_T("MANUAL"),

	NULL
};


//-----------------------------------------------------------------------------
// CWnd_TestInfo
//-----------------------------------------------------------------------------
class CWnd_TestInfo : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestInfo)

public:
	CWnd_TestInfo();
	virtual ~CWnd_TestInfo();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds		(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	CFont			m_Font;	
	CVGStatic		m_st_Item[TS_MaxEnum];
	CMFCMaskedEdit	m_ed_Item[TE_MaxEnum];
	CVGGroupWnd		m_Group;
public:
	CMFCButton		m_bn_Test[TB_MaxEnum];

	void	PermissionMode	(enPermissionMode InspMode);
	void	ButtonEnable	(BOOL bMode);
	//void	ManualAutoMode	(BOOL bMode);
	void	ManualBtnEnable	(BOOL bMode);
	int		 m_mode;
	void	ChangeBtnState	(BOOL bMode);
	void	ChangeStartBtnState(BOOL bMode);

};

#endif // Wnd_TestInfo_h__


