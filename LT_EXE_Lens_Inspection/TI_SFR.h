﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_SFR
{
public:
	CTI_SFR();
	~CTI_SFR();

	BOOL	SetImageSize	(DWORD dwWidth, DWORD dwHeight);

	// 전체 SFR TEST
	UINT	SFR_Test		(ST_LT_TI_SFR *pstSFR, LPBYTE pImageBuf, LPWORD	pImageSourceBuf);

	void	SFR_8Bit		(ST_LT_TI_SFR *pstSFR, LPBYTE pImageBuf);
	void	SFR_16Bit		(ST_LT_TI_SFR *pstSFR, LPWORD pImageSourceBuf);

	void MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void Inverse(double **dataMat, unsigned int n, double **MatRtn);
	void pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat);

	double	GetSFRValue(BYTE *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair, UINT nMode);
	double	GetSFRValue_16bit	(WORD *atemp, int width, int height, double dbPixelSz, double LinePerPixel, UINT nItem);

	void	SearchROIArea(LPBYTE IN_RGB, int tempWidth, int tempHeight, int FieldCenterX, int FieldCenterY, ST_LT_TI_SFR *pstSFR);
	void	SetROI_CenterRef(LPBYTE IN_RGB, ST_LT_TI_SFR *pstSFR, CvPoint CenterPoint);

	CvPoint SearchCenterPoint(LPBYTE IN_RGB);

	double	GetDistance(int x1, int y1, int x2, int y2);

	int		FiducialMark_Test(IN IplImage* pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN CRect rtROI, OUT POINT& ptOUT_Center);


protected:

	UINT m_nWidth;
	UINT m_nHeight;

	CvPoint	m_currPt;
	BOOL m_bLimit[Region_SFR_MaxEnum];

};