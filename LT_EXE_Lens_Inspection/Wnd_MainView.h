﻿//*****************************************************************************
// Filename	: Wnd_MainView.h
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_MainView_h__
#define Wnd_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Grid_ModelInfo.h"
#include "Wnd_BaseView.h"
#include "Wnd_SiteInfo.h"
#include "Wnd_TestInfo.h"
#include "Wnd_ModelInfo.h"
#include "Wnd_LotInfo.h"
#include "Wnd_MasterSet.h"
#include "Def_TestDevice.h"

//=============================================================================
// CWnd_MainView
//=============================================================================
class CWnd_MainView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_MainView)

public:
	CWnd_MainView();
	virtual ~CWnd_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	// 데이터
	enPermissionMode	m_InspMode;
	ST_InspectionInfo*	m_pstInspInfo;
	ST_Device*			m_pDevice;

	CWnd_ModelInfo		m_wnd_ModelInfo;
	CWnd_LotInfo		m_wnd_LotInfo;
	CWnd_MasterSet		m_wnd_MasterSet;

public:
	CWnd_TestInfo		m_wnd_TestInfo;

	CWnd_SiteInfo		m_wnd_SiteInfo;
	
	void	SetPtrInspectionInfo		(__inout ST_InspectionInfo* pstInspInfo)
	{
		if (pstInspInfo == NULL)
			return;

		m_pstInspInfo = pstInspInfo;
	};

	void	SetPtr_Device				(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	// 검사 모드 설정
	void	SetInspectionMode			(enPermissionMode InspMode);
	void	SetMasterMode				(__in BOOL bMode);
	//void	ManualAutoMode				(__in BOOL bMode);
	void	ManualBtnEnable				(__in BOOL bMode);

	// UI 갱신
	void	UpdatSetResult				(__in enTestResult Result);
	void	UpdatSetResult				(__in enTestResult Result, __in CString strText);
	void	UpdatSetErrorCode			(__in LPCTSTR szErrorCode);
	void	UpdateModelInfo				();
	void	UpdateMasterInfo			(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);
	void	UpdateLotInfo				();
	void	UpdateMESInfo				();
	void	UpdateYield					();
	void	UpdatePogoCount				();
	void	UpdateCycleTime				();
	void	UpdatePogoCnt				();
	//void	UpdateTestCurrent			(__in enTestEachResult EachResult, __in CString strText);	// 소비전류 검사
	void	UpdateTestCenterAdjust		(__in enTestEachResult EachResult, __in CString strText);	// 광축조정 검사
	void	UpdateTestResolution		(__in enTestEachResult EachResult, __in CString strText);	// 해상력 검사
	void	UpdateTestRotation			(__in enTestEachResult EachResult, __in CString strText);	// Rotation 검사
	void	UpdateTestActiveAlgin		(__in enTestEachResult EachResult, __in CString strText);	// ActiveAlgin
	void	UpdateTestFocus				(__in enTestEachResult EachResult, __in CString strText);	// Focus
	void	UpdateTestParticle			(__in enTestEachResult EachResult, __in CString strText);	// 이물 검사
	
// 	void	UpdateTestAFPostion			(__in enTestEachResult EachResult, __in CString strText);	// AF 위치

	void	UpdateTestInitialize		(__in enTestEachResult EachResult, __in CString strText);	// 검사 초기화
	void	UpdateTestFinalize			(__in enTestEachResult EachResult, __in CString strText);	// 검사 마무리


	void	UpdateSetStartBtnChange		(__in BOOL bMode);

	void UpdateAngleColor (__in enTestResult nResult, __in int nRetryTestCnt, BOOL bInitmode = FALSE);
	// 검사별 상세 정보 --------------------------------------------------------
	void	UpdateTestProcess			();
	void	UpdateTestResult			();
	void	SetTestResult_Unit			(__in enTestResult nResult);
	void	UpdateElapsedTime_All		(__in DWORD	dwTime);
	void	UpdateInputTime				();

	void	InitTestResult				();

	//void	InsertBarcode				(__in LPCTSTR szBarcode);

	void	UpdateResetSiteInfo			();
};

#endif // Wnd_MainView_h__
