﻿#ifndef Overlay_Proc_h__
#define Overlay_Proc_h__

#pragma once

typedef enum enOverlayMode
{
	OvrMode_LINE,		// 직선
	OvrMode_RECTANGLE,	// 직사각형
	OvrMode_CIRCLE,		// 원
	OvrMode_TXT,		// 글씨
	OvrMode_MaxNum,
};

#include "cv.h"
#include "highgui.h"
#include "Def_DataStruct.h"

// COverlay_Proc
class COverlay_Proc
{

public:
	COverlay_Proc();
	virtual ~COverlay_Proc();
	BOOL	m_bTestmode = TRUE;

	
	void Overlay_OpticalCenter(__inout IplImage* lpImage, __in ST_CenterPoint_Op stOption, __in ST_CenterPoint_Data stData);
	void	Overlay_Current				(__inout IplImage* lpImage, __in ST_Current_Op		stOption, __in ST_Current_Data			stData);
	void	Overlay_SFR					(__inout IplImage* lpImage, __in ST_SFR_Op				stOption, __in ST_SFR_Data				stData);
	void	Overlay_EIAJ(__inout IplImage* lpImage, __in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData);

	int		Overlay_PeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);
	int		Overlay_StandarData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);
	int		Overlay_StandardPeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI);

protected:

	void	Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor = RGB(255, 255, 0), __in int iLineSize = 1, __in double dbFontSize = 1.0, __in CString szText = _T(""));

};


#endif // Overlay_Proc_h__
