﻿//*****************************************************************************
// Filename	: Def_WindowMessage.h
// Created	: 2012/1/16
// Modified	: 2016/08/17
//
// Author	: PiRing
//	
// Purpose	: 윈도우 메세지 정의 (0x0400 ~ 0x7FFF)
//*****************************************************************************
#ifndef Def_WindowMessage_h__
#define Def_WindowMessage_h__

#include "Def_WindowMessage_Cm.h"

//-------------------------------------------------------------------
// 프로그램 운영
//-------------------------------------------------------------------

#define		WM_LOGMSG_INDICATOR			WM_USER + 1000	// Indicator로부터 로그 메세지
#define		WM_ZEROSET_INDICATOR		WM_USER + 1001	// Zero Set 버튼 메세지
#define		WM_MANUAL_CONTROL			WM_USER + 1002	// Manual 버튼 메세지
#define		WM_TAB_CHANGE_PIC			WM_USER + 1003	
#define		WM_MASTER_SET				WM_USER + 1004
#define		WM_MODEL_SAVE				WM_USER + 1006	
#define		WM_RECV_MAIN_BRD_ACK		WM_USER + 1007	// 제어 보드로부터 데이터 수신
#define		WM_MANUAL_TEST				WM_USER + 1008	// Manual TEST 메세지
#define		WM_EDGE_ONOFF				WM_USER + 1009	// Manual TEST 메세지
#define		WM_DISTORTION_CORRECT		WM_USER + 1010	// Manual TEST 메세지
#define		WM_MANUAL_TEST_IMAGESAVE	WM_USER + 1011	// Manual TEST 메세지

#define		WM_SELECT_AXIS				WM_USER + 5001
#define		WM_UPDATA_AXIS				WM_USER + 5002
#define		WM_CHANGED_MOTOR			WM_USER + 5003	
#define		WM_COMM_STATUS_MES			WM_USER + 5004	// MES 통신상태
#define		WM_ALIGN					WM_USER + 5005
#define		WM_MANUAL_DEGREE			WM_USER + 5006



//-------------------------------------------------------------------
//
//-------------------------------------------------------------------



#endif // Def_WindowMessage_h__
