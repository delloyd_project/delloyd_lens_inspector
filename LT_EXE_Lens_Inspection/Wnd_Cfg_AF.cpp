﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_AF.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Vision.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_AF.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum AF_ID
{
	IDC_ED_STD_Displace = 1000,
	IDC_ED_STD_DisplaceDev,
	IDC_ED_LensConchoid,
	IDC_ED_StepDegree_1,
	IDC_ED_StepDegree_2,
	IDC_ED_StepDegree_3,
	IDC_ED_RotateCnt,

	IDC_CB_LensDirection = 2000,

};

// CWnd_Cfg_MotionTeach
IMPLEMENT_DYNAMIC(CWnd_Cfg_AF, CWnd_BaseView)

CWnd_Cfg_AF::CWnd_Cfg_AF()
{
	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_AF::~CWnd_Cfg_AF()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_AF, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_Cfg_AF message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_AF::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_AF_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szAF_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < Edit_AF_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_STD_Displace + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
	}

	// Combo box
	for (UINT nIdx = 0; nIdx < Combo_AF_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_LensDirection + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; NULL != g_szAF_LensDirCombo[nIdx]; nIdx++)
		m_cb_Item[Combo_AF_LensDirection].AddString(g_szAF_LensDirCombo[nIdx]);

	m_cb_Item[Combo_AF_LensDirection].SetCurSel(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_AF::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iCnt = 0;

	int iMagrin		 = 10;
	int iSpacing	 = 5;
	int iCateSpacing = 5;
	int iLeft		 = iMagrin;
	int iTop		 = iMagrin;
	int iWidth		 = cx - iMagrin - iMagrin;
	int iHeight		 = cy - iMagrin - iMagrin;
	int iStWidth	 = 140;
	int iEdWidth	 = 200;
	int iStHeight	 = 25;

	for (int iStatic = 0; iStatic < STI_AF_MAXNUM; iStatic++)
	{
		m_st_Item[iStatic].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
		iTop += iStHeight + iSpacing;
	}

	iTop = iMagrin;
	iLeft += iStWidth + iSpacing;
	
	for (int iEdit = 0; iEdit < Edit_AF_MAXNUM; iEdit++)
	{
		m_ed_Item[iEdit].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);
		iTop += iStHeight + iSpacing;

		if (iEdit == Edit_AF_LensConchoid)
		{
			m_cb_Item[Combo_AF_LensDirection].MoveWindow(iLeft, iTop, iEdWidth, 100);
			iTop += iStHeight + iSpacing;
		}
	}
}


//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_AF::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_AF::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

		// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
		// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_AF::GetUIData(__out ST_ModelInfo& stModelInfo)
{
	CString strValue;	

	m_ed_Item[Edit_AF_STD_Displace].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dSTD_Displace = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_STD_DisplaceDev].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dSTD_DisplaceDev = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_LensConchoid].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dLensConchoid = _ttof(strValue);
	strValue.ReleaseBuffer();

	stModelInfo.stAFInfo.bLensDirection = m_cb_Item[Combo_AF_LensDirection].GetCurSel();
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_StepDegree_1].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dAFStepDegree1 = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_StepDegree_2].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dAFStepDegree2 = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_StepDegree_3].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dAFStepDegree3 = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_AF_RotateCnt].GetWindowTextW(strValue);
	stModelInfo.stAFInfo.dAFRotateCnt = _ttof(strValue);
	strValue.ReleaseBuffer();
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_AF::SetUIData(__in const ST_ModelInfo* pModelInfo)
{
	CString strValue;

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dSTD_Displace);
	m_ed_Item[Edit_AF_STD_Displace].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dSTD_DisplaceDev);
	m_ed_Item[Edit_AF_STD_DisplaceDev].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dLensConchoid);
	m_ed_Item[Edit_AF_LensConchoid].SetWindowTextW(strValue);

	m_cb_Item[Combo_AF_LensDirection].SetCurSel(pModelInfo->stAFInfo.bLensDirection);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dAFStepDegree1);
	m_ed_Item[Edit_AF_StepDegree_1].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dAFStepDegree2);
	m_ed_Item[Edit_AF_StepDegree_2].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dAFStepDegree3);
	m_ed_Item[Edit_AF_StepDegree_3].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stAFInfo.dAFRotateCnt);
	m_ed_Item[Edit_AF_RotateCnt].SetWindowTextW(strValue);
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_AF::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	SetUIData(pModelInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_AF::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo);
}
