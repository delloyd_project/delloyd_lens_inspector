﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.h
// Created	:	2017/1/2 - 15:41
// Modified	:	2017/1/2 - 15:41
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_h__
#define Wnd_Cfg_TestItem_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestItem : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem)

public:
	CWnd_Cfg_TestItem();
	virtual ~CWnd_Cfg_TestItem();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	afx_msg void	OnLvnItemchanged		(NMHDR *pNMHDR, LRESULT *pResult);

	CFont		m_font_Data;

	// 검사 항목 목록
	CVGStatic		m_st_TestItem;
	CListCtrl		m_lc_TestItem;

	// 선택한 검사 항목
	CVGStatic		m_st_SelectedItem;
	CListCtrl		m_lc_SelectedItem;

	CArray<UINT, UINT&>		m_arSelectedItemz;

	void			InitListCtrl();
	virtual void	InitTestItem();

	UINT			GetTestItemID(__in UINT nListIndex);
	UINT			GetTestItemListIndex(__in UINT nItemID);

	void			UpdateSelectedItemz();

public:

	// 모델 데이터를 UI에 표시
	void			SetModelInfo(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void			GetModelInfo(__out ST_ModelInfo& stModelInfo);
};

#endif // Wnd_Cfg_TestItem_h__

