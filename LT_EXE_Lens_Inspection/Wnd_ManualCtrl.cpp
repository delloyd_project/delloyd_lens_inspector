﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.cpp
// Created	:	2017/1/4 - 13:32
// Modified	:	2017/1/4 - 13:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_ManualCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ManualCtrl.h"
#include "VGStatic.h"
#include "resource.h"

typedef enum ManualCtrl_ID
{
	IDC_BTN_POWER_ON = 1001,
	IDC_BTN_MAX_NUM = IDC_BTN_POWER_ON + 100,
	
	IDC_ED_MANUAL_CTRL = 2000,
};
// CWnd_ManualCtrl

IMPLEMENT_DYNAMIC(CWnd_ManualCtrl, CVGGroupWnd)

CWnd_ManualCtrl::CWnd_ManualCtrl()
{
	m_pDevice	= NULL;

	VERIFY(m_font_Default.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Edit.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_ManualCtrl::~CWnd_ManualCtrl()
{
	m_font_Default.DeleteObject();
	m_font_Edit.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ManualCtrl, CVGGroupWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_POWER_ON, IDC_BTN_MAX_NUM, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_ManualCtrl message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/4 - 13:39
// Desc.		:
//=============================================================================
int CWnd_ManualCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwEtReadStyle = WS_VISIBLE | WS_BORDER | ES_CENTER | ES_READONLY;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < ST_MCTRL_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szManual_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < BT_MCTRL_IMAGELOAD; nIdx++)
	{
		m_Btn_Item[nIdx].Create(g_szManual_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_POWER_ON + nIdx);
		m_Btn_Item[nIdx].SetFont(&m_font_Default);
	}

	m_Btn_Item[BT_MCTRL_IMAGELOAD].Create(g_szManual_Button[BT_MCTRL_IMAGELOAD], dwStyle  | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_POWER_ON + BT_MCTRL_IMAGELOAD);
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetFont(&m_font_Default);
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetImage(IDB_UNCHECKED_16);
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetCheckedImage(IDB_CHECKED_16);
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SizeToContent();
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetCheck(BST_UNCHECKED);

	for (UINT nIdx = ED_MCTRL_VISION_DATA; nIdx < ED_MCTRL_MAXNUM; nIdx++)
	{
		if (nIdx == ED_MCTRL_DEGREE)
			m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_ED_MANUAL_CTRL + nIdx);
		else
			m_ed_Item[nIdx].Create(dwEtReadStyle, rectDummy, this, IDC_ED_MANUAL_CTRL + nIdx);


		m_ed_Item[nIdx].SetFont(&m_font_Edit);
		m_ed_Item[nIdx].SetWindowText(_T(""));
	}



	return 0;
}


// void CWnd_ManualCtrl::SetInit_InitLoad()
// {
// 	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetCheck(BST_UNCHECKED);
// 	
// 	
// }

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	cy = (int)(HEIGHT_SIZE_T * cy);

	int iSpacing = 5;
	int iCateSpacing = 3;
	int iLeft = iSpacing;
	int iTop = 27;
	int iWidth = cx - iSpacing - iSpacing;
	int iHeight = cy - iSpacing - iTop;
	int iTemp = 0;

	if ((BT_MCTRL_MAXNUM + 0) % 2 == 1)
	{
		iTemp = (BT_MCTRL_MAXNUM + 0) / 2 + 1;
	}
	else
	{
		iTemp = (BT_MCTRL_MAXNUM + 0) / 2;
	}
	
	iWidth = (iWidth - iSpacing) / 2;
	iHeight = (iHeight - iCateSpacing * iTemp) / iTemp;

	for (int i = 0; i < BT_MCTRL_MAXNUM; i+=2)
	{
		iLeft = iSpacing;
		m_Btn_Item[i].MoveWindow(iLeft, iTop, iWidth, iHeight);

		iLeft += iWidth + iSpacing;
		m_Btn_Item[i + 1].MoveWindow(iLeft, iTop, iWidth, iHeight);

		iTop += iHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:48
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_POWER_ON;
	ClickOut(nIndex);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_ManualCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: ClickOut
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:44
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::ClickOut(UINT nID)
{
	AllBtnEnableMode(false);

	int nCheck = 0;
	if (nID == BT_MCTRL_IMAGELOAD)
	{
		nCheck = m_Btn_Item[BT_MCTRL_IMAGELOAD].GetCheck();
	}

	GetOwner()->SendMessage(WM_MANUAL_CONTROL, nCheck, nID);

// 	Sleep(200);
// 
// 	for (UINT nIdx = 0; nIdx < Man_Cmd_Total; nIdx++)
// 		m_Btn_Item[nIdx].EnableWindow(TRUE);
}


//=============================================================================
// Method		: OnSetResultVision
// Access		: public  
// Returns		: void
// Parameter	: __in CString strResult
// Qualifier	:
// Last Update	: 2017/10/19 - 11:01
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnSetResultVision(__in CString strResult)
{
	m_ed_Item[ED_MCTRL_VISION_DATA].SetWindowTextW(strResult);
}


//=============================================================================
// Method		: OnSetResultDisplace
// Access		: public  
// Returns		: void
// Parameter	: __in CString strResult
// Qualifier	:
// Last Update	: 2017/10/19 - 11:01
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnSetResultDisplace(__in CString strResult)
{
	m_ed_Item[ED_MCTRL_DISPLACE_DATA].SetWindowTextW(strResult);
}

//************************************
// Method:    OnGetManualDegree
// FullName:  CWnd_ManualCtrl::OnGetManualDegree
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: __out double & dDegree
//************************************
void CWnd_ManualCtrl::OnGetManualDegree(__out double& dDegree)
{
	CString strData;

	m_ed_Item[ED_MCTRL_DEGREE].GetWindowTextW(strData);
	dDegree = _ttof(strData);
}


void CWnd_ManualCtrl::ImageLoadMode(bool bMode){	
	m_Btn_Item[BT_MCTRL_IMAGELOAD].SetCheck(bMode);
}

void CWnd_ManualCtrl::AllBtnEnableMode(bool bMode){

	for (UINT nIdx = 0; nIdx < Man_Cmd_Total; nIdx++)
		m_Btn_Item[nIdx].EnableWindow(bMode);
}

void CWnd_ManualCtrl::SetBtnDisable()
{
	for (UINT nIdx = 0; nIdx <= Man_Cmd_Power_Off; nIdx++)
		m_Btn_Item[nIdx].EnableWindow(FALSE);
}