﻿#ifndef Wnd_MasterSet_h__
#define Wnd_MasterSet_h__

#pragma once

#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "TI_PicControl.h"
#include "VGGroupWnd.h"

// CWnd_MasterSet

enum enMaster_Static{
	STI_MST_X = 0,
	STI_MST_Y,
 	STI_MST_R,
	STI_MST_MasterInfo,
	STI_MST_MasterSpc,
	STI_MST_MasterOffset,
	STI_MST_MAXNUM,
};
static LPCTSTR	g_szMaster_Static[] =
{
	_T("OFFSET X \n [Pix]"),
	_T("OFFSET Y \n [Pix]"),
 	_T("OFFSET R \n [Degree]"),
	_T("MASTER \n INFO"),
	_T("MASTER \n SPC"),
	_T("MASTER \n OFFSET"),
	NULL
};

enum enMaster_Edit{
	EIT_MST_MasterInfoX = 0,
	EIT_MST_MasterInfoY,
	EIT_MST_MasterInfoR,
	EIT_MST_MasterSpcX,
	EIT_MST_MasterSpcY,
	EIT_MST_MasterSpcR,
	EIT_MST_MasterOffsetX,
	EIT_MST_MasterOffsetY,
	EIT_MST_MasterOffsetR,
	EIT_MST_MAXNUM,
};

class CWnd_MasterSet : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MasterSet)

public:
	CWnd_MasterSet();
	virtual ~CWnd_MasterSet();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedTest();
	afx_msg void	OnBnClickedSave();
	afx_msg void	OnBnClickedExit();
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);

	CFont			m_font;
	CFont			m_font_Default;

	CMFCButton		m_bn_MasterTest;
	CMFCButton		m_bn_MasterSave;
	CMFCButton		m_bn_MasterExit;

	CVGStatic		m_st_UIItem;
	CVGStatic		m_st_Item[STI_MST_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EIT_MST_MAXNUM];
	CVGGroupWnd		m_Group;

	ST_ModelInfo	*m_pstModelInfo;

public:

	void		GetUIData		();
	void		SetUIData		();
	void		SetMasterData	(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);
	void		PermissionMode		(enPermissionMode InspMode);


	void		SetModelInfo(__in ST_ModelInfo* pModelInfo)
	{
		if (pModelInfo == NULL)
			return;

		m_pstModelInfo = pModelInfo;
		SetUIData();
	}

};
#endif // Wnd_CameraView_h__
