﻿#ifndef Wnd_SFROp_h__
#define Wnd_SFROp_h__

#pragma once

#include "List_SFROp.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "CommonFunction.h"

enum enSFR_Button
{
	BTN_SF_SFR_TEST = 0,
	BTN_SF_SFR_TEST_STOP,
	BTN_SF_DEFAULT_SET,
	BTN_SF_SMOOTH,
	BTN_SF_FIELD,
	BTN_SF_EDGE,
	BTN_SF_DISTORTION,
	BTN_SF_MAXNUM,
};

static LPCTSTR	g_szSFR_Button[] =
{
	_T("SFR TEST"),
	_T("SFR STOP"),
	_T("DEFAULT SET"),
	_T("SMOOTH"),
	_T("FIELD"),
	_T("EDGE"),
	_T("DISTORTION"),
	NULL
};

enum enSFR_Static{
	STI_SF_PIXSIZE = 0,
	STI_SF_MAXNUM,
};

static LPCTSTR	g_szSFR_Static[] =
{
	_T("PIXEL SIZE"),
	NULL
};

enum enSFR_Editbox{
	Edt_SF_PIXELSIZE = 0,
	Edt_SF_MAXNUM,
};

//-----------------------------------------------------------------------------
// CWnd_SFROp
//-----------------------------------------------------------------------------
class CWnd_SFROp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_SFROp)

public:
	CWnd_SFROp();
	virtual ~CWnd_SFROp();

protected:

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_SF_MAXNUM];
	CMFCButton		m_bn_Item[BTN_SF_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[Edt_SF_MAXNUM];
	CList_SFROp		m_ListSFROp;
	CComboBox		m_CbDataType;

	ST_ModelInfo	*m_pstModelInfo;

	DECLARE_MESSAGE_MAP()
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnBnClickedBnTest		();
	afx_msg void	OnBnClickedBnTestStop	();
	afx_msg void	OnBnClickedBnDefaultSet	();
	afx_msg void	OnBnClickedBnSmooth		();
	afx_msg void	OnBnClickedBnField		();
	afx_msg void	OnBnClickedBnEdge		();
	afx_msg void	OnBnClickedBnDistortion	();

	BOOL		m_bTest_Flag;

public:

	void	SetUI	();
	void	GetUI	();
	BOOL	IsTest	();

	void	SetStatusEngineerMode	(__in enPermissionMode InspMode);

	void	SetPtr_ModelInfo		(__in ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo	= pstModelInfo;
	}
};


#endif // Wnd_SFROp_h__
