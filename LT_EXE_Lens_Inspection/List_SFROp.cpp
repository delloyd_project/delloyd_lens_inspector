﻿// List_SFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_SFROp.h"

#define ISFROp_ED_CELLEDIT			5001
#define ISFROp_CB_CELLCOMBO_TYPE	5002
#define ISFROp_CB_CELLCOMBO_FONT	5003
// CList_SFROp

IMPLEMENT_DYNAMIC(CList_SFROp, CListCtrl)

CList_SFROp::CList_SFROp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol  = 0;
	m_nEditRow  = 0;
	m_pstSFR	= NULL;
}

CList_SFROp::~CList_SFROp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_SFROp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_SFROp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_SFROp::OnNMDblclk)
	ON_EN_KILLFOCUS(ISFROp_ED_CELLEDIT, &CList_SFROp::OnEnKillFocusESfrCOpellEdit)
	ON_CBN_KILLFOCUS(ISFROp_CB_CELLCOMBO_TYPE, &CList_SFROp::OnEnKillFocusESfrOpellCombo)
	ON_CBN_SELCHANGE(ISFROp_CB_CELLCOMBO_TYPE, &CList_SFROp::OnEnSelectESfrOpellCombo)
	ON_CBN_KILLFOCUS(ISFROp_CB_CELLCOMBO_FONT, &CList_SFROp::OnEnKillFocusESfrFontOpellCombo)
	ON_CBN_SELCHANGE(ISFROp_CB_CELLCOMBO_FONT, &CList_SFROp::OnEnSelectESfrFontOpellCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// CList_SFROp 메시지 처리기입니다.
int CList_SFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	CString str;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER , CRect(0, 0, 0, 0), this, ISFROp_ED_CELLEDIT);

	for (UINT nIdx = 0; nIdx < SfrCombo_MaxCol; nIdx++)
	{
		m_cb_Mode[nIdx].Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, ISFROp_CB_CELLCOMBO_TYPE + nIdx);
		m_cb_Mode[nIdx].ResetContent();
	}

	for (UINT nIdx = 0; nIdx < SFR_Mode_NO; nIdx++)
	{
		str.Format(_T("%s"), g_szSFRMode[nIdx]);
		m_cb_Mode[SfrCombo_Type].AddString(str);
	}

	for (UINT nIdx = 0; nIdx < SFR_Mode_F_Max; nIdx++)
	{
		str.Format(_T("%s"), g_szSFR_FontMode[nIdx]);
		m_cb_Mode[SfrCombo_Font].AddString(str);
	}

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/7 - 22:33
// Desc.		:
//=============================================================================
void CList_SFROp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[SfrOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < SfrOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_SfrCOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < SfrOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_SfrCOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_SfrCOp[SfrOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(SfrOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/7 - 22:34
// Desc.		:
//=============================================================================
BOOL CList_SFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/7 - 22:34
// Desc.		:
//=============================================================================
void CList_SFROp::InitHeader()
{
	for (int nCol = 0; nCol < SfrOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_Sfr[nCol], iListAglin_SfrOp[nCol], iHeaderWidth_SfrCOp[nCol]);

	for (int nCol = 0; nCol < SfrOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_SfrCOp[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/7 - 22:34
// Desc.		:
//=============================================================================
void CList_SFROp::InsertFullData()
{
	if (NULL == m_pstSFR)
		return;
 
 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
 	{
 		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/8/7 - 22:40
// Desc.		:
//=============================================================================
void CList_SFROp::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("S%02d"), nRow + 1);
	SetItemText(nRow, SfrOp_Object, strText);

	strText.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X);
	SetItemText(nRow, SfrOp_X, strText);

	strText.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y);
	SetItemText(nRow, SfrOp_Y, strText);

	strText.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth);
	SetItemText(nRow, SfrOp_W, strText);

	strText.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight);
	SetItemText(nRow, SfrOp_H, strText);

	strText.Format(_T("%s"), g_szSFRMode[m_pstSFR->stSFROp.stSFR_Region[nRow].nType]);
	SetItemText(nRow, SfrOp_TestType, strText);

	strText.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbThreshold);
	SetItemText(nRow, SfrOp_Threshold, strText);

	strText.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbOffset);
	SetItemText(nRow, SfrOp_Offset, strText);

	strText.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbLinePair);
	SetItemText(nRow, SfrOp_Linepare, strText);

	strText.Format(_T("%s"), g_szSFR_FontMode[m_pstSFR->stSFROp.stSFR_Region[nRow].nFont]);
	SetItemText(nRow, SfrOp_Font, strText);

	if (m_pstSFR->stSFROp.stSFR_Region[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

}

//=============================================================================
// Method		: OnNMClick
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/8/8 - 10:27
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == SfrOp_Object)
		{
			UINT nBuffer;

			nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

			if (nBuffer == 0x2000)
			{
				m_pstSFR->stSFROp.stSFR_Region[pNMItemActivate->iItem].bEnable = FALSE;
				SetItemState(pNMItemActivate->iItem, 0x1000, LVIS_STATEIMAGEMASK);
			}

			if (nBuffer == 0x1000)
			{
				m_pstSFR->stSFROp.stSFR_Region[pNMItemActivate->iItem].bEnable = TRUE;
				SetItemState(pNMItemActivate->iItem, 0x2000, LVIS_STATEIMAGEMASK);
			}
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/8/8 - 11:09
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem <SfrOp_MaxCol && pNMItemActivate->iSubItem >0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			ModifyStyle(WS_VSCROLL, 0);

			if (pNMItemActivate->iSubItem == SfrOp_TestType)
			{
				m_cb_Mode[SfrCombo_Type].SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Mode[SfrCombo_Type].SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Mode[SfrCombo_Type].SetFocus();
				m_cb_Mode[SfrCombo_Type].SetCurSel(m_pstSFR->stSFROp.stSFR_Region[pNMItemActivate->iItem].nType);
			}
			else if (pNMItemActivate->iSubItem == SfrOp_Font)
			{
				m_cb_Mode[SfrCombo_Font].SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Mode[SfrCombo_Font].SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Mode[SfrCombo_Font].SetFocus();
				m_cb_Mode[SfrCombo_Font].SetCurSel(m_pstSFR->stSFROp.stSFR_Region[pNMItemActivate->iItem].nFont);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
			
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusESfrCOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/8 - 11:11
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusESfrCOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (0 != strText.Compare(GetItemText(m_nEditRow, m_nEditCol)))
	{		
		if (m_nEditCol == SfrOp_Threshold || m_nEditCol == SfrOp_Offset || m_nEditCol == SfrOp_Linepare)
		{
			UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText));
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
		}
	}

	CRect rc;

	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusESfrOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/8 - 11:11
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusESfrOpellCombo()
{
	SetItemText(m_nEditRow, SfrOp_TestType, g_szSFRMode[m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nType]);
	m_cb_Mode[SfrCombo_Type].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectESfrOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/8 - 11:12
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnSelectESfrOpellCombo()
{
	m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nType = m_cb_Mode[SfrCombo_Type].GetCurSel();

	SetItemText(m_nEditRow, SfrOp_TestType, g_szSFRMode[m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nType]);
	m_cb_Mode[SfrCombo_Type].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusESfrFontOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/8 - 19:29
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusESfrFontOpellCombo()
{
	SetItemText(m_nEditRow, SfrOp_Font, g_szSFR_FontMode[m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nFont]);
	m_cb_Mode[SfrCombo_Font].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectESfrFontOpellCombo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/8 - 19:29
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnSelectESfrFontOpellCombo()
{
	m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nFont = m_cb_Mode[SfrCombo_Font].GetCurSel();

	SetItemText(m_nEditRow, SfrOp_Font, g_szSFR_FontMode[m_pstSFR->stSFROp.stSFR_Region[m_nEditRow].nFont]);
	m_cb_Mode[SfrCombo_Font].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/8/8 - 11:24
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (iValue < 0)
		iValue = 0;

	CString strData;

	switch (nCol)
	{
	case SfrOp_X:
		
		m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X = iValue;
		
		if (CAM_IMAGE_WIDTH <= m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X = CAM_IMAGE_WIDTH;
		}
		else if (m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X < 0)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X = 0;
		}

		m_pstSFR->stSFROp.stSFR_InitRegion[nRow].nPos_X = m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X;

		strData.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_X);
		break;

	case SfrOp_Y:

		m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y = iValue;
		
		if (CAM_IMAGE_HEIGHT <= m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y = CAM_IMAGE_HEIGHT;
		}
		else if (m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y < 0)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y = 0;
		}
		m_pstSFR->stSFROp.stSFR_InitRegion[nRow].nPos_Y = m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y;

		strData.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nPos_Y);
		break;

	case SfrOp_W:

		m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth = iValue;

		if (CAM_IMAGE_WIDTH <= (long)m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth = CAM_IMAGE_WIDTH;
		}
		else if (m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth < 0)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth = 0;
		}
		m_pstSFR->stSFROp.stSFR_InitRegion[nRow].nWidth = m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth;

		strData.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nWidth);
		break;

	case SfrOp_H:

		m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight = iValue;

		if (CAM_IMAGE_HEIGHT <= (long)m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight = CAM_IMAGE_HEIGHT;
		}
		else if (m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight < 0)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight = 0;
		}
		m_pstSFR->stSFROp.stSFR_InitRegion[nRow].nHeight = m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight;

		strData.Format(_T("%d"), m_pstSFR->stSFROp.stSFR_Region[nRow].nHeight);

		break;
	case SfrOp_Offset:
		m_pstSFR->stSFROp.stSFR_Region[nRow].dbOffset = double(iValue);
		strData.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbOffset);
		break;
	case SfrOp_Linepare:
		m_pstSFR->stSFROp.stSFR_Region[nRow].dbLinePair = double(iValue);
		strData.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbLinePair);
		break;

	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strData);

	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/8 - 11:33
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	if (dValue < 0)
		dValue = 0;

	CString strData;

	switch (nCol)
	{
	case SfrOp_Threshold:
		if (m_pstSFR->stSFROp.stSFR_Region[nRow].dbThreshold < 0)
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].dbThreshold = 0;
		}
		else
		{
			m_pstSFR->stSFROp.stSFR_Region[nRow].dbThreshold = dValue;
		}

		strData.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbThreshold);
		break;
	case SfrOp_Offset:
		m_pstSFR->stSFROp.stSFR_Region[nRow].dbOffset = dValue;
		strData.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbOffset);
		break;
	case SfrOp_Linepare:
		m_pstSFR->stSFROp.stSFR_Region[nRow].dbLinePair = dValue;
		strData.Format(_T("%.2f"), m_pstSFR->stSFROp.stSFR_Region[nRow].dbLinePair);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strData);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/8/8 - 11:47
// Desc.		:
//=============================================================================
BOOL CList_SFROp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;


	return TRUE;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/8 - 11:47
// Desc.		:
//=============================================================================
BOOL CList_SFROp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		int casenum = pWndFocus->GetDlgCtrlID();

		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			
			if (SfrOp_Threshold == m_nEditCol)
			{
				dValue = dValue + ((zDelta / 120));
			}
			else
			{
			dValue = dValue + ((zDelta / 120)*0.1);
		}
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + ((zDelta / 120));
			}

			if (0 < dValue)
			{
				if (SfrOp_Threshold == m_nEditCol)
				{
					dValue = dValue + ((zDelta / 120));
				}
				else
				{
				dValue = dValue + ((zDelta / 120)*0.1);
			}
		}
		}

		if (iValue < 0)
		{
			iValue = 0;
		}

		if (iValue >2000)
		{
			iValue = 2000;
		}

		if (dValue < 0.0)
		{
			dValue = 0.0;
		}

		if (dValue > 1000.0)
		{
			dValue = 1000.0;
		}
		if (m_nEditCol == SfrOp_Threshold || m_nEditCol == SfrOp_Offset || m_nEditCol == SfrOp_Linepare)
		{
			UpdateCellData_double(m_nEditRow, m_nEditCol, dValue);
		}
		else
			{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
			}
		}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
