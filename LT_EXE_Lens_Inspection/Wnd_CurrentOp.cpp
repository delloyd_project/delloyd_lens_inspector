﻿// Wnd_CurrentOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CurrentOp.h"

// CWnd_CurrentOp

typedef enum CurrentOp_ID
{
	IDC_BTN_CURRENT		= 1001,
	IDC_LIST_CURRENTOP,
};

IMPLEMENT_DYNAMIC(CWnd_CurrentOp, CWnd)

CWnd_CurrentOp::CWnd_CurrentOp()
{
	m_pstModelInfo = NULL;

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_CurrentOp::~CWnd_CurrentOp()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_CurrentOp, CWnd)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BTN_CURRENT, OnBnClickedBnTest)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_CurrentOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 18:01
// Desc.		:
//=============================================================================
int CWnd_CurrentOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | /*WS_CLIPCHILDREN |*/ WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_bn_Test.Create(_T("TEST"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_CURRENT);
	m_bn_Test.SetFont(&m_font_Data);

	m_ListCurrOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_CURRENTOP);

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_CurrentOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/13 - 14:58
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::OnBnClickedBnTest()
{
	m_bn_Test.EnableWindow(FALSE);

	GetOwner()->SendMessage(WM_MANUAL_TEST, 0, MT_Cmd_Current);

	m_bn_Test.EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 14:59
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Current, 0);
	else
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iBtnWidth  = iWidth / 7;
	int iBtnHeight = 26;

	int Static_W = iWidth / 4;
	int Combo_W = iWidth / 3;

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Test.MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iLeft = iMargin;

	int List_W = (Static_W + Combo_W + iSpacing);
	int List_H = iHeight / 2;

	m_ListCurrOp.MoveWindow(iLeft, iTop, List_W, List_H);
}

//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::SetUI()
{
	if (m_pstModelInfo == NULL)
		return;

	m_ListCurrOp.SetPtr_Current(&m_pstModelInfo->stCurrent);
	m_ListCurrOp.InsertFullData();
}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::GetUI()
{
	m_ListCurrOp.GetCellData();
}
