﻿// List_CenterPointOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_CenterPointOp.h"

#define IRtOp_ED_CELLEDIT		5001

// CList_CenterPointOp

IMPLEMENT_DYNAMIC(CList_CenterPointOp, CListCtrl)

CList_CenterPointOp::CList_CenterPointOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstCenterPoint = NULL;
	m_nTestMode = CP_Measurement_TestMode;
}

CList_CenterPointOp::~CList_CenterPointOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_CenterPointOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_CenterPointOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_CenterPointOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_CenterPointOp::OnEnKillFocusECpOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_CenterPointOp 메시지 처리기입니다.
int CList_CenterPointOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[CpOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_CpOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_CpOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_CpOp[CpOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(CpOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::InitHeader()
{
	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CpOp[nCol], iListAglin_CpOp[nCol], iHeaderWidth_CpOp[nCol]);
	}

	for (int nCol = 0; nCol < CpOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_CpOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_CenterPointOp::InsertFullData()
{
	if (m_pstCenterPoint == NULL)
		return;

 	DeleteAllItems();
 
	m_stCenterPoint.stCenterPointOp.nStandard_X = m_pstCenterPoint->stCenterPointOp.nStandard_X;
	m_stCenterPoint.stCenterPointOp.nStandard_Y = m_pstCenterPoint->stCenterPointOp.nStandard_Y;

	m_stCenterPoint.stCenterPointOp.nStandard_OffsetX = m_pstCenterPoint->stCenterPointOp.nStandard_OffsetX;
	m_stCenterPoint.stCenterPointOp.nStandard_OffsetY = m_pstCenterPoint->stCenterPointOp.nStandard_OffsetY;

	m_stCenterPoint.stCenterPointOp.nTarget_OffsetX = m_pstCenterPoint->stCenterPointOp.nTarget_OffsetX;
	m_stCenterPoint.stCenterPointOp.nTarget_OffsetY = m_pstCenterPoint->stCenterPointOp.nTarget_OffsetY;

	m_stCenterPoint.stCenterPointOp.dApplied_RatioX = m_pstCenterPoint->stCenterPointOp.dApplied_RatioX;
	m_stCenterPoint.stCenterPointOp.dApplied_RatioY = m_pstCenterPoint->stCenterPointOp.dApplied_RatioY;

	m_stCenterPoint.stCenterPointOp.nMax_PixX = m_pstCenterPoint->stCenterPointOp.nMax_PixX;
	m_stCenterPoint.stCenterPointOp.nMax_PixY = m_pstCenterPoint->stCenterPointOp.nMax_PixY;

	for (UINT nIdx = 0; nIdx < CpOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_CenterPointOp::SetRectRow(UINT nRow)
{
	if (m_nTestMode == CP_Measurement_TestMode)
	{
		if (nRow == CpOp_TargetOffset || nRow == CpOp_MaxOffset)
			return;
	}

	CString strText;
	strText.Format(_T("%s"), g_lpszItem_CpOp[nRow]);
	SetItemText(nRow, CpOp_Object, strText);

	switch (nRow)
	{
	case CpOp_Standard:
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nStandard_X);
		SetItemText(nRow, CpOp_X, strText);
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nStandard_Y);
		SetItemText(nRow, CpOp_Y, strText);
		break;
	case CpOp_StandardOffset:
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nStandard_OffsetX);
		SetItemText(nRow, CpOp_X, strText);
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nStandard_OffsetY);
		SetItemText(nRow, CpOp_Y, strText);
		break;
	case CpOp_TargetOffset:
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nTarget_OffsetX);
		SetItemText(nRow, CpOp_X, strText);
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nTarget_OffsetY);
		SetItemText(nRow, CpOp_Y, strText);
		break;
	case CpOp_MaxOffset:
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nMax_PixX);
		SetItemText(nRow, CpOp_X, strText);
		strText.Format(_T("%d"), m_stCenterPoint.stCenterPointOp.nMax_PixY);
		SetItemText(nRow, CpOp_Y, strText);
		break;
	default:
		break;
	}

 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < CpOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECpOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_CenterPointOp::OnEnKillFocusECpOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText)))
	{
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	switch (nRow)
	{
	case CpOp_Standard:
		if (CpOp_X == nCol)
			m_stCenterPoint.stCenterPointOp.nStandard_X =iValue;
		else
			m_stCenterPoint.stCenterPointOp.nStandard_Y = iValue;

		break;
	case CpOp_StandardOffset:
		if (CpOp_X == nCol)
			m_stCenterPoint.stCenterPointOp.nStandard_OffsetX = iValue;
		else
			m_stCenterPoint.stCenterPointOp.nStandard_OffsetY = iValue;
		break;
	case CpOp_TargetOffset:
		if (CpOp_X == nCol)
			m_stCenterPoint.stCenterPointOp.nTarget_OffsetX = iValue;
		else
			m_stCenterPoint.stCenterPointOp.nTarget_OffsetY = iValue;
		break;
	case CpOp_MaxOffset:
		if (CpOp_X == nCol)
			m_stCenterPoint.stCenterPointOp.nMax_PixX = iValue;
		else
			m_stCenterPoint.stCenterPointOp.nMax_PixY = iValue;
		break;
	default:
		break;
	}
 
	CString str;
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.1f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}
//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CenterPointOp::GetCellData()
{
	if (m_pstCenterPoint == NULL)
		return;

	m_pstCenterPoint->stCenterPointOp.nStandard_X = m_stCenterPoint.stCenterPointOp.nStandard_X;
	m_pstCenterPoint->stCenterPointOp.nStandard_Y = m_stCenterPoint.stCenterPointOp.nStandard_Y;

	m_pstCenterPoint->stCenterPointOp.nStandard_OffsetX = m_stCenterPoint.stCenterPointOp.nStandard_OffsetX;
	m_pstCenterPoint->stCenterPointOp.nStandard_OffsetY = m_stCenterPoint.stCenterPointOp.nStandard_OffsetY;

	m_pstCenterPoint->stCenterPointOp.nTarget_OffsetX = m_stCenterPoint.stCenterPointOp.nTarget_OffsetX;
	m_pstCenterPoint->stCenterPointOp.nTarget_OffsetY = m_stCenterPoint.stCenterPointOp.nTarget_OffsetY;

	m_pstCenterPoint->stCenterPointOp.dApplied_RatioX = m_stCenterPoint.stCenterPointOp.dApplied_RatioX;
	m_pstCenterPoint->stCenterPointOp.dApplied_RatioY = m_stCenterPoint.stCenterPointOp.dApplied_RatioY;

	m_pstCenterPoint->stCenterPointOp.nMax_PixX = m_stCenterPoint.stCenterPointOp.nMax_PixX;
	m_pstCenterPoint->stCenterPointOp.nMax_PixY = m_stCenterPoint.stCenterPointOp.nMax_PixY;

}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CenterPointOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dValue = dValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dValue)
				dValue = dValue + ((zDelta / 120)*0.1);
			}

		if (iValue < 0)
			iValue = 0;

		if (iValue >1000)
			iValue = 1000;

		if (dValue < 0.0)
			dValue = 0.0;

		if (dValue > 2.0)
			dValue = 2.0;
		
		if (UpdateCellData(m_nEditRow, m_nEditCol, iValue))
		{
			strText.Format(_T("%d"), iValue);
			m_ed_CellEdit.SetWindowText(strText);
		}
	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
