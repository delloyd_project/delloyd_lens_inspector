﻿//*****************************************************************************
// Filename	: 	Wnd_LotInfo.cpp
// Created	:	2016/7/7 - 16:22
// Modified	:	2016/7/7 - 16:22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_LotInfo.h"

IMPLEMENT_DYNAMIC(CWnd_LotInfo, CWnd)

CWnd_LotInfo::CWnd_LotInfo()
{
}


CWnd_LotInfo::~CWnd_LotInfo()
{
}

BEGIN_MESSAGE_MAP(CWnd_LotInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
int CWnd_LotInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	if (!m_grid_LotInfo.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 11))
	{
		TRACE("m_grid_LotInfo 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_grid_Yield.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 12))
	{
		TRACE("m_grid_Yield 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_Group.SetTitle(L"LOT INFO");
	if (!m_Group.Create(_T("LOT INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}


	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
void CWnd_LotInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 10;

	int iLeft = iSpacing;
	int iTop = 26;
	int iWidth = cx - iSpacing - iSpacing;
	int iHeight = (cy - iTop - iSpacing - 5) / 2;

	m_Group.MoveWindow(0, 0, cx, cy);

	m_grid_LotInfo.MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iHeight + 5;
	m_grid_Yield.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/7/14 - 15:15
// Desc.		:
//=============================================================================
BOOL CWnd_LotInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2016/7/14 - 16:08
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetInspectionMode(__in enPermissionMode InspMode)
{
}

//=============================================================================
// Method		: SetLotInifo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LOTInfo * pstLotInfo
// Qualifier	:
// Last Update	: 2016/7/22 - 11:31
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetLotInifo(__in const ST_LOTInfo* pstLotInfo)
{
	m_grid_LotInfo.SetLotInifo(pstLotInfo);
}
void CWnd_LotInfo::SetMESInifo(__in const ST_MES_TotalResult* pstMESInfo)
{
	m_grid_MESInfo.SetMESInifo(pstMESInfo);
}

//=============================================================================
// Method		: SetYield
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Yield * pstYield
// Qualifier	:
// Last Update	: 2016/7/22 - 11:47
// Desc.		:
//=============================================================================
void CWnd_LotInfo::SetYield(__in const ST_Yield* pstYield)
{
	m_grid_Yield.SetYield(pstYield);
}
