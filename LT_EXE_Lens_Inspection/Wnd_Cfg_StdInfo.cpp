﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_StdInfo.cpp
// Created	:	2017/1/3 - 10:39
// Modified	:	2017/1/3 - 10:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_Cfg_StdInfo.h"


IMPLEMENT_DYNAMIC(CWnd_Cfg_StdInfo, CWnd_BaseView)

CWnd_Cfg_StdInfo::CWnd_Cfg_StdInfo()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pstDevice		= NULL;
	m_pstModelInfo  = NULL;
}

CWnd_Cfg_StdInfo::~CWnd_Cfg_StdInfo()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_StdInfo, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Cfg_StdInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_StdInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_TOP);

// 	m_wnd_CurrentOp.SetOwner(GetOwner());
// 	m_wnd_CurrentOp.Create(NULL, _T("Current"), dwStyle, rectDummy, &m_tc_Option, 100);

	m_wnd_CenterPointOp.SetOwner(GetOwner());
	m_wnd_CenterPointOp.Create(NULL, _T("CenterPoint"), dwStyle, rectDummy, &m_tc_Option, 110);

	m_wnd_EIAJOp.SetOwner(GetOwner());
	m_wnd_EIAJOp.Create(NULL, _T("EIAJ"), dwStyle, rectDummy, &m_tc_Option, 120);

// 	m_wnd_SFROp.SetOwner(GetOwner());
// 	m_wnd_SFROp.Create(NULL, _T("SFR"), dwStyle, rectDummy, &m_tc_Option, 130);

// 	m_wnd_RotateOp.SetOwner(GetOwner()); 
// 	m_wnd_RotateOp.Create(NULL, _T("Rotate"), dwStyle, rectDummy, &m_tc_Option, 140);
// 
// 	m_wnd_ParticleOp.SetOwner(GetOwner());
// 	m_wnd_ParticleOp.Create(NULL, _T("Particle"), dwStyle, rectDummy, &m_tc_Option, 150);

// 	m_wnd_LEDOp.SetOwner(GetOwner());
// 	m_wnd_LEDOp.Create(NULL, _T("LED"), dwStyle, rectDummy, &m_tc_Option, 160);

//	m_tc_Option.AddTab(&m_wnd_CurrentOp,	 _T("Current"),			LT_Current,		FALSE);
	m_tc_Option.AddTab(&m_wnd_CenterPointOp, _T("CenterPoint"),		LT_CenterPoint, FALSE);
	m_tc_Option.AddTab(&m_wnd_EIAJOp,		 _T("Focus ( EIAJ )"),	LT_EIAJ,		FALSE);
//	m_tc_Option.AddTab(&m_wnd_SFROp,		 _T("Focus ( SFR )"),	LT_SFR,			FALSE);
// 	m_tc_Option.AddTab(&m_wnd_RotateOp,		 _T("Rotate"),			LT_Rotate,		FALSE);
// 	m_tc_Option.AddTab(&m_wnd_ParticleOp,	 _T("Particle"),		LT_Particle,	FALSE);
	//m_tc_Option.AddTab(&m_wnd_LEDOp,		 _T("LED"),				LT_LED,			FALSE);

//	m_tc_Option.ShowTab(LT_Current,		m_stTestItem.bCurrent,		TRUE, TRUE);
	m_tc_Option.ShowTab(LT_CenterPoint, m_stTestItem.bCenterPoint,	TRUE, TRUE);
	m_tc_Option.ShowTab(LT_EIAJ,		m_stTestItem.bEIAJ,			TRUE, TRUE);
//	m_tc_Option.ShowTab(LT_SFR,			m_stTestItem.bSFR,			TRUE, TRUE);
// 	m_tc_Option.ShowTab(LT_Rotate,		m_stTestItem.bRotate,		TRUE, TRUE);
// 	m_tc_Option.ShowTab(LT_Particle,	m_stTestItem.bParticle,		TRUE, TRUE);
	//m_tc_Option.ShowTab(LT_LED,			m_stTestItem.bLED,			TRUE, TRUE);
	
	m_tc_Option.EnableTabSwap(FALSE);
	m_tc_Option.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_StdInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_StdInfo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

void CWnd_Cfg_StdInfo::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		m_tc_Option.SetActiveTab(0);
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_CenterAdjust, 0);
	}
	else
	{
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
	}
}

//=============================================================================
// Method		: IsTestAll
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:41
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_StdInfo::IsTestAll()
{
	if (m_wnd_CenterPointOp.IsTest() == TRUE || m_wnd_EIAJOp.IsTest() == TRUE)
		return TRUE;

	return FALSE;
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/7/13 - 17:41
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::SetModelInfo(ST_ModelInfo* pstModelInfo)
{
	if (pstModelInfo == NULL)
		return;

	m_pstModelInfo = pstModelInfo;

// 	m_wnd_CurrentOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_CurrentOp.SetUI();

	m_wnd_CenterPointOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_CenterPointOp.SetUI();

	m_wnd_EIAJOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_EIAJOp.SetUI();

// 	m_wnd_SFROp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_SFROp.SetUI();

// 	m_wnd_RotateOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_RotateOp.SetUI();
// 
// 	m_wnd_ParticleOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_ParticleOp.SetUI();

// 	m_wnd_LEDOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_LEDOp.SetUI();
}

void CWnd_Cfg_StdInfo::SetImageMode(ST_ImageMode* pstImageMode)
{
	if (pstImageMode == NULL)
		return;

	m_pstImageMode = pstImageMode;

	m_wnd_CenterPointOp.SetPtr_ImageMode(m_pstImageMode);
	m_wnd_EIAJOp.SetPtr_ImageMode(pstImageMode);
}



//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/13 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::GetModelInfo()
{
//	m_wnd_RotateOp.GetUI();
//	m_wnd_CurrentOp.GetUI();
	//m_wnd_LEDOp.GetUI();
	m_wnd_CenterPointOp.GetUI();
	m_wnd_EIAJOp.GetUI();
//	m_wnd_SFROp.GetUI();
//	m_wnd_ParticleOp.GetUI();
}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/7/7 - 11:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
	m_wnd_EIAJOp.SetStatusEngineerMode(InspMode);

	if (InspMode == Permission_Engineer)
	{	
// 		for (UINT nIdx = 0; nIdx < LT_PatternNoise; nIdx++)
// 		{
// 			m_tc_Option.ShowTab(nIdx, SW_SHOW, TRUE, TRUE);
// 		}
//		m_tc_Option.ShowTab(LT_Current,		m_stTestItem.bCurrent,		TRUE, TRUE);
		m_tc_Option.ShowTab(LT_CenterPoint, m_stTestItem.bCenterPoint,	TRUE, TRUE);
		m_tc_Option.ShowTab(LT_EIAJ,		m_stTestItem.bEIAJ,			TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_SFR,			m_stTestItem.bSFR,			TRUE, TRUE);
// 		m_tc_Option.ShowTab(LT_Rotate,		m_stTestItem.bRotate,		TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_Particle,	m_stTestItem.bParticle,		TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_LED,			m_stTestItem.bLED,			TRUE, TRUE);
	}
	else
	{
//		m_tc_Option.ShowTab(LT_Current,		m_stTestItem.bCurrent,		TRUE, TRUE);
		m_tc_Option.ShowTab(LT_CenterPoint, m_stTestItem.bCenterPoint,	TRUE, TRUE);
		m_tc_Option.ShowTab(LT_EIAJ,		m_stTestItem.bEIAJ,			TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_SFR,			m_stTestItem.bSFR,			TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_Rotate,		m_stTestItem.bRotate,		TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_Particle,	m_stTestItem.bParticle,		TRUE, TRUE);
//		m_tc_Option.ShowTab(LT_LED,			m_stTestItem.bLED,			TRUE, TRUE);
	}
}
