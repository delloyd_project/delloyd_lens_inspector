﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.cpp
// Created	: 2016/03/18
// Modified	: 2016/03/18
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_RecipeView.h"
#include "Dlg_ChkPassword.h"

#define		IDC_STATIC_VIDEO		1000
#define		IDC_STATIC_VVIDEO		1000
#define		IDC_ED_OUPUTVOLT		1050
#define		IDC_BN_NEW				1001
#define		IDC_BN_SAVE				1002
#define		IDC_BN_SAVE_AS			1003
#define		IDC_BN_LOAD				1004
#define		IDC_BN_REFRESH			1005
#define		IDC_LSB_MODEL			1006
#define		IDC_RB_TEST_OPT			1021
#define		IDC_RB_MOTOR_OPT		1022
#define		IDC_BN_POGOCNT			1011
#define		IDC_BN_MOTION			1012
#define		IDC_TB_TESTITEM			1013

//=============================================================================
// CWnd_RecipeView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_RecipeView, CWnd_BaseView)

//=============================================================================
//
//=============================================================================
CWnd_RecipeView::CWnd_RecipeView()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		26,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pDevice		= NULL;
	m_nPicViewMode = PIC_Standby;
	m_stModelInfo.nPicViewMode = PIC_Standby;
	//m_iTabDaqIdx_Eng = 0;
	m_dDisplaceData = 0;
	m_dVColletData = 0;
	
	for (UINT nIdx = 0; nIdx < Man_Cmd_Total; nIdx++)
		m_bFlag_Test[nIdx] = FALSE;

	SetFullPath	(_T(""));

	m_LoadImage = NULL;
}

CWnd_RecipeView::~CWnd_RecipeView()
{
	TRACE(_T("<<< Start ~CWnd_RecipeView >>> \n"));
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_RecipeView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED		(IDC_BN_NEW,		OnBnClickedBnNew)
	ON_BN_CLICKED		(IDC_BN_SAVE,		OnBnClickedBnSave)
	ON_BN_CLICKED		(IDC_BN_SAVE_AS,	OnBnClickedBnSaveAs)
	ON_BN_CLICKED		(IDC_BN_LOAD,		OnBnClickedBnLoad)
	ON_BN_CLICKED		(IDC_BN_REFRESH,	OnBnClickedBnRefresh)
	ON_LBN_SELCHANGE	(IDC_LSB_MODEL,		OnLbnSelChangeModel)
	ON_MESSAGE			(WM_FILE_MODEL,		OnFileModel)
	ON_MESSAGE			(WM_CHANGED_MODEL,	OnChangeModel)
	ON_MESSAGE			(WM_REFESH_MODEL,	OnRefreshModelList)
	ON_MESSAGE			(WM_MANUAL_TEST,	OnManualTestCmd)
	ON_MESSAGE			(WM_MANUAL_TEST_IMAGESAVE,	OnManualTestCmd_ImageSave)
	ON_MESSAGE			(WM_EDGE_ONOFF,		OnEdgeOnOff)
	ON_MESSAGE			(WM_DISTORTION_CORRECT,		OnDistortionCorr)
	ON_MESSAGE			(WM_MANUAL_CONTROL, OnManualCtrlCmd)
	ON_MESSAGE			(WM_TAB_CHANGE_PIC,	OnTabChangePic)
	ON_MESSAGE			(WM_ALIGN,			OnAlignPic)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// CWnd_RecipeView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_RecipeView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
int CWnd_RecipeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wnd_ImgProc.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_VIDEO);
	m_wnd_VVideo.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC_VVIDEO);

	m_st_BackColor.SetBackColor(RGB(160, 160, 160));
	m_st_BackColor.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 11.0F);

	m_st_Location.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Location.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Location.SetFont_Gdip(L"Arial", 11.0F);

// 	m_st_VCam_T.SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
// 	m_st_VCam_T.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
// 	m_st_VCam_T.SetFont_Gdip(L"Arial", 11.0F);
// 
// 	m_st_VCam_V.SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_VCam_V.SetColorStyle(CVGStatic::ColorStyle_White);
// 	m_st_VCam_V.SetFont_Gdip(L"Arial", 11.0F);

	m_st_File.Create(_T("Model Files"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Location.Create(_T("Location"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

//	m_st_VCam_T.Create(_T("Vision Camera"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
//	m_st_VCam_V.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	
	m_lst_ModelList.Create(dwStyle | WS_HSCROLL | LBS_STANDARD | LBS_SORT | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, IDC_LSB_MODEL);
	m_bn_Refresh.Create(_T("Refresh File List"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_REFRESH);

	m_bn_New.Create(_T("New"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NEW);
	m_bn_New.SetFont(&m_font_Default);

	m_bn_Save.Create(_T("Save (Apply)"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	m_bn_Save.SetFont(&m_font_Default);

	m_bn_SaveAs.Create(_T("Save As"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE_AS);
	m_bn_SaveAs.SetFont(&m_font_Default);

	m_bn_Load.Create(_T("Load"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LOAD);
	m_bn_Load.SetFont(&m_font_Default);

	// 탭 컨트롤
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, IDC_TB_TESTITEM, CMFCTabCtrl::LOCATION_TOP);
 	
 	m_wnd_ModelCfg.SetOwner(this);
 	m_wnd_ModelCfg.Create(NULL, _T("Model"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 10);
 	
 	m_wnd_TestItemCfg.SetOwner(GetOwner());
 	m_wnd_TestItemCfg.Create(NULL, _T("Test Item"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 11);

	m_wnd_StdInfoCfg.SetOwner(this);
 	m_wnd_StdInfoCfg.Create(NULL, _T("Standard Info"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 12);

	m_wnd_PogoCfg.SetOwner(GetOwner());
	m_wnd_PogoCfg.Create(NULL, _T("Pogo"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 13);

// 	m_wnd_VisionCfg.SetOwner(GetOwner());
// 	m_wnd_VisionCfg.SetPtr_Device(m_pDevice);
// 	m_wnd_VisionCfg.Create(NULL, _T("Vision"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 14);

// 	m_wnd_LightCtrlCfg.SetOwner(GetOwner());
// 	m_wnd_LightCtrlCfg.Create(NULL, _T("Light"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 15);

//	m_wnd_DAQCfg.SetOwner(GetOwner());
//	m_wnd_DAQCfg.Create(NULL, _T("DAQ"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 16);

// 	m_wnd_AFCfg.SetOwner(GetOwner());
// 	m_wnd_AFCfg.Create(NULL, _T("AF Info"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 17);

// 	m_wnd_MotionTeachCfg.SetOwner(GetOwner());
// 	m_wnd_MotionTeachCfg.SetPtr_Device(m_pDevice);
// 	m_wnd_MotionTeachCfg.Create(NULL, _T("Motion Teach"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, 18);

	int TabCnt = 0;
  	m_tc_Option.AddTab(&m_wnd_ModelCfg,			_T("Model"),			TabCnt,		FALSE);
	m_tc_Option.AddTab(&m_wnd_TestItemCfg,		_T("Test Item"),		TabCnt++,	 FALSE);
 	m_tc_Option.AddTab(&m_wnd_StdInfoCfg,		_T("Test Info"),		TabCnt++,	 FALSE);
  	m_tc_Option.AddTab(&m_wnd_PogoCfg,			_T("Pogo"),				TabCnt++,	 FALSE);
//  m_tc_Option.AddTab(&m_wnd_VisionCfg,		_T("Vision"),			TabCnt++,	 FALSE);
// 	m_tc_Option.AddTab(&m_wnd_AFCfg,			_T("AF Info"),			TabCnt++,	 FALSE);
// 	m_tc_Option.AddTab(&m_wnd_MotionTeachCfg,	_T("Motion Teach"),		TabCnt++,	 FALSE);

#ifdef USE_LIGHTBRD_MODE
  	m_tc_Option.AddTab(&m_wnd_LightCtrlCfg,	_T("Light"),			TabCnt++, FALSE);

	if (g_InspectorTable[SET_INSPECTOR].nLightBrdCount <= 0)
	{
		m_tc_Option.ShowTab(4, FALSE, TRUE, TRUE);
	}
#endif

//  	m_tc_Option.AddTab(&m_wnd_DAQCfg,		_T("LVDS"),				TabCnt++, FALSE);
	//m_iTabDaqIdx_Eng = TabCnt;

	m_tc_Option.EnableTabSwap(FALSE);


//	m_tc_Option.ShowTab(m_iTabDaqIdx_Eng, FALSE, TRUE, TRUE);

	m_tc_Option.SetActiveTab(0);
 
 	m_lst_ModelList.SetFont(&m_font_Default);

 	m_TIProcessing.SetPtr_Device(m_pDevice);
	m_TIProcessing.SetImagehwnd(m_wnd_ImgProc.m_hWnd);

  	m_wnd_StdInfoCfg.SetPtr_Device(m_pDevice);
//	m_wnd_LightCtrlCfg.SetPtr_Device(m_pDevice);
 
 	// 파일 감시 쓰레드 설정
 	m_ModelWatch.SetOwner(GetSafeHwnd(), WM_REFESH_MODEL);
 	if (!m_strModelPath.IsEmpty())
 	{
 		m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
 		m_ModelWatch.BeginWatchThrFunc();
 
 		m_ModelWatch.RefreshList();
 
 		RefreshModelFileList(m_ModelWatch.GetFileList());
 	}

 	CStringW strText;
 	strText = _T("MANUAL CONTROL");
 	m_wnd_ManualCtrl.SetBackgroundColor(Gdiplus::Color::White);
 	m_wnd_ManualCtrl.SetTitle(strText.GetBuffer());
 	strText.ReleaseBuffer();
 
 	m_wnd_ManualCtrl.Create(_T(""), dwStyle, rectDummy, this, 100);
	m_wnd_ManualCtrl.SetOwner(this);


	ChangeImageMode(ImageMode_LiveCam);

	return 0;
}

//=============================================================================
// Method		: CWnd_RecipeView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iLeftMargin = 7;
	int iRightMargin = 7;
	int iTopMargin = 7;
	int iBottomMargin = 7;
	int iSpacing = 5;
	int iCateSpacing = 10;
	int iCateHeight = 31;
	int iBtnHeight = 50;
	int iCamHeight = 175;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;
	int iBottom = cy - iBottomMargin;
	int iWidth = (int)(cx * 0.75) - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;
	int iHeightVideo = (int)(cy * 0.65) - iTopMargin - iBottomMargin;

	// 영상 (720 x 480)
	int iVideoWidth = m_stModelInfo.dwWidth; // VIDEO_WIDTH;//(iWidth - iSpacing) * 3 / 5;
	int iVideoHeight = m_stModelInfo.dwHeight; // VIDEO_HEIGHT;//VIDEO_HEIGHT * iVideoWidth / VIDEO_WIDTH;

	int iWidthTemp = iVideoWidth;
	int iHeightTemp = iVideoHeight;

	if (iWidthTemp > iVideoWidth)
		iWidthTemp = iVideoWidth;
	else
		iWidthTemp = iWidth;

	iHeightTemp = iVideoHeight * iWidthTemp / iVideoWidth;

	if (iHeightTemp > iHeightVideo)
	{
		iHeightTemp = iHeightVideo;
		iWidthTemp = iVideoWidth * iHeightTemp / iVideoHeight;
	}

	iWidthTemp = 720;
	iHeightTemp = 480;

	m_st_BackColor.MoveWindow(iLeft, iTop, iWidth, iHeightVideo);
	m_wnd_ImgProc.MoveWindow(iLeft + (abs(iWidthTemp - iWidth) / 2), iTop + (abs(iHeightTemp - iHeightVideo) / 2), iWidthTemp, iHeightTemp);

	// 파일 리스트
	iLeft += iWidth + iLeftMargin;
	int iCateWidth = cx - iLeft - iLeftMargin;

//	m_st_VCam_T.MoveWindow(iLeft, iTop, iCateWidth, iCateHeight);

//	iTop += iCateHeight;
//	m_wnd_VVideo.MoveWindow(iLeft, iTop, iCateWidth, iCamHeight);

//	iTop += iCamHeight + iCateSpacing;
	m_st_Location.MoveWindow(iLeft, iTop, iCateWidth, iCateHeight);
 
 	// 메뉴
	//iTop = iCateHeight + iCateSpacing + 2;
	iTop += iCateHeight + iSpacing;
	iCateWidth = (iCateWidth - iLeftMargin) / 2 + 1;
	m_bn_New.MoveWindow(iLeft, iTop, iCateWidth, iBtnHeight);
 
	int iLeftTemp = iLeft + iLeftMargin + iCateWidth;
	m_bn_SaveAs.MoveWindow(iLeftTemp, iTop, iCateWidth, iBtnHeight);
 
	iTop += iBtnHeight + iSpacing;
	m_bn_Save.MoveWindow(iLeft, iTop, iCateWidth, iBtnHeight);
 
	iLeftTemp = iLeft + iLeftMargin + iCateWidth;
	m_bn_Load.MoveWindow(iLeftTemp, iTop, iCateWidth, iBtnHeight);
 
	iTop += iBtnHeight + iCateSpacing;
	m_st_File.MoveWindow(iLeft, iTop, iCateWidth * 2 + iLeftMargin, iCateHeight);
 
	iTop += iCateHeight + 1;
 	int iHeightTemp2 = iHeightVideo - iTop - iCateHeight + iTopMargin;
	m_lst_ModelList.MoveWindow(iLeft, iTop, iCateWidth * 2 + iLeftMargin, iHeightTemp2);
 
	iTop += iHeightTemp2;
	m_bn_Refresh.MoveWindow(iLeft, iTop, iCateWidth * 2 + iLeftMargin, iCateHeight);
 
// 	// 탭 컨트롤
	iLeft = iLeftMargin;
	iTop += iCateHeight + iSpacing;
	iHeight = cy - iSpacing - iTop;
	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);

	iLeft += iWidth + iLeftMargin;
	iCateWidth = cx - iLeft - iLeftMargin;
	m_wnd_ManualCtrl.MoveWindow(iLeft, iTop, iCateWidth, iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/5/28 - 22:31
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	m_nPicViewMode = PIC_Standby;
}

//=============================================================================
// Method		: CWnd_RecipeView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 13:10
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnNew()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Work in progress.\r\nPlease try after the operation is complete."));
		return;
	}
	
	New_Model();
}

void CWnd_RecipeView::OnBnClickedBnSave()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Work in progress.\r\nPlease try after the operation is complete."));
		return;
	}

	Save_Model();
}

void CWnd_RecipeView::OnBnClickedBnSaveAs()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Work in progress.\r\nPlease try after the operation is complete."));
		return;
	}

	SaveAs_Model();
}

void CWnd_RecipeView::OnBnClickedBnLoad()
{
	if (IsRecipeTest())
	{
		MessageView(_T("Work in progress.\r\nPlease try after the operation is complete."));
		return;
	}
	
	Load_Model();
}

void CWnd_RecipeView::OnBnClickedBnRefresh()
{
	RefreshModelFileList(m_ModelWatch.GetFileList());
}

//=============================================================================
// Method		: OnLbnSelChangeModel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/27 - 12:54
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnLbnSelChangeModel()
{
	CString strValue;
	int iSel = -1;
	if (0 <= (iSel = m_lst_ModelList.GetCurSel()))
	{		
		m_lst_ModelList.GetText(iSel, strValue);

		// 현재 편집중인 모델파일과 같으면 리턴
		if (0 == strValue.Compare(m_stModelInfo.szModelFile))
			return;

		m_stModelInfo.szModelFile = strValue;

		CString strFullPath;

		strFullPath.Format(_T("%s%s.%s"), m_strModelPath, m_stModelInfo.szModelFile, MODEL_FILE_EXT);

		// 파일 불러오기
 		if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
 		{
 			SetFullPath(m_stModelInfo.szModelFile);
 
 			// UI에 세팅
 			SetModelInfo();
 		}

		// 모델 데이터 불러오기
		//GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelName.GetBuffer(), 0);
		//m_stModelInfo.szModelName.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: OnFileModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnFileModel(WPARAM wParam, LPARAM lParam)
{
	UINT nType = (UINT)wParam;

	switch (nType)
	{
	case ID_FILE_NEW:
		New_Model();
		break;

	case ID_FILE_SAVE:
		Save_Model();
		break;

	case ID_FILE_SAVE_AS:
		SaveAs_Model();
		break;

	case ID_FILE_OPEN:
		Load_Model();
		break;

	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: OnChangeModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 14:16
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnChangeModel(WPARAM wParam, LPARAM lParam)
{
	CString strModel = (LPCTSTR)wParam;
	CString strFullPath;

	strFullPath.Format(_T("%s%s.%s"), m_strModelPath, strModel, MODEL_FILE_EXT);

	// 파일 불러오기
	if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
	{
		//m_stModelInfo.szModelName = strModel;

		// UI에 세팅
		SetModelInfo();
	}

	return 0;
}

//=============================================================================
// Method		: OnRefreshModelList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 15:18
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnRefreshModelList(WPARAM wParam, LPARAM lParam)
{
	RefreshModelFileList(m_ModelWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: OnChangeTab
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/13 - 17:05
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnTabChangePic(WPARAM wParam, LPARAM lParam)
{
	m_stModelInfo.nPicViewMode = (UINT)wParam;
	return 0;
}

//=============================================================================
// Method		: OnAlignPic
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/9/19 - 14:53
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnAlignPic(WPARAM wParam, LPARAM lParam)
{
	m_stModelInfo.nPicViewMode  = (UINT)PIC_Align;
	m_stModelInfo.nAlign		= (UINT)wParam;
	return 0;
}

//=============================================================================
// Method		: OnManualTestCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/7/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnManualTestCmd(WPARAM wParam, LPARAM lParam)
{
	m_bFlag_Test[Man_Cmd_Power_On] = TRUE;
	switch (lParam)
	{
	case MT_Cmd_Current:
		m_TIProcessing.CameraCurrent();
		break;
	case MT_Cmd_CenterPoint:
		if (wParam == CP_Measurement_TestMode /*&& m_stModelInfo.bCenterPoint_Ck==TRUE*/)
		m_TIProcessing.CenterPointRun();

// 		if (wParam == CP_Fine_tune_TestMode)
// 			m_TIProcessing.CenterPointTuningRun();
		break;
	case MT_Cmd_EIAJ:
		m_TIProcessing.EIAJRun(FALSE);
		break;
	case MT_Cmd_SFR:
		m_TIProcessing.SFRRun();
		break;
	case MT_Cmd_Rotate:
		if (wParam == RT_Fine_tune_TestMode)
			m_TIProcessing.RotateTuningRun();

		if (wParam == RT_Measurement_TestMode)
		m_TIProcessing.RotateRun();

		break;
	case MT_Cmd_Particle:
		m_TIProcessing.ParticleRun();
		break;

	default:
		break;
	}

	m_bFlag_Test[Man_Cmd_Power_On] = FALSE;
	
	return TRUE;
}

//  [1/17/2019 Admin]
LRESULT CWnd_RecipeView::OnManualTestCmd_ImageSave(WPARAM wParam, LPARAM lParam)
{

	switch (lParam)
	{
	case MT_Cmd_Current:
		break;
	case MT_Cmd_CenterPoint:
	case MT_Cmd_EIAJ:
	case MT_Cmd_SFR:
	case MT_Cmd_Rotate:
	case MT_Cmd_Particle:
		ImageSave(lParam);
		break;

	default:
		break;
	}


	return TRUE;
}

LRESULT CWnd_RecipeView::OnEdgeOnOff(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case 0: // Off
		m_TIProcessing.EdgeOnOff(FALSE);
		break;

	case 1: // On
		m_TIProcessing.EdgeOnOff(TRUE);
		break;

	default:
		break;
	}

	return TRUE;
}


LRESULT CWnd_RecipeView::OnDistortionCorr(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case 0: // Off
		m_TIProcessing.DistortionCorrection(FALSE);
		break;

	case 1: // On
		m_TIProcessing.DistortionCorrection(TRUE);
		break;

	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnManualCtrlCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/7/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnManualCtrlCmd(WPARAM wParam, LPARAM lParam)
{
	CString str;
	double dResultValue = 0;
	double dResultPos = 0;
	int nCheck = (int)wParam;

	// 1 -> ON, UP, KEEP .. 0 -> STOP, OFF, DN, OUT 
	switch (lParam)
	{
	case Man_Cmd_Power_On:
		if (m_bFlag_Test[lParam] == TRUE)
			return 0;

		if (!m_TIProcessing.CameraPowerOnOff(TRUE))
		{
			MessageView(_T("Camera Set Volt On Err"));
		}

		break;

	case Man_Cmd_Power_Off:
		if (m_bFlag_Test[lParam] == TRUE)
			return 0;
		
		if (!m_TIProcessing.CameraPowerOnOff(FALSE))
		{
			MessageView(_T("Camera Set Volt Off Err"));
		}
		break;

	case Man_Cmd_Image_Save:
		if (m_bFlag_Test[lParam] == FALSE)
		{
			m_bFlag_Test[lParam] = TRUE;

			if (!m_TIProcessing.ImageSaveOriginal())
			{
				MessageView(_T("[ERR] Original Image Save Fail !"));
			}
 
// 			if (!m_TIProcessing.ImageSaveGray())
// 			{
// 				MessageView(_T("[ERR] Gray Image Save Fail !"));
// 			}

			m_bFlag_Test[lParam] = FALSE;
		}
		break;

	case Man_Cmd_Pic_Image_Save:
		if (m_bFlag_Test[lParam] == FALSE)
		{
			m_bFlag_Test[lParam] = TRUE;

			if (!m_TIProcessing.ImageSavePic())
			{
				MessageView(_T("[ERR] Pic Image Save Fail !"));
			}

			m_bFlag_Test[lParam] = FALSE;
		}
		break;

	case Man_Cmd_ImageLoad:
	{
							  if (nCheck == TRUE)
							  {
								  OnPopImageLoad();
								 
							  }
							  else{
								  ChangeImageMode(ImageMode_LiveCam);
							  }

							  if (m_pstImageMode->eImageMode == ImageMode_StillShotImage)
							  {
								  m_TIProcessing.CameraPowerOnOff(FALSE);
								  m_wnd_ManualCtrl.SetBtnDisable();

								  //MessageBox(_T(" Disable camera device using in 'Imagemode' cases only."));
							  }
	}
		break;
	default:
		break;
	}
	
	m_wnd_ManualCtrl.AllBtnEnableMode(true);
	
	return TRUE;
}

//=============================================================================
// Method		: New_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
void CWnd_RecipeView::New_Model()
{
	if (IDYES != AfxMessageBox(_T("Do you want to create a new one?"), MB_YESNO))
	{
		return;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();
		m_stModelInfo.Reset();

		// 저장	 		
		if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
		{
			// 리스트 모델 갱신
			m_stModelInfo.szModelFile = strFileTitle;
			SetFullPath(m_stModelInfo.szModelFile);
			SetModelInfo();
			RedrawWindow();
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: Save_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Save_Model()
{

	// UI상의 데이터 얻기
	GetModelInfo();
	
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

 	if (m_stModelInfo.szModelFile.IsEmpty())
 	{
 	 	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
 	 	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;
 	 
 	 	if (fileDlg.DoModal() == IDOK)
 	 	{
 	 		strFullPath = fileDlg.GetPathName();
 	 		strFileTitle = fileDlg.GetFileTitle();	 		
 	 
			// 광축 찾기 SFR
			m_TIProcessing.CenterPointOffset(m_stModelInfo.stSFR.stSFROp.iOffsetX, m_stModelInfo.stSFR.stSFROp.iOffsetY);

 	 		// 저장	 		
 			if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
 			{				
 				// 리스트 모델 갱신
 				m_stModelInfo.szModelFile = strFileTitle;

				SetFullPath(m_stModelInfo.szModelFile);
				SetModel(m_stModelInfo.szModelFile);
				RedrawWindow();
			}

			// 모델 데이터 불러오기
			GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
			m_stModelInfo.szModelFile.ReleaseBuffer();
 	 	}
 	}
 	else
 	{
 		strFullPath.Format(_T("%s%s.%s"), m_strModelPath, m_stModelInfo.szModelFile, MODEL_FILE_EXT);
 
		// 광축 찾기 SFR
		m_TIProcessing.CenterPointOffset(m_stModelInfo.stSFR.stSFROp.iOffsetX, m_stModelInfo.stSFR.stSFROp.iOffsetY);

 	 	// 저장	 	
 	 	if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
		{
			SetModel(m_stModelInfo.szModelFile);
			RedrawWindow();
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
 	}

}

//=============================================================================
// Method		: SaveAs_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SaveAs_Model()
{
	// UI상의 데이터 얻기
	GetModelInfo();

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;

 	if (fileDlg.DoModal() == IDOK)
 	{
 		strFullPath = fileDlg.GetPathName();
 		strFileTitle = fileDlg.GetFileTitle();
 
 		// 저장	 		
 		if (m_fileModel.SaveModelFile(strFullPath, &m_stModelInfo))
 		{
 			// 리스트 모델 갱신
 			m_stModelInfo.szModelFile = strFileTitle; 			
			SetFullPath(m_stModelInfo.szModelFile);
			RedrawWindow();
 		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
 	}
}

//=============================================================================
// Method		: Load_Model
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:16
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Load_Model()
{
	if (IDNO == AfxMessageBox(_T("Data being edited will be deleted.\r\n Do you want to Continue?"), MB_YESNO))
	{
		return;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), MODEL_FILE_EXT); 

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, MODEL_FILE_EXT, strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strModelPath;
	 
	if (fileDlg.DoModal() == IDOK)
	{
	 	strFullPath = fileDlg.GetPathName();
	 	strFileTitle = fileDlg.GetFileTitle();	 	
	 
 		if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))	 	
 	 	{
 	 		// UI에 세팅
 	 		SetModelInfo();
			RedrawWindow();

			m_stModelInfo.szModelFile = strFileTitle;
			SetFullPath(m_stModelInfo.szModelFile);
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stModelInfo.szModelFile.GetBuffer(), 0);
		m_stModelInfo.szModelFile.ReleaseBuffer();
	}
}

//=============================================================================
// Method		: GetModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::GetModelInfo()
{	
	m_wnd_ModelCfg.GetModelInfo(m_stModelInfo);
	//if (m_stModelInfo.szModelFile == L"") return;
	m_wnd_TestItemCfg.GetModelInfo(m_stModelInfo);
//	m_wnd_DAQCfg.GetModelInfo(m_stModelInfo);
// 	m_wnd_VisionCfg.GetModelInfo(m_stModelInfo);
// 	m_wnd_AFCfg.GetModelInfo(m_stModelInfo);
// 	m_wnd_MotionTeachCfg.GetModelInfo(m_stModelInfo);

	// 검사 기준 정보
	m_wnd_StdInfoCfg.GetModelInfo();
	// Pogo
	m_wnd_PogoCfg.SavePogoSetting();
	m_stModelInfo.szPogoName = m_wnd_PogoCfg.GetPogoName();

//	m_wnd_LightCtrlCfg.GetLightInfo(m_stModelInfo.stLightInfo);
}


//=============================================================================
// Method		: SaveMasterSet
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/28 - 17:53
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SaveMasterSet()
{
	SetModelInfo();
	Save_Model();
}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/7/7 - 11:03
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
// 	// DAQ 옵션 
// 	if (InspMode == Permission_Engineer)
// 		m_tc_Option.ShowTab(m_iTabDaqIdx_Eng, SW_SHOW, TRUE, TRUE);
// 	else
// 		m_tc_Option.ShowTab(m_iTabDaqIdx_Eng, SW_HIDE, TRUE, TRUE);
// 
	m_wnd_StdInfoCfg.SetStatusEngineerMode(InspMode);
}

//=============================================================================
// Method		: GetVideoPicStatus
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/7/7 - 9:38
// Desc.		:
//=============================================================================
UINT CWnd_RecipeView::GetVideoPicStatus()
{
	return m_stModelInfo.nPicViewMode;
}

//=============================================================================
// Method		: IsRecipeTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:38
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::IsRecipeTest()
{
	return m_wnd_StdInfoCfg.IsTestAll();
}

//=============================================================================
// Method		: SetModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetModelInfo()
{
	m_TIPicControl.SetPtr_ModelInfo(&m_stModelInfo);
	m_TIProcessing.SetPtr_ModelInfo(&m_stModelInfo);
	m_TIProcessing.SetPtr_ImageMode(m_pstImageMode);

	m_wnd_ModelCfg.SetModelInfo(&m_stModelInfo);
	m_wnd_ModelCfg.SetPath(m_strI2CPath);
  	m_wnd_TestItemCfg.SetModelInfo(&m_stModelInfo);
//	m_wnd_DAQCfg.SetModelInfo(&m_stModelInfo);
// 	m_wnd_VisionCfg.SetModelInfo(&m_stModelInfo);
// 	m_wnd_AFCfg.SetModelInfo(&m_stModelInfo);
// 	m_wnd_MotionTeachCfg.SetModelInfo(&m_stModelInfo);

 	// 검사 기준 정보	
 	m_wnd_StdInfoCfg.SetModelInfo(&m_stModelInfo);
	m_wnd_StdInfoCfg.SetImageMode(m_pstImageMode);
 	
 	// Pogo
 	m_wnd_PogoCfg.SetPogoFile(m_stModelInfo.szPogoName);

	// 광원 보드
//	m_wnd_LightCtrlCfg.SetLightInfo(&m_stModelInfo.stLightInfo);

	RedrawWindow();
}

//=============================================================================
// Method		: SetFullPath
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szModelName
// Qualifier	:
// Last Update	: 2016/5/27 - 11:33
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetFullPath(__in LPCTSTR szModelName)
{
	m_strModelFullPath.Format(_T("%s%s.%s"), m_strModelPath, szModelName, MODEL_FILE_EXT);
	
	m_st_Location.SetText(m_strModelFullPath);
}

//=============================================================================
// Method		: ChangePath
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR lpszModelPath
// Qualifier	:
// Last Update	: 2016/6/24 - 13:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::ChangePath(__in LPCTSTR lpszModelPath)
{
	if (NULL != lpszModelPath)
		m_strModelPath = lpszModelPath;

	m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
	m_ModelWatch.EndWatchThrFunc();
	m_ModelWatch.BeginWatchThrFunc();
}

//=============================================================================
// Method		: SetModel
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szModel
// Qualifier	:
// Last Update	: 2016/3/18 - 16:43
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetModel(__in LPCTSTR szModel)
{
	CString strFullPath;
	strFullPath.Format(_T("%s%s.%s"), m_strModelPath, szModel, MODEL_FILE_EXT);

	if (m_fileModel.LoadModelFile(strFullPath, m_stModelInfo))
	{
	 	// UI에 세팅
	 	SetModelInfo();
		
		m_stModelInfo.szModelFile = szModel;
		if (!m_stModelInfo.szModelFile.IsEmpty())
		{
			int iSel = m_lst_ModelList.FindStringExact(0, m_stModelInfo.szModelFile);

			if (0 <= iSel)
			{
				m_lst_ModelList.SetCurSel(iSel);
			}

			SetFullPath(m_stModelInfo.szModelFile);
		}
	}
}

//=============================================================================
// Method		: RefreshModelFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2016/6/25 - 14:24
// Desc.		:
//=============================================================================
void CWnd_RecipeView::RefreshModelFileList(__in const CStringList* pFileList)
{
	m_lst_ModelList.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();
	 
	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_lst_ModelList.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_stModelInfo.szModelFile.IsEmpty())
	{
		int iSel = m_lst_ModelList.FindStringExact(0, m_stModelInfo.szModelFile);

		if (0 <= iSel)
		{
			m_lst_ModelList.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: InitOptionView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/25 - 14:37
// Desc.		:
//=============================================================================
void CWnd_RecipeView::InitOptionView()
{
	//SetOptionView(OPTV_TEST);

	m_tc_Option.SetActiveTab(0);
}

//=============================================================================
// Method		: SavePogoCount
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/18 - 15:42
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SavePogoCount()
{
	m_wnd_PogoCfg.SavePogoSetting();
}

//=============================================================================
// Method		: RedrawWindow
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/16 - 19:26
// Desc.		:
//=============================================================================
void CWnd_RecipeView::RedrawWindow()
{
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: MessageView
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/13 - 17:18
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::MessageView(__in CString szText, __in BOOL bMode)
{
	CWnd_MessageView*	m_pwndMessageView;
	m_pwndMessageView = new CWnd_MessageView;

	BOOL bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);

	m_pwndMessageView->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	m_pwndMessageView->EnableWindow(TRUE);
	m_pwndMessageView->CenterWindow();
	m_pwndMessageView->SetWarringMessage(szText);

	if (m_pwndMessageView->DoModal(bMode) == TRUE)
		bResult = TRUE;
	else
		bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);

	delete m_pwndMessageView;

	return bResult;
}



void CWnd_RecipeView::OnPopImageLoad()
{
	CString strFileExt;
	strFileExt.Format(_T("Image File (*.BMP, *.PNG) | *.BMP;*.PNG; | All Files(*.*) |*.*||"));

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);


	if (IDOK == fileDlg.DoModal())
	{
		Stop_ImageView_Mon();
		Sleep(100);
		CString strFilePath = fileDlg.GetPathName();

		m_pstImageMode->szImagePath = strFilePath;
		OnImage_LoadDisplay(strFilePath);
		ChangeImageMode(ImageMode_StillShotImage);
		Start_ImageView_Mon();
	}
	else
	{
		m_wnd_ManualCtrl.ImageLoadMode(FALSE);
	}
}


void CWnd_RecipeView::OnImage_LoadDisplay(CString strPath)
{

	if (m_LoadImage != NULL)
	{
		cvReleaseImage(&m_LoadImage);
		m_LoadImage = NULL;
	}

	if (strPath.IsEmpty())
	{
		return;
	}

	IplImage *testImage = cvLoadImage((CStringA)strPath);

	if (testImage == NULL || testImage->width < 1 || testImage->height < 1)
	{
		return;
	}
	m_LoadImage = cvCreateImage(cvSize(testImage->width, testImage->height), IPL_DEPTH_8U, 3);




	cvCopyImage(testImage, m_LoadImage);

	cvReleaseImage(&testImage);
}

BOOL CWnd_RecipeView::Start_ImageView_Mon()
{
	if (NULL != m_hThr_ImageViewMon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			//AfxMessageBox(_T("Image 모니터링 쓰레드가 동작 중 입니다."), MB_SYSTEMMODAL);
			return FALSE;
		}
	}

	if (NULL != m_hThr_ImageViewMon)
	{
		CloseHandle(m_hThr_ImageViewMon);
		m_hThr_ImageViewMon = NULL;
	}
	m_bFlag_ImageViewMon = TRUE;
	m_hThr_ImageViewMon = HANDLE(_beginthreadex(NULL, 0, Thread_ImageViewMon, this, 0, NULL));

	return TRUE;
}
BOOL CWnd_RecipeView::Stop_ImageView_Mon()
{
	m_bFlag_ImageViewMon = FALSE;

	if (NULL != m_hThr_ImageViewMon)
	{
		WaitForSingleObject(m_hThr_ImageViewMon, m_dwImageViewMonCycle);

		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_ImageViewMon, dwExitCode);
			WaitForSingleObject(m_hThr_ImageViewMon, WAIT_ABANDONED);
			CloseHandle(m_hThr_ImageViewMon);
			m_hThr_ImageViewMon = NULL;
		}

		//m_bGrabImageStatus = FALSE;

	}

	return TRUE;
}
UINT WINAPI CWnd_RecipeView::Thread_ImageViewMon(__in LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CWnd_RecipeView* pThis = (CWnd_RecipeView*)lParam;

	DWORD dwEvent = 0;

	__try
	{
		while (pThis->m_bFlag_ImageViewMon)
		{
			// 			// 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != pThis->m_hExternalExitEvent)
			{
				dwEvent = WaitForSingleObject(pThis->m_hExternalExitEvent, pThis->m_dwImageViewMonCycle);

				switch (dwEvent)
				{
				case WAIT_OBJECT_0:	// Exit Program
					TRACE(_T(" -- Exit Program m_hExternalExitEvent Event Detected \n"));
					pThis->m_bFlag_ImageViewMon = FALSE;
					break;

				case WAIT_TIMEOUT:
					pThis->OnLoadImageDisplay();

					break;
				}
			}
			else
			{
				pThis->OnLoadImageDisplay();

				DoEvents(pThis->m_dwImageViewMonCycle);
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_RecipeView::Thread_ImageViewMon()\n"));
	}

	TRACE(_T("Exit Thread : CWnd_RecipeView::Thread_ImageViewMon Loop Exit\n"));
	return TRUE;
}

void CWnd_RecipeView::OnLoadImageDisplay(){



 	if (m_bGrabImageStatus == TRUE)
 	{
 		return;
 	}
 	m_bGrabImageStatus = TRUE;
 
 	if (m_LoadImage != NULL)
 	{
 		IplImage *TestImage = cvCreateImage(cvSize(m_LoadImage->width, m_LoadImage->height), IPL_DEPTH_8U, 3);
 
 		cvCopy(m_LoadImage, TestImage);

		DisplayVideo_Overlay(0, (enPic_TestItem)m_stModelInfo.nPicViewMode, TestImage);

		ShowVideo(0, (LPBYTE)TestImage->imageData, m_LoadImage->width, m_LoadImage->height);
//  		ShowVideo_Overlay(0, TestImage, TestImage->width, TestImage->height, m_stRecipeInfo.nGideLineUseState);
//  
//  		if (*m_pbFlag_PicSave)
//  		{
//  
//  			if (TRUE == m_pstImageMode->bImageSaveMode)
//  			{
//  				CString szFile;
//  
//  				SYSTEMTIME tmLocal;
//  				GetLocalTime(&tmLocal);
//  
//  				szFile.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
//  
//  				CString szPath = m_pstImageMode->szImageCommonPath;
//  
//  				// 날짜별로 생성 폴더 생성
//  				szPath.Format(_T("%s/%02d%02d%02d"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
//  				MakeDirectory(szPath);
//  
//  				CString strFile;
//  				strFile.Format(_T("%s/%s_%s.png"), szPath, szFile, g_szTestItem[m_nTestItem]);
//  
//  				cvSaveImage(CT2A(strFile), TestImage);
//  			}
//  			*m_pbFlag_PicSave = FALSE;
// 		}

		if (m_TIProcessing.m_pstPicImageCaptureMode->nImageCaptureMode == TRUE)
		{
			cvSaveImage((CStringA)m_TIProcessing.m_pstPicImageCaptureMode->szImagePath, TestImage);
			m_TIProcessing.m_pstPicImageCaptureMode->nImageCaptureMode = FALSE;

		}
 
 		cvReleaseImage(&TestImage);
 
 	}
 	m_bGrabImageStatus = FALSE;
}


void CWnd_RecipeView::ChangeImageMode(enImageMode eImageMode){

	if (eImageMode == ImageMode_LiveCam)
	{
		Stop_ImageView_Mon();
		m_pstImageMode->szImagePath.Empty();
		m_wnd_ManualCtrl.ImageLoadMode(false);
	}
	else{
		m_wnd_ManualCtrl.ImageLoadMode(true);
	}
	m_pstImageMode->eImageMode = eImageMode;
}


void CWnd_RecipeView::ImageDisplayVideo(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight)
{
 //	BITMAPINFO		m_biInfo;
 //	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
 //	m_biInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
 //	m_biInfo.bmiHeader.biWidth = nWidth;//CAM_IMAGE_WIDTH;
 //	m_biInfo.bmiHeader.biHeight = nHeight;//CAM_IMAGE_HEIGHT;
 //	m_biInfo.bmiHeader.biPlanes = 1;
 //	m_biInfo.bmiHeader.biBitCount = 24;
 //	m_biInfo.bmiHeader.biCompression = 0;
 //	m_biInfo.bmiHeader.biSizeImage = dwRGBSize;
 //
 //	CDC *pcDC;
 //
 //	//-----------가상메모리를 만든다.------------------
 //	CDC	*pcMemDC = new CDC;
 //	CBitmap	*pcDIB = new CBitmap;
 //
 //	pcDC = m_wnd_ImageView.m_stVideo.GetDC();
 //	pcMemDC->CreateCompatibleDC(pcDC);
 //	pcDIB->CreateCompatibleBitmap(pcDC, nWidth, nHeight);
 //	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
 //
 //	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);
 //	::StretchDIBits(pcMemDC->m_hDC,
 //		0, nHeight - 1,
 //		nWidth, (-1) * nHeight,
 //		0, 0,
 //		m_biInfo.bmiHeader.biWidth,
 //		m_biInfo.bmiHeader.biHeight,
 //		lpbyRGB, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
 //
 //
 //
	//CRect rcWnd;
	//m_wnd_ImageView.m_stVideo.GetClientRect(rcWnd);
	//DisplayVideoRecipePic(rcWnd, pcMemDC);
 //	
 //
	////--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
 //
	//CRect rt;
	//m_wnd_ImageView.m_stVideo.GetClientRect(rt);
	//::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	//::StretchBlt(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), pcMemDC->m_hDC, 0, 0, nWidth, nHeight, SRCCOPY);
	//m_wnd_ImageView.m_stVideo.ReleaseDC(pcDC);
 //
 //	delete pcDIB;
 //	delete pcMemDC;
}

void CWnd_RecipeView::DisplayVideo_Overlay(__in UINT nChIdx, __in enPic_TestItem enItem, __inout IplImage *TestImage)
{
	if (NULL == TestImage)
		return;

	switch (enItem)
	{
	case PIC_Standby:
		break;
	case PIC_Current:
		m_OverlayProc.Overlay_Current(TestImage, m_stModelInfo.stCurrent.stCurrentOp[0],m_stModelInfo.stCurrent.stCurrentData);
		break;
	case PIC_CenterAdjust:
		m_OverlayProc.Overlay_OpticalCenter(TestImage,m_stModelInfo.stCenterPoint.stCenterPointOp,m_stModelInfo.stCenterPoint.stCenterPointData);
		break;
	case PIC_EIAJ:
		m_OverlayProc.Overlay_EIAJ(TestImage,m_stModelInfo.stEIAJ.stEIAJOp,m_stModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_SFR:
		m_OverlayProc.Overlay_SFR(TestImage,m_stModelInfo.stSFR.stSFROp,m_stModelInfo.stSFR.stSFRData);
		break;
	case PIC_Particle:
		break;
	case PIC_Rotation:
		break;
	case PIC_ActiveAlgin:
		m_OverlayProc.Overlay_OpticalCenter(TestImage,m_stModelInfo.stCenterPoint.stCenterPointOp,m_stModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage,m_stModelInfo.stEIAJ.stEIAJOp,m_stModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_Center_EIAJ:
		m_OverlayProc.Overlay_OpticalCenter(TestImage,m_stModelInfo.stCenterPoint.stCenterPointOp,m_stModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage,m_stModelInfo.stEIAJ.stEIAJOp,m_stModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_Center_SFR:
		m_OverlayProc.Overlay_OpticalCenter(TestImage,m_stModelInfo.stCenterPoint.stCenterPointOp,m_stModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_SFR(TestImage,m_stModelInfo.stSFR.stSFROp,m_stModelInfo.stSFR.stSFRData);
		break;
	case PIC_Center_Rotation:
		break;
	case PIC_EIAJ_Rotation:
		break;
	case PIC_SFR_Rotation:
		break;
	case PIC_SFR_Rotation_Center:
		break;
	case PIC_TotalPic:
		break;
	case PIC_Align:
		m_OverlayProc.Overlay_OpticalCenter(TestImage,m_stModelInfo.stCenterPoint.stCenterPointOp,m_stModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage,m_stModelInfo.stEIAJ.stEIAJOp,m_stModelInfo.stEIAJ.stEIAJData);

		break;
	case PIC_MaxEnum:
		break;
	default:
		break;
	}


}

void CWnd_RecipeView::DisplayVideoRecipePic(__in CRect rcSize, __in CDC *pCDC)
{
	UINT nMode = 0;

	nMode = GetVideoPicStatus();

	switch (nMode)
	{
	case PIC_Current:
		m_TIPicControl.CurrentPic(pCDC);
		break;
		// 	case PIC_LED:
		// 		m_TIPicControl.LEDPic(pCDC);
		// 		break;

	case PIC_CenterAdjust:
		m_TIPicControl.CenterPointPic(pCDC);
		break;

	case PIC_EIAJ:
		m_TIPicControl.EIAJPic(pCDC);
		break;

	case PIC_SFR:
		m_TIPicControl.SFRPic(rcSize, pCDC);
		break;

	case PIC_Rotation:
		m_TIPicControl.RotatePic(pCDC);
		break;

	case PIC_Particle:
		m_TIPicControl.ParticlePic(pCDC);
		break;

	case PIC_Align:
		m_TIPicControl.AlignPic(pCDC);
		break;

	default:
		break;
	}
}



void CWnd_RecipeView::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if ((0 <= iChIdx) && (iChIdx < USE_CHANNEL_CNT))
	{

		m_wnd_ImgProc.Render(lpVideo, dwWidth, dwHeight);

	}
}

//  [1/17/2019 Admin]
void CWnd_RecipeView::GetSaveImageFilePath(__out CString& szOutPath)
{
	CString szPath ;
	CString szPathTime;
	SYSTEMTIME tmLocal;

	GetLocalTime(&tmLocal);

	//- 경로 /날짜/ 모델/ Lot명 / 결과
		
	m_TIProcessing.GetSaveImageFileModelPath(szPath);

	szPath += _T("\\ManualTest");

	MakeDirectory(szPath);

	szOutPath.Format(_T("%04d%02d%02d_%02d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	szOutPath = szPath + _T("\\") + szOutPath;

}

//  [1/17/2019 Admin]
void CWnd_RecipeView::ImageSave(UINT nTestItemID){


	//-이미지 풀 path + 이미지 파일명 앞에 날짜까지 strpath에 입력 됨.
	CString strPath;
	GetSaveImageFilePath(strPath);

	//날짜와 시간 이외에 다른 파일명을 밑에 소스로 생성 시킨다.	

	CString szTestName = g_szManual_TESTCommend[nTestItemID];

	strPath += _T("_") + szTestName;

	// 날짜 / 모델 / Lot / 날짜시간_test명 

	m_TIProcessing.ReportImageSaveOriginal(strPath);
	m_TIProcessing.ReportImageSavePic(strPath);

}