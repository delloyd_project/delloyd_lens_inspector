﻿#ifndef Wnd_CenterPointOp_h__
#define Wnd_CenterPointOp_h__

#pragma once

#include "List_CenterPointOp.h"
#include "Def_DataStruct.h"
#include "CommonFunction.h"
#include "VGStatic.h" 
// #include "Wnd_ManualCtrl.h"//2018cws
// #include "TI_Processing.h"//2018cws


enum enCenterPoint_Static{
	STI_CP_TESTMODE = 0,
	STI_CP_CAMERASTATE ,
	STI_CP_IMAGESENSOR,
	STI_CP_TRYCOUNT,
	STI_CP_MAXNUM,
};

static LPCTSTR	g_szCenterPoint_Static[] =
{
	_T("TEST MODE"),
	_T("CAMERA STATE"),
	_T("IMAGE SENSOR PIXSL"),
	_T("TRY COUNT"),
	NULL
};

enum enCenter_Buttonbox{
	Btn_CT_CENTER_TEST = 0,
	Btn_CT_CENTER_TEST_STOP,
	Btn_CT_MAXNUM,
};

static LPCTSTR	g_szCenter_Button[] =
{
	_T("CENTER TEST"),
	_T("CENTER STOP"),
	NULL
};

enum enCenterPoint_Combobox{
	ComboI_CP_TESTMODE = 0,
	ComboI_CP_CAMERASTATE,
	ComboI_CP_MAXNUM,
};

enum enCenterPoint_Edit
{
	Edit_CP_TRYCOUNT = 0,
	Edit_CP_IMAGESENSOR,
	Edit_CP_MAXNUM,
};

// CWnd_CenterPointOp

class CWnd_CenterPointOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CenterPointOp)

public:
	
	CWnd_CenterPointOp();
	virtual ~CWnd_CenterPointOp();

	void SetUI	();
	void GetUI	();
	BOOL IsTest	();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void	SetPtr_ImageMode(__in ST_ImageMode *stImageMode)
	{
		if (stImageMode == NULL)
			return;

		m_pstImageMode = stImageMode;
	};//2018cws




protected:
	DECLARE_MESSAGE_MAP()

	CFont				m_font_Data;
	CVGStatic			m_st_Item[STI_CP_MAXNUM];
	CComboBox			m_Cb_Item[ComboI_CP_MAXNUM];
	CMFCMaskedEdit		m_ed_Item[Edit_CP_MAXNUM];
	//CTI_Processing		m_CT_TIProcessing;//2018cws

	ST_ModelInfo		*m_pstModelInfo;
	ST_ImageMode		* m_pstImageMode;//2018cws
	CList_CenterPointOp m_ListCPOp;

	CButton				m_bn_Item[Btn_CT_MAXNUM];

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnCbnSelendTestMode();
	afx_msg void OnBnClickedBnTest();
	afx_msg void OnBnClickedBnTestStop();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	BOOL m_bTest_Flag;


};


#endif // Wnd_CenterPointOp_h__
