﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"

class CTI_Current : public CTestItem
{
public:
	CTI_Current();
	virtual ~CTI_Current();

	ST_Device *m_pDevice;

	void		SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	//전류측정
	UINT	Current_Test	(ST_LT_TI_Current *pstCurrent, int iNum = 0);
};

