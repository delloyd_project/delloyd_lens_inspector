﻿#ifndef List_CenterPointOp_h__
#define List_CenterPointOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_CpOp
{
	CpOp_Object,
	CpOp_X,
	CpOp_Y,
	CpOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_CpOp[] =
{
	_T(""),
	_T("X [ pix ]"),
	_T("Y [ pix ]"),
	NULL
};

typedef enum enListItemNum_CpOp
{
	CpOp_Standard,
	CpOp_StandardOffset,
	CpOp_TargetOffset,
	CpOp_MaxOffset,
	CpOp_ItemNum,
};

static const TCHAR*	g_lpszItem_CpOp[] =
{
	_T("Standard"),
	_T("Standard Spc"),
	_T("Target Offset"),
	_T("Max Offset"),
	NULL
};

const int	iListAglin_CpOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CpOp[] =
{
	95,
	95,
	95,
};
// List_CurrentInfo

class CList_CenterPointOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CenterPointOp)

public:
	CList_CenterPointOp();
	virtual ~CList_CenterPointOp();

	ST_LT_TI_CenterPoint  m_stCenterPoint;
	ST_LT_TI_CenterPoint  *m_pstCenterPoint;

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_CenterPoint	(ST_LT_TI_CenterPoint *pstCenterPoint)
	{
		if (pstCenterPoint == NULL)
			return;

		m_pstCenterPoint = pstCenterPoint;
	};

	UINT m_nTestMode;
	void SetTestModeChange(UINT nTestMode)
	{
		m_nTestMode = nTestMode;
	}

protected:
	CFont	m_Font;

	DECLARE_MESSAGE_MAP()

	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;
	BOOL		UpdateCellData(UINT nRow, UINT nCol, int  iValue);

	BOOL		UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL		CheckRectValue(__in const CRect* pRegionz);

	afx_msg void	OnEnKillFocusECpOpellEdit();

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};


#endif // List_CurrentInfo_h__
