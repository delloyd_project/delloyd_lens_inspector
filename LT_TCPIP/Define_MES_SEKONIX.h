﻿//*****************************************************************************
// Filename	: 	Define_MES_SEKONIX.h
// Created	:	2016/5/10 - 15:32
// Modified	:	2016/5/10 - 15:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_MES_SEKONIX_h__
#define Define_MES_SEKONIX_h__

#include <afxwin.h>

typedef enum enSEKONIX_MES_Const
{
	LG_MES_STX			= 'H',
	LG_MES_ETX			= 'T',
	LG_MES_DELIMETER	= '*',

	LG_MES_DELIMETER_CNT	= 3,
	LG_MES_MinProtocolSize	= 5,
	LG_MES_MaxProtocolSize	= 50,
};

typedef struct _tag_SEKONIX_MES_Protocol
{
	char		STX;
	char		Delimeter_1;
	CStringA	szLotID;
	char		Delimeter_2;
	CStringA	szLotTryCount;
	char		Delimeter_3;
	char		ETX;

	CStringA	szProtocol;

	_tag_SEKONIX_MES_Protocol()
	{
		;
	}

	BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
	{
		if (NULL == pszProtocol)
			return FALSE;

		if (nLength < LG_MES_MinProtocolSize)
			return FALSE;

		szProtocol = pszProtocol;
		CStringA strProtocol = pszProtocol;

		CStringA resToken;
		CStringA szToken;
		szToken.AppendChar(LG_MES_DELIMETER);
		int curPos = 0;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		STX = resToken.GetAt(0);

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotID = resToken;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotTryCount = resToken;

		ETX = strProtocol.GetAt(curPos);
		

		// Check STX
		if (LG_MES_STX != STX)
			return FALSE;

		// Check ETX
		if (LG_MES_ETX != ETX)
			return FALSE;

		return TRUE;
	};

}ST_SEKONIX_MES_Protocol, *PST_SEKONIX_MES_Protocol;



#endif // Define_MES_SEKONIX_h__
