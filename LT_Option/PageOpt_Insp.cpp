﻿//*****************************************************************************
// Filename	: PageOpt_Insp.cpp
// Created	: 2010/9/6
// Modified	: 2010/9/6 - 15:51
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_Insp.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_Insp, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::CPageOpt_Insp(LPCTSTR lpszCaption /*= NULL*/) : CPageOption (lpszCaption)
{
	
}

CPageOpt_Insp::CPageOpt_Insp(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{
	
}

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::~CPageOpt_Insp(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_Insp, CPageOption)	
END_MESSAGE_MAP()

// CPageOpt_Insp 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_Insp::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_Insp::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_Insp::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::InitPropList()
{
	CPageOption::InitPropList();

	CMFCPropertyGridProperty* pProp = NULL;

	//-----------------------------------------------------
	// 검사기 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Inspector(new CMFCPropertyGridProperty(_T("System Setting")));

// 	pProp = new CMFCPropertyGridProperty(_T("Use Barcode"), lpszUsableTable[0], _T("바코드 사용 유무"));
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Inspector->AddSubItem(pProp
	// Door Open 오류 처리 사용여부
//  	pProp = new CMFCPropertyGridProperty(_T("Use Error Checking : Door Sensor"), lpszUsableTable[0], _T("도어 센서 사용 유무."));
//  	pProp->AddOption(lpszUsableTable[0]);
//  	pProp->AddOption(lpszUsableTable[1]);
//  	pProp->AllowEdit(FALSE);
//  	apGroup_Inspector->AddSubItem(pProp);

	// Image Save Type
	pProp = new CMFCPropertyGridProperty(_T("Image Save Type"), lpszImageSave[0], OPT_DESC_SAVEIMAGE);
	for (int iIndex = 0; NULL != lpszImageSave[iIndex]; iIndex++)
	{
		pProp->AddOption(lpszImageSave[iIndex]);
	}
	pProp->AllowEdit(FALSE);
	apGroup_Inspector->AddSubItem(pProp);


	// 설비 코드
	apGroup_Inspector->AddSubItem(new CMFCPropertyGridProperty(_T("Equipment Code"), _T(""), _T("Settings Equipment Code")));



	// 안전 센서처리 사용여부
// 	if (SYS_FOCUS_CENTER_ADJUSTMENT == g_InspectorTable[m_InsptrType].SysType
// 		|| SYS_AUTO_FOCUS_ADJUSTMENT == g_InspectorTable[m_InsptrType].SysType)
// 	{
// 		pProp = new CMFCPropertyGridProperty(_T("Use Error Checking : Area Sensor"), lpszUsableTable[0], _T("Set whether the motor stop function is used when a safety sensor is detected."));
// 		pProp->AddOption(lpszUsableTable[0]);
// 		pProp->AddOption(lpszUsableTable[1]);
// 		pProp->AllowEdit(FALSE);
// 		apGroup_Inspector->AddSubItem(pProp);
// 	}

	m_wndPropList.AddProperty(apGroup_Inspector.release());

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Path(new CMFCPropertyGridProperty(_T("Path Setting")));

	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Log Path"), _T("C:\\Log")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Report Path"), _T("C:\\Report")));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Model Path"), _T("C:\\Model")));

	if (g_InspectorTable[m_InsptrType].bPCIMotion == TRUE)
		apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Motor Path"), _T("C:\\Motor")));
	
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Pogo Path"), _T("C:\\Pogo")));

	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Image Path"), _T("C:\\Image")));

//  	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("I2C Path"), _T("C:\\I2C")));

// 	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("MES Path (Receive)"), _T("C:\\MES\\Receive")));
// 	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("MES Path (Send)"), _T("C:\\MES\\Receive")));

	m_wndPropList.AddProperty(apGroup_Path.release());
//	
//#ifdef _DEBUG
//	//-----------------------------------------------------
//	// 프로그램 운영 설정
//	//-----------------------------------------------------
//	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("Operation Setting")));
//
//	// 자동 재 실행 사용 여부
//	pProp = new CMFCPropertyGridProperty(_T("비정상 종료 시 자동 재 실행 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_RESTART);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//
//	// 장치 연결상태 표시 윈도우 사용여부
//	pProp = new CMFCPropertyGridProperty(_T("장치 연결상태 표시 윈도우 사용여부"), lpszUsableTable[1], OPT_DESC_USE_DEVICE_INFO_PANE);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//
//	// 자동 통신 연결 사용 여부
//	pProp = new CMFCPropertyGridProperty(_T("자동 통신 연결 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_CONN);
//	pProp->AddOption(lpszUsableTable[0]);
//	pProp->AddOption(lpszUsableTable[1]);
//	pProp->AllowEdit(FALSE);
//	apGroup_Comm->AddSubItem(pProp);
//	
//	m_wndPropList.AddProperty(apGroup_Comm.release());
//#endif
//
	//-----------------------------------------------------
	// 기타 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Etc(new CMFCPropertyGridProperty(_T("Etc Setting")));
	apGroup_Etc->AddSubItem(new CPasswordProp(_T("Password Administrator"), _T(""), _T("Settings Editable administrator password")));

	// Logo Type
	pProp = new CMFCPropertyGridProperty(_T("Logo Type"), lpszLogoType[0], _T("Choose Logo Type"));
	for (UINT nIndex = 0; NULL != lpszLogoType[nIndex]; nIndex++)
		pProp->AddOption(lpszLogoType[nIndex]);
	pProp->AllowEdit(FALSE);
	apGroup_Etc->AddSubItem(pProp);

	//apGroup_Etc->AddSubItem(new CPasswordProp(_T("Password Read Only"), _T(""), _T("Administrator password that can only view settings")));
	m_wndPropList.AddProperty(apGroup_Etc.release());
}

//=============================================================================
// Method		: CPageOpt_Insp::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_Inspector(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_Inspector(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOption_Inspector
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_Insp CPageOpt_Insp::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	USES_CONVERSION;

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// 바코드 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.bUseBarcode = (BOOL)nIndex;



 	// Door Open 오류 처리 사용여부
//  	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
//  	varData = rVariant.Detach();
//  	ASSERT(varData.vt == VT_BSTR);
//  	strValue = OLE2A(varData.bstrVal);
//  	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
//  	{
//  		if (lpszUsableTable[nIndex] == strValue)
//  			break;
//  	}
//  	m_stOption.bUseDoorOpen_Err = (BOOL)nIndex;

	// Image Save Type
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszImageSave[nIndex]; nIndex++)
	{
		if (lpszImageSave[nIndex] == strValue)
			break;
	}
	m_stOption.nImageSaveType = nIndex;



	// 설비코드
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);

	m_stOption.EqpCode = strValue;


	
	
// 
// 	// 안전 센서 처리 사용여부
// 	if (SYS_FOCUS_CENTER_ADJUSTMENT == g_InspectorTable[m_InsptrType].SysType
// 		|| SYS_AUTO_FOCUS_ADJUSTMENT == g_InspectorTable[m_InsptrType].SysType)
// 	{
// 		rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 		varData = rVariant.Detach();
// 		ASSERT(varData.vt == VT_BSTR);
// 		strValue = OLE2A(varData.bstrVal);
// 		for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 		{
// 			if (lpszUsableTable[nIndex] == strValue)
// 				break;
// 		}
// 		m_stOption.bUseAreaSen_Err = (BOOL)nIndex;
// 	}	

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Log = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Report = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Model = strValue;

	if ((TRUE == g_InspectorTable[m_InsptrType].bPCIMotion))
	{
		rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
		varData = rVariant.Detach();
		ASSERT(varData.vt == VT_BSTR);
		strValue = OLE2A(varData.bstrVal);
		m_stOption.szPath_Motor = strValue;
	}

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Pogo = strValue;
	
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Image = strValue;

// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_I2CFile = strValue;

// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_MES = strValue;
// 
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_MES2 = strValue;
// 	
#ifdef _DEBUG
	//---------------------------------------------------------------
	// 프로그램 운영 설정
	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoRestart = (BOOL)nIndex;
// 
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseDeviceInfoPane = (BOOL)nIndex;
// 
// 	// 자동 통신 연결 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoConnection = (BOOL)nIndex;

#endif

	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();
	ASSERT (varData.vt == VT_BSTR);	
	strValue = OLE2A(varData.bstrVal);

	m_stOption.Password_Admin = strValue;

	// Logo Type
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	for (nIndex = 0; NULL != lpszLogoType[nIndex]; nIndex++)
	{
		if (lpszLogoType[nIndex] == strValue)
			break;
	}
	m_stOption.nLogoType = nIndex;


	// Password -----------------------------
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	m_stOption.Password_ReadOnly = strValue;

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_Insp::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOption_Inspector stOption
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetOption( stOpt_Insp stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// 바코드 사용 여부
/*	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseBarcode]);*/

	//Door Open 오류 처리 사용여부
//	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseDoorOpen_Err]);
	//안전 센서 처리 사용여부
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseAreaSen_Err]);
	


	// Image Save Type
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszImageSave[m_stOption.nImageSaveType]);
	
	// 설비 코드
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.EqpCode);


	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Log);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Report);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Model);

	if (g_InspectorTable[m_InsptrType].bPCIMotion == TRUE)
		(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Motor);

	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Pogo);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Image);
//	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_I2CFile);
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_MES);
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_MES2);
	
// #ifdef _DEBUG
// 	//---------------------------------------------------------------
// 	// 프로그램 운영 설정
// 	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoRestart]);
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseDeviceInfoPane]);
// 	// 자동 통신 연결 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoConnection]);
// #endif

	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.Password_Admin);
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.Password_ReadOnly);

	// Logo Type
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszLogoType[m_stOption.nLogoType]);
}